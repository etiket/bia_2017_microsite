<nav class="main_nav_container" id="main_nav_container">
		<ul class="main_nav DESKTOP">
        	<a href="<?php echo $project_data['full_address']; ?>"><li class="nav_item">Home</li></a>
			<hr />
			<a href="<?php echo $project_data['pages']['entry_login']; ?>"><li class="nav_item">Login / Register</li></a>
			<hr />
            <a href="<?php echo $project_data['pages']['password_recovery']; ?>"><li class="nav_item">Reset password</li></a>
			<hr />
            <?php
			if (isset($_SESSION['entry_userinfo'])) {
				?>
                <a href="<?php echo $project_data['pages']['entry_form']; ?>"><li class="nav_item">My Entry</li></a>
				<hr />
                <?php
			}
			?>
            <a href="<?php echo $project_data['pages']['entry_login']; ?>"><li class="nav_item">Login</li></a>
            <hr />
		</ul>
        
        
        <ul class="main_nav MOBILE">
        	<a href="<?php echo $project_data['full_address']; ?>"><li class="nav_item">Home</li></a>
			<hr />
			<a href="<?php echo $project_data['pages']['entry_login']; ?>" ><li class="nav_item">Login / Register</li></a>
			<hr />
            <a href="<?php echo $project_data['pages']['password_recovery']; ?>"><li class="nav_item">Reset password</li></a>
			<hr />
            <?php
			if (isset($_SESSION['entry_userinfo'])) {
				?>
                <a href="<?php echo $project_data['pages']['entry_form']; ?>"><li class="nav_item">My Entry</li></a>
				<hr />
                <?php
			}
			?>
            <a href="<?php echo $project_data['pages']['entry_login']; ?>"><li class="nav_item">Login</li></a>
            <hr />
		</ul>
        
        <div class="nav_social_btns">
            <img src="<?php echo $image_folder; ?>nav_btn_fb.png" class="fb_share_thispage">
            <img src="<?php echo $image_folder; ?>nav_btn_twitter.png" class="twitter_share_site">
            <img src="<?php echo $image_folder; ?>nav_btn_in.png" class="in_share_btn">
        </div>
        
        <img src="<?php echo $image_folder; ?>web_menu_btn_contract.png" class="FULL_WIDTH" id="nav_menu_btn_close">
	</nav> <!-- main nav container -->