
			<div class="alumni_full_heading alumni_slider_bg_color_dt">
                <p>2016</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_dt_btn" myContainerID="judges_slider_container2016"></div>
            <div class="clear"></div>
            <!-- 2016 Judges slider -->
            <div class="slider_expand_cont" id="judges_slider_container2016">
            	<div class="judge_desktop_slider">
                    <div id="judges_slider2016" class="p6_slider">

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Howard.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Howard Arrand</h2> -->
                            <p class="BLACK_COPY"><strong>HOWARD ARRAND</strong><br><br>Howard is the <strong>Provincial Head for FNB Business in KwaZulu-Natal</strong>. He has previously participated as an ISP judge in the Endeavor process, and also served as a judge/convenor of judges for the EY World Entrepreneur Awards.</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>MarcelClaasen.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">MARCEL KLAASSEN</h2> -->
                            <p class="BLACK_COPY"><strong>MARCEL KLAASSEN</strong><br><br>As the current <strong>Head of Sales at FNB</strong>, Marcel is responsible for new sales and client value management. </p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Cynthia.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">CYNTHIA MKHOMBO</h2> -->
                            <p class="BLACK_COPY"><strong>CYNTHIA MKHOMBO</strong><br><br>Cynthia Mkhombo is the <strong>founder and current CEO of Masana Hygiene Services</strong>. In 2010, <span class="COPY_ITALIC">CEO Magazine</span> called her "one of the most influential women in business and professional services".</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Raymond.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Raymond Ndlovu</h2> -->
                            <p class="BLACK_COPY"><strong>RAYMOND NDLOVO</strong><br><br>Raymond currently serves as an <strong>Investment Executive at Remgro Limited</strong>, utilising his extensive local and international experience as a specialist in various fields in the financial services industry.</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Cathrine.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Catherine Townshend</h2> -->
                            <p class="BLACK_COPY"><strong>CATHERINE TOWNSHEND</strong><br><br><strong>Joining Endeavor SA in 2012</strong>, Catherine has assisted some of the country’s most innovative entrepreneurs realise their global growth aspirations.</p>
                        </div>
                    

                    </div> <!-- judges_slider -->
                    <div class="clear">&nbsp;</div>
                    <img src="<?php echo $image_folder; ?>finalist_slider_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="sj_2016_arrow_left">
					<img src="<?php echo $image_folder; ?>finalist_slider_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="sj_2016_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->