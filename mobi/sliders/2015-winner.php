
            <div class="alumni_full_heading alumni_slider_bg_color_lt">
                <p>2015</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_btn" myContainerID="winner_container_2015"></div>
            <div class="clear"></div>
            <!-- 2016 Finalists slider -->
            <div class="slider_expand_cont" id="winner_container_2015">
                <!-- Winner2016 slider -->
                <div>
                    <p class="TURQ_COPY winnercopy">Congratulations to <strong>Bruce Morgan</strong>, founder of <strong>GreatSoft CRM</strong> on being awarded the <strong>FNB Business Innovator of the Year 2015</strong>.</p>
                    <img src="<?php echo $image_folder; ?>winner_photo.jpg" class="judge_profile_img LEFT">
                    <div class="winner_content_cont">
                        
                        <img src="<?php echo $image_folder; ?>winner_page_logo.png" class="judge_profile_logo" >
                            <div>
                                <p>
                                South African CRM software provider GreatSoft (Pty) Ltd impressed the 2015 BIA judges’ panel, earning them the title of winner of the inaugural <strong>FNB Business INNOVATION Awards</strong>.
                                <br><br>
    The developmental strides made by CEO Bruce Morgan toward realising the company’s ambition of global expansion are a testament to the power of the awards to propel winners towards the achievement of their business goals.
                                <br/><br/>
                                GreatSoft is a provider of its own internally developed customer relationship management systems for tax management, document management, billing and payroll for financial professionals. Despite the competition, they differentiated themselves by making their solution cloud-based, answering their customers’ need for accessibility. This customer-centric stance is just one of the many reasons why GreatSoft stood out as a fit for FNB and the Endeavor network.
                                Winning the BIA Awards does more than offer a business the opportunity for expansion; it affords momentum for the owners and management, and changes their perceptions of what is achievable. Morgan explains that the gruelling judging process helped them identify aspects of their business strategy that needed to be refined. Being flown to Silicon Valley, San Francisco, afforded them the opportunity to network with judges and fellow entrepreneurs from around the world, gaining insights into the global market into which they hoped to expand.
                                <br/><br/>
                                The results have been shifts in goals from doubling over two years to tripling over four to five years and the staff headcount has grown from 55 to 75. Increased focus on acquiring valued staff and developing talent through their internship programme has improved their capacity, which has become imperative as the group’s combined revenues are 40% higher than a year ago and they intend to double revenues into Q3 and Q4 of 2016.
                                </p>
                            </div>
                    </div> <!-- winner content cont -->
                    <div class="clear"></div>
                </div> <!-- Winner slider container -->



            </div> <!-- judges slider container -->