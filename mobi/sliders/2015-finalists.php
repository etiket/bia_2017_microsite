
            <div class="alumni_full_heading alumni_slider_bg_color_t">
                <p>2015</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_btn" myContainerID="finalists_slider_container2015"></div>
            <div class="clear"></div>
            <!-- 2015 Finalists slider -->
            <div class="slider_expand_cont" id="finalists_slider_container2015">
            	<div class="judge_desktop_slider">
                    <div id="finalists_slider2015" class="p6_slider">
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_01.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_01_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><strong>SAM HUTCHINSON</strong><br/><br/>Everlytic is a digital communications company that solves the problem of letting their clients send out bulk email and mobile communication to their customers.<br/><br/><strong>Website</strong>: <a href="http://www.everlytic.co.za" class="finalist-link" target="_blank">www.everlytic.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_02.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_02_photo.jpg" class="judge_profile_img LEFT">
                            
                            <p class="BLACK_COPY"><br/><strong>TIM MATTHIS &amp; PETAR SOLDO</strong><br/><br/>Genex Insights is focused on providing research insights to its customers. The focus is on three main areas: marketing research, customer experience measurement and online media analysis.<br/><br/><strong>Website</strong>: <a href="http://www.genex.co.za" class="finalist-link" target="_blank">www.genex.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>                        
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_04.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_04_photo.jpg" class="judge_profile_img LEFT">
                            
                            <!-- <p class="BLACK_COPY">inQuba is a company offering technology and services aligned with creating positive customer experiences and engagement.</p> -->
                            <p class="BLACK_COPY"><br/><strong>TRENT ROSSINI</strong><br/><br/>inQuba is a company offering technology and services aligned with creating positive customer experiences and engagement.<br/><br/><strong>Website</strong>: <a href="http://www.inquba.com" class="finalist-link" target="_blank">www.inquba.com</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_05.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_05_photo.jpg" class="judge_profile_img LEFT">
                            
                            <!-- <p class="BLACK_COPY">Merchant Capital was launched to provide working capital solutions to entrepreneurs that are different from what typical business owners get access to; there is no interest rate and there is no fixed repayment term.</p> -->
                            <p class="BLACK_COPY"><br/><strong>DOV GIRNUN</strong><br/><br/>Merchant Capital provides working capital solutions to entrepreneurs that are different from what typical business owners get access to; there is no interest rate and no fixed repayment term.<br/><br/><strong>Website</strong>: <a href="http://www.merchantcapital.co.za" class="finalist-link" target="_blank">www.merchantcapital.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_06.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_06_photo.jpg" class="judge_profile_img LEFT">
                            
                            <!-- <p class="BLACK_COPY">Smoke CCS has developed a software solution – Eyerys – that makes it possible for companies to listen to customers at every touchpoint. This helps companies build more profitable businesses driven by the voice of the customer. </p> -->
                            <p class="BLACK_COPY"><br/><strong>ANDREW COOK &amp; ANDREW BURNS</strong><br/><br/>Smoke CCS has developed a software solution, Eyerys, that makes it possible for companies to listen to customers at every touchpoint. This helps companies build more profitable businesses driven by the voice of the customer. <br/><br/><strong>Website</strong>: <a href="http://www.smokeccs.co.za" class="finalist-link" target="_blank">www.smokeccs.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                            <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_07.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_07_photo.jpg" class="judge_profile_img LEFT">
                            
                            <!-- <p class="BLACK_COPY">SPARK Schools was founded with a mission to provide access to high-quality education for all South Africans at a more affordable rate.</p> -->
                            <p class="BLACK_COPY"><br/><strong>STACEY BREWER &amp; RYAN HARRISON</strong><br/><br/>SPARK Schools was founded with a mission to provide access to high-quality education for all South Africans at a more affordable rate.<br/><br/><strong>Website</strong>: <a href="http://www.sparkschools.co.za" class="finalist-link" target="_blank">www.sparkschools.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                    </div> <!-- judges_slider -->
                    <div class="clear">&nbsp;</div>
                    <img src="<?php echo $image_folder; ?>finalist_slider_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="sf_2015_arrow_left">
                    <img src="<?php echo $image_folder; ?>finalist_slider_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="sf_2015_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->