
            <div class="alumni_full_heading alumni_slider_bg_color_t">
                <p>2016</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_btn" myContainerID="finalists_slider_container2016"></div>
            <div class="clear"></div>
            <!-- 2016 Finalists slider -->
            <div class="slider_expand_cont" id="finalists_slider_container2016">
            	<div class="judge_desktop_slider">
                    <div id="finalists_slider2016" class="p6_slider">

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_01.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_01_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">CATHERINE L&Uuml;CKHOFF</span><br/><br/>NicheStreem is a custom music streaming service that offers music-lovers access to pre-compiled playlists, in multiple streams, without the need for Internet access.<br/><br/><strong>Website</strong>: <a href="http://www.nichestreem.com" class="finalist-link" target="_blank">www.nichestreem.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_6" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_02.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_02_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">STUART PRIOR &amp; SCOTT SARGENT</span><br/><br/>Rhino Wood turns sustainably sourced softwood into durable hardwood – without the price tag.<br/><br/><strong>Website</strong>: <a href="http://www.rhinowood.co.za" class="finalist-link" target="_blank">www.rhinowood.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_7" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_03.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_03_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">TONI GLASS</span><br/><br/>The Toni Glass Collection is a contemporary tea brand that produces tea in the old-fashioned way. Only full-leaf teas from select tea gardens are used, insuring that the brand’s tea retains its natural integrity.<br/><br/><strong>Website</strong>: <a href="http://www.toniglasscollection.co.za" class="finalist-link" target="_blank">www.toniglasscollection.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_8" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_04.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_04_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">KIRSTY CHADWICK</span><br/><br/>The Training Room Online is an innovative, cost-effective digital learning platform that streamlines e-learning material for any audience, in any location, from 
any device.<br/><br/><strong>Website</strong>: <a href="http://www.ttro.com" class="finalist-link" target="_blank">www.ttro.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_9" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_05.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_05_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">TREVOR WOLFE &amp; REMON GEYSER</span><br/><br/>delvv.io offers clients feedback from a network of more than 120 000 creative professionals, faster and more cost-effectively than traditional research vendors.<br/><br/><strong>Website</strong>: <a href="http://www.delvv.io" class="finalist-link" target="_blank">www.delvv.io</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_0" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_06.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_06_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">MELVYN LUBEGA &amp; ANDREW BARNES</span><br/><br/>GO1 is an innovative e-learning platform that offers a training and education solution that aggregates both online and offline training options.<br/><br/><strong>Website</strong>: <a href="http://www.go1.com" class="finalist-link" target="_blank">www.go1.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_2" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_07.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_07_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">DENNIS MARKETOS, PAUL MARKETOS &amp; PAUL DE KOCK</span><br/><br/>IsoMetrix, a rapidly expanding product from Metrix Software Solutions, allows companies to track and report on governance, risk management and compliance via a user-friendly, customisable dashboard.<br/><br/><strong>Website</strong>: <a href="http://www.isometrix.com" class="finalist-link" target="_blank">www.isometrix.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_3" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_08.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_08_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">ERIK OOSTHUIZEN &amp; GARETH FARROW</span><br/><br/>Kliq Holdings is a wireless Internet data provider offering listed companies, corporates and SMEs in remote locations their own affordable, high-performance and reliable 
network packages.<br/><br/><strong>Website</strong>: <a href="http://www.kliq.co.za" class="finalist-link" target="_blank">www.kliq.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_4" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_09.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_09_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">DAVID SHAPIRO &amp; JONATHAN SHAPIRO</span><br/><br/>LESCO Manufacturing is an innovative local company, designing and manufacturing ground-breaking new electrical products for the local market.<br/><br/><strong>Website</strong>: <a href="http://www.lescosk.co.za" class="finalist-link" target="_blank">www.lescosk.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_5" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_10.png" class="judge_profile_logo LEFT">
                            <img src="<?php echo $image_folder; ?>finalist2016_10_photo.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><br/><span class="COPY_BOLD">GRAHAM ROWE &amp; RICHARD JOHNSON</span><br/><br/>Guidepost is a software solution that enhances healthcare delivery for chronic illness patients around
 the globe in a cost-effective and scaleable way.<br/><br/><strong>Website</strong>: <a href="http://www.guidepost.co.za" class="finalist-link" target="_blank">www.guidepost.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_1" class="square_submit_btn watch_now_btn2"/>
                        </div>


                    </div> <!-- judges_slider -->
                    <div class="clear">&nbsp;</div>
                    <img src="<?php echo $image_folder; ?>finalist_slider_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="sf_2016_arrow_left">
					<img src="<?php echo $image_folder; ?>finalist_slider_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="sf_2016_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->