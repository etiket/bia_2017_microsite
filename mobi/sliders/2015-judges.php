
			<div class="alumni_full_heading alumni_slider_bg_color_dt">
                <p>2015</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_dt_btn" myContainerID="judges_slider_container2015"></div>
            <div class="clear"></div>
<!-- 2015 Judges slider -->
            <div class="slider_expand_cont" id="judges_slider_container2015">
            	<div class="judge_desktop_slider">
                    <div id="judges_slider2015" class="p6_slider">

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_cynthia.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">CYNTHIA MKHOMBO</span><br><br>Cynthia Mkhombo is the <strong>founder and current CEO of Masana Hygiene Services</strong>. In 2010, <span class="COPY_REGULAR_ITALIC">CEO Magazine</span> called her "one of the most influential women in business and professional services".</p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_hlumelo.jpg" class="judge_profile_img LEFT">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">HLUMELO BIKO</span><br><br>Hlumelo, <strong>Endeavor board member, executive chairman and co-founder of Spinnaker</strong>, has been involved in venture capital and private equity for 12 years and is currently engaged with several companies focusing on introducing low-cost, high-quality offerings in market segments currently dominated by oligopolies.</p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_michael.jpg" class="judge_profile_img LEFT">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">MICHAEL VACY-LYLE</span><br><br>Michael is <span class="COPY_BOLD">CEO of FNB Business</span>. His vision for FNB Business is to become the number one transactional bank for businesses in South Africa. He believes in an entrepreneurial culture and can-do attitude in everything the bank does.</p>
                        </div>
                        
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_catherine.jpg" class="judge_profile_img LEFT">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">CATHERINE TOWNSHEND</span><br><br><strong>Joining Endeavor SA in 2012</strong>, Catherine has assisted some of the country’s most innovative entrepreneurs realise their global growth aspirations. </p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_clair.jpg" class="judge_profile_img LEFT">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">CLAIARE BUSETTI</span><br><br>Claire is a <span class="COPY_BOLD">council member of the National Advisory Council on Innovation</span>, a member of FirstRand’s Vumela Enterprise Development Fund’s investment committee and a member of the Gauteng Enterprise Development Board, among others. </p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_howard.jpg" class="judge_profile_img LEFT">
                            <p class="BLACK_COPY"><span class="COPY_BOLD">HOWARD ARRAND</span><br><br>Howard is the <strong>Provincial Head for FNB Business in KwaZulu-Natal</strong>. He has previously participated as an ISP judge in the Endeavor process, and also served as a judge/convenor of judges for the EY World Entrepreneur Awards.</p>
                        </div>
                    

                    </div> <!-- judges_slider -->
                    <div class="clear">&nbsp;</div>
                    <img src="<?php echo $image_folder; ?>finalist_slider_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="sj_2015_arrow_left">
					<img src="<?php echo $image_folder; ?>finalist_slider_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="sj_2015_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->