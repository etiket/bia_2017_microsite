
            <div class="alumni_full_heading alumni_slider_bg_color_lt">
                <p>2016</p>
            </div>
            <div class="alumni_nav_btn_cont alumni_full_heading read_more_btn" myContainerID="winner_container_2016"></div>
            <div class="clear"></div>
            <!-- 2016 Finalists slider -->
            <div class="slider_expand_cont" id="winner_container_2016">
                <!-- Winner2016 slider -->
                <div>
                    <!-- <h1>2016 WINNER</h1> -->
                    <p class="TURQ_COPY winnercopy">Congratulations to <strong>Bevan Ducasse</strong>, founder of <strong>wiGroup</strong> on being awarded the <strong>FNB Business Innovator of the Year 2016</strong>.</p>
                    <img src="<?php echo $project_data['mobi_full_address']; ?>images/2016_winner_photo.jpg" class="judge_profile_img LEFT">
                    <div class="winner_content_cont">
                        
                        <img src="<?php echo $project_data['mobi_full_address']; ?>images/2016_winner_logo.png" class="judge_profile_logo" >
                        <div id="winnercopyslider2016">
                            <p>
                            The awards not only recognised Ducasse for his innovation in driving business growth, it also credited him and his company for their potential to scale through the Endeavor network.
                            <br/><br/>
                            wiGroup has demonstrated a trajectory of exponential innovation, from its start as a wallet to its current standing as an international platform that enables point-of-sale trade.
                            <br/><br/>
                            Ducasse embodies entrepreneurship and is passionate about building a significant company that embraces disruptive thinking and scale. Not unlike the FNB Business culture, he rewards and empowers a team of remarkable people who bring the business to life. The decision-making process and the habits of the organisation are all supportive of testing and driving improvements and new efficiencies. FNB is proud to be associated with a business that exudes such passion, drive, courage and fun, all whilst revolutionising the customer payment experience.
                            <br/><br/>
                            As the <strong>FNB Business Innovator of the Year</strong>, Ducasse will attend the prestigious Endeavor ISP in Boston in September this year, courtesy of FNB Business and SWISS International Airlines. As an <strong>Endeavor Entrepreneur</strong>, he gains access to a global network of founders as well as access to a pool of mentors comprising infuential business leaders, and an advisory board to help him achieve his business goals.
                            </p>
                        </div>
                    </div> <!-- winner content cont -->
                    <div class="clear"></div>
                </div> <!-- Winner slider container -->
            </div> <!-- judges slider container -->