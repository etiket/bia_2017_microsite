/*
* frame_functions.js
* Developed by: Arno Cilliers
* My first attempt to create a standardised system frame
* javascript file that will hold all the basic re
* re-usable js functions
*/


/*
CLEAN OPEN A PAGE
Display a div by first running through an array of pages to hide then display
the one that was spesified
*/
function cleanOpenPage(pageOpen,closeArray) {
	for(var i = 0; i < closeArray.length; i++) {
		var pagename = closeArray[i];
		if (pageOpen == pagename) {
			//openPage(pagename);
		}else {
			noDisplay(pagename);
		}
	}
	
	openPage(pageOpen);
}

// function that opens selected page and closes all other pages
function switchPages(pageClose,pageOpen) {
	$("#"+pageClose).fadeOut(150, function() {
		noDisplayObject(this);
		$("#"+pageOpen).fadeIn(150);
  	});
}
function noDisplay(target) {
	var targett = document.getElementById(target);
	targett.style.display = "none";
}
function noDisplayObject(target) {
	target.style.display = "none";
}

//- display div/element by id
function closePage(pageID) {
	$("#"+pageID).fadeOut(100, function() {
		noDisplayObject(this);
	});
}
//hide div/element by id 
function openPage(pageID) {
	$("#"+pageID).fadeIn(250);

}
// VALIDATION FUNCTIONS

/* VALIDATE: EMAIL */
function frame_validateEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}
/* VALIDATE: PASSWORD */
function frame_validatePassword(password,repassword) {
	$.trim(password);
	$.trim(repassword);
	if (password.length >= 2) {
		if (password == repassword) {
			return true;
		}else {
			return false;
		}
	}else {
		return false;
	}
}
/* CHARACTER COUNT */
function frame_charCount(string,minChars,maxChars) {
	$.trim(string);
	if (string.length < minChars) {
		return false;
	}else {
		if (string.length > maxChars) {
			return false;
		}else {
			return true;
		}
	}
}
/* WORD COUNT */
function frame_wordCount(string,minWords,maxWords) {
	$.trim(string);
	var wordsArray = string.split(" ");
	if (wordsArray.length < minWords) {
		return false;
	}else {
		if (wordsArray.length > maxWords) {
			return false;
		}else {
			return true;
		}
	}
}
/* MARK ERROR FIELDS */
function frame_markErrorFields(inputID) {
	//console.log('InputID Marked: '+inputID);
	$('#'+inputID).addClass('frame_input_marked');
}
/* UNMARK ERROR FIELDS */
function frame_unmarkErrorFields(inputID) {
	$('#'+inputID).removeClass('frame_input_marked');
}
/* MARK CHECKED FIELDS */
function frame_markCheckedFields(inputID) {
	$('#'+inputID).addClass('frame_input_checked');
}
/* UNMARK CHECKED FIELDS */
function frame_unmarkCheckedFields(inputID) {
	$('#'+inputID).removeClass('frame_input_checked');
}

/* BUILD ERROR */
function frame_buildErrorModal(errorArray) {
	if (errorArray.length >= 1) {
		var errormessage = "";
		for(var i = 0; i < errorArray.length; i++) {
			errormessage+=errorArray[i]+'\n';
		}
		errorArray = null;
		//alert(" \n"+errormessage);
		//alert("Please complete all the marked fields.");
		$('#user_form_container').show(0,function() {
			$('#lightbox_mask').show(0);
		});	
	}else {
		return true;
	}
}

function displayDelayRemove(target,delaytime) {
	setTimeout(function (){
		$(target).remove();
	}, delaytime);
}

function animatePosition(targetID,distance,miliseconds) {
	$( "#"+targetID ).animate({
		right: distance
	}, miliseconds, function() {
		
	});
}
function gotoURL(linkurl) {
	window.location.href = linkurl;
}
// FACEBOOK AND TWITTER SHARE WINDOWS
/*  ----------- */
/*  BASIC JS    */	
function open_fb_share(address) {
	if (address == null) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		address = "http://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fm.fnbbusinessinnovationawards.co.za";
	}else {
		address = "http://www.facebook.com/sharer/sharer.php?u="+address;
	}
	//var URI = encodeURIComponent(address)
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function open_twitter_share(text,address) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		text = encodeURIComponent(text);
		pathname = encodeURIComponent(pathname);
	if (address == null) {
		address = "https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F"+domainname+pathname+"&via=FNBSA&related=FNBSA&text="+text;
	}else {
		address = encodeURIComponent(address);
		address = "https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F"+address+"&via=FNBSA&related=FNBSA&text="+text;
	}
	//var URI = encodeURIComponent(address)
	//alert("Twitter address: "+address);
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function open_linkedin_share(address) {
	// https://www.linkedin.com/cws/share?url=http%3A%2F%2Fgoogle.com
	if (address == null) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		address = "https://www.linkedin.com/cws/share?url=http%3A%2F%2F"+domainname+pathname;
	}else {
		address = "https://www.linkedin.com/cws/share?url=http%3A%2F%2F"+address;
	}
	//var URI = encodeURIComponent(address)
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}


