// JavaScript Document
// FUNCTIONS ----
	// Set expanding accordian myHeight Attributes
	function setExHeights(IDs) {
		for (var i = 0; i < IDs.length; i++) {
			var id = IDs[i];
			var curHeight = $('#'+id).height();
			if (id == 'p4_col_ex_left' || id == 'p4_col_ex_right') { curHeight = 330; }
			$('#'+id).attr('myHeight',curHeight);
			$('#'+id).height(0)
			////console.log(id+" | height: "+curHeight);
		}
		
		return true;
	}
	// Position main containers in the middle vertically
	function verticalCenterAll() {
		for (var i = 2; i <= 10; i++) {
			var containerID = "PAGE_"+i+"_CONTENT";
			var curHeight = $('#'+containerID).height();
			var NewTopMargin = Math.round((windowHeight-curHeight)/2);
			$('#'+containerID).animate({top: NewTopMargin},'slow','swing');
			$('#'+containerID).attr('myHeight',curHeight);
			$('#'+containerID).attr('myTop',NewTopMargin);
			////console.log(containerID+" | height: "+curHeight+" | myTop: "+NewTopMargin);
		}
	}
	function verticalCenterMe(element) {
		if (windowWidth < 800 || loadingPage == true) {
		}else {
			var myHeight = $(element).height();
			var NewTopMargin = Math.round((windowHeight-myHeight)/2);
			$(element).animate({top: NewTopMargin},'slow','swing');
		}
	}
	
	// CLOSE AND CENTER ALL EXPANDABLE ACCORDIANS
	function closeAndCenterAll(IDs) {
		if (scrolling != true) {
			if (loadingPage == true) {
				// do nothing
			}else {
				for (var i = 0; i < IDs.length; i++) {
					// Get Button Element
					var btnElement = IDs[i];
					btnElement = $('#'+btnElement);
					// Get Accordian Element
					var accElement = $(btnElement).attr('myAccordian');
					accElement = $('#'+accElement);
					var curHeight = $(accElement).height();
					$(accElement).height(0);
					// Get the Parent Main Container
					var myParent = $(accElement).parent();
					if ($(myParent).attr('myHeight')) {
					}else {
						myParent = $(myParent).parent();
					}
					// Align in the Center
					verticalCenterMe(myParent);
					
					// Alternative Image for button when clicked
					var myAltImg = $(btnElement).attr('myAltImg');
					if (myAltImg.indexOf("_alt") > -1) {
						//myAltImg = myAltImg.replace('_alt','');
					}else {
						$(btnElement).attr('src','images/'+myAltImg);
						myAltImg = myAltImg.replace('.png','_alt.png');
						$(btnElement).attr('myAltImg',myAltImg);
					}
					////console.log('closing and centering ALL: '+IDs[i]);
				}
			}
		}
		
		return true;
	}
	
	// On Button click Accordian open
	function openMyAccordian(target) {
		lastOpenedAcc = target;
		var myAcc = $(target).attr('myAccordian');
		// Alternative Image for button when clicked
		var myAltImg = $(target).attr('myAltImg');
		$(target).attr('src','images/'+myAltImg);
		if (myAltImg.indexOf("_alt") > -1) {
			myAltImg = myAltImg.replace('_alt','');
		}else {
			myAltImg = myAltImg.replace('.png','_alt.png');
		}
		$(target).attr('myAltImg',myAltImg);
		var myParent = $('#'+myAcc).parent();
		if ($(myParent).attr('myHeight')) {
		}else {
			myParent = $(myParent).parent();
		}
		// check to start slider
		if ($(target).attr('id') == 'btn_acc_drop_p3') {
			web_owl.trigger('owl.play',4000);
		}
		expandAccordian($('#'+myAcc),myParent);
	}
	function expandAccordian(element,myParent) {
		// Accordian Height
		var myHeight = $(element).attr('myHeight');
		myHeight = parseInt(myHeight);
		var toHeight = myHeight;
		
		// Parent Height
		var myParentTop = $(myParent).attr('myTop');
		var parentHeight = $(myParent).attr('myHeight');
		// Total Height
		////console.log('myHeight: '+myHeight+' + parentHeight: '+parentHeight);
		var totalHeight = (parseInt(myHeight))+(parseInt(parentHeight));
		////console.log('totalHeight: '+totalHeight);
		var newTop = (windowHeight-totalHeight)/2;
		if (totalHeight > windowHeight) { newTop = (windowHeight-(totalHeight+20)); }
		////console.log('newTop: '+newTop);
		//newTop = newTop-myParentTop;
		if ($(element).height() > 1) {
			////console.log('close me');
			$(element).height(0);
			verticalCenterMe(myParent);
		}else {
			////console.log('Open me');
			myHeight = parseInt(myHeight)+20;
			$(element).height(myHeight);
			if (windowWidth < 800) {
			}else {
				$(myParent).animate({top: newTop},'slow','swing');
			}
		}
	}
	
	function getScrollTime(gotoPos) {
		var dif = gotoPos-curDataPos;
		if (dif < 0) { dif=dif*(-1); }
		// get 0.5 for each page
		dif = Math.floor(dif/1000);
		if (dif <= 0) { dif = 1; }
		////console.log('Scroll Time: '+(dif*500));
		return dif*500;
	}
	function blindScroll(gotoPos) {
		scrolling = true;
		$('#main_nav_container').hide(250);
		//$('#lightbox_mask').show();
		//$.scrollTo(gotoPos,getScrollTime(gotoPos),function() { $('#lightbox_mask').hide(); scrolling=false; });
		s.animateTo(gotoPos,
		{duration:1,
			beforerender: function(data) {return false;}
		});
	}
	
	
	function showFNBerror(errorArray,containerID) {
		$('#'+containerID).html('');
		if (errorArray.length >= 1) {
			var errormessage = "";
			
				errormessage += errorArray[0];

			errorArray = null;
			//alert(" \n"+errormessage);
			//alert("Please complete all the marked fields.");
			$('#'+containerID).html(errormessage);
		}else {
			return true;
		}
	}
	
/* SOCIAL SHARE BUTTON FUNCTIONS */
$('.fb_share_thispage').click(function() {
	open_fb_share();
});
$('.in_share_btn').click(function() {
	open_linkedin_share();
});
$('.twitter_share').click(function() {
	open_twitter_share("We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised.");
});
// share domain
$('.fb_share_site').click(function() {
	open_fb_share("http://www.fnbbusinessinnovationawards.co.za");
});
$('.twitter_share_site').click(function() {
	open_twitter_share("FNB Business Innovation Awards http://www.fnbbusinessinnovationawards.co.za","www.fnbbusinessinnovationawards.co.za");
});


function clearFakePlaceholder(target,action) {
	if (action == 'focus') {
		if ($(target).val() === $(target).attr('placeholder')) {
			$(target).val('');
		}
	}else if (action == 'blur') {
		if ($(target).val() == "") {
			$(target).val($(this).attr('placeholder'))
		}
	}
}