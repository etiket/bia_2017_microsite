<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
$image_folder = $project_data['web_full_address']."images/";
$today = $vandag." ".$tyd;
$closeDate = "2017-01-31 23:59";
$finallyDate = "2017-06-9 23:00";

?><!DOCTYPE html>
<html>
<head id="m.fnbbusinessinnovationawards-co.za" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">

<title>FNB Business INNOVATION Awards 2017</title>


	<!-- Mobile Specific Metas
  ================================================== -->
    <meta name="HandheldFriendly" content="true" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="800"/>
    

	<!-- CSS -->
  	<!-- Important Owl stylesheet -->
	<link rel="stylesheet" href="css/owl-carousel/owl.carousel.css">
    <!-- Default Theme -->
    <link rel="stylesheet" href="css/owl-carousel/owl.theme.css">
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
    
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    
    
    <!-- MAJOR META TAGS -->
    <link rel="canonical" href="<?php echo $project_data['full_address']; ?>" />
    <meta name="description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="The FNB Business Innovation Awards" />
    <meta property="og:site_name" content="m-fnbbusinessinnovationawards-co.za" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $project_data['full_address']; ?>" />
    <meta property="og:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png" />
    <meta property="fb:admins" content="670010811"/>
    
    <!-- Modify the project_data in a headscript to customise each page -->
    <meta property="og:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="keywords" content="FNB, Business, Innovation Awards" />
    
    
    
    <!-- TWITTER META TAGS -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="FNB BUSINESS INNOVATION AWARDS." />
    <meta name="twitter:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="twitter:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png">
    
    <!-- REL -->
    <link rel="publisher" href="https://plus.google.com/116427055070017697691/about" />

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	 
	  ga('create', 'UA-35690077-36', 'auto');
	  ga('send', 'pageview');
	 
	</script>
    

</head>

<body>
<?php
	// INCLUDE FNB floodlight tag
	include('_include/floodlight_tag.php');
?>
<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>







    <div class="mobi_page_1_container MOBILE"  id="PAGE_1">
    	<div class="main_container mobi_page_1">
        	<div class="mobi_p1_cta">
        		<img src="<?php echo $image_folder; ?>mobi_p1_cta.png" class="FULL_WIDTH" alt="FNB Business Innovation Awards" />
                <?php
				// CLOSE ENTRIES / CHANGE BUTTON
				
				if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
					?>
                <a href="#MOBI_PAGE_9"><div class="square_btn left_btn MOBILE"><p class="square_btn_p">Finalists</p></div></a>
                	<?php
				}else {
					?>
                <a href="<?php echo $project_data['pages']['entry_login']; ?>" class="MOBILE" target="_blank"><div class="square_btn left_btn MOBILE"><p class="square_btn_p">Enter now</p></div></a>
                    <?php
				}
				?>
                <div class="closing-date">Entries close<br><strong>31 January 2017.</strong></div>
            </div>
            
                <!-- <a href="http://fnbbusinessinnovationawards.co.za/products.php"><img src="images/btn-products.jpg" class="bottom_right_btn"></a> -->
            <div class="clear"></div>
        </div>
        <!-- <img src="<?php echo $image_folder; ?>page1_footer.png" class="DISPLAY_BLOCK FULL_WIDTH"> -->
        <div class="bottom_container">
                <div class="bottom_left">
                <div class="countdown">
                    <div class="countdown_close-text">Entries close in</div>
                    <div class="countdown__unit countdown__unit--first"><span class="countdown__counter days_counter">00</span>d</div>
                    <div class="countdown__unit"><span class="countdown__counter hours_counter">00</span>h</div>
                    <div class="countdown__unit countdown__unit--noborder"><span class="countdown__counter minutes_counter">00</span>m</div>
                </div>
                    <p>Be the next <strong>FNB Business
                        <BR>
                        Innovator of the Year</strong>.
                    </p>
                    
                    <p class="footer_small"><a href="<?php echo $project_data['pages']['entry_terms']; ?>" target="_blank">Terms and conditions</a><!-- <br/><a href="<?php echo $project_data['web_full_address']; ?>privacy.html" target="_blank">Privacy policy</a> --><BR>&nbsp;</p>
                    
                </div> <!-- bottom left -->
                <!-- <a href="http://fnbbusinessinnovationawards.co.za/products.php"><img src="images/btn-products.jpg" class="bottom_right"></a> -->
                <div class="clear"></div>
                
            </div> <!-- bottom container -->
            <a href="http://fnbbusinessinnovationawards.co.za/products.php"><img src="images/btn-products.jpg" class="bottom_right_btn"></a>
    </div> <!-- MOBI PAGE 1 CONTAINER -->
    

    <!-- Bevan 2016 Winner Video page -->
    <div class="std_page page_2_container MOBILE BG_WHITE"  id="PAGE_VIDEO">
        <div class="main_container">
            <div class="winner-holder">
                <video poster="<?php echo $image_folder; ?>poster.jpg" id="winnervideo" class="winner_video" controls>
                    <source src="<?php echo $project_data['web_full_address']; ?>assets/wiGroup.mp4" type="video/mp4">
                    <source src="<?php echo $project_data['web_full_address']; ?>assets/wiGroup.webm" type="video/webm">
                    <source src="<?php echo $project_data['web_full_address']; ?>assets/wiGroup.ogv" type="video/ogv">
                </video>
                <!-- <img src="<?php //echo $image_folder; ?>landing_winner.png" class="landing_9june"> -->
                <img src="<?php echo $image_folder; ?>mobi_bevan.png" class="landing_9june">
                <img src="<?php echo $image_folder; ?>web_01_video_playbtn.png" class="video_play_btn" id="btn_video_play">
            </div>
            <div class="clear"></div>
        </div>
    </div> <!-- Bevan 2016 Winner Video page -->

    
    <!-- PAGE: ABOUT THE AWARDS -->
    <div class="std_page page_2_container BG_TURQ" id="PAGE_2">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_2"></div>
    	<div class="main_container" id="PAGE_2_CONTENT" myHeight="0" myTop="0">
            <h1 class="page_h1 WHITE_COPY">About the awards</h1>
            <p class="WHITE_COPY">
                <span class="COPY_BOLD">Innovation is a theme that remains central to FNB’s business philosophy</span>. We believe that innovation is a key driver of business growth and scaleability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised.
            </p>
            <div class="quote_container">
                <img src="<?php echo $image_folder; ?>web_03_quote-turq-left.png" class="quote_img_left">
                <p class="quote_p WHITE_COPY">
                    The <span class="COPY_BOLD"><i>FNB Business INNOVATION Awards</i></span>, supported by Endeavor, are a real opportunity for high-impact entrepreneurs to unleash their potential. Endeavor’s extensive experience in working with high-impact businesses is perfectly complemented by FNB’s commitment to innovation. This is a necessary platform for every business that has an ambition to become a successful global business. We challenge established businesses to enter the awards to inspire them to think bigger.
                    <img src="<?php echo $image_folder; ?>web_03_quote-turq-right.png" class="quote_img_right">
                </p>
                <p class="quote_author WHITE_COPY" title="Paul Harris">
                    <span class="COPY_BOLD">Paul Harris</span><BR><span>Endeavor Board Member</span>
                </p>
            </div> <!-- quote container -->
            
            <div>   
                <hr class="accordian_drop_hr WHITE_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_turq.png" class="accordian_drop_btn" id="btn_acc_drop_p2" myAccordian="acc_ex_2" myAltImg="web_accordian_btn_turq_alt.png">
            </div>
            <div class="accordian_ex_container" id="acc_ex_2" myHeight="0">
                <p class="WHITE_COPY">
                    <span class="COPY_BOLD">FNB Business, supported by Endeavor SA, is proud to announce that entries for the FNB Business INNOVATION Awards 2017 are now open.</span>
                </p>
                <p class="WHITE_COPY">
                    The <span class="COPY_BOLD">FNB Business INNOVATION Awards 2017</span> aim to recognise and celebrate the efforts of high-impact entrepreneurs who have embraced many challenges to create <span class="COPY_BOLD">world-class, innovative and scaleable companies</span> that continue to forge unique ways of doing business.
                </p>
                <p class="WHITE_COPY">
                    High-growth, founder-led entrepreneurs will be selected based on their potential to become part of Endeavor's international network of innovators, business leaders, entrepreneurs, investors and academics; and become the <span class="COPY_BOLD">FNB Business Innovator of the Year 2017</span>.
                </p>
            </div> <!-- accordian ex container -->
        </div> <!-- PAGE 2 main container -->
    </div> <!-- PAGE 2 -->
    
    
    
    
    <!-- PAGE: THE PRIZE -->
    <div class="std_page page_3_container BG_GREY" id="PAGE_3">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_3"></div>
    	<div class="main_container" id="PAGE_3_CONTENT" myHeight="0" myTop="0">
            <h1 class="page_h1 TURQ_COPY">What the FNB business<BR>innovator of the year will gain</h1>
            <p class="BLACK_COPY">The <span class="COPY_BOLD">prize</span> for the <span class="COPY_BOLD">FNB Business INNOVATION Awards 2017</span> is <span class="COPY_BOLD">an all-expenses-paid trip to attend the prestigious Endeavor International Selection Panel (ISP)</span> in New York. <span class="COPY_BOLD">The FNB Business Innovator of the Year</span> will have the opportunity to pitch their business to a credible panel of global business leaders, meet top investors and rub shoulders with their peers from around the world. Pending feedback from the panel about their business and capability as a global entrepreneur, the winner could walk away with the international recognition and prestigious title of an <span class="COPY_BOLD">Endeavor Entrepreneur</span>.
            <br/><br/>
Finalists will also have the opportunity to attend the ISP at their own cost.</p>
            <div>   
                <hr class="accordian_drop_hr TURQ_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_white.png" class="accordian_drop_btn" id="btn_acc_drop_p3" myAccordian="acc_ex_3" myAltImg="web_accordian_btn_white_alt.png">
            </div>
            
            <div class="accordian_ex_container" id="acc_ex_3" myHeight="0">
            <p class="BLACK_COPY COPY_REGULAR">What is the process for becoming an <strong>Endeavor Entrepreneur</strong>?</p>
                <div class="DESKTOP">
                    <div id="owl-demo" class="p6_slider">
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-01.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-02.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-03.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-04.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-05.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-06.png" alt="Owl Image"></div>
                    </div>
                </div>
                
                <div class="MOBILE">
                    <div id="mobi_owl-demo" class="p6_slider">
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-01.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-02.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-03.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-04.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-05.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-06.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                    </div>
                </div>

                <p class="BLACK_COPY">
                The ISP selection process is akin to a two-day immersion course in business practice. The insight and wisdom that panellists bring to the process never fail to inspire candidates and observers alike. Whether they are chosen or not, entrepreneurs describe the selection process as a life-changing experience that provides them with the tools to take their businesses to the next level.</p>

                <p class="BLACK_COPY">The process takes place over two days. During Interview Day, candidates “pitch” their businesses to a panel; and the panel responds with a series of probing questions relating to finances, talent management, corporate strategy, general business administration, marketing, etc. Once the panel has sufficient information about the candidate and the business, it is able, on Deliberation Day, to consider the suitability of the candidate. </p>

                <p class="BLACK_COPY">The questions panellists will want to answer are: is this business truly innovative; is it scaleable; can it create a large number of jobs; will it contribute to the economic growth of the country; and is the entrepreneur a role model who will help to sustain the entrepreneurial ecosystem in the future?
                </p>
                <div class="clear"></div>
                <div style="position:relative;"></div>
            </div> <!-- accordian ex container -->
            <div class="clear"></div>
        </div> <!-- PAGE 3 main container -->
    </div> <!-- PAGE 3 -->
    
    
    
    <!-- PAGE: SELECTION CRITERIA -->
    <div class="std_page page_4_container BG_WHITE" id="PAGE_4">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_4"></div>
    	<div class="main_container" id="PAGE_4_CONTENT" myHeight="0" myTop="0">
            <h1 class="page_h1 TURQ_COPY">Selection criteria</h1>
            <p class="BLACK_COPY">The selection criteria for the <span class="COPY_BOLD">FNB Business INNOVATION Awards</span> are based on Endeavor’s entrepreneur selection model, which caters for scale-ups. Below are the set criteria.</p>
            <div class="HALF_WIDTH_PX p4_col_container p4_col_left_container">
                <h2>Business criteria</h2>
                <div class="p4_col_ex_left" id="p4_col_ex_left" myHeight="0">
                    <div class="p4_col_content">
                        <ul type="disc">
                            <li>It has a minimum annual turnover of R10-million.</li>
                            <li>It is founder led. </li>
                            <li>It is a unique business. The entrepreneur owns 
the Intellectual Property for the products, model 
or service of the business. It cannot easily be 
replicated. It is not a &ldquo;me-too business&rdquo;, such as a 
consultancy or agency.</li>
                            <li>It has the potential to grow exponentially.</li>
                            <li>It has successfully raised the capital it needed to 
get to this point.</li>
                            <li>It is scaleable, in other words the business 
   has the potential to grow and become a 
   market leader.</li> 
                            <li>It has a business model that is repeatable in any 
country/region.</li>
                        </ul>
                    </div> <!-- p4 col ex left -->
                </div> <!-- p4 col ex left -->
                <div class="p4_col_ex_btn_container">
                    <img src="<?php echo $image_folder; ?>web_04_col1-btn.png" class="btn_p4_col_left FULL_WIDTH" id="btn_p4_col_left" myAccordian="p4_col_ex_left" myAltImg="web_04_col1-btn_alt.png">
                </div> <!-- p4 col ex btn container -->
            </div> <!-- p4 col left container -->
            
            <div class="HALF_WIDTH_PX p4_col_container p4_col_right_container">
                <h2>Business owner criteria</h2>
                <div class="p4_col_ex_right" id="p4_col_ex_right" myHeight="0">
                    <div class="p4_col_content">
                        <p class="WHITE_COPY p4_list_first">He or she:</p>
                        <ul type="disc">
                            <li>Is ambitious and wants the business to become a market leader.</li>
                            <li>Embodies the qualities of a local and global role model.</li>
                            <li>Is open to feedback and mentorship.</li>
                            <li>Is humble and committed to paying it forward.</li>
                            <li>Is mature enough to appreciate the power of Endeavor’s global network.</li>
                        </ul>
                    </div> <!-- p4_col_content -->
                </div> <!-- p4 col ex right -->
                <div class="p4_col_ex_btn_container">
                    <img src="<?php echo $image_folder; ?>web_04_col2-btn.png" class="btn_p4_col_right FULL_WIDTH" id="btn_p4_col_right" myAccordian="p4_col_ex_right" myAltImg="web_04_col2-btn_alt.png">
                </div> <!-- p4 col ex btn container -->
            </div> <!-- p4 col left container -->
            
            <div class="clear form_bottom_margin"></div>
            
            <p class="BLACK_COPY">Facilitated by Endeavor South Africa, the selection criteria of the awards focus on businesses that are able to meet the following standards:</p>
            
            
            <!-- P4 3 COLUMN CONTAINERS -->
            <div class="p4_3col_container">
                <div class="p4_3col p4_3col_col1">
                    <h2 class="TURQ_COPY">Development impact:</h2>
                    <p>Exhibit high-growth potential and the capacity to add substantial economic value by creating a number of high-value jobs.</p>
                </div> <!-- p4_3col_col1 -->
                
                <div class="p4_3col_col2">
                    <h2 class="TURQ_COPY">Business innovation:</h2>
                    <p>Demonstrate real innovation that has the potential to change the way an industry operates locally and/or internationally.</p>
                </div> <!-- p4_3col_col1 -->
                
                <div class="p4_3col p4_3col_col3">
                    <h2 class="TURQ_COPY">Fit with FNB and Endeavor:</h2>
                    <p>Be interested in accepting advice and support from FNB and Endeavor, as well as contributing to the Endeavor network.</p>
                </div> <!-- p4_3col_col1 -->
                <div class="clear"></div>
            </div> <!-- p4 3col container -->
            
            <div class="clear"></div>
            
            <!-- <p class="BLACK_COPY">Factors such as brand and reputation, stakeholder relations and goodwill, environmental sustainability, social responsibility and quality of governance are all taken into account.</p> -->

        </div> <!-- PAGE 4 main container -->
    </div> <!-- PAGE 4 -->
    
    
    
    <!-- PAGE: THE PROCESS -->
    <div class="std_page page_5_container BG_GREY" id="PAGE_5">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_5"></div>
    	<div class="main_container" id="PAGE_5_CONTENT" myHeight="0" myTop="0">
            <h1 class="page_h1 TURQ_COPY">The Process</h1>
            <?php
            if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
            ?>
            <p class="BLACK_COPY"><strong>Entries are now closed.</strong> The inaugural winner will be announced in June 2016. Below is the process that will be followed, which starts with the stringent Endeavor Entrepreneur selection process.</p>
            <?php
            }else {
            ?>
            <p class="BLACK_COPY">Entries are now open and will close on <span class="COPY_BOLD">31 January 2017.</span> The winner will be announced in June 2017. Below is the process that will be followed in selecting the <span class="COPY_BOLD">FNB Business Innovator of the Year 2017</span>, who will then have an opportunity to attend the Endeavor ISP.</p>
            <?php
            }
            ?>
            <div>   
                <hr class="accordian_drop_hr TURQ_COPY"/>
            </div>
            <img src="<?php echo $image_folder; ?>the_process_main_img.png" class="FULL_WIDTH">
            <h1 class="page_h1 page_bottom_h1 TURQ_COPY">FNB Business innovation awards process</h1>
            <p class="COPY_ITALIC process_disclaimer">Note that should you enter the <span class="COPY_BOLD">FNB Business INNOVATION Awards</span>, you will need to make yourself available for all the stages of the process, including the Gala Award Ceremony.</p>
        </div> <!-- PAGE 5 main container -->
    </div> <!-- PAGE 5 -->
    
    
    
    
    
    
   <!-- ABOUT ENDEAVOR -->
    <div class="std_page page_7_container BG_WHITE" id="PAGE_6">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_6"></div>
    	<div class="main_container" id="PAGE_6_CONTENT" myHeight="0" myTop="0">
            <h1 class="page_h1 TURQ_COPY">About endeavor</h1>
            <img src="<?php echo $image_folder; ?>web_07_endeavor-logo.png" class="p7_logo" />
            <p class="BLACK_COPY">Endeavor South Africa is the leading supporter of high-impact entrepreneurs around the world. By high-impact they mean individuals with the biggest dreams, the greatest potential to create companies that matter and grow, and the highest likelihood to inspire others. They are the only organisation to focus on the scale-up, not the start-up, because that is where Endeavor believes the highest job and wealth creation happen. 
            </p>
            <div>   
                <hr class="accordian_drop_hr TURQ_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_white.png" class="accordian_drop_btn" id="btn_acc_drop_p7" myAccordian="acc_ex_7" myAltImg="web_accordian_btn_white_alt.png">
            </div>
            <div class="accordian_ex_container" id="acc_ex_7" myHeight="0">
                <p class="BLACK_COPY">
            FNB partnered with Endeavor as they are leading the global movement to catalyse long-term economic growth by selecting, mentoring and accelerating the best high-impact entrepreneurs around the world. Once selected, <span class="COPY_BOLD">Endeavor Entrepreneurs</span> are provided with customised support from a volunteer network of 2 700+ global and local business leaders who serve as mentors, advisors, connectors, investors and role models.</p>

                <p class="BLACK_COPY">Endeavor provides its Entrepreneurs with access to funding, markets, talent and a support system with the purpose of helping them to:</p>
                <p class="COPY_REGULAR BLACK_COPY">THINK BIGGER</p>
                
                <ul type="disc">
                    <li>Through the selection process.</li>
                    <li>By inspiration and example of a global network of business leaders.</li>
                    <li>By peer pressure (role modelling).</li>
                    <li>Custom Advisory Board.</li>
                </ul>
                <p class="BLACK_COPY">For more information on Endeavor, visit <a href="http://www.endeavor.co.za" target="_blank" class="BLACK_COPY">www.endeavor.co.za</a></p> 
            </div> <!-- accordian ex container -->
        </div> <!-- PAGE 6 main container -->
    </div> <!-- PAGE 6 -->
    
    
    
    <!-- ALUMNI -->
    <div class="std_page page_6_container BG_GREY" id="PAGE_7">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_7"></div>
        <div class="main_container" id="PAGE_7_CONTENT" myHeight="0" myTop="0">
        	<h1 class="page_h1 TURQ_COPY">Alumni</h1>
            <p>The <span class="COPY_BOLD">FNB Business INNOVATION Awards</span> has been built on the strength of its previous judges, finalists and winners, as well as the ongoing support that is provided by both FNB Business and the Endeavor mentors. This ensures that winners and finalists remain a part of the awards, benefiting from the mentorship for years to come.</p>
            <div class="alumni_colour_cont">
                <div class="alumni_nav_cont">
                    <p>2015</p>
                </div> <!-- alumni nav cont 1 -->
                <div class="alumni_nav_cont">
                    <p>2016</p>
                </div> <!-- alumni nav cont 2 -->
            </div> <!-- alumni colour cont -->
            <div class="clear"></div>
            <div class="alumni_nav_btn_cont read_more_dt_btn" id="alumni_btn_2015" myContainerID="sliders_container_2015"></div> <!-- alumni nav cont 1 -->
            <div class="alumni_nav_btn_cont read_more_btn" id="alumni_btn_2016" myContainerID="sliders_container_2016"></div> <!-- alumni nav cont 2 -->
            <div class="clear"></div>
            <div class="slider_expand_cont" id="sliders_container_2015">
                <?php
                include('sliders/2015-judges.php');
                include('sliders/2015-finalists.php');
                include('sliders/2015-winner.php');
                ?>
            </div>

            <div class="slider_expand_cont" id="sliders_container_2016">
                <?php
                include('sliders/2016-judges.php');
                include('sliders/2016-finalists.php');
                include('sliders/2016-winner.php');
                ?>
            </div>


        </div> <!-- ALUMNI MAIN CONT -->
    </div> <!-- PAGE ALUMNI -->   
    
    
    <!-- CONTACT US PAGE -->
    <div class="std_page page_8_container BG_WHITE" id="PAGE_8">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_8"></div>
    	<div class="main_container" id="PAGE_8_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">Contact us</h1>
            <p class="contact_us_copy TURQ_COPY">For any queries, email us at <a href="mailto:innovationawards@fnb.co.za" class="TURQ_COPY">innovationawards@fnb.co.za</a></p>
            <hr class="accordian_drop_hr page_8_line TURQ_COPY"/>
        </div> <!-- PAGE 8 main container -->
        <div class="footer_container">
        	<p class="footer_copy BLACK_COPY"><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
            <div class="clear">&nbsp;</div>
        </div> <!-- footer container -->
    </div> <!-- PAGE 8 -->
    
<!-- PAGES / LAYERS END HERE -------------------------------------- -->
    
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>hamburgur_icon.svg" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->

    <nav class="main_nav_container" id="main_nav_container" data-0="top:15px;">
        <ul class="main_nav MOBILE">
			<a href="#MOBI_PAGE_2"><li class="nav_item" id="mobi_nav1">About the awards</li></a>
			<hr />
			<a href="#MOBI_PAGE_3"><li class="nav_item" id="mobi_nav2">The prize</li></a>
			<hr />
			<a href="#MOBI_PAGE_4"><li class="nav_item" id="mobi_nav3">Selection criteria</li></a>
			<hr />
			<a href="#MOBI_PAGE_5"><li class="nav_item" id="mobi_nav4">The process</li></a>
			<hr />
			<a href="#MOBI_PAGE_6"><li class="nav_item" id="mobi_nav5">About Endeavor</li></a>
			<hr />
            <a href="#MOBI_PAGE_7"><li class="nav_item" id="mobi_nav6">Alumni</li></a>
			<hr />
            <a href="#MOBI_PAGE_8"><li class="nav_item" id="mobi_nav7">Contact us</li></a>
            <hr />
		</ul>
        
        <div class="nav_social_btns">
        	<img src="<?php echo $image_folder; ?>social_btn_fb_turq.svg" class="fb_share_thispage">
            <img src="<?php echo $image_folder; ?>social_btn_twitter_turq.svg" class="twitter_share_site">
            <img src="<?php echo $image_folder; ?>social_btn_in_turq.svg" class="in_share_btn">
        </div>
        <img src="<?php echo $image_folder; ?>web_menu_btn_contract.png" class="DISPLAY_BLOCK FULL_WIDTH" id="nav_menu_btn_close">
	</nav> <!-- main nav container -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
	<script src="js/owl.carousel.min.js"></script>
    <!-- custom fnb functions -->
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	// ACCORDIAN ARRAYS
	var accordianIDs = new Array();
	accordianIDs[0] = 'acc_ex_2';
	accordianIDs[1] = 'acc_ex_3';
	accordianIDs[2] = 'p4_col_ex_left';
	accordianIDs[3] = 'p4_col_ex_right';
	accordianIDs[4] = 'acc_ex_7';
	var accBtnIDs = new Array();
	accBtnIDs[0] = 'btn_acc_drop_p2';
	accBtnIDs[1] = 'btn_acc_drop_p3';
	accBtnIDs[2] = 'btn_p4_col_left';
	accBtnIDs[3] = 'btn_p4_col_right';
	accBtnIDs[4] = 'btn_acc_drop_p7';
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;
	var lastOpenedAcc = null;
	var curDataPos = 0;
	var scrolling = false;
	var animContainerHeight = 0;
	// On loading page
	var loadingPage = true;	
	// -----------------------------------
	// **
	// BUTTON ACTIONS	
	$("#btn_video_play").click(function() {
		// $("#bgvid").attr('poster','images/video_image.png');
		document.getElementById('winnervideo').play();
		$(this).css('display','none');
	});
	$("#bgvid").click(function() {
		// $("#bgvid").attr('poster','');
		document.getElementById('winnervideo').pause();
		$('#btn_video_play').css('display','block');
	});

	$('.accordian_drop_btn').click(function() {
		openMyAccordian(this);
	});
	$('.btn_p4_col_left').click(function() {
		openMyAccordian(this);
	});
	$('.btn_p4_col_right').click(function() {
		openMyAccordian(this);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

	// MObi Navigation buttons
	// Navigation items
	$('.nav_item').click(function () {
		$('#main_nav_container').hide(250);
	});







    var sliderBtns = {
        'alumni_btn_2015': {
            'myContainerID' : 'slider_container_2015',
            'readMoreClass' : 'read_more_dt_btn',
            'mySliders' : ['judges_slider_container2015','finalists_slider_container2015','winner_container_2015']
        },
        'alumni_btn_2016': {
            'myContainerID' : 'slider_container_2016',
            'readMoreClass' : 'read_more_btn',
            'mySliders' : ['judges_slider_container2016','finalists_slider_container2016','winner_container_2016']
        },
    }


    



	var s;
    var web_owl;
    var mobi_owl;
    //  SLIDER VARIABLES
    //  slider judges 2016
    var sj_2016;
    var sj_2016_data;
    //  slider finalists 2016
    var sf_2016;
    var sf_2016_data;
    

     //  slider judges 2015
    var sj_2015;
    var sj_2015_data;
    //  slider finalists 2015
    var sf_2015;
    var sf_2015_data;
	$(document).ready(function (){
	//window.onload = function() {
		$('#main_nav_container').hide();
		
		// Activate Skrollr or not
		if (windowWidth < 800) {
			// DO MOBI STUFFS
		}else {
			window.location = "<?php echo $project_data['web_full_address']; ?>";
		}

        web_owl = $("#owl-demo").owlCarousel({
            slideSpeed: 200, //Set AutoPlay to 3 seconds
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        mobi_owl = $("#mobi_owl-demo").owlCarousel({
            slideSpeed: 200, //Set AutoPlay to 3 seconds
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        //  define sliders

        //  2016
        //  finalists 2016
        sf_2016 = $("#finalists_slider2016").owlCarousel({
            slideSpeed: 200,
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        sf_2016_data = $("#finalists_slider2016").data('owlCarousel');
        //  judges 2016
        sj_2016 = $("#judges_slider2016").owlCarousel({
            slideSpeed: 200,
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        sj_2016_data = $("#judges_slider2016").data('owlCarousel');

        //  2015
        //  judges 2015
        sj_2015 = $("#judges_slider2015").owlCarousel({
            slideSpeed: 200,
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        sj_2015_data = $("#judges_slider2015").data('owlCarousel');
        //  finalists 2015
        sf_2015 = $("#finalists_slider2015").owlCarousel({
            slideSpeed: 200,
            paginationSpeed: 800,
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [760,1]
        });
        sf_2015_data = $("#finalists_slider2015").data('owlCarousel');


        $("#sf_2016_arrow_left").click(function() {
            sf_2016_data.prev();
        });
        $("#sf_2016_arrow_right").click(function() {
            sf_2016_data.next();
        });
        $("#sj_2016_arrow_left").click(function() {
            sj_2016_data.prev();
        });
        $("#sj_2016_arrow_right").click(function() {
            sj_2016_data.next();
        });


        $("#sj_2015_arrow_left").click(function() {
            sj_2015_data.prev();
        });
        $("#sj_2015_arrow_right").click(function() {
            sj_2015_data.next();
        });
        $("#sf_2015_arrow_left").click(function() {
            sf_2015_data.prev();
        });
        $("#sf_2015_arrow_right").click(function() {
            sf_2015_data.next();
        });

        /*
        $('.alumni_nav_btn_cont').click(function() {
            var myContainerID = $(this).attr('myContainerID');
            if ($(this).hasClass('read_more_dt_btn')) {
                $(this).removeClass('read_more_dt_btn');
                hideShowSliders(myContainerID);
            }else if ($(this).hasClass('read_more_btn')) {
                $(this).removeClass('read_more_btn');
                hideShowSliders(myContainerID);
            }else {
                $(this).addClass('read_more_btn');
                hideShowSliders(myContainerID,"HIDEALL");
            }
        });
    */


		setTimeout(function() {
			if (setExHeights(accordianIDs)) {
				if (windowWidth < 800) {
					// do nothing
					$('#lightbox_mask').hide();
					loadingPage = false;
				}else if (windowWidth >= 1000) {
					$('#lightbox_mask').hide();
					loadingPage = false;
					// redirect to micro site
					window.location = "<?php echo $project_data['web_full_address']; ?>";
				}






                //  SLIDER BUTTONS - for slider year containers
                $('#alumni_btn_2015').click(function() {
                    $('#sliders_container_2016').hide(0,function() {
                        $('#sliders_container_2015').show();    
                    })
                    $(this).removeClass('read_more_dt_btn');
                    $('#alumni_btn_2016').addClass('read_more_btn');
                });
                $('#alumni_btn_2016').click(function() {
                    $('#sliders_container_2015').hide(0,function() {
                        $('#sliders_container_2016').show();    
                    })
                    $(this).removeClass('read_more_btn');
                    $('#alumni_btn_2015').addClass('read_more_dt_btn');
                });

                //  SLIDER BUTTONS - for individual sliders
                /*$('#btn_slider_js2015').click(function() {
                    // $('#judges_slider_container2015').hide();
                    $('#finalists_slider_container2015').hide();
                    //$('#winner_container_2015').hide();
                    $('#winner_container_2015').hide(0,function() {
                        $('#judges_slider_container2015').show(250);
                    })
                    $(this).removeClass('read_more_dt_btn');
                    //$('#btn_slider_js2015').addClass('read_more_dt_btn');
                    $('#btn_slider_fs2015').addClass('read_more_btn');
                    $('#btn_slider_ws2015').addClass('read_more_lt_btn');
                });*/


                $('#btn_slider_js2015').click(function() {
                    // $('#judges_slider_container2015').hide();
                    $('#finalists_slider_container2015').hide();
                    //$('#winner_container_2015').hide();
                    $('#winner_container_2015').hide(0,function() {
                        toggleDisplay('judges_slider_container2015','btn_slider_js2015','read_more_dt_btn');
                    })
                    //$('#btn_slider_js2015').addClass('read_more_dt_btn');
                    $('#btn_slider_fs2015').addClass('read_more_btn');
                    $('#btn_slider_ws2015').addClass('read_more_lt_btn');
                });
                $('#btn_slider_fs2015').click(function() {
                    $('#judges_slider_container2015').hide();
                    //$('#finalists_slider_container2015').hide();
                    //$('#winner_container_2015').hide();
                    $('#winner_container_2015').hide(0,function() {
                        toggleDisplay('finalists_slider_container2015','btn_slider_fs2015','read_more_btn');
                    })
                    $('#btn_slider_js2015').addClass('read_more_dt_btn');
                    //$('#btn_slider_fs2015').addClass('read_more_btn');
                    $('#btn_slider_ws2015').addClass('read_more_lt_btn');
                });
                $('#btn_slider_ws2015').click(function() {
                    $('#judges_slider_container2015').hide();
                    //$('#finalists_slider_container2015').hide();
                    //$('#winner_container_2015').hide();
                    $('#finalists_slider_container2015').hide(0,function() {
                        toggleDisplay('winner_container_2015','btn_slider_ws2015','read_more_lt_btn');
                    });
                    $('#btn_slider_js2015').addClass('read_more_dt_btn');
                    $('#btn_slider_fs2015').addClass('read_more_btn');
                    //$('#btn_slider_ws2015').addClass('read_more_lt_btn');
                });



                // SLIDERS 2016 --------------------------------------
                $('#btn_slider_js2016').click(function() {
                    // $('#judges_slider_container2016').hide();
                    $('#finalists_slider_container2016').hide();
                    //$('#winner_container_2016').hide();
                    $('#winner_container_2016').hide(0,function() {
                        toggleDisplay('judges_slider_container2016','btn_slider_js2016','read_more_dt_btn');
                    })
                    //$('#btn_slider_js2016').addClass('read_more_dt_btn');
                    $('#btn_slider_fs2016').addClass('read_more_btn');
                    $('#btn_slider_ws2016').addClass('read_more_lt_btn');
                });
                $('#btn_slider_fs2016').click(function() {
                    $('#judges_slider_container2016').hide();
                    //$('#finalists_slider_container2016').hide();
                    //$('#winner_container_2016').hide();
                    $('#winner_container_2016').hide(0,function() {
                        toggleDisplay('finalists_slider_container2016','btn_slider_fs2016','read_more_btn');
                    })
                    $('#btn_slider_js2016').addClass('read_more_dt_btn');
                    //$('#btn_slider_fs2016').addClass('read_more_btn');
                    $('#btn_slider_ws2016').addClass('read_more_lt_btn');
                });
                $('#btn_slider_ws2016').click(function() {
                    $('#judges_slider_container2016').hide();
                    //$('#finalists_slider_container2016').hide();
                    //$('#winner_container_2016').hide();
                    $('#finalists_slider_container2016').hide(0,function() {
                        toggleDisplay('winner_container_2016','btn_slider_ws2016','read_more_lt_btn');
                    });
                    $('#btn_slider_js2016').addClass('read_more_dt_btn');
                    $('#btn_slider_fs2016').addClass('read_more_btn');
                    //$('#btn_slider_ws2015').addClass('read_more_lt_btn');
                });

                $('.slider_expand_cont').hide(250);

			}
		}, 1000);
		window.addEventListener( 'resize', onWindowResize, false );
	});



    function toggleDisplay(sID,btnID,btnClass) {
        var slider = $('#'+sID);
        console.log(btnID);
        if (slider.is(":hidden")) {
            slider.show(250);
            $('#'+btnID).removeClass(btnClass);
        }else {
            slider.hide(250);
            $('#'+btnID).addClass(btnClass);
        }
    }

    // function hideShowSliders(showContID,action) {
    //     var showAction = action || "SHOW";
    //     var myParent = document.getElementById(showContID).parentNode;

    //     var conts = myParent.querySelectorAll('[class="slider_expand_cont"]');
    //     var totalConts = 0;
    //     for (var c = 0; c < conts.length; c++) {
    //         var log = "";
    //         if ($(conts[c]).attr('id') === showContID) {
    //             if (showAction === "HIDEALL") {
    //                 $(conts[c]).hide();    
    //             }else {
    //                 $(conts[c]).show();
    //             }
                
    //         }else { $(conts[c]).hide(); }
    //         log += $(conts[c]).attr('id')+" | ";
    //         if (console.log(log));
    //         totalConts++;
    //     }
    //     console.log(totalConts);
    // }


	function onWindowResize() {
		windowWidth = window.innerWidth;
		windowHeight = window.innerHeight;
	}	

    // VIDEO PLAYER FOR PROFILES
    // $('.finalist-img-btn .watch_now_btn').on('click', function(event){
    $('.watch_now_btn2').on('click', function(event){
        event.preventDefault();
        var index = parseInt($(event.currentTarget).attr('id').substr($(event.currentTarget).attr('id').indexOf('_')+1, $(event.currentTarget).attr('id').length), 10);
        var videoIDs=[
            'tO5TWC2CytI',
            'zcpSJz2Mnf8',
            'WQJM6xU5jZc',
            '0sAU8rXZzqo',
            'YIZ884WG6AY',
            '58VadeF_Co0',
            'CPS42-xvbSA',
            'C0z1QyM22Tk',
            'u-4ISVEkThk',
            'c5l4kQbibUs',
            'umMp6mc7Y_o'
        ];
        $('#profiles_video_container').show();
        $('#profiles_h1').css({
            "opacity": 0
        });
        //player.loadVideoById(videoIDs[index]);

        window.open(
          'https://www.youtube.com/watch?v=' + videoIDs[index],
          '_blank' // <- This is what makes it open in a new window.
        );
    })  


     /*-------------------countdown--------------------*/

    var deadline = 'January 31 2017 23:59:59 GMT+0200';
    function getTimeRemaining(endtime){
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor( (t/1000) % 60 );
      var minutes = Math.floor( (t/1000/60) % 60 );
      var hours = Math.floor( (t/(1000*60*60)) % 24 );
      var days = Math.floor( t/(1000*60*60*24) );

      function formatcounter(n){
        if( n < 0){
            n = 0;
        }
        if(n < 10 && n >= 0){
            n = "0" + n;
        }

        return n;
      }
      return {
        'total': t,
        'days': formatcounter(days),
        'hours': formatcounter(hours),
        'minutes': formatcounter(minutes),
        'seconds': formatcounter(seconds),
        'valid': (t > 0)?true : false
      };
    }

    setInterval(function(){
        var t = getTimeRemaining(deadline);

        for (var i = 0; i<document.querySelectorAll('.days_counter').length; i++ ){
            document.querySelectorAll('.days_counter')[i].innerHTML = t.days;
        }

        // document.querySelectorAll('.days_counter').forEach(function(i){
        //     i.innerHTML = t.days
        // })
        // 
        // 
         for (var i = 0; i<document.querySelectorAll('.hours_counter').length; i++ ){
            document.querySelectorAll('.hours_counter')[i].innerHTML = t.hours;
        }

        // document.querySelectorAll('.hours_counter').forEach(function(i){
        //     i.innerHTML = t.hours
        // })

        for (var i = 0; i<document.querySelectorAll('.minutes_counter').length; i++ ){
            document.querySelectorAll('.minutes_counter')[i].innerHTML = t.minutes;
        }
        // document.querySelectorAll('.minutes_counter').forEach(function(i){
        //     i.innerHTML = t.minutes
        // })

        if(!t.valid){

            for (var i = 0; i<document.querySelectorAll('.enter_now_btn').length; i++ ){
                document.querySelectorAll('.enter_now_btn')[i].style.display = 'none';
            }
            // document.querySelectorAll('.enter_now_btn').forEach(function(i){
            //     i.style.display = 'none'
            // })
        }

    }, 1000)

    /*-------------------END countdown--------------------*/

</script>
</body>

</html>