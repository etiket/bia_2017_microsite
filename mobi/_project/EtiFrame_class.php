<?php
/**
* EtiFrame Class description goes here
* @author Arno Cilliers <arno@etiket.co.za>
* Copyright 2014 Etiket
*/
class EtiFrame {
	
	protected $_pageData;
	protected $_projectData;
	protected $_facebook_data;
	
	function __construct($projectData,$pageData,$fb_data = NULL) {
		$this->_pageData = $pageData;
		$this->_projectData = $projectData;
		$this->_facebook_data = $fb_data;
	}
	
	public function generate_token() {
		return md5($_SERVER['REMOTE_ADDR']);
	}
	
	public function getFrameFooter() {
		$folder_string = $this->_pageData['file_folder'];
		$folderarray = explode("/",$folder_string);
		$folder_string = "";
		if (count($folderarray) > 1) {
			foreach($folderarray as $folder) {
				$folder_string.="../";
			}
		}else {
			$folder_string = "../../";
		}
		$len = strlen($folder_string) - 3;
		$folder_string = substr($folder_string, 0, $len);
		return $folder_string.$this->_projectData['frame_footer'];
	}
	
	public function get_projectData() {
		return $this->_projectData;
	}
	public function get_project_page($page_name) {
		return $this->_projectData['pages'][$page_name];
	}
	/**
	* Get Meta Data from remote page
	* Run like this $tags = getUrlData($entry->link);
	* $returnArray['channel_image'] = $tags['metaTags']['og:image']['value'];
	* The getUrlData method uses the getUrlContents() protected method and parse the data
	*/
	public function getPageMeta($url)
	{
		$result = false;
		$contents = $this->getUrlContents($url);
		
		if (isset($contents) && is_string($contents))
		{
			$title = null;
			$metaTags = null;

			preg_match('/<title>([^>]*)<\/title>/si', $contents, $match );
	
			if (isset($match) && is_array($match) && count($match) > 0)
			{
				$title = strip_tags($match[1]);
			}
			
			preg_match_all('/<[\s]*meta[\s]*name|property="?' . '([^>"]*)"?[\s]*' . 'content="?([^>"]*)"?[\s]*[\/]?[\s]*>/si', $contents, $match);
			
			if (isset($match) && is_array($match) && count($match) == 3)
			{
				$originals = $match[0];
				$names = $match[1];
				$values = $match[2];
				
				if (count($originals) == count($names) && count($names) == count($values))
				{
					$metaTags = array();
					
					for ($i=0, $limiti=count($names); $i < $limiti; $i++)
					{
						$metaTags[$names[$i]] = array (
							'html' => htmlentities($originals[$i]),
							'value' => $values[$i]
						);
					}
				}
			}
			
			$result = array (
				'title' => $title,
				'metaTags' => $metaTags
			);
		}
		
		return $result;
	}
	
	/**
	* Supply raw URL content data to the getUrlData method
	*/
	protected function getUrlContents($url, $maximumRedirections = null, $currentRedirection = 0)
	{
		$result = false;
		$contents = @file_get_contents($url);
		
		// Check if we need to go somewhere else
		if (isset($contents) && is_string($contents))
		{
			preg_match_all('/<[\s]*meta[\s]*http-equiv="?REFRESH"?' . '[\s]*content="?[0-9]*;[\s]*URL[\s]*=[\s]*([^>"]*)"?' . '[\s]*[\/]?[\s]*>/si', $contents, $match);
			
			if (isset($match) && is_array($match) && count($match) == 2 && count($match[1]) == 1)
			{
				if (!isset($maximumRedirections) || $currentRedirection < $maximumRedirections)
				{
					return $this->getUrlContents($match[1][0], $maximumRedirections, ++$currentRedirection);
				}
				$result = false;
			}
			else
			{
				$result = $contents;
			}
		}
		
		return $contents;
	}
	
	/**
	* @param string remoteURL of the xml object
	* @return object Returns an object with all the XML data.
	*/
	function getRemoteXML($remoteURL) {
		// Reads contents of file into a string
		$content = file_get_contents($remoteURL);
		// Pass string into and XML object
		//$x = new SimpleXmlElement($content);
		return simplexml_load_string($content);
	}
	
	
	/**
	* Facebook Standard PAGE posting method
	*/
	public function fb_page_post($facebook) {
		$return = false;
		$post = $facebook->post(array(
					'message'	=> $_facebook_data["share_message"],
					'caption' => $_facebook_data['share_caption'],
					'picture' => $_facebook_data['share_picture'],
					'link' => $_facebook_data['share_link'],
					'description' => $_facebook_data['share_description'],
					'icon' => $_facebook_data['share_icon']
				));
			
		if($post != false) {
			$return = true;
		}else {
			$return = false;
		}
		return $return;
	}
	
	/**
	* Facebook Custom posting method
	* $param array $postData Data array with: message,caption,picture,link,description,icon
	*/
	public function fb_custom_post($postData,$facebook) {
		$return = false;
		$post = $facebook->post(array(
					'message'	=> $postData['message'],
					'caption' => $postData['caption'],
					'picture' => $postData['picture'],
					'link' => $postData['link'],
					'description' => $postData['description'],
					'icon' => $postData['icon']
				));
			
		if($post != false) {
			$return = true;
		}else {
			$return = false;
		}
		return $return;
	}
	
	public function EncodeForEmail($string) {
		// http://stackoverflow.com/questions/1364933/htmlentities-in-php-but-preserving-html-tags
		$list = get_html_translation_table(HTML_ENTITIES);
		//var_dump($list);
		// Remove Characters to NOT encode
		unset($list['"']);
		unset($list['<']);
		unset($list['>']);
		unset($list['&']);
		unset($list['\\']);
		//  And now, you just have to extract the list of keys and values :
		$search = array_keys($list);
		$values = array_values($list);
		// And, finally, you can use str_replace to do the replacement :
		$str_in = $string;
		$str_out = str_replace($search, $values, $str_in);
		//var_dump($str_out);
		return $str_out;
	}
	
	public function cleanStringURL($string) {
		$safeString = str_replace("ê","e",$string);
		$safeString = str_replace("ë","e",$safeString);
		$safeString = str_replace("è","e",$safeString);
		$safeString = str_replace("é","e",$safeString);
		$safeString = strip_tags($safeString);
		return $safeString;
	}
	public function cleanString($string) {
		$safeString = strip_tags($string);
		return $safeString;
	}
	public function cleanStringHTML($string) {
		$safeString = strip_tags($string);
		return $safeString;
	}
	
	public function ConvertSimbols($var, $ConvertQuotes = 0) {
		if ($ConvertQuotes > 0) {
			$var = htmlentities($var, ENT_NOQUOTES, 'UTF-8');
			$var = str_replace('\"', '', $var);
			$var = str_replace("\'", '', $var);
		} else {
			$var = htmlentities($var, ENT_QUOTES, 'UTF-8');
		}
		return $var;
	}

	public function sendBasicEmail($message,$fromname,$fromaddress,$nameto,$emailto,$Subject) {	
		
		$mailmessage=$this->EncodeForEmail($message);
		$eol="\r\n";
		$mime_boundary=md5(time());
		$msg = "";	   
		// HTML Version
		$htmlalt_mime_boundary = $mime_boundary.'_htmlalt';
		//$msg .= $htmlalt_mime_boundary.$eol;
		$msg .= $eol;
		$msg .= $mailmessage.$eol.$eol;
		// SEND THE EMAIL
		$fromname = html_entity_decode($fromname);
		// Common Headers
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'To: '.$nameto.' <'.$emailto.'>' . "\r\n";
		//$headers .= "From: \"".$fromaddress."\"\n";
		$headers .= 'From:'.$fromname.' <'.$fromaddress.'>'.$eol;
		$headers .= 'Reply-To: '.$fromname.' <'.$fromaddress.'>'.$eol;
		$headers .= 'Return-Path: '.$fromname.' <'.$fromaddress.'>'.$eol;    // these two to set reply address
		// Boundry for marking the split & Multitype Headers
		//$headers .= 'MIME-Version: 1.0'.$eol;
		// $headers .= "Content-Type: text/html; boundary=\"".$mime_boundary."\"".$eol; 
		 ini_set('sendmail_from',$fromaddress);  // the INI lines are to force the From Address to be used !
		 mail($emailto, $Subject, $msg, $headers);
		 ini_restore('sendmail_from');
		 
		 return true;
	}
	
	/**
    * setCookie
    * @param string $cookieName Contains the name of the cookie variable ($_COOKIE['cookieName'])
    * @param array $cookieData an Named Array with straight forward values, no arrays inside.
	* @param int $lifeDays is a number/integer of the amount of days that this cookie should last/be alive.
    * @return boolean True or false;
	* Note: The value of the cookie will automatically be URL encoded when you send the cookie (and automatically decoded when received).
	* ----- If you don't want this, you can use setrawcookie() instead.
    */
	//---------------------------------------
	//===  SET COOKIE  ======================
	public function setCookie($cookieName,$cookieData,$lifeDays) {
		//$username = htmlspecialchars($username);
		$cookieLife = 86400 * $lifeDays;   //-- (86400)1 day * N days  -----------
		foreach ($cookieData as $name=>$value) {
			if (is_array($value)) {
				$arraystring = "";
				foreach($value as $v) {
					$arraystring .= $v;
				}
				$value = $arraystring;
			}
			$arrayName = $cookieName."[".$name."]";
			$userCookie = setcookie($arrayName, $value, time()+$cookieLife, '/');
		}
		// check if cookie was set
		// display error if not
		return $userCookie;
	}
	
	/*
	*
	*
	*
	*/
	public function getServerMaxUpload() {
		$val = ini_get('post_max_size');
		$last = strtolower($val[strlen($val)-1]);
		switch($last) {
			// The 'G' modifier is available since PHP 5.1.0
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
		return $val;
	}
	
	/*
	*
	*
	*
	*/
	public function bytestoMB($val) {
		
		$val /= 1024;
		$val /= 1024;
			$returnVal = explode(".",$val);
			$returnDes = substr($returnVal[1],0,2);
			$returnPrime = $returnVal[0];
		if ($returnDes <= 0) {
			$returnVal = $returnPrime;
		}else {
			$returnVal = $returnPrime.".".$returnDes;
		}
		
			
		return $returnVal;
	}
}

