<?php
/**
* Project Class description goes here
* @author Arno Cilliers <arno@etiket.co.za>
* Copyright 2014 Etiket
*/
class Project {
	
	protected $_pageData;
	
	function __construct() {
		
	}
	
	public function checkRm($rm_email,$rm_fnumber) {
		global $_Project_db;
		global $_db_;
		$returnArray = array();
		$_Project_db->where('fnumber',$rm_fnumber);
		$existing_rms = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existing_rms) && count($existing_rms) >= 1) {
			if ($existing_rms[0]['email'] == $rm_email) {
				$returnArray = $existing_rms[0];
			}else {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "The Fnumber does not match the email associated with it.";
			}
		}else {
			
			$_Project_db->where('fnumber',$rm_fnumber);
			$existingFs = $_Project_db->get($_db_['table']['fnumbers'],'fnumber');
			if (is_array($existingFs) && count($existingFs) >= 1) {
				$fnumber_id = $existingFs[0]['id'];
				$insertArray = array(
					'fnumber' => $rm_fnumber,
					'fnumber_id' => $fnumber_id,
					'email' => $rm_email
					);
				if ($_Project_db->insert($_db_['table']['rms'],$insertArray)) {
					$insert_id = $_Project_db->insert_id();
					$returnArray['fnumber'] = $rm_fnumber;
					$returnArray['fnumber_id'] = $fnumber_id;
					$returnArray['email'] = $rm_email;
					$returnArray['id'] = $insert_id;
				}else {
					$returnArray['error'] = true;
					$returnArray['errormessage'] = "System error, please try again.";
				}
			}else {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "To login, please remove the ".urlencode('&quot;<strong>F</strong>&quot; before your employee<BR>')." number and use your FNB email address.";
			}
		}
		
		return $returnArray;
	}
	
	public function rmCheckIn() {
		global $_Project_db;
		global $_db_;
		//echo "SESSION ID: ".$_SESSION['rm_info']['id'];
		$_Project_db->where('id',$_SESSION['rm_info']['id']);
		$existing_rms = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existing_rms) && count($existing_rms) >= 1) {
			if ($existing_rms[0]['fnumber'] == $_SESSION['rm_info']['fnumber'] && $existing_rms[0]['email'] == $_SESSION['rm_info']['email']) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public function generateInviteCode() {
		return md5($_SERVER['REMOTE_ADDR']);
	}
	
	public function checkClientInvite($email) {
		global $_Project_db;
		global $_db_;
		
		$_Project_db->where('client_email',$email);
		$existingEmail = $_Project_db->get($_db_['table']['rm_invites'],'id');
		if (is_array($existingEmail) && count($existingEmail) >= 1) {
			return false;
		}else {
			return true;
		}
	}
	
	public function checkClientEntries($email) {
		global $_Project_db;
		global $_db_;
		
		$_Project_db->where('email',$email);
		$existingEmail = $_Project_db->get($_db_['table']['entries']);
		if (is_array($existingEmail) && count($existingEmail) >= 1) {
			return false;
		}else {
			return true;
		}
	}
	
	public function rsvpInvite($unique_code,$rsvpStatus) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('unique_code',$unique_code);
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites']);
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			$invite_id = $existingInvites[0]['id'];
			$rm_id = $existingInvites[0]['rm_id'];
			$invite_email = $existingInvites[0]['client_email'];
			$insertArray = array(
				'accept_date' => $_DateTime->vandag(),
				'accept_time' => $_DateTime->tyd(),
				'accept_code' => $unique_code,
				'status' => $rsvpStatus,
				'invite_id' => $invite_id,
				'rm_id' => $rm_id
			);
			if ($_Project_db->insert($_db_['table']['invite_accepts'],$insertArray)) {
				$insert_id = $_Project_db->insert_id();
				$returnArray['email'] = $invite_email;
				$returnArray['invite_id'] = $invite_id;
				$returnArray['rm_id'] = $rm_id;
				$returnArray['success'] = true;
				$returnArray['accept_id'] = $insert_id;
			}else {
				$returnArray['error'] = "DB insert Fail";
			}
		}else {
			$returnArray['error'] = "No Existing Invites Match.";
		}
		
		return $returnArray;
	}
	
	public function acceptInvite($accept_id) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('id',$accept_id);
		$existingInvites = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			foreach($existingInvites as $invite) {
				$updateArray = array(
					'accept_date' => $_DateTime->vandag(),
					'accept_time' => $_DateTime->tyd(),
					'status' => 'ACCEPTED'
				);
				$_Project_db->where('id',$accept_id);
				if ($_Project_db->update($_db_['table']['invite_accepts'],$updateArray)) {
					$returnArray['success'] = true;
				}else {
					$returnArray['error'] = "DB update Fail";
				}
			}
		}else {
			$returnArray['error'] = "No Existing Invites Match.";
		}
		
		return $returnArray;
	}
	
	public function acceptRMInvite($entryEmail) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('client_email',$entryEmail);
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			foreach($existingInvites as $invite) {
				$updateArray = array(
					'accept_date' => $_DateTime->vandag(),
					'accept_time' => $_DateTime->tyd(),
					'status' => 'ACCEPTED'
				);
				$_Project_db->where('accept_code',$invite['unique_code']);
				$existingAccepts = $_Project_db -> get($_db_['table']['invite_accepts'],'-id');
				if (is_array($existingAccepts) && count($existingAccepts) >= 1) {
					foreach($existingAccepts as $accept) {
						$_Project_db -> where('id',$accept['id']);
						if ($_Project_db->update($_db_['table']['invite_accepts'],$updateArray)) {
							$returnArray['success'] = true;
						}else {
							//$returnArray['error'] = "DB update Fail";
						}
					}
				}
				
			}
		}else {
			$returnArray['error'] = "No Existing Invites Match.";
		}
		
		return $returnArray;
	}
	
	public function checkAndAcceptInvite($clientEmail) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('client_email',$clientEmail);
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			$invite_id = $existingInvites[0]['id'];
			$unique_code = $existingInvites[0]['unique_code'];
			$_Project_db->where('invite_id',$invite_id);
			$exInviteAccepts = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
			if (is_array($exInviteAccepts) && count($exInviteAccepts) >= 1) {
				$this->acceptInvite($invite_id);
			}else {
				$rsvpResult = $this->rsvpInvite($unique_code,'ACCEPTED');
			}
			return true;
		}else {
			return false;
		}
	}
	
	public function getStatus($inviteId) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('invite_id',$inviteId);
		$invite = $_Project_db->get($_db_['table']['invite_accepts']);
		if (isset($invite[0]) && is_array($invite[0])) {
			if ($invite[0]['status'] == "RSVP") {
				return "Entry incomplete";
			}else if ($invite[0]['status'] == "ACCEPTED") {
				return "Entry complete";
			}else {
				return "Unknown action";
			}
		}else {
			return "Entry incomplete";
		}
	}
	
	public function getAllStatus($inviteId,$email) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('invite_id',$inviteId);
		$invite = $_Project_db->get($_db_['table']['invite_accepts']);
		if (isset($invite[0]) && is_array($invite[0])) {
			if ($invite[0]['status'] == "RSVP") {
				return "Entry incomplete";
			}else if ($invite[0]['status'] == "ACCEPTED") {
				return "Entry complete";
			}else {
				return "Unknown action";
			}
		}else {
			// Check for entry in entries table
			$_Project_db -> where('email',$email);
			$existingEntries = $_Project_db -> get($_db_['table']['entries'],'-id');
			if (count($existingEntries) >= 1) {
				$entry_id = $existingEntries[0]['id'];
				$_Project_db -> where('entry_id',$entry_id);
				$existingUsers = $_Project_db -> get($_db_['table']['entry_users'],'-id');
				if (count($existingUsers) >= 1) {
					$result = $existingUsers[0]['status'];
					return $result;
				}else {
					return "Entry incomplete";
				}
			}else {
				return 'No Action';
			}
		}
	}
	
	
	
	public function registerFNBuser($email,$password,$confirmpass) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		$email = strtolower($email);
		$_Project_db->where('email',$email);
		$existiong_users = $_Project_db->get($_db_['table']['entry_users'],'id');
		if (is_array($existiong_users) && count($existiong_users) >= 1) {
			$returnArray['existing_email'] = true;
		}else {
			// Create new user
			/**/
			// Password retype the same
			if ($password == $confirmpass) {
				if (is_string($password) && $password != "") {
					// Create password Hash
					$passwordHash = $_Project_db->create_hash($password);
					// Insert user into DB
					$insertData = array(
						'email' => $email,
						'password' => $passwordHash,
						'register_date' => $_DateTime->vandag(),
						'register_time' => $_DateTime->tyd(),
						'last_login_date' => $_DateTime->vandag(),
						'last_login_time' => $_DateTime->tyd(),
						'status' => 'REGISTERED',
						'entry_id' => 0
					);
					if ($_Project_db -> insert($_db_['table']['entry_users'],$insertData)) {
						$returnArray['register'] = true;
						// Login user
						if ($userLogin = $this->loginFNBuser($email,$password)) {
							if (isset($userLogin['entry_userinfo'])) {
								$returnArray['entry_userinfo'] = $userLogin['entry_userinfo'];
								$returnArray['signin'] = true;
							}else if (isset($userLogin['invalid_user'])) {
								$returnArray['invalid_user'] = true;
							}
						}else {
							$returnArray['signin'] = false;
						}
					}else {
						$returnArray['insert_error'] = true;
					}
					
				}else {
					$returnArray['password_not_valid'] = true;
				}
			}else {
				$returnArray['password_no_match'] = true;
			}
		}
		
		return $returnArray;
	}
	
	public function loginFNBuser($email,$login_password) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		$email = strtolower($email);
		$_Project_db->where('email',$email);
		$existiong_users = $_Project_db->get($_db_['table']['entry_users'],'id');
		if (is_array($existiong_users) && count($existiong_users) >= 1) {
			foreach($existiong_users as $userDATA) {
				$userPassword = $userDATA['password'];
				$validatedPass = $_Project_db->validate_password($login_password, $userPassword);
				if ($validatedPass == 1 && $userDATA['status'] != "DISABLED") {
					
					// Update last login
					$updateData = array(
						'last_login_date' => $_DateTime->vandag(),
						'last_login_time' => $_DateTime->tyd()
					);
					$_Project_db -> where('id',$userDATA['id']);
					$_Project_db -> update($_db_['table']['entry_users'],$updateData);
					
					
					$returnArray['signin']= true;
					$returnArray['entry_userinfo'] = $userDATA;
					$_SESSION['entry_userinfo'] = $userDATA;
				}else {
					$returnArray['signin'] = false;
					$_SESSION['entry_userinfo'] = NULL;
				}
			}
		}else {
			$returnArray['invalid_email'] = true;
		}
		
		return $returnArray;
	}
	
	
	public function saveEmailtoDB($email_type,$user_id,$emailMessage,$name_to,$email_to,$subject) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$insertData = array(
			'email' => $email_to,
			'name_to' => $name_to,
			'email_type' => $email_type,
			'subject' => $subject,
			'email_message' => $emailMessage,
			'send_date' => $_DateTime -> vandag(),
			'send_time' => $_DateTime -> tyd(),
			'user_id' => $user_id
		);
		if ($_Project_db -> insert($_db_['table']['emails_sent'],$insertData)) {
			//$insert_id = $_Project_db -> insert_id();
			return true;
		}else {
			return false;
		}
	}
	
	public function checkReminded($email_type,$email) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		$_Project_db -> where('email',$email);
		$existingReminders = $_Project_db -> get($_db_['table']['emails_sent'],'-id');
		
		$returnValue = 'DONTSEND';
		if (is_array($existingReminders) && count($existingReminders) >= 1) {
			// Check if reminder email is older than 3 days.
			$daysDif = $_DateTime -> datum_diff($existingReminders[0]['send_date']." ".$existingReminders[0]['send_time'], $_DateTime->vandag()." ".$_DateTime->tyd());
			if ($daysDif < 3 && $existingReminders[0]['email_type'] == 'ENTRY REMINDER') {
				$returnValue = 'DONTSEND';
			}else if ($daysDif >= 3) {
				$returnValue = 'SEND';
			}
		}else {
			$returnValue = 'SEND';
		}
		
		return $returnValue;
	}
}

