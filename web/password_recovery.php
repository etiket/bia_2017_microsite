<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Recover your password";
$project_page['file_name'] = "password_recovery.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);



$image_folder = $project_data['full_address']."images/";

function sendPassword($useremail,$password) {
	global $_EtiFrame;
	global $project_data;
	global $_Project_db;
	global $_db_;
	global $vandag;
	global $tyd;
	global $project_page;
	$emailMessage = "<p style='font-family:Calibri,Arial,Arial,Helvetica, sans-serif; font-size:14px; color:#000000; text-align:left;'>";
	$emailMessage .= "<strong>You have requested a password reset:</strong><BR>";
	$emailMessage .= "<strong>New password: </strong> ".$password."<BR>";
	$emailMessage .= "<BR>";
	$emailMessage .= "Click on the link below go to the login page:<BR>";
	$emailMessage .= "<a href='".$project_data['pages']['entry_login']."' style='font-weight:bold; color:#099; text-decoration:underline'>".$project_data['pages']['entry_login']."</a>";
	$emailMessage .= "</p>";
	

	//	Save password Request as an error in db
	$SaveError = array(
		'error_type' => 'PasswordRecovery',
		'error_description' => 'Password Reset request for - client_email: '.$useremail.' New password: '.$password,
		'error_page' => $project_page['file_name'],
		'user_id' => 0,
		'user_type' => $_db_['table']['entry_users'],
		'error_datetime' => $vandag." ".$tyd
	);
	$_Project_db -> insert($_db_['table']['errors'],$SaveError);


	$fromname = "fnbbusinessinnovationawards.co.za";
	$fromaddress = 'competition@fnbbusinessinnovationawards.co.za';
	$nameto = "Arno";
	$emailto = "arno@etiket.co.za";
	$Subject = "Password Request for FNB Business Innovation Awards.";
	if ($_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject)) {

		

		// To User
		$nameto = $useremail;
		$emailto = $useremail;
		$_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject);


		
		return true;
	}
}

if (isset($_POST['action_type']) && $_POST['action_type'] == 'recover') {
	sleep(3);
	if (isset($_POST['input_email'])) {
		$_Project_db -> where('email',$_POST['input_email']);
		$existing = $_Project_db -> get($_db_['table']['entry_users']);
		if (count($existing) >= 1) {
			$thisUser = $existing[0];
			$newPassword = $_Project_db -> randomPassword(8);
			
			$hashedPassword = $_Project_db -> create_hash($newPassword);
			// Reset User Password
			$_Project_db -> where('id',$thisUser['id']);
			$updateData = array(
				'password' => $hashedPassword
			);
			if ($_Project_db -> update($_db_['table']['entry_users'],$updateData)) {
				if (sendPassword($thisUser['email'],$newPassword)) {
					session_write_close();
					header("Location: ".$project_data['pages']['password_recovery']."?successmessage=Your new password has been sent to the email address that you supplied.");
					exit;
				}
			}
		}else {
			session_write_close();
			header("Location: ".$project_data['pages']['password_recovery']."?errormessage=The email address is not registered on our system, please try again.");
			exit;
		}
	}
}
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style-responsive.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>

<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>

    
    <!-- ------------------------------------------------------------------------------------------------------------------ -->
	<div class="main_container recover_pass_container">
    	
        
        	<h1 class="page_h1 rm_list_h1 TURQ_COPY">reset password</h1>
            <?php
			if (isset($_GET['successmessage'])) {
				?>
                <p><?php echo $_GET['successmessage']; ?></p>
                <a href="<?php echo $project_data['pages']['entry_login']; ?>" class="">
                	<button class="LEFT square_btn btn_orange_arrow_left form_field_right_margin">
						<p>Back</p>
					</button>
                </a>
               	<?php
			}else {
				if (isset($_GET['errormessage'])) {
					?><p><?php //echo $_GET['errormessage']; ?></p><?php
				}
				?>
            <p class="recover_pass_message COPY_REGULAR DARK_GREY_COPY">To reset your password, enter your email address. 
An email will be sent to you with your new password.</p>
            <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="form_entry_login" id="form_recover_pass">
				<div class="l1_container">
                    <input type="text" name="input_email" id="input_email" maxlength="150" class="input_text full_text_width" placeholder="Email*" value="Email*">
                </div> <!-- l1 container -->
                <div class="clear"></div>
                <input type="hidden" name="action_type" value="recover">
                <!-- <input type="button" name="btn_recover" value="Reset" id="btn_recover" class="square_submit_btn recover_btn LEFT"/> -->
                <button class="LEFT square_btn btn_orange_arrow_right form_field_right_margin" name="btn_recover" value="Reset" id="btn_recover" onclick="return false;">
					<p>Reset</p>
				</button>
				<div class="clear"></div>
				<p class="validation_error_message COPY_BOLD" style="padding:15px 0px 0px 0px"><?php
				if (isset($_SESSION['entry_userinfo'])) {
					//$_Project_db->debug($_SESSION['entry_userinfo']);
				}else if (isset($_GET['errormessage'])) {
					echo $_GET['errormessage'];
				}
				
				?></p>
            </form>
            	<?php
			}
			?>
        <div class="clear">&nbsp;</div>
    </div> <!-- form container -->

    
    <!-- LIGHTBOX STUFFS -->
    <div class="lightbox-mask" id="lightbox_mask">
    </div>
    <div class="lightbox" id="user_form_container">
        <div class="lightbox_content">
            <p class="recover_pass_message COPY_REGULAR validation_error_message_grey">Please fill in all the required fields (*) to complete your submission.</p>
            <!-- <div class="square_btn back_btn" id="close_form_btn"><p class="square_btn_p">Back</p></div> -->
            <button class="LEFT square_btn btn_orange_arrow_right form_field_right_margin" name="close_form_btn" value="Reset" id="close_form_btn" onclick="return false;">
				<p>Back</p>
			</button>
            <div class="clear"></div>
        </div> <!-- lightbox content -->
    </div> <!-- lightbox -->


<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn DESKTOP"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site DESKTOP">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage DESKTOP">
</div> <!-- topRightLogo WHITE -->
<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 
<img src="<?php echo $image_folder; ?>/web_right-bottom-copy-white.png" class="web_right_bottom_img"/>
<!-- DISCLAIMER FOOTER -->
<div class="rm_footer_container">
	<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><span class="COPY_REGULAR_ITALIC" style="font-size:9px;">Terms and conditions apply.</span><BR><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
</div> <!-- footer container -->
<?php include('_include/navigation.php'); ?>
    
	<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	$('input:text').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('');
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).val($(this).attr('placeholder'))
            }
        }
    );
	$('textarea').focus(function () {
            if ($(this).html() === $(this).attr('placeholder')) {
                $(this).html('');
            }
        }).blur(function () {
            if ($(this).html() == "") {
                $(this).html($(this).attr('placeholder'))
            }
        }
    );
	
	
	
	
	//$(document).ready(function (){
	window.onload = function() {
		window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide(200);
	};
	function onWindowResize() {
		windowWidth = window.innerWidth;
		windowHeight = window.innerHeight;
	}
	
	
	// BUTTONS AND USER ACTIONS
	$('#close_form_btn').click(function() {
		$('#user_form_container').hide(0,function() {
			$('#lightbox_mask').hide(0);
		});	
	});

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	
	var checkTextValues = new Array();
	$('#btn_recover').click(function() {
	//$("#myform").submit(function(e) {
		var error = new Array();
		
		// Text values
		checkTextValues['input_email'] = $('#input_email').val();
		
		// CHECK NORMAL TEXT INPUT VALUES
		for(var key in checkTextValues)
		{
			//alert("key " + key + " has value " + checkTextValues[key]);
			if (frame_charCount(checkTextValues[key],2,100) && checkTextValues[key] != $('#'+key).attr('placeholder')) {
				frame_markCheckedFields(key);
			}else {
				frame_markErrorFields(key);
				error.push('Please enter a value. | '+key);
			}
		}
	
		
		if (frame_buildErrorModal(error)) {
			$("form#form_recover_pass").submit();
		}
	});
	
	</script>
</body>

</html>