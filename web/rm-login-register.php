<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Relationship Manager Login";
$project_page['file_name'] = "rm-login-register.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);
include('_include/cd.php');

// END OF CAMPAIGN
/*
session_write_close();
header('Location: '.$project_data['pages']['landing']);
exit;
*/
// END --


$image_folder = $project_data['full_address']."images/";

/* ----------------------- REGISTER AND LOGIN ---------------------------------- */
function buildFormDataQuery($formInfoArray) {
	if (count($formInfoArray) >= 1) {
		$valueStr = "";
		foreach($formInfoArray as $key => $value) {
			$valueStr .= "&".$key."=".$value;
		}
		return $valueStr;
	}else {
		return false;
	}
}
if (isset($_POST) && (isset($_GET['action']) || isset($_REQUEST['action']))) {
	$post_action = $_GET['action'];
	// REGISTER
	if ($post_action === "register") {
		$registerForm = array();
		if (isset($_POST['rm_register_fnumber'])) {
			$rm_register_fnumber = $_POST['rm_register_fnumber'];
			$registerForm['rm_register_fnumber'] = $rm_register_fnumber;
			if (isset($_POST['rm_register_email'])) {
				$rm_register_email = $_POST['rm_register_email'];
				$registerForm['rm_register_email'] = $rm_register_email;
				if (isset($_POST['rm_register_name'])) {
					$rm_register_name = $_POST['rm_register_name'];
					$registerForm['rm_register_name'] = $rm_register_name;
					if (isset($_POST['rm_region'])) {
						$rm_region = $_POST['rm_region'];
						$registerForm['rm_region'] = $rm_region;
						$rm_result = $_ProjectF->RM_Register($rm_register_name,$rm_register_fnumber,$rm_register_email,$rm_region);
						if (isset($rm_result['error'])) {
							session_write_close();
							header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=".$rm_result['errormessage'].buildFormDataQuery($registerForm));
							exit;
						}else {
							$_SESSION['rm_info']['name'] = $rm_result['name'];
							$_SESSION['rm_info']['fnumber'] = $rm_result['fnumber'];
							$_SESSION['rm_info']['fnumber_id'] = $rm_result['fnumber_id'];
							$_SESSION['rm_info']['email'] = $rm_result['email'];
							$_SESSION['rm_info']['region'] = $rm_result['region'];
							$_SESSION['rm_info']['id'] = $rm_result['id'];
							//session_write_close();
							header('Location: '.$project_data['pages']['rm_client_invites']);
							exit;
						}
					}else {
						session_write_close();
						header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please select a region.".buildFormDataQuery($registerForm));
						exit;
					}
				}else {
					session_write_close();
					header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please enter your name and surname.".buildFormDataQuery($registerForm));
					exit;
				}
			}else {
				session_write_close();
				header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please enter a valid FNB email address.".buildFormDataQuery($registerForm));
				exit;
			}
		}else {
			session_write_close();
			header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please enter a valid FNB employee number.".buildFormDataQuery($registerForm));
			exit;
		}
	}else if ($post_action === "login") {
		//	LOGIN
		if (isset($_POST['rm_login_fnumber'])) {
			$rm_login_fnumber = $_POST['rm_login_fnumber'];
			if (isset($_POST['rm_login_email'])) {
				$rm_login_email = $_POST['rm_login_email'];
				$rm_result = $_ProjectF -> RM_Login($rm_login_email,$rm_login_fnumber);
				if (isset($rm_result['error'])) {
					session_write_close();
					header("Location: ".$project_data['pages']['rm_login']."?error=LOGIN ERROR&errormessage=".$rm_result['errormessage']."&loginfn=".$rm_login_fnumber."&loginemail=".$rm_login_email);
					exit;
				}else {
					$_SESSION['rm_info']['fnumber'] = $rm_result['fnumber'];
					$_SESSION['rm_info']['fnumber_id'] = $rm_result['fnumber_id'];
					$_SESSION['rm_info']['email'] = $rm_result['email'];
					$_SESSION['rm_info']['id'] = $rm_result['id'];
					$_SESSION['rm_info']['name'] = $rm_result['name'];
					//session_write_close();
					header('Location: '.$project_data['pages']['rm_client_invites']);
					exit;
				}
			}
		}else {
			session_write_close();
			header("Location : ".$project_data['pages']['rm_login']);
			exit;
		}

	}
}else {
	if (isset($_SESSION['rm_info'])) {
		$_SESSION['rm_info'] = NULL;
	}
	$_SESSION = NULL;
	session_unset();
	session_destroy();
	session_write_close();
}

function calcValue($placeHolder,$queryValue) {
	global $_GET;
	if (isset($_GET[$queryValue])) {
		return $_GET[$queryValue];
	}else {
		return $placeHolder;
	}
}
?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>
	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>
</head>

<body class="rm_login_body">
<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>

<div class="login_left_container">
	<h1 class="TURQ_COPY">Nominate your<br/>clients and <strong>WIN</strong></h1>

	<div class="countdown">
      <div class="countdown_close-text">Entries close in</div>
      <div class="countdown__unit countdown__unit--first"><span class="countdown__counter days_counter">00</span>d</div>
      <div class="countdown__unit"><span class="countdown__counter hours_counter">00</span>h</div>
      <div class="countdown__unit countdown__unit--noborder"><span class="countdown__counter minutes_counter">00</span>m</div>
  </div>

</div> <!-- left container -->

<div class="login_right_container">
	<p class="BLACK_COPY">To nominate your clients, login below and submit your clients' details.</p>

	<!-- LOGIN FORM --- -->
	<form action="<?php echo $_SERVER['PHP_SELF']."?action=login"; ?>" method="post" enctype="multipart/form-data" name="form_rm_login" id="form_rm_login">
		<input type="text" name="rm_login_fnumber" id="rm_login_fnumber" class="rm_login_input FULL_WIDTH placeholder_input" maxlength="15" placeholder="Your employee number (without an F)*" value="<?php
		 if (isset($_GET['loginfn'])) {
		 	echo $_GET['loginfn'];
		 }else {
		 	echo "Your employee number (without an F)*";
		 }
		 ?>" onfocus="javascript:clearFakePlaceholder(this,'focus');" onblur="javascript:clearFakePlaceholder(this,'blur');"/>
		<input type="text" name="rm_login_email" id="rm_login_email" class="rm_login_input FULL_WIDTH placeholder_input" maxlength="60" placeholder="Your email address*" value="<?php
		 if (isset($_GET['loginemail'])) {
		 	echo $_GET['loginemail'];
		 }else {
		 	echo "Your email address*";
		 }
		 ?>" onfocus="javascript:clearFakePlaceholder(this,'focus');" onblur="javascript:clearFakePlaceholder(this,'blur');"/>
		<button class="LEFT square_btn btn_orange_arrow_right" id="rm_btn_login">
			<p>Login</p>
		</button>
		<button class="LEFT square_btn btn_orange_arrow_right" id="rm_btn_open_register" <?php
		if (isset($_GET['error']) && $_GET['error'] === "REGISTER ERROR") {
			echo 'style="display:none;"';
		}
		?>>
			<p>Register</p>
		</button>
		<div class="clear"></div>
	</form> <!-- LOGIN FORM -->

	<div id="register_form_container" <?php
		if (isset($_GET['error']) && $_GET['error'] === "REGISTER ERROR") {
			echo 'style="display:inherit;"';
		}
		?>>

		<!-- REGISTER FORM -->
		<form action="<?php echo $_SERVER['PHP_SELF']."?action=register"; ?>" method="post" enctype="multipart/form-data" name="form_rm_register" id="form_rm_register">
			<input type="text" name="rm_register_name" id="rm_register_name" maxlength="100" class="rm_login_input FULL_WIDTH placeholder_input" placeholder="Your name and surname*" value="<?php
				echo calcValue("Your name and surname*","rm_register_name");
			?>" onfocus="javascript:clearFakePlaceholder(this,'focus');" onblur="javascript:clearFakePlaceholder(this,'blur');"/>       
			<input type="text" name="rm_register_fnumber" id="rm_register_fnumber" maxlength="100" class="rm_login_input FULL_WIDTH placeholder_input" placeholder="Your employee number (without an F)*" value="<?php
				echo calcValue("Your employee number (without an F)*","rm_register_fnumber");
			?>" onfocus="javascript:clearFakePlaceholder(this,'focus');" onblur="javascript:clearFakePlaceholder(this,'blur');"/>
		    <input type="text" name="rm_register_email" id="rm_register_email" maxlength="100" class="rm_login_input FULL_WIDTH placeholder_input" placeholder="Your email address*" value="<?php
				echo calcValue("Your email address*","rm_register_email");
			?>" onfocus="javascript:clearFakePlaceholder(this,'focus');" onblur="javascript:clearFakePlaceholder(this,'blur');"/>
		    <select name="rm_region" id="rm_region" class="rm_login_select_list FULL_WIDTH">
		            <option  value="0" >Your region*</option>
		            <option value="Eastern Cape" >Eastern Cape</option>
		            <option value="Free State" >Free State</option>
		            <option value="KwaZulu-Natal" >KwaZulu-Natal</option>
		            <option value="Gauteng South West" >Gauteng South West</option>
		            <option value="Gauteng North" >Gauteng North</option>
		            <option value="Gauteng East" >Gauteng East</option>
		            <option value="Tshwane" >Tshwane</option>
		            <option value="Limpopo" >Limpopo</option>
		            <option value="Mpumalanga" >Mpumalanga</option>
		            <option value="Northern Cape" >Northern Cape</option>
		            <option value="North West" >North West</option>
		            <option value="Western Cape" >Western Cape</option>
			</select>
			<button class="LEFT square_btn btn_orange_arrow_right" id="rm_btn_submit_register">
				<p>Register</p>
			</button>
			<div class="clear"></div>
		</form> <!-- REGISTER FORM -->
		<div class="clear"></div>
	</div> <!-- register form container -->
	<div class="clear"></div>
	<p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
		if (isset($_GET['error']) && ($_GET['error'] === "REGISTER ERROR" || $_GET['error'] === "LOGIN ERROR")) {
			echo 'style="display:inherit;"';
		}
		?>><?php
    	if (isset($_GET['errormessage'])) {
			echo $_GET['errormessage'];
		}
	?></p>
    <p class="cant_login_p">Can't login or register? <a href="mailto:innovationawards@fnb.co.za?subject=Please help me register or login for the FNB Business Innovation Awards RM competition." class="BLACK_COPY COPY_BOLD">Click here</a>.</p>
    <div class="clear"></div>
</div> <!-- right container -->

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->
<!-- DISCLAIMER FOOTER -->
<div class="rm_footer_container">
	<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><a href="<?php echo $project_data['pages']['rm_terms']; ?>" class="COPY_REGULAR_ITALIC BLACK_COPY" style="font-size:7px;text-decoration:underline;" target="_blank"><strong>Terms and conditions.</strong></a><BR><span style="font-size:5px;"><strong>First National Bank - a division of FirstRand Bank Limited.</strong> An Authorised Financial Services and Credit Provider (NCRCP20).</span></p>
</div> <!-- footer container -->
<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 
<img src="<?php echo $image_folder; ?>/web_right-bottom-copy-white.png" class="web_right_bottom_img"/>

	<?php
	// ADD RM navigation here
	//include('_include/rm_navigation.php');
	?>

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='<?php echo $project_data['full_address']; ?>js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/frame_functions.js"></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/rm-functions.js"></script>
	<script type="text/javascript">

		//	Set variables before on page load function
		var checkTextValues = new Array();
		var rm_email = "";

		//$(document).ready(function (){
		window.onload = function() {
			$('#main_nav_container').hide();
			$('#nav_menu_btn_open').click(function() {
				$('#main_nav_container').show(250);
			});
			$('#mobi_nav_menu_btn_open').click(function() {
				$('#main_nav_container').show(250);
			});
			$('#nav_menu_btn_close').click(function() {
				$('#main_nav_container').hide(250);
			});

			$('#rm_btn_open_register').click(function(event) {
				event.preventDefault();
				$('#register_form_container').show();
				$(this).hide();
				// console.log("show register form.")
			})
			$('#rm_btn_submit_register').click(function(event) {
				event.preventDefault();
				/// do validation and registration
				onRegisterClick();
			})
			$('#rm_btn_login').click(function(event) {
				event.preventDefault();
				onLoginClick();
			})
			errorMarkFields();
		};

		function errorMarkFields() {
			var fieldIDS = new Array(<?php
		if (isset($_GET['error']) && ($_GET['error'] === "REGISTER ERROR" || $_GET['error'] === "LOGIN ERROR")) {
			echo 'style="display:inherit;"';
		}
		?>);
		}

		function onLoginClick() {
			var error = new Array();
			// Text values
			checkTextValues['rm_fnumber'] = $('#rm_login_fnumber').val();
			rm_email = $('#rm_login_email').val();
			// Check Fnumber
			checkTextValues['rm_fnumber'] = $.trim(checkTextValues['rm_fnumber']);
			/*
			if (frame_charCount(checkTextValues['rm_fnumber'],2,15) && checkTextValues['rm_fnumber'] != $('#rm_login_fnumber').attr('placeholder')) {
				frame_markCheckedFields('rm_login_fnumber');
			}else {
				frame_markErrorFields('rm_login_fnumber');
				error.push('*Please enter your employee number without an F.');
			}
			*/
			if (frame_validateNumber(checkTextValues['rm_fnumber']) && checkTextValues['rm_fnumber'] != $('#rm_login_fnumber').attr('placeholder')) {
				frame_markCheckedFields('rm_login_fnumber');
			}else {
				frame_markErrorFields('rm_login_fnumber');
				error.push('*Please enter your employee number without an F.');
			}
			
			// Check Email address
			rm_email = $.trim(rm_email);
			if (frame_validateEmail(rm_email)) {
				if (rm_email.indexOf("fnb.co.za") > -1) {
					frame_markCheckedFields('rm_login_email');
				}else {
					frame_markErrorFields('rm_login_email');
					error.push('*Please enter a valid FNB email address.');
				}
			}else {
				frame_markErrorFields('rm_login_email');
				error.push('*Please enter a valid FNB email address.');
			}
			calculateErrorBeforeSubmit(error,'form_rm_login');
			
		}

		


		function onRegisterClick() {
			var error = new Array();
			// Text values
			checkTextValues['rm_name'] = $('#rm_register_name').val();
			checkTextValues['rm_fnumber'] = $('#rm_register_fnumber').val();
			checkTextValues['rm_region'] = $('#rm_region').val();
			rm_email = $('#rm_register_email').val();
			
			// Check Name
			checkTextValues['rm_name'] = $.trim(checkTextValues['rm_name']);
			if (frame_charCount(checkTextValues['rm_name'],2,100) && checkTextValues['rm_name'] != $('#rm_register_name').attr('placeholder')) {
				frame_markCheckedFields('rm_register_name');
			}else {
				frame_markErrorFields('rm_register_name');
				error.push('*Please enter your name and surname.');
			}
			// Check Fnumber
			checkTextValues['rm_fnumber'] = $.trim(checkTextValues['rm_fnumber']);
			/*
			if (frame_charCount(checkTextValues['rm_fnumber'],2,15) && checkTextValues['rm_fnumber'] != $('#rm_register_fnumber').attr('placeholder')) {
				frame_markCheckedFields('rm_register_fnumber');
			}else {
				frame_markErrorFields('rm_register_fnumber');
				error.push('*Please enter your employee number without an F.');
			}*/
			if (frame_validateNumber(checkTextValues['rm_fnumber']) && checkTextValues['rm_fnumber'] != $('#rm_register_fnumber').attr('placeholder')) {
				frame_markCheckedFields('rm_register_fnumber');
			}else {
				frame_markErrorFields('rm_register_fnumber');
				error.push('*Please enter your employee number without an F.');
			}
			
			// Check Email address
			rm_email = $.trim(rm_email);
			if (frame_validateEmail(rm_email)) {
				if (rm_email.indexOf("fnb.co.za") > -1) {
					frame_markCheckedFields('rm_register_email');
				}else {
					frame_markErrorFields('rm_register_email');
					error.push('*Please enter a valid FNB email address.');
				}
			}else {
				frame_markErrorFields('rm_register_email');
				error.push('*Please enter a valid FNB email address.');
			}
			
			// Check Region
			checkTextValues['rm_region'] = $.trim(checkTextValues['rm_region']);
			if (frame_charCount(checkTextValues['rm_region'],2,15) && checkTextValues['rm_region'] != "0" && checkTextValues['rm_region'] != 0) {
				frame_markCheckedFields('rm_region');
			}else {
				frame_markErrorFields('rm_region');
				error.push('*Please select a region.');
			}
			
			calculateErrorBeforeSubmit(error,'form_rm_register');
		}


    /*-------------------countdown--------------------*/

    var deadline = 'January 31 2017 23:59:59 GMT+0200';
    function getTimeRemaining(endtime){
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor( (t/1000) % 60 );
      var minutes = Math.floor( (t/1000/60) % 60 );
      var hours = Math.floor( (t/(1000*60*60)) % 24 );
      var days = Math.floor( t/(1000*60*60*24) );

      function formatcounter(n){
        if( n < 0){
            n = 0;
        }
        if(n < 10 && n >= 0){
            n = "0" + n;
        }

        return n;
      }
      return {
        'total': t,
        'days': formatcounter(days),
        'hours': formatcounter(hours),
        'minutes': formatcounter(minutes),
        'seconds': formatcounter(seconds),
        'valid': (t > 0)?true : false
      };
    }

    setInterval(function(){
        var t = getTimeRemaining(deadline);

        for (var i = 0; i<document.querySelectorAll('.days_counter').length; i++ ){
            document.querySelectorAll('.days_counter')[i].innerHTML = t.days;
        }

        // document.querySelectorAll('.days_counter').forEach(function(i){
        //     i.innerHTML = t.days
        // })
        // 
        // 
         for (var i = 0; i<document.querySelectorAll('.hours_counter').length; i++ ){
            document.querySelectorAll('.hours_counter')[i].innerHTML = t.hours;
        }

        // document.querySelectorAll('.hours_counter').forEach(function(i){
        //     i.innerHTML = t.hours
        // })

        for (var i = 0; i<document.querySelectorAll('.minutes_counter').length; i++ ){
            document.querySelectorAll('.minutes_counter')[i].innerHTML = t.minutes;
        }
        // document.querySelectorAll('.minutes_counter').forEach(function(i){
        //     i.innerHTML = t.minutes
        // })

        if(!t.valid){

            for (var i = 0; i<document.querySelectorAll('.enter_now_btn').length; i++ ){
                document.querySelectorAll('.enter_now_btn')[i].style.display = 'none';
            }
            // document.querySelectorAll('.enter_now_btn').forEach(function(i){
            //     i.style.display = 'none'
            // })
        }

    }, 1000)

    /*-------------------END countdown--------------------*/


	</script>
</body>

</html>