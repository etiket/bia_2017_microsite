<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');

$image_folder = $project_data['full_address']."images/";
$today = $vandag." ".$tyd;
$closeDate = "2017-01-31 23:59";
$_addToURL = "?uq=";
if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
	if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
		$_SESSION['uq'] = $_GET['uq'];
		$_addToURL = "?uq=".$_GET['uq'];
	}else if (isset($_SESSION['uq']) && $_SESSION['uq'] == "c7e8c79013697044886") {
		$_addToURL = "?uq=".$_SESSION['uq'];
	}else {
		header("Location: ".$project_data['full_address']);
	}
}


?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FNB Business INNOVATION Awards</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fixed-positioning.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>

<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>
	<img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_form_topLogo DESKTOP">
	<div class="form_container">
    	
    	<div class="clear"></div>
        <h3 class="thankyou_h3">&nbsp;</h3>
    	<?php
        if ($_SESSION['entry_userinfo']['status'] == 'SUBMITTED') {
			?>
    		<h3 class="thankyou_h3">Thank you for entering the FNB Business Innovation Awards.</h3>
            <?php
		}else {
			
		}
		?>
       <!--  <p class="thankyou_copy">Your entry form has been <span style="text-transform:lowercase; font-weight:bold;"><?php echo $_SESSION['entry_userinfo']['status']; ?></span>.</p> -->
       	<p class="thankyou_copy">Your entry form has been <span style="text-transform:lowercase;"><?php echo $_SESSION['entry_userinfo']['status']; ?></span>.</p>
        <a href="<?php echo $project_data['pages']['entry_form'].$_addToURL; ?>" class=""><div class="square_btn back_btn"><p class="square_btn_p">Back</p></div></a>
    </div> <!-- form container -->
	
    <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img DESKTOP" id="nav_menu_btn_open">
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->
    <?php include('_include/navigation.php'); ?>
    
    
    
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script src='js/frame_functions.js'></script>
	<script type="text/javascript">
	//$(document).ready(function (){
	window.onload = function() {
		//window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide();
	};
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>