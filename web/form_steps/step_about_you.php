<?php
//	Identify the fields in this form with their respective validation functions
$_thisFormFields = array(
	'user_name' => 'validateString(targetField,{"min": 8, "max": 60,"error": "Please enter your full name and surname. This field requires that you use at least 8 characters."})',
	'tel' => 'validateContact(targetField,{"error": "Please ensure that you have entered a ten digit contact number."})',
	//'address' => 'validateString(targetField,{"min": 6, "max": 250,"error": "This field requires that you use at least 6 characters."})',
	'email' => 'validateEmail(targetField,{"error": "Please ensure that you have entered your email dress correctly, eg, joe.soap@gmail.com"})',
	'city' => 'validateString(targetField,{"min": 4, "max": 60,"error": "Please enter the full name of the city in which your business operates. This field requires that you use at least 4 characters."})',
	'province' => 'validateDropdown(targetField,{"error": "Please select the province in which your business operates."})'
);

?>
		<!-- ABOUT YOU -->
		<div class="form_step_cont">
			<div class="main_container">
				<h1 class="TURQ_COPY">About you</h1>
				<!-- FORM STEP 1 -->
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?submitstep=1" method="post" enctype="multipart/form-data" name="form_entryform" id="form_step_about_you">                	
                	<?php
                		//$_ProjectF -> createField($type,$id,$placeholder,$data,$floatSide,$width,$margins)
                		$_ProjectF -> createField('text','user_name','Your name and surname',$_thisFormData,'left','11','none');
                		$_ProjectF -> createField('text','tel','Your contact number eg, 082 123 4567 or 011 123 4567',$_thisFormData,'right','15','none');

                		//$_ProjectF -> createField('text','address','Address',$_thisFormData,'left','11','none');

                		if (!(isset($_thisFormData['email']))) {
                			$_thisFormData['email'] = $_SESSION['entry_userinfo']['email'];
                		}
                		$_ProjectF -> createField('text','email','Your email address  eg, joe.soap@gmail.com',$_thisFormData,'left','11','none');

                		$_ProjectF -> createField('text','city','City',$_thisFormData,'right','15','none');
                	//	province value identify
                	$_listval = "0";
                	if (isset($_thisFormData['province']) && $_thisFormData['province'] != NULL && $_thisFormData['province'] != "") { $_listval = $_thisFormData['province']; }else { $_listval = "0"; }
                	?>
					<div class="form_field_container form_left_container col-11">
						<select name="province" id="province" class="ift placeholder_input" placeholder="0" onchange="javascript:onSelectListChange(this);" onblur="javascript:onSelectListChange(this);">
        					<option  value="0" <?php if ($_listval === "0") { echo "selected"; } ?>>Province</option>
				            <option value="Eastern Cape" <?php if ($_listval === "Eastern Cape") { echo "selected"; } ?>>Eastern Cape</option>
				            <option value="Free State" <?php if ($_listval === "Free State") { echo "selected"; } ?>>Free State</option>
				            <option value="KwaZulu-Natal" <?php if ($_listval === "KwaZulu-Natal") { echo "selected"; } ?>>KwaZulu-Natal</option>
				            <option value="Gauteng" <?php if ($_listval === "Gauteng") { echo "selected"; } ?>>Gauteng</option>
				            <option value="Limpopo" <?php if ($_listval === "Limpopo") { echo "selected"; } ?>>Limpopo</option>
				            <option value="Mpumalanga" <?php if ($_listval === "Mpumalanga") { echo "selected"; } ?>>Mpumalanga</option>
				            <option value="Northern Cape" <?php if ($_listval === "Northern Cape") { echo "selected"; } ?>>Northern Cape</option>
				            <option value="North West" <?php if ($_listval === "North West") { echo "selected"; } ?>>North-West</option>
				            <option value="Western Cape" <?php if ($_listval === "Western Cape") { echo "selected"; } ?>>Western Cape</option>
	        			</select>
                	</div>
					<div class="clear"></div>

					<input type="hidden" name="form_action" value="<?php
							if (isset($_thisFormData['id']) && $_thisFormData['id'] >= 1) {
								echo "UPDATE";
							}else {
								echo "NEW";
							}
							?>">
					<input type="hidden" name="this_step" value="1" />
					<input type="hidden" name="action_type" value="SAVE" id="action_type" />
                </form> <!-- FORM STEP 1 -->
                <div class="clear"></div>
                <p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
					if (isset($_GET['error']) && ($_GET['error'] === "FORM ERROR")) {
						echo 'style="display:inherit;"';
					}
					?>><?php
			    	if (isset($_GET['errormessage'])) {
						echo $_GET['errormessage'];
					}
				?></p>
				<div class="clear"></div>
				<?php $_ProjectF -> createStepNav(1,'form_step_about_you'); ?>

			</div> <!-- main container -->
		</div> <!-- form step cont -->