<?php
//	Identify the fields in this form with their respective validation functions
$_thisFormFields = array(
	'entered_before' => 'validateRadio(targetField)',
	'how_far_description' => 'validateString(targetField,{"min": 8, "max": 2000,"error": "This field requires at least 8 characters."})',
	'scale_description' => 'validateString(targetField,{"min": 140, "max": 5000,"error": "Please provide more information. This field requires at least 140 characters."})',
	'already_endeavor' => 'validateRadio(targetField)'
);

?>
		<!-- THE AWARDS -->
		<div class="form_step_cont">
			<div class="main_container">
				<h1 class="TURQ_COPY">The awards</h1>
				<!-- FORM STEP 4 -->
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?submitstep=4" method="post" enctype="multipart/form-data" name="form_step_theawards" id="form_step_theawards">

					<?php
						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['entered_before']) && $_thisFormData['entered_before'] != NULL && $_thisFormData['entered_before'] != "") { $_radioValue = $_thisFormData['entered_before']; }else { $_radioValue = "NO"; }
	                ?>

					<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Have you entered the awards before?</p>
	                	<div class="clear MOBILE"></div>
	                	<!-- <img src="<?php //echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="7"> -->
	                	<input type="radio" name="entered_before_radio" id="entered_before-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="entered_before-yes" class="css-label">Yes</label>
	                	<input type="radio" name="entered_before_radio" id="entered_before-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="entered_before-no" class="css-label">No</label>
	                	<input type="hidden" name="entered_before" id="entered_before" value="<?php echo $_radioValue; ?>" myDesc="how_far_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','how_far_description','If yes, how far did you get in the process?',$_thisFormData,'left','full','none');
						$_ProjectF -> createField('textarea','scale_description','What does scale mean to you? (max 500 words)',$_thisFormData,'left','full','none');

						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['already_endeavor']) && $_thisFormData['already_endeavor'] != NULL && $_thisFormData['already_endeavor'] != "") { $_radioValue = $_thisFormData['already_endeavor']; }else { $_radioValue = "NO"; }
					?>

					<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Are you already an <strong>Endeavor Entrepreneur</strong>?</p>
	                	<div class="clear MOBILE"></div>
	                	<!-- <img src="<?php //echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="8"> -->
	                	<input type="radio" name="already_endeavor_radio" id="already_endeavor-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="already_endeavor-yes" class="css-label">Yes</label>
	                	<input type="radio" name="already_endeavor_radio" id="already_endeavor-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="already_endeavor-no" class="css-label">No</label>
	                	<input type="hidden" name="already_endeavor" id="already_endeavor" value="<?php echo $_radioValue; ?>"/>
	                </div>

	                <input type="hidden" name="form_action" value="<?php if (isset($_thisFormData['id']) && $_thisFormData['id'] >= 1) { echo "UPDATE"; }else { echo "NEW"; } ?>">
	                <input type="hidden" name="this_step" value="4" />
					<input type="hidden" name="action_type" value="SAVE" id="action_type" />
                </form> <!-- FORM STEP 4 -->
                <p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
					if (isset($_GET['error']) && ($_GET['error'] === "FORM ERROR")) {
						echo 'style="display:inherit;"';
					}
					?>><?php
			    	if (isset($_GET['errormessage'])) {
						echo $_GET['errormessage'];
					}
				?></p>
                <div class="clear"></div>
				<?php $_ProjectF -> createStepNav(4,'form_step_theawards'); ?>

			</div> <!-- main container -->
		</div> <!-- form step cont -->