<?php
//	Identify the fields in this form with their respective validation functions
$_thisFormFields = array(
	'part_of_inc_acc' => 'validateRadio(targetField)',
	'part_of_inc_acc_description' => 'validateString(targetField,{"min": 8, "max": 10000,"error": "This field requires at least 8 characters."})',
	'gone_through_endeavor' => 'validateRadio(targetField)',
	'gone_through_endeavor_description' => 'validateString(targetField,{"min": 8, "max": 10000,"error": "This field requires at least 8 characters."})',
	'biggest_challenge_description' => 'validateString(targetField,{"min": 140, "max": 10000,"error": "Please provide more information. This field requires at least 140 characters."})',
	'where_comp_source' => 'validateDropdown(targetField,{"error": "Please select where you heard about the awards."})',
	'where_other_source' => 'validateString(targetField,{"min": 2, "max": 500,"error": "This field requires at least 2 characters."})'
);

?>

		<!-- GENERAL -->
		<div class="form_step_cont">
			<div class="main_container">
				<h1 class="TURQ_COPY">General</h1>
				<!-- FORM STEP 5 -->
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?submitstep=5" method="post" enctype="multipart/form-data" name="form_step_general" id="form_step_general">

					<?php
						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['part_of_inc_acc']) && $_thisFormData['part_of_inc_acc'] != NULL && $_thisFormData['part_of_inc_acc'] != "") { $_radioValue = $_thisFormData['part_of_inc_acc']; }else { $_radioValue = "NO"; }
	                ?>

                	<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Are you part of any incubator or accelerator programme?</p>
	                	<div class="clear MOBILE"></div>
	                	<input type="radio" name="part_of_inc_acc_radio" id="part_of_inc_acc-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="part_of_inc_acc-yes" class="css-label">Yes</label>
	                	<input type="radio" name="part_of_inc_acc_radio" id="part_of_inc_acc-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="part_of_inc_acc-no" class="css-label">No</label>
	                	<input type="hidden" name="part_of_inc_acc" id="part_of_inc_acc" value="<?php echo $_radioValue; ?>" myDesc="part_of_inc_acc_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','part_of_inc_acc_description','If yes, please elaborate.',$_thisFormData,'left','full','none');
						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['gone_through_endeavor']) && $_thisFormData['gone_through_endeavor'] != NULL && $_thisFormData['gone_through_endeavor'] != "") { $_radioValue = $_thisFormData['gone_through_endeavor']; }else { $_radioValue = "NO"; }
					?>

					<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Have you gone through the Endeavor selection process before?</p>
	                	<div class="clear MOBILE"></div>
	                	<input type="radio" name="gone_through_endeavor_radio" id="gone_through_endeavor-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="gone_through_endeavor-yes" class="css-label">Yes</label>
	                	<input type="radio" name="gone_through_endeavor_radio" id="gone_through_endeavor-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="gone_through_endeavor-no" class="css-label">No</label>
	                	<input type="hidden" name="gone_through_endeavor" id="gone_through_endeavor" value="<?php echo $_radioValue; ?>" myDesc="gone_through_endeavor_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','gone_through_endeavor_description','If yes, please elaborate.',$_thisFormData,'left','full','none');
						$_ProjectF -> createField('textarea','biggest_challenge_description','What are the biggest challenges that you foresee over the next 12 months?',$_thisFormData,'left','full','none');
					?>




					<?php
                		//	wHERE DID YOU HEAR ABOUT THE AWARDS DROPDOWN
	                	$_listval = "0";
	                	if (isset($_thisFormData['where_comp_source']) && $_thisFormData['where_comp_source'] != NULL && $_thisFormData['where_comp_source'] != "") { $_listval = $_thisFormData['where_comp_source']; }else { $_listval = "0"; }
                	?>
                	<p class="LEFT col-full listlabel">Where did you hear about the <strong>FNB Business INNOVATION Awards?</strong></p>
                	<div class="form_field_container form_right_container col-full">
						<select name="where_comp_source" id="where_comp_source" class="ift smallist <?php if ($_listval === '0') { echo 'placeholder_input'; } ?>" placeholder="0" onchange="javascript:onSelectWhereCompSource(this);" onblur="javascript:onSelectWhereCompSource(this);" myDesc="where_other_source">
							<option value="0" <?php if ($_listval === "0") { echo "selected"; } ?> >Select an option.</option>
        					<option value="Radio" <?php if ($_listval === "Radio") { echo "selected"; } ?> >Radio</option>
        					<option value="Billboard" <?php if ($_listval === "Billboard") { echo "selected"; } ?> >Billboard</option>
        					<option value="Print" <?php if ($_listval === "Print") { echo "selected"; } ?> >Print</option>
        					<option value="Social media" <?php if ($_listval === "Social media") { echo "selected"; } ?> >Social media</option>
        					<option value="Digital" <?php if ($_listval === "Digital") { echo "selected"; } ?> >Digital</option>
        					<option value="Other" <?php if ($_listval === "Other") { echo "selected"; } ?> >Other</option>
						</select>
					</div>
					<input type="hidden" name="where_other_hidden" id="where_other_hidden" value="<?php if ($_listval !== "0") { echo "YES"; }else { echo "NO"; } ?>" myDesc="where_other_source" />
					<?php
						$_ProjectF -> createField('text','where_other_source','Please specify.',$_thisFormData,'left','full','none');
					?>
					<div class="clear"></div>






					<div class="form_field_container form_left_container col-full">
						<img src="<?php echo $project_data['full_address']; ?>images/code.png" class="LEFT" />
						<div class="clear"></div>
					</div>

					<div class="form_field_container form_left_container col-full">
						<p>Enter code shown:</p>
						<div class="clear"></div>
					</div>
					<?php
						$_ProjectF -> createField('text','code_input','',$_thisFormData,'left','10','none');
					?>

					<input type="hidden" name="form_action" value="<?php if (isset($_thisFormData['id']) && $_thisFormData['id'] >= 1) { echo "UPDATE"; }else { echo "NEW"; } ?>">
					<input type="hidden" name="this_step" value="5" />
					<input type="hidden" name="action_type" value="SAVE" id="action_type" />
					<input type="hidden" name="action_submit_form" id="action_submit_form" value="NO" />
                
                <div class="clear"></div>
                <?php $_ProjectF -> createStepNav(5,'form_step_general'); ?>
				</form> <!-- FORM STEP 5 -->
				<p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
					if (isset($_GET['error']) && ($_GET['error'] === "FORM ERROR")) {
						echo 'style="display:inherit;"';
					}
					?>><?php
			    	if (isset($_GET['errormessage'])) {
						echo $_GET['errormessage'];
					}
				?></p>
			</div> <!-- main container -->
		</div> <!-- form step cont -->
