<!-- THANK YOU -->
		<div class="form_step_cont">
			<div class="main_container thankyou_main_cont">
				<h1 class="TURQ_COPY NO_BORDER">
					<span class="thankyou_line1">Thank you for entering the</span><br/>
				 	<span class="thankyou_line2">FNB Business INNOVATION Awards.</span><br/>
					<span class="thankyou_line3">Your entry form has been submitted.</span><br/>
				</h1>
				<button class="LEFT square_btn btn_orange" onclick="javascript:window.location='<?php echo $project_data['pages']['home']; ?>'">
					<p>Home</p>
				</button>
			</div> <!-- main container -->
		</div> <!-- form step cont -->