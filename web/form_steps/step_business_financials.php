<?php

//	Identify the fields in this form with their respective validation functions
$_thisFormFields = array(
	'annual_turnover' => 'validateDropdown(targetField,{"error": "Please select the business’ annual turnover."})',
	'revenue_lastyear' => 'validateString(targetField,{"min": 1, "max": 50,"error": "Please fill in the business’ revenue for last year."})',
	'projected_revenue' => 'validateString(targetField,{"min": 1, "max": 50,"error": "Please fill in the business’ projected revenue."})',
	'opportunity_to_grow' => 'validateRadio(targetField)',
	'opportunity_to_grow_description' => 'validateString(targetField,{"min": 140, "max": 10000,"error": "Please provide more information. This field requires at least 140 characters."})',
	'is_unique' => 'validateRadio(targetField)',
	'is_unique_description' => 'validateString(targetField,{"min": 140, "max": 10000,"error": "Please provide more information. This field requires at least 140 characters."})',
	'own_intellectual_prop' => 'validateRadio(targetField)',
	'intellectual_prop_description' => 'validateString(targetField,{"min": 140, "max": 10000,"error": "Please provide more information. This field requires at least 140 characters."})'
);

?>

		<!-- BUSINESS FINANCIALS -->
		<div class="form_step_cont">
			<div class="main_container wide_form_cont">
				
				<h1 class="TURQ_COPY">Business financials</h1>
				<!-- FORM STEP 3 -->
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?submitstep=3" method="post" enctype="multipart/form-data" name="form_entryform" id="form_step_businessfinantials">
                	<?php
                		//	Industry list value identify
	                	$_listval = "0";
	                	if (isset($_thisFormData['annual_turnover']) && $_thisFormData['annual_turnover'] != NULL && $_thisFormData['annual_turnover'] != "") { $_listval = $_thisFormData['annual_turnover']; }else { $_listval = "0"; }
                	?>
                	<p class="LEFT col-full listlabel">Annual turnover (How much money does your business turn over per year?)</p>
                	<div class="form_field_container form_right_container col-full">
						<select name="annual_turnover" id="annual_turnover" class="ift smallist <?php if ($_listval === '0') { echo 'placeholder_input'; } ?>" placeholder="0" onchange="javascript:onSelectListChange(this);" onblur="javascript:onSelectListChange(this);">
							<option value="0" <?php if ($_listval === "0") { echo "selected"; } ?> >Select an option.</option>
        					<option value="R0 - to R8-million" <?php if ($_listval === "R0 - to R8-million") { echo "selected"; } ?> >R0 - to R8-million</option>
        					<option value="R8 - R10-million" <?php if ($_listval === "R8 - R10-million") { echo "selected"; } ?> >R8 - R10-million</option>
        					<option value="R10 - R60-million" <?php if ($_listval === "R10 - R60-million") { echo "selected"; } ?> >R10 - R60-million</option>
        					<option value="R60 - R150-million" <?php if ($_listval === "R60 - R150-million") { echo "selected"; } ?> >R60 - R150-million</option>
        					<option value="R150-million +" <?php if ($_listval === "R150-million +") { echo "selected"; } ?> >R150-million +</option>
						</select>
					</div>
					<div class="clear"></div>
					<?php
						$_ProjectF -> createField('textarea','revenue_lastyear','Revenue - last year (How much money did your business make in 2015-16)',$_thisFormData,'left','full','none');
						$_ProjectF -> createField('textarea','projected_revenue','Projected revenue - current year (How much money do you project your business will make in 2017-18?)',$_thisFormData,'left','full','none');

						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['opportunity_to_grow']) && $_thisFormData['opportunity_to_grow'] != NULL && $_thisFormData['opportunity_to_grow'] != "") { $_radioValue = $_thisFormData['opportunity_to_grow']; }else { $_radioValue = "NO"; }
					?>
					<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Does your business have the opportunity to grow?</p>
	                	<div class="clear MOBILE"></div>
	                	<!-- <img src="<?php //echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="4"> -->
	                	<input type="radio" name="opportunity_to_grow_radio" id="opportunity_to_grow-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="opportunity_to_grow-yes" class="css-label">Yes</label>
	                	<input type="radio" name="opportunity_to_grow_radio" id="opportunity_to_grow-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="opportunity_to_grow-no" class="css-label">No</label>
	                	<input type="hidden" name="opportunity_to_grow" id="opportunity_to_grow" value="<?php echo $_radioValue; ?>" myDesc="opportunity_to_grow_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','opportunity_to_grow_description','If yes, what steps have you taken that will allow your business to grow exponentially? (Provide projected business growth timelines).',$_thisFormData,'left','full','none');

						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['is_unique']) && $_thisFormData['is_unique'] != NULL && $_thisFormData['is_unique'] != "") { $_radioValue = $_thisFormData['is_unique']; }else { $_radioValue = "NO"; }
					?>

					<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Is your business unique?</p>
	                	<div class="clear MOBILE"></div>
	                	<img src="<?php echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="5">
	                	<input type="radio" name="is_unique_radio" id="is_unique-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="is_unique-yes" class="css-label">Yes</label>
	                	<input type="radio" name="is_unique_radio" id="is_unique-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="is_unique-no" class="css-label">No</label>
	                	<input type="hidden" name="is_unique" id="is_unique" value="<?php echo $_radioValue; ?>" myDesc="is_unique_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','is_unique_description','If yes, please provide a brief description and mention which part of your business is unique (eg, business model, product, service, distribution, technology)',$_thisFormData,'left','full','none');


						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['own_intellectual_prop']) && $_thisFormData['own_intellectual_prop'] != NULL && $_thisFormData['own_intellectual_prop'] != "") { $_radioValue = $_thisFormData['own_intellectual_prop']; }else { $_radioValue = "NO"; }
					?>


					<div class="form_field_container form_left_container col-full double_line_p">
	                	<p class="LEFT">Does your business own any Intellectual Property?<br/>(Have you patented or copyrighted anything?)</p>
	                	<div class="clear MOBILE"></div>
	                	<img src="<?php echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="6">
	                	<input type="radio" name="own_intellectual_prop_radio" id="own_intellectual_prop-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="own_intellectual_prop-yes" class="css-label">Yes</label>
	                	<input type="radio" name="own_intellectual_prop_radio" id="own_intellectual_prop-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="own_intellectual_prop-no" class="css-label">No</label>
	                	<input type="hidden" name="own_intellectual_prop" id="own_intellectual_prop" value="<?php echo $_radioValue; ?>" myDesc="intellectual_prop_description" />
	                </div>
	                <?php
						$_ProjectF -> createField('textarea','intellectual_prop_description','If yes, tell us what your Intellectual Property consists of.',$_thisFormData,'left','full','none');
					?>

					<input type="hidden" name="form_action" value="<?php if (isset($_thisFormData['id']) && $_thisFormData['id'] >= 1) { echo "UPDATE"; }else { echo "NEW"; } ?>">
					<input type="hidden" name="this_step" value="3" />
					<input type="hidden" name="action_type" value="SAVE" id="action_type" />
                </form> <!-- FORM STEP 3 -->
                <div class="clear"></div>
                <p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
					if (isset($_GET['error']) && ($_GET['error'] === "FORM ERROR")) {
						echo 'style="display:inherit;"';
					}
					?>><?php
			    	if (isset($_GET['errormessage'])) {
						echo $_GET['errormessage'];
					}
				?></p>
                <div class="clear"></div>
				<?php $_ProjectF -> createStepNav(3,'form_step_businessfinantials'); ?>

			</div> <!-- main container -->
		</div> <!-- form step cont -->