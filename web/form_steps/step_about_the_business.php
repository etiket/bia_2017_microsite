<?php
//	Identify the fields in this form with their respective validation functions
$_thisFormFields = array(
	'business_name' => 'validateString(targetField,{"min": 5, "max": 100,"error": "This field requires that you use at least 5 characters."})',
	'business_reg_no' => 'validateRegistrationNumber(targetField,{"min": 11, "max": 60,"error": "You have not entered a valid business registration number. Please enter a valid business registration number using the following format: 1234/567891/00."})',
	'founded_year' => 'checkNumberValue(targetField,{"min": 1900, "max": 2016,"error": "Please use the full four-digit format for the year and ensure that it is no later than 2016."})',
	'founder_name' => 'validateString(targetField,{"min": 8, "max": 60,"error": "Please enter the founder of your business’ full name and surname. This field requires at least 8 characters."})',
	'role_of_founder' => 'validateString(targetField,{"min": 8, "max": 600,"error": "Please provide more information. This field requires at least 8 characters."})',
	'founder_led' => 'validateRadio(targetField)',
	'number_of_staff' => 'checkNumberValue(targetField,{"min": 1, "max": 99999,"error": "Please enter a number of 1 or more."})',
	'industry' => 'validateDropdown(targetField,{"error": "Select the industry."})',
	'industry_other' => 'validateString(targetField,{"min": 5, "max": 450, "error": "Please enter the industry. This field requires at least 5 characters."})',
	'service_summary' => 'validateString(targetField,{"min": 140, "max": 5000,"error": "Please provide more information. This field requires at least 140 characters."})',
	'target_market' => 'validateString(targetField,{"min": 40, "max": 1000,"error": "Please provide more information. This field requires at least 40 characters."})',
	'operate_global' => 'validateRadio(targetField)',
	'global_countries' => 'validateString(targetField,{"min": 2, "max": 1000,"error": "This field requires at least 2 characters."})',
	'bank_with_fnb' => 'validateRadio(targetField)',
	'rm_name' => 'validateString(targetField,{"min": 8, "max": 60,"error": "Please provide your Relationship Manager’s full name. This field requires at least 8 characters."})'
);

//	!!! -- 	ERROR MESSAGE FOR MORE FOUNDERS: 
//			One or more of the numbers entered are not valid. The numbers should be from 0 to 100.
//			Please enter a number of 1 or more.

?>

		<!-- ABOUT THE BUSINESS -->
		<div class="form_step_cont">
			<div class="main_container wide_form_cont">
				<h1 class="TURQ_COPY">About your business</h1>

				<?php
					//$_Project_db -> debug($_SESSION['entry_userinfo']);
				?>

				<!-- FORM STEP 2 -->
                <form action="<?php echo $_SERVER['PHP_SELF']; ?>?submitstep=2" method="post" enctype="multipart/form-data" name="form_entryform" id="form_step_about_thebusiness">
					<?php
                		$_ProjectF -> createField('text','business_name','Business name eg, Toys R Us',$_thisFormData,'left','11','none');
                		$_ProjectF -> createField('text','business_reg_no','Business registration number eg. 1234/567891/00',$_thisFormData,'right','15','none');

                		$_ProjectF -> createField('text','founded_year','Founded (year) eg. 1990',$_thisFormData,'left','11','none');
                		$_ProjectF -> createField('text','founder_name','Founder (Who started the business?)',$_thisFormData,'right','15','none');

                		$_ProjectF -> createField('textarea','role_of_founder','What is the role of the founder in the business? (What does the founder do in the business?)',$_thisFormData,'left','full','none');


                		//	Amount of founders list value identify
	                	$_listval = "1";
	                	if (isset($_thisFormData['amount_of_founders']) && $_thisFormData['amount_of_founders'] != NULL && $_thisFormData['amount_of_founders'] != "") { $_listval = $_thisFormData['amount_of_founders']; }else { $_listval = "0"; }

	                	$_Project_db -> where('entry_id',$_thisFormData['id']);
						$existing = $_Project_db -> get($_db_['table']['entry_founders']);
						if (count($existing) >= 1) {
							$_listval = count($existing);
						}
	                	
                	?>

                	<p class="LEFT col-full listlabel">If your business has more than one founder, select the appropriate number below:</p>
                	<div class="form_field_container form_left_container col-20">
						<select name="amount_of_founders" id="amount_of_founders" class="ift smallist <?php if ($_listval <= 1) { echo 'placeholder_input'; } ?>" placeholder="0" onchange="javascript:onFoundersSelect(this);">
	                    	<?php
							$selected = "";
							$i = 1;
							while ($i < 11) {
								if ($_listval == $i) { $selected = " selected"; }else { $selected = ""; }
								if ($i == 1) {
									?><option value="1" <?php echo $selected; ?>>Select an option.</option><?php
								}else {
									?><option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option><?php
								}
								$i++;
							}
							?>
	    				</select>
	    			</div>
                	<div class="clear"></div>


					<?php
						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['founder_led']) && $_thisFormData['founder_led'] != NULL && $_thisFormData['founder_led'] != "") { $_radioValue = $_thisFormData['founder_led']; }else { $_radioValue = "NO"; }
					?>
                	<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Is your business founder-led? (Does the person who started the business run the business?)</p>
	                	<div class="clear MOBILE"></div>
	                	<img src="<?php echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="1">

						<!-- RADIO GROUP -->
	                	<input type="radio" name="founder_led_radio" id="founder_led-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="founder_led-yes" class="css-label">Yes</label>

	                	<input type="radio" name="founder_led_radio" id="founder_led-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="founder_led-no" class="css-label">No</label>
						<input type="hidden" name="founder_led" id="founder_led" value="<?php echo $_radioValue; ?>"/>

	                </div>

	                <p class="LEFT col-full" id="extrafounders_p_label_for_QC" <?php
	                	if ($_listval >= 2) {
	                		echo " style='display:inherit;'";
	                	}
	                	?>>What is the % shareholding of the founder/s?</p>
	                
	                <!-- EXTRA FOUNDERS -->
	                <div id="extra_founders">
		                <?php
		                	// Add founders from db
		                	if ($_listval >= 2) {
		                		$_Project_db -> where('entry_id',$_thisFormData['id']);
		                		$existing = $_Project_db -> get($_db_['table']['entry_founders']);
		                		$f = 0;
								foreach($existing as $founder) {
									?>
									<div class="form_field_container form_left_container col-17 form_field_none_margin">
										<input type="text" name="founders[]" id="founder_<?php echo $f; ?>" class="ift" placeholder="Founder name" value="<?php echo $founder['name']; ?>" />
									</div>
									<div class="form_field_container form_right_container col-9 form_field_none_margin">
										<input type="text" name="founder_shareholdings[]" id="founder_shareholding_<?php echo $f; ?>" class="ift" placeholder="% shareholding" value="<?php echo $founder['shareholding']; ?>" />
									</div>
									<?php
									$f++;
								}
		                	}
	                	?>
                	</div> <!-- extra founders -->
                	<?php
                		$_ProjectF -> createField('textarea','number_of_staff','Number of staff (How many people are employed by the business?)',$_thisFormData,'left','full','none');
	                	//	Industry list value identify
	                	$_listval = "0";
	                	if (isset($_thisFormData['industry']) && $_thisFormData['industry'] != NULL && $_thisFormData['industry'] != "") { $_listval = $_thisFormData['industry']; }else { $_listval = "0"; }
                	?>
                	<p class="LEFT col-full listlabel">What industry does your business operate in?</p>
					<div class="form_field_container form_right_container col-full">
						<select name="industry" id="industry" class="ift <?php if ($_listval === '0') { echo 'placeholder_input'; } ?>" placeholder="0" onchange="javascript:onIndustrySelectList(this);" onblur="javascript:onIndustrySelectList(this);" myDesc="industry_other">
							<option value="0" <?php if ($_listval === "0") { echo "selected"; } ?>>Select an option.</option>
        					<option value="Agriculture" <?php if ($_listval === "Agriculture") { echo "selected"; } ?>>Agriculture</option>
        					<option value="Automotive" <?php if ($_listval === "Automotive") { echo "selected"; } ?>>Automotive</option>
        					<option value="Contractors/Trade" <?php if ($_listval === "Contractors/Trade") { echo "selected"; } ?>>Contractors/Trade</option>
        					<option value="Entertainment" <?php if ($_listval === "Entertainment") { echo "selected"; } ?>>Entertainment</option>
        					<option value="Finance" <?php if ($_listval === "Finance") { echo "selected"; } ?>>Finance</option>
        					<option value="Healthcare" <?php if ($_listval === "Healthcare") { echo "selected"; } ?>>Healthcare</option>
        					<option value="Media/Marketing" <?php if ($_listval === "Media/Marketing") { echo "selected"; } ?>>Media/Marketing</option>
        					<option value="Education" <?php if ($_listval === "Education") { echo "selected"; } ?>>Education</option>
        					<option value="Retail/eCommerce" <?php if ($_listval === "Retail/eCommerce") { echo "selected"; } ?>>Retail/eCommerce</option>
        					<option value="Travel" <?php if ($_listval === "Travel") { echo "selected"; } ?>>Travel</option>
        					<option value="Manufacturing" <?php if ($_listval === "Manufacturing") { echo "selected"; } ?>>Manufacturing</option>
        					<option value="Food/Hospitality" <?php if ($_listval === "Food/Hospitality") { echo "selected"; } ?>>Food/Hospitality</option>
        					<option value="FMCG" <?php if ($_listval === "FMCG") { echo "selected"; } ?>>FMCG</option>
        					<option value="Transport/Logistics" <?php if ($_listval === "Transport/Logistics") { echo "selected"; } ?>>Transport/Logistics</option>
        					<option value="Medical" <?php if ($_listval === "Medical") { echo "selected"; } ?>>Medical</option>
        					<option value="ICT" <?php if ($_listval === "ICT") { echo "selected"; } ?>>ICT</option>
        					<option value="Mining" <?php if ($_listval === "Mining") { echo "selected"; } ?>>Mining</option>
        					<option value="Other" <?php if ($_listval === "Other") { echo "selected"; } ?>>Other</option>
						</select>
					</div> <!-- industry list -->
					<input type="hidden" name="industry_other_hidden" id="industry_other_hidden" value="<?php if ($_listval === "Other") { echo "YES"; }else { echo "NO"; } ?>" myDesc="industry_other" />
					<?php

						$_ProjectF -> createField('text','industry_other','If other, please state the industry your business operates in.',$_thisFormData,'left','full','none');

						$_ProjectF -> createField('textarea','service_summary','Provide a short summary of what your business sells or what services it provides.',$_thisFormData,'left','full','none');
						$_ProjectF -> createField('textarea','target_market','Business target market (Who buys/uses your products/services?)',$_thisFormData,'left','full','none');

						//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['operate_global']) && $_thisFormData['operate_global'] != NULL && $_thisFormData['operate_global'] != "") { $_radioValue = $_thisFormData['operate_global']; }else { $_radioValue = "NO"; }
					?>
                	<div class="form_field_container form_left_container col-full">
	                	<p class="LEFT">Does the business operate in countries outside of South Africa?</p>
	                	<div class="clear MOBILE"></div>
	                	<!-- <img src="<?php //echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="2"> -->
	                	<input type="radio" name="operate_global_radio" id="operate_global-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="operate_global-yes" class="css-label">Yes</label>
	                	<input type="radio" name="operate_global_radio" id="operate_global-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="operate_global-no" class="css-label">No</label>
	                	<input type="hidden" name="operate_global" id="operate_global" value="<?php echo $_radioValue; ?>" myDesc="global_countries" />
	                </div>
	                <div class="clear"></div>
					<?php
                		$_ProjectF -> createField('text','global_countries','If yes, please state in which countries outside of South Africa your business operates.',$_thisFormData,'left','full','none');

                		//	Identify which radio button to check based on the form data
	                	$_radioValue = "NO";
	                	if (isset($_thisFormData['bank_with_fnb']) && $_thisFormData['bank_with_fnb'] != NULL && $_thisFormData['bank_with_fnb'] != "") { $_radioValue = $_thisFormData['bank_with_fnb']; }else { $_radioValue = "NO"; }
                	?>
                	<div class="form_field_container form_left_container col-15">
	                	<p class="LEFT">Does your business bank with FNB?</p>
	                	<div class="clear MOBILE"></div>
	                	<!-- <img src="<?php //echo $project_data['full_address']; ?>images/btn_form_question-01.svg" class="btn_form_info help" id="3"> -->
	                	<input type="radio" name="bank_with_fnb_radio" id="bank_with_fnb-yes" class="ift css-checkbox" value="YES" <?php if ($_radioValue === "YES") { echo 'checked="checked"'; } ?> /><label for="bank_with_fnb-yes" class="css-label">Yes</label>
	                	<input type="radio" name="bank_with_fnb_radio" id="bank_with_fnb-no" class="css-checkbox" value="NO" <?php if ($_radioValue === "NO" && $_radioValue != "YES") { echo 'checked="checked"'; }else if ($_radioValue != "YES") { echo 'checked="checked"'; } ?> /><label for="bank_with_fnb-no" class="css-label">No</label>
	                	<input type="hidden" name="bank_with_fnb" id="bank_with_fnb" value="<?php echo $_radioValue; ?>" myDesc="rm_name" />
	                </div>
	                <?php
                		$_ProjectF -> createField('text','rm_name','If yes, what is your Relationship Manager\'s name?',$_thisFormData,'right','12','none');
                	?>


					<input type="hidden" name="form_action" value="<?php if (isset($_thisFormData['id']) && $_thisFormData['id'] >= 1) { echo "UPDATE"; }else { echo "NEW"; } ?>">
					<input type="hidden" name="this_step" value="2" />
					<input type="hidden" name="action_type" value="SAVE" id="action_type" />
                </form> <!-- FORM STEP 2 -->
                <div class="clear"></div>
                <p class="validation_error_message COPY_BOLD" id="form_validate_error" <?php
					if (isset($_GET['error']) && ($_GET['error'] === "FORM ERROR")) {
						echo 'style="display:inherit;"';
					}
					?>><?php
			    	if (isset($_GET['errormessage'])) {
						echo $_GET['errormessage'];
					}
				?></p>
                <div class="clear"></div>
				<?php $_ProjectF -> createStepNav(2,'form_step_about_thebusiness'); ?>

			</div> <!-- main container -->
		</div> <!-- form step cont -->