<!-- STEP CONTAINER -->
		<div class="form_step_cont" id="form_start_form_cont">
			<div class="main_container" id="step_intro">
				<h1 class="TURQ_COPY">Entry Form</h1>
				<p>
					The selection criteria are based on the understanding that Endeavor's model caters only for scale-ups with particular characteristics as set out below:
					<br/><br/>
				<span class="COPY_BOLD">Your business should:</span>
				</p>
				<ul class="form_ul_list">
					<li>Have a minimum annual turnover of R10-million.</li>
					<li>Be founder led.</li>
					<li>Be a unique business that cannot easily be replicated. It should not be a "me too” business, such as a consultancy or agency.</li>
					<li>Have the potential to grow exponentially.</li>
					<li>Have successfully raised the capital it needed to get to this point.</li>
					<li>Be scaleable; in other words, the business should have the potential to grow and become a market leader with a business model that is repeatable in any country/region.</li>
				</ul>
				<p>
				<span class="COPY_BOLD">Please note the following:</span>
				</p>
				<ul class="form_ul_list">
					<li>All fields must be complete to submit your entry.</li>
					<li>You may save your entry form and edit it as many times as you wish by clicking on “<span class="COPY_BOLD">Save</span>”. Once you click on “<span class="COPY_BOLD">Submit</span>”, your entry form will be sent and you will not be able to edit it.</li>
				</ul>
				
				<button class="LEFT square_btn btn_orange" onclick="javascript:window.location='<?php echo $project_data['pages']['entry_form']; ?>?startform=1'">
					<p>Start</p>
				</button>
			</div> <!-- main container -->
		</div> <!-- form step cont -->