<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - RM Nominations";
$project_page['file_name'] = "update-rm-fnumbers.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
	
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['rm_login'].'');
	exit;
}
$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>UPDATE - RM F numbers</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
	<style>
	 a, a:link, a:visited { color:#009999; }
	</style>
</head>

<body id="client_invite_page">
    
	<div class="form_container client_list_wide_container">
    	<h1 class="page_h1 rm_list_h1 TURQ_COPY">RM F Numbers</h1>
        <p>Note: UPDATE RM FNUMBERS IN THE MIDDLE OF THE CAMPAIGN..</p>
        <?php
		
		$newFNUMBERS = $_Project_db->get("fnb_new_fnumbers",'fnumber');
		if (is_array($newFNUMBERS) && count($newFNUMBERS) >= 1) {
			$_match = false;
			$_updated = false;
			?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
          	<td class="status_head head_name">New RM info</td>
            <td class="status_head head_client_name">F num Match</td>
            <td class="status_head head_company">Double</td>
            <td class="status_head head_email">Email</td>
            <td class="status_head head_status">Used</td>
          </tr>
		  <?php
		  $bgStyle = "grey_tr";
			foreach($newFNUMBERS as $newF) {
				$_newFnumber = $newF['fnumber'];
				
				// Check if rm with same f number exists
				$_Project_db -> where('fnumber',$_newFnumber);
				$_existingRM = $_Project_db->get("fnb_fnumbers",'fnumber');
				if (count($_existingRM) >= 1) {
					$_existingRM = $_existingRM[0];
					$_match = true;
					$_updated = false;
				}else {
					$_existingRM = false;
					$_match = false;
					$_updated = false;
				}
				
				
				//****** */
				if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
				
				if ($_match === true) {
					$_double = false;

					$_Project_db -> where('fnumber',$_newFnumber);
					$_double = $_Project_db -> get("fnb_new_fnumbers",'fnumber');
					if (count($_double) >= 2) {
					}else {
						$_double = false;
					}
				?>
				<tr class="status_tr <?php echo $bgStyle; ?>">
                	<td><?php
                    if ($_match === true || $_double != false) {
						?><span class="TURQ_COPY"><strong><?php echo $newF['first_name']."<BR>".$newF['surname']."<BR></strong>F#: ".$newF['fnumber']."<BR>Email: ".$newF['email']; ?></span>
                        <?php
					}else {

						// insert into original table
						$_insertData = array(
							'fnumber' => $newF['fnumber'],
							'surname' => $newF['surname'],
							'first_name' => $newF['first_name'],
							'preferred_name' => $newF['preferred_name'],
							'email' => $newF['email']
						);
						//$_Project_db -> insert($_db_['table']['fnumbers'],$_insertData);


						?><span class="DARK_GREY_COPY"><strong><?php echo $newF['first_name']."<BR>".$newF['surname']."<BR></strong>F#: ".$newF['fnumber']."<BR>Email: ".$newF['email']; ?></span><?php
					}
					?></td>
					<td><?php
					if ($_match === true) {
						?><span class="TURQ_COPY"><strong><?php echo $_existingRM['first_name']."<BR>".$_existingRM['surname']."<BR></strong>F#: ".$_existingRM['fnumber']."<BR>Email: ".$_existingRM['email']; ?></span>
                        <?php
					}else {
						?><span class="DARK_GREY_COPY"><strong>NO MATCH</strong></span><?php
					}
					?></td>
					<td><?php
						if ($_double != false) {
							foreach($_double as $_d) {
								echo "<strong>DBL".$_d['id']."</strong><BR>";
							}
						}else {
							echo "no double";
							
						}
					?></td>
					<td><?php echo $newF['email']; ?></td>
					<td><?php echo "NA"; ?></td>
				</tr>

          		<?php
          		}
			}
		  ?>
          <tr class="status_footer">
          	<td></td>
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
		<?php
		}else {
		?>
        	<p>You have not yet nominated your clients. <a href="<?php echo $project_data['pages']['rm_client_invites']; ?>" class="BLACK_COPY">Click here</a> to nominate now.</p>
        <?php
		}
		?>
        
        
        <hr>
        
    </div> <!-- form container -->
	
    <div class="clear">&nbsp;<BR><BR>&nbsp;<BR><BR>&nbsp;<BR></div>

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>