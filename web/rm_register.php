<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Relationship Manager Register";
$project_page['file_name'] = "rm_register.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);
$today = $vandag." ".$tyd;
$closeDate = "2016-01-31 23:59";
if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
	header("Location: ".$project_data['full_address']);
}

// END OF CAMPAIGN
/*
session_write_close();
header('Location: '.$project_data['pages']['landing']);
exit;
*/
// END --


$image_folder = $project_data['full_address']."images/";
// REGISTER
if (isset($_POST['btn_register'])) {
	if (isset($_POST['rm_fnumber'])) {
		$rm_fnumber = $_POST['rm_fnumber'];
		if (isset($_POST['rm_email'])) {
			$rm_email = $_POST['rm_email'];
			if (isset($_POST['rm_name'])) {
				$rm_name = $_POST['rm_name'];
				if (isset($_POST['rm_region'])) {
					$rm_region = $_POST['rm_region'];
					$rm_result = $_ProjectF->RM_Register($rm_name,$rm_fnumber,$rm_email,$rm_region);
					if (isset($rm_result['error'])) {
						session_write_close();
						header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=".$rm_result['errormessage']);
						exit;
					}else {
						$_SESSION['rm_info']['name'] = $rm_result['name'];
						$_SESSION['rm_info']['fnumber'] = $rm_result['fnumber'];
						$_SESSION['rm_info']['fnumber_id'] = $rm_result['fnumber_id'];
						$_SESSION['rm_info']['email'] = $rm_result['email'];
						$_SESSION['rm_info']['region'] = $rm_result['region'];
						$_SESSION['rm_info']['id'] = $rm_result['id'];
						//session_write_close();
						header('Location: '.$project_data['pages']['rm_client_invites']);
						exit;
					}
				}else {
					session_write_close();
					header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please select a region.");
					exit;
				}
			}else {
				session_write_close();
				header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please enter your name and surname.");
				exit;
			}
		}else {
			session_write_close();
			header("Location: ".$project_data['pages']['rm_register']."?error=REGISTER ERROR&errormessage=*Please enter a valid FNB email address.");
			exit;
		}
	}else {
		session_write_close();
		header("Location : ".$project_data['pages']['rm_error']);
		exit;
	}
}else {
	if (isset($_SESSION['rm_info'])) {
		$_SESSION['rm_info'] = NULL;
	}
	$_SESSION = NULL;
	session_unset();
	session_destroy();
	session_write_close();
}
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fixed-positioning.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body class="rm_login_body">

<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>

	<img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_form_topLogo">
    <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img rm_menu_btn DESKTOP" id="nav_menu_btn_open">
    <div class="clear"></div>
    <div class="rm_socialbtns_cont DESKTOP">
        <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
        <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
        <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    </div>
    <div class="clear"></div>
    <?php
	// ADD RM navigation here
	include('_include/rm_navigation.php');
	?>
    
    
    
    
    
	<div class="form_container rm_login_container">
    	<h1 class="rm_login_h1 TURQ_COPY">Nominate your clients and<BR>
<span class="COPY_BOLD ORANGE_COPY">WIN</span> an all-expenses-paid<BR>
trip to Boston, USA</h1>
        <p>To nominate your clients, simply register/login and<BR>submit your clients' details.</p>
        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="form_rm_register" id="form_rm_register">
        	<!-- LAYER 1 -->
            <div class="l1_container">
            	<input type="text" name="rm_name" id="rm_name" maxlength="100" class="input_text full_text_width" placeholder="Your name and surname*" value="Your name and surname*">
                
				<input type="text" name="rm_fnumber" id="rm_fnumber" maxlength="100" class="input_text full_text_width" placeholder="Your employee number (without an F)*" value="Your employee number (without an F)*">
                
                <input type="text" name="rm_email" id="rm_email" maxlength="100" class="input_text full_text_width" placeholder="Your email address*" value="Your email address*">
                
                <select name="rm_region" id="rm_region" class="select_list full_text_width">
                        <option  value="0" >Your region*</option>
                        <option value="Eastern Cape" >Eastern Cape</option>
                        <option value="Free State" >Free State</option>
                        <option value="KwaZulu-Natal" >KwaZulu-Natal</option>
                        <option value="Gauteng" >Gauteng</option>
                        <option value="Limpopo" >Limpopo</option>
                        <option value="Mpumalanga" >Mpumalanga</option>
                        <option value="Northern Cape" >Northern Cape</option>
                        <option value="North West" >North West</option>
                        <option value="Western Cape" >Western Cape</option>
				</select>
            </div> <!-- l1 container -->
            <div class="clear"></div>
            <input type="hidden" name="btn_register" value="Register">
            <input type="button" name="btn_register" value="Register" id="btn_register" class="square_submit_btn rm_register_btn LEFT"/><p class="rm_login_reg_href"> | <a href="<?php echo $project_data['pages']['rm_login']; ?>" class="COPY_BOLD DARK_GREY_COPY">Login</a></p>
		</form>
        <div class="clear">&nbsp;</div>
        <p>Can't register? <a href="mailto:innovationawards@fnb.co.za?subject=Please help me register for the FNB Business Innovation Awards RM competition." class="BLACK_COPY COPY_BOLD">Click here</a>.</p>
        <div class="clear">&nbsp;</div>
        <p class="validation_error_message COPY_BOLD" id="form_validate_error"><?php
        	if (isset($_GET['errormessage'])) {
				echo $_GET['errormessage'];
			}
		?></p>
    </div> <!-- form container -->
	
    <div class="rm_footer_container">
		<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><span class="COPY_REGULAR_ITALIC" style="font-size:9px;">Terms and conditions apply.</span><BR><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
		<img src="images/web_right-bottom-copy-white.png" class="rm_swisslogo">
	</div> <!-- footer container -->
    
    
    
    
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	$('input:text').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('');
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).val($(this).attr('placeholder'))
            }
        }
    );


	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
		posFuckingFooter();
	};	
	// BUTTONS AND USER ACTIONS
	$('#close_form_btn').bind('mouseup touchend MozTouchRelease',function() {
		$('#user_form_container').hide(0,function() {
			$('#lightbox_mask').hide(0);
		});	
	});
	
	/* ----- */
	/* SUBMIT FUNCTION */
	var checkTextValues = new Array();
	var rm_email = "";
	$('#btn_register').bind('mouseup touchend MozTouchRelease',function() {
	//$("#myform").submit(function(e) {
		var error = new Array();
		
		// Text values
		checkTextValues['rm_name'] = $('#rm_name').val();
		checkTextValues['rm_fnumber'] = $('#rm_fnumber').val();
		checkTextValues['rm_region'] = $('#rm_region').val();
		rm_email = $('#rm_email').val();
		
		
		// Check Name
		checkTextValues['rm_name'] = $.trim(checkTextValues['rm_name']);
		if (frame_charCount(checkTextValues['rm_name'],2,100) && checkTextValues['rm_name'] != $('#rm_name').attr('placeholder')) {
			frame_markCheckedFields('rm_name');
		}else {
			frame_markErrorFields('rm_name');
			error.push('*Please enter your name and surname.');
		}
		// Check Fnumber
		checkTextValues['rm_fnumber'] = $.trim(checkTextValues['rm_fnumber']);
		if (frame_charCount(checkTextValues['rm_fnumber'],2,15) && checkTextValues['rm_fnumber'] != $('#rm_fnumber').attr('placeholder')) {
			frame_markCheckedFields('rm_fnumber');
		}else {
			frame_markErrorFields('rm_fnumber');
			error.push('*Please enter your employee number without an F.');
		}
		
		// Check Email address
		rm_email = $.trim(rm_email);
		if (frame_validateEmail(rm_email)) {
			if (rm_email.indexOf("fnb.co.za") > -1 || rm_email.indexOf("etiket.co.za") > -1) {
				frame_markCheckedFields('rm_email');
			}else {
				frame_markErrorFields('rm_email');
				error.push('*Please enter a valid FNB email address.');
			}
		}else {
			frame_markErrorFields('rm_email');
			error.push('*Please enter a valid FNB email address.');
		}
		
		// Check Region
		checkTextValues['rm_region'] = $.trim(checkTextValues['rm_region']);
		if (frame_charCount(checkTextValues['rm_region'],2,15) && checkTextValues['rm_region'] != "0" && checkTextValues['rm_region'] != 0) {
			frame_markCheckedFields('rm_region');
		}else {
			frame_markErrorFields('rm_region');
			error.push('*Please select a region.');
		}
		
		var errorMessage = "";
		if (error.length >= 1) {
			for (var i = 0; i < error.length; i++) {
				errorMessage += error[i]+"<BR>";
			}
			$('#form_validate_error').html(errorMessage);
		}else {
			$("form#form_rm_register").submit();
		}
	});
	
	/* useless footer copy positioning */
	var resizeTimer = null;
	$(window).bind('resize', function() {
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(posFuckingFooter, 100);
	});
	function posFuckingFooter() {
		var mywindowHeight = window.innerHeight;
		if (mywindowHeight <= 600) { mywindowHeight = 600; }
		var leftPos = Math.ceil(mywindowHeight * 0.098039);
		$('#magic_footer_pos').css('padding-left',leftPos+"px");
	}
	</script>
</body>

</html>