<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "RM terms and conditions";
$project_page['file_name'] = "rm-terms.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

$_addToURL = "";
$image_folder = $project_data['full_address']."images/";


?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Terms and conditions</title>

	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>

	<link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/terms-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <style>

    	
    </style>
</head>
<body>
<div class="main_container terms_cont">
  <h1 class="page_h1 TURQ_COPY">TERMS AND CONDITIONS</h1>
  <p class="TURQ_COPY COPY_BOLD"><span style="text-transform:uppercase;">COMPETITION RULES:</span></p>
	<p class="COPY_BOLD">
Date these rules were first published: 12 September 2016<br/>
Date these rules were last changed:  12 September 2016
	</p>
	<p>
Read these competition rules carefully. These competition rules (“rules”) explain your rights and duties in connection with this competition. If you take part in this competition and/or accept any prize, these rules will apply to you and you agree that the promoter(s) can assume that you have read and agreed to be legally bound by these competition rules.
	</p>

	<table class="terms_main_table" cellpadding="0" cellspacing="0">
		<tr>
			<td class="col1 header_row"><p>Competition name:</p></td>
			<td class="col2 header_row"><p><strong>FNB Business Innovation Awards 2017</strong></p></td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td><p>Promoter(s) name(s):</p></td>
			<td><p>This competition is run by FNB Business, a segment of First National Bank, a division of FirstRand Bank Limited, Reg. No. 1929/001225/06 (“FNB”)
in partnership with Endeavor Entrepreneurship Institute NPC, Reg. No. 2004/014973/08 (“Endeavor SA”).
<br><br>
In these rules we refer to the above promoter(s) as “the promoter(s)”, 
or “us” or “we”. We will refer to participants and winners as “you”. </p></td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Entries open:</p>
			</td>
			<td>
				<p>Entries open on Monday 12 September 2016 at 08h00.</p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Entries close: </p>
			</td>
			<td>
				<p>Entries close on Tuesday 31 January 2017 at 23h59. No submissions will be accepted after these deadlines.
<br><br>
All fully completed entries must be received by the promoter(s) before the closing time. 
<br><br>
The promoter(s) reserve the right to extend the competition. Notice of this will be posted on <a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a></p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Prize details:</p>
			</td>
			<td>
				<p>The FNB Business Relationship Manager whose nominated client wins the FNB Business Innovation Awards 2017 will win the chance to accompany their client on an all-expenses-paid trip to New York to attend the Endeavor International Selection Panel (ISP).
<br><br>
Details of what the all-expenses-paid trip will entail will be provided to the winner of the competition and FNB reserves the right to determine in its sole discretion what trip expenses will be included in the prize.</p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Winners announced in:</p>
			</td>
			<td>
				<p>Winner will be announced in June 2017 or as soon as possible thereafter.</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Eligibility: who qualiﬁes to take part? </p>
			</td>
			<td>
				<p>This competition is open to FNB Business Relationship Managers of FNB Business. Please note that only FNB Business Relationship Managers 
who have portfolios of clients to whom they sell products may enter 
this competition. Entrants who do not meet the eligibility criteria will 
be disqualified. 
<br><br>
<strong>
Note: Prizes will only be awarded to employees of FNB Business who are still employed by FNB Business at the time the prizes are announced and paid or redeemed.</strong></p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Important conditions – read carefully:</p>
			</td>
			<td>
				<ol type="1">
					<li>Before the main prize winner will be awarded a prize, he/she must sign a contract with Endeavor SA and must agree to Endeavor SA’s confidentiality and other terms &amp; conditions. Winners have the right to decline to do so. If they decline to do so, they will forfeit (give up) their participation in the ISP.</li>
					<li>Winners must abide by the rules, terms and conditions which are determined by any third party carrier, transporter, accommodation provider, catering provider or other service provider associated with the prize. </li>
				</ol>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How to enter? </p>
			</td>
			<td>
				<p>You must nominate your client/s to enter the FNB Business Innovation 
Awards 2017. You have to submit your nomination online at 
<a href="http://www.rmnominations.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.rmnominations.co.za</strong></a>, using the online forms and templates we make available. You must submit all the required information on time. Once you have supplied your nomination, your client/s will receive a confirmation email notifying them of the nomination and supplying them with a link to the entry form. 
<br><br>
<strong>
Note: Your client/s will need to complete and submit the entry 
form to secure your entry into the completion. Please note you may nominate more than 1 client.
</strong>
<br><br>
The client/s you nominate must meet the following criteria based on 
Endeavor SA’s model:
</p>
				<ul>
					<li>Have a minimum annual turnover of R10 000 000 (ten million rand).</li>
					<li>Be founder led.</li>
					<li>Be a unique business that cannot easily be replicated. It should not be a “me-too business” such as a consultancy or agency.</li>
					<li>Have the potential to grow exponentially.</li>
					<li>Have successfully raised the capital it needed to get to this point.</li>
					<li>Be scalable, in other words, the business should have potential to grow and become a market leader with a business model that is repeatable in any country/region.</li>
				</ul>
			</td>
		</tr>
	
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How will winner(s) 
be chosen?</p>
			</td>
			<td>
				<p>Please refer to the judging criteria for the FNB Business Innovation Awards set out on <a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a>
<br><br>
<strong>
Note: The judges’ decision is final and no correspondence will be entered into. This means you cannot appeal any decision by the judges.</strong>
</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How will winner(s) names be announced?</p>
			</td>
			<td>
				<p>Winners will be contacted by phone or email, and will be announced on <a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a> and at the 2017 FNB Business Innovation Awards gala event.
<br><br>
<strong>
Note: While finalists and/or prize winners may be asked to take part in publicity for the competition, they have the right to refuse to do so.</strong></p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Deadline for 
claiming prize(s):</p>
			</td>
			<td>
				<p>Within 6 (six) months of announcement of the winner. It is also dependent upon the FNB Business Relationship Manager’s client accepting the opportunity to attend the ISP in New York</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Questions about 
these rules:</p>
			</td>
			<td>
				<p>You can forward your questions/queries to the following email address: <a href="mailto:innovationawards@fnb.co.za" style="color:#000000; text-decoration:underline;"><strong>innovationawards@fnb.co.za</strong></a></p>
			</td>
		</tr>
		
		<!-- ROW -------------------
		<tr>
			<td>
				<p></p>
			</td>
			<td>
				<p></p>
			</td>
		</tr> -->		
		<!-- tr>td*2>p -->
	</table>

	<p class="TURQ_COPY"><span style="text-transform:uppercase;"><strong>General rules that apply:</strong></span></p>

	<div class="border_cont">
		<p><strong>Important:</strong></p>
		<ul>
			<li>You agree to indemnify the promoter(s) fully for any loss or damage the promoter(s) may suffer 
      because you breached the competition rules. This means you agree to reimburse the promoter(s) for 
      the following: any loss or damage they suffer, any expenses and costs they paid or are responsible for.
      Legal costs means costs on an attorney and own client scale.</li>
			<li>You also agree to indemnify the promoter(s) for any loss or damage you suffered because you took 
      part in this competition or used the prize. If you enter yourself, or use or accept the prize, you 
      understand that you do so of your own free will. This means that you cannot hold the promoter(s) 
      legally responsible for any loss or damage or legal expenses you suffered because you took part in this 
      competition or used the prize. </li>
			<li>You will protect the promoter(s) from being held legally responsible for the loss or damage or legal 
      expenses of another person (legal or natural) if such loss or damage or expense was incurred because 
       you: a) breached the competition rules b) took part in this competition or c) you or such person used 
      a prize. </li>
		</ul>
	</div>

	<ul>
		<li>If the prize involves international travel you must have a valid passport and obtain the necessary VISAs. The 
      promoter(s) are not responsible if you cannot arrange this on time.</li>
		<li>Unless the promoter(s) say otherwise the advertised dates and times of prizes involving travel or tickets to 
      events cannot change. Unless these rules specifically say otherwise the prize will not include the cost of 
       transport, accommodation, spending money or food. This means you will be responsible for organizing and 
      paying for your own transport to the venue, accommodation, food and spending money. Unless the 
      promoter(s) agree otherwise the winner must use the prize.</li>
		<li> Tickets must be collected before the deadline.  Proof of identity may be required. You must be able to 
      provide proof of your identity if required.</li>
		<li>You are responsible for arranging your own travel and medical insurance.</li>
		<li>You must comply with the rules of any third party service provider. This includes but is not limited to, any airline, transport service, accommodation provider and venue.</li>
		<li>If the promoter(s) are not able to get hold of you after making reasonable efforts to do so, or you do not 
       claim your prize on time, you will lose your prize and the judges may award it to someone else. </li>
		<li>If you fail to comply with any part of these rules you will be disqualified and you will forfeit any prize(s).</li>
		<li>Unless we say otherwise you must be at least 18 (eighteen) to enter.</li>
		<li>Unless we say otherwise entry is restricted to 1 (one) entry per person and multiple entries will be disqualified.</li>
		<li> Automated or bulk entries or votes will be disqualified.</li>
		<li> The prizes may not be sold or given to someone else.</li>
		<li> The prizes cannot be swapped for cash or a different prize.</li>
		<li> You are responsible for the tax associated with using or accepting any prize.</li>
		<li> You may not attempt to do anything to change the outcome of the competition in any way.</li>
		<li> The promoter(s) have the right to end this competition at any time. If this happens you agree to waive
       (give up) any rights that you may have about this competition and agree that you will have no rights against
        the promoter(s).</li>
		<li> The promoter(s) reserve the right to change the rules of the competition. The promoter(s) can change the 
       rules of the competition throughout the duration of the competition.  For convenience only, the date on 
        which these rules were last amended will be shown below the heading. It is your responsibility to check the 
       rules for amendments.</li>
		<li> The clauses in these rules are severable. This means that if any clause in these rules is found to be unlawful, 
       it will be removed and the remaining clauses will still apply.</li>
		<li> Where any dates or times need to be calculated in terms of this agreement, the international standard time: 
       GMT plus two hours will be used.</li>
		<li> While the promoter(s) may allow you extra time to comply with your obligations or decide not to exercise 
       some or all of our rights, or waive certain requirements, the promoter(s) can still insist on the strict 
       application of any or all of its rights at a later stage. You must not assume that this means that the rules have   
       been changed or that it no longer applies to you.</li>
		<li> You must send all legal notices to FNB Legal, 3rd Floor, No 1 First Place, Bank City, Johannesburg, 2001.</li>
		<li> This competition and its rules will be governed by the law of the Republic of South Africa regardless of    
     where you live or work, or where or how you enter. </li>
	</ul>

	<ul style="color:#ffffff">
		<li>&nbsp;</li>
		<li>&nbsp;</li>
	</ul>
</div>

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont">
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_in_white.svg" class="in_share_btn DESKTOP"/>
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_twitter_white.svg" class="twitter_share_site DESKTOP">
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_fb_white.svg" class="fb_share_thispage DESKTOP">
</div> <!-- topRightLogo WHITE -->
<?php
// ADD RM navigation here
include('_include/rm_navigation.php');
?>


    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='<?php echo $project_data['full_address']; ?>js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/frame_functions.js"></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/rm-functions.js"></script>
<script>
	//$(document).ready(function (){
	window.onload = function() {
		//window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide();
	};
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

</script>
</body>
</html>