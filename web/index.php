<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "FNB Business INNOVATION Awards 2017";
$project_page['file_name'] = "index.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);


$image_folder = $project_data['full_address']."images/";
$today = $vandag." ".$tyd;
$closeDate = "2017-01-31 23:59";
$finallyDate = "2017-06-9 23:00";

// rsvp the invite
include('_include/rm_rsvp_invite.php');
?><!DOCTYPE html>
<html>
<head id="www-fnbbusinessinnovationawards-co.za" class="no-skrollr">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<META HTTP-EQUIV="Expires" CONTENT="-1" />

<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->
    <meta name="HandheldFriendly" content="true" />    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="800"/>
    

	<!-- CSS
  	<!-- Important Owl stylesheet -->
	<link rel="stylesheet" href="css/owl-carousel/owl.carousel.css" />
    <!-- Default Theme -->
    <link rel="stylesheet" href="css/owl-carousel/owl.theme.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/fixed-positioning.css" rel="stylesheet" type="text/css" />
    
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    
    
    <!-- MAJOR META TAGS -->
    <link rel="canonical" href="http://www.fnbbusinessinnovationawards.co.za" />
    <meta name="description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="The FNB Business Innovation Awards" />
    <meta property="og:site_name" content="www-fnbbusinessinnovationawards-co.za" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $project_data['full_address']; ?>" />
    <meta property="og:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png" />
    <meta property="fb:admins" content="670010811"/>
    
    <!-- Modify the project_data in a headscript to customise each page -->
    <meta property="og:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="keywords" content="FNB, Business, Innovation Awards" />
    
    
    
    <!-- TWITTER META TAGS -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="FNB BUSINESS INNOVATION AWARDS." />
    <meta name="twitter:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="twitter:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png">
    
    <!-- REL -->
    <link rel="publisher" href="https://plus.google.com/116427055070017697691/about" />

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.20.0/polyfill.min.js"></script>
    <script>
		var windowWidth = window.innerWidth;
		var windowHeight = window.innerHeight;
		
		if (windowWidth < 1000) {
	(function (a, b) { if (/Mobi/.test(a)) window.location = b }
)(navigator.userAgent || navigator.vendor || window.opera, '<?php echo $project_data['mobi_full_address']; ?>');
		}
	</script>
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>
<?php
	// INCLUDE FNB floodlight tag
	include('_include/floodlight_tag-home.php');
?>
<?php
	// INCLUDE VerSaDUHDUH tag
	include('_include/versaduhTag.php');
?>
    
    <!-- LANDING PAGE 1 -->
	<div class="std_page page_1_container DESKTOP" id="PAGE_1" data-0="top:0%;" data-1000="top:0%;">
     
        <p id="pAGEx" style="font-size:30px; color:#FFFFFF; position:relative; left:50%;">al;sdjkfl;akjsdl;fkjasd</p>
        <img src="images/faceanim/face2-001.jpg" class="face-anim-face" id="faceImg" />
        <img src="images/faceanim/parra-layer-dots-1.png" class="face-anim-parra dots1" id="parraLayerDots1" />
        <img src="images/faceanim/parra-layer-lines-1.png" class="face-anim-parra lines1" id="parraLayerLines1" />
        <img src="images/faceanim/parra-layer-lines-2.png" class="face-anim-parra lines2" id="parraLayerLines2" />

    </div>
    
    <!-- mobile fix -->
    <div class="mobi_page_1_container MOBILE">
    	<div class="main_container mobi_page_1">
        	<p class="COPY_BOLD TURQ_COPY"><a href="<?php echo $project_data['mobi_full_address']; ?>">CLICK HERE </a>to visit the mobile friendly page.</p>
        </div>
    </div> <!-- MOBI PAGE 1 CONTAINER -->
    
    <!-- 2016 WINNER VIDEO PAGE -->
    <div class="std_page page_1b_container DESKTOP" id="PAGE_2016winnervideo" data-0="top:100%;" data-1000="top:0%;">
        <div class="winner-holder">
            <div class="video-holder"><video poster="images/poster.jpg" id="bgvid" class="winner_video" controls>
                <source src="assets/wiGroup.mp4" type="video/mp4">
                <source src="assets/wiGroup.webm" type="video/webm">
                <source src="assets/wiGroup.ogv" type="video/ogv">
            </video></div>
            <!-- <img src="images/landing_winner.png" class="landing_9june"> -->
            <img src="images/bevan.png" class="landing_9june">
            <img src="<?php echo $image_folder; ?>web_01_video_playbtn_watch.png" class="video_play_btn" id="btn_video_play">
        </div>
    </div>

    
    <!-- PAGE: ABOUT THE AWARDS -->
    <div class="std_page page_2_container BG_TURQ" id="PAGE_2" data-menu-offset="0"" data-1000="top:100%;" data-1500="top:100%;" data-2500="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_2"></div>
    	<div class="main_container" id="PAGE_2_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 WHITE_COPY">About the awards</h1>
            <p class="WHITE_COPY"><span class="COPY_BOLD">Innovation is a theme that remains central to FNB’s business philosophy</span>. We believe that innovation is a key driver of business growth and scaleability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised.</p>
			<div class="quote_container">
            	<img src="<?php echo $image_folder; ?>web_03_quote-turq-left.png" class="quote_img_left">
                <p class="quote_p WHITE_COPY">The <strong><i>FNB Business INNOVATION Awards</i></strong>, supported by Endeavor, are a real opportunity for high-impact entrepreneurs to unleash their potential. Endeavor’s extensive experience in working with high-impact businesses is perfectly complemented by FNB’s commitment to innovation. This is a necessary platform for every business that has an ambition to become a successful global business. We challenge established businesses to enter the awards to inspire them to think bigger.
                <img src="<?php echo $image_folder; ?>web_03_quote-turq-right.png" class="quote_img_right"></p>
                <p class="quote_author WHITE_COPY" title="Paul Harris"><span class="COPY_BOLD">Paul Harris</span><BR><span>Endeavor Board Member</span></p>
                
            </div> <!-- quote container -->
			
            <div>	
                <hr class="accordian_drop_hr WHITE_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_turq.png" class="accordian_drop_btn" id="btn_acc_drop_p2" myAccordian="acc_ex_2" myAltImg="web_accordian_btn_turq_alt.png">
            </div>
            <div class="accordian_ex_container" id="acc_ex_2" myHeight="0">
            	<p class="WHITE_COPY"><span class="COPY_BOLD">FNB Business, supported by Endeavor SA, is proud to announce that entries for the <strong>FNB Business INNOVATION Awards</strong> 2017 are now open.</span></p>

<p class="WHITE_COPY">The <strong>FNB Business INNOVATION Awards 2017</strong> aim to recognise and celebrate the efforts of high-impact entrepreneurs who have embraced many challenges to create <strong>world-class, innovative and scaleable companies</strong> that continue to forge unique ways of doing business.</p>
<p class="WHITE_COPY">High-growth, founder-led entrepreneurs will be selected based on their potential to become part of Endeavor's international network of innovators, business leaders, entrepreneurs, investors and academics; and become the <strong>FNB Business Innovator of the Year 2017. </p>
            </div> <!-- accordian ex container -->
        </div> <!-- PAGE 2 main container -->
    </div> <!-- PAGE 2 -->
    
    
    
    
    <!-- PAGE: THE PRIZE -->
    <div class="std_page page_3_container BG_GREY" id="PAGE_3" data-menu-offset="0" data-2500="top:100%;" data-3000="top:100%;" data-4000="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_3"></div>
    	<div class="main_container" id="PAGE_3_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">What the FNB business<BR>innovator of the year will gain</h1>
            <p class="BLACK_COPY">The <span class="COPY_BOLD">prize</span> for the <span class="COPY_BOLD">FNB Business INNOVATION Awards 2017</span> is <span class="COPY_BOLD">an all-expenses-paid trip to attend the prestigious Endeavor International Selection Panel (ISP)</span> in New York. <span class="COPY_BOLD">The FNB Business Innovator of the Year</span> will have the opportunity to pitch their business to a credible panel of global business leaders, meet top investors and rub shoulders with their peers from around the world. Pending feedback from the panel about their business and capability as a global entrepreneur, the winner could walk away with the international recognition and prestigious title of an <span class="COPY_BOLD">Endeavor Entrepreneur</span>.
            <br/><br/>
Finalists will also have the opportunity to attend the ISP at their own cost.</p>
            <div>	
                <hr class="accordian_drop_hr TURQ_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_white.png" class="accordian_drop_btn" id="btn_acc_drop_p3" myAccordian="acc_ex_3" myAltImg="web_accordian_btn_white_alt.png">
            </div>
            
            <div class="accordian_ex_container" id="acc_ex_3" myHeight="0">
            <p class="BLACK_COPY COPY_REGULAR">What is the process for becoming an <strong>Endeavor Entrepreneur</strong>?</p>
            	<div class="DESKTOP">
                    <div id="owl-demo" class="p6_slider">
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-01.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-02.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-03.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-04.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-05.png" alt="Owl Image"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>slider-06.png" alt="Owl Image"></div>
                    </div>
                </div>
                        
                <div class="MOBILE">
                    <div id="mobi_owl-demo" class="p6_slider">
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-01.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-02.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-03.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-04.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-05.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                      <div class="item"><img src="<?php echo $image_folder; ?>mobi_slider-06.png" alt="Owl Image" class="FULL_WIDTH_PX"></div>
                    </div>
                </div>

            	<p class="BLACK_COPY">
                The ISP selection process is akin to a two-day immersion course in business practice. The insight and wisdom that panellists bring to the process never fail to inspire candidates and observers alike. Whether they are chosen or not, entrepreneurs describe the selection process as a life-changing experience that provides them with the tools to take their businesses to the next level.</p>

				<p class="BLACK_COPY">The process takes place over two days. During Interview Day, candidates “pitch” their businesses to a panel; and the panel responds with a series of probing questions relating to finances, talent management, corporate strategy, general business administration, marketing, etc. Once the panel has sufficient information about the candidate and the business, it is able, on Deliberation Day, to consider the suitability of the candidate. </p>

				<p class="BLACK_COPY">The questions panellists will want to answer are: is this business truly innovative; is it scaleable; can it create a large number of jobs; will it contribute to the economic growth of the country; and is the entrepreneur a role model who will help to sustain the entrepreneurial ecosystem in the future?
				</p>
                <div class="clear"></div>
                <div style="position:relative;"></div>
            </div> <!-- accordian ex container -->
            <div class="clear"></div>
        </div> <!-- PAGE 3 main container -->
    </div> <!-- PAGE 3 -->
    
    
    
    <!-- PAGE: SELECTION CRITERIA -->
    <div class="std_page page_4_container BG_WHITE" id="PAGE_4" data-menu-offset="0" data-4000="top:100%;" data-4500="top:100%;" data-5500="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_4"></div>
    	<div class="main_container" id="PAGE_4_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">Selection criteria</h1>
            <p class="BLACK_COPY">The selection criteria for the <span class="COPY_BOLD">FNB Business INNOVATION Awards</span> are based on Endeavor’s entrepreneur selection model, which caters for scale-ups. Below are the set criteria.</p>
            <div class="HALF_WIDTH_PX p4_col_container p4_col_left_container">
            	<h2>Business criteria</h2>
                <div class="p4_col_ex_left" id="p4_col_ex_left" myHeight="0">
                    <div class="p4_col_content">
                        <ul type="disc">
                            <li>It has a minimum annual turnover of R10-million.</li>
                            <li>It is founder led. </li>
                            <li>It is a unique business. The entrepreneur owns 
the Intellectual Property for the products, model 
or service of the business. It cannot easily be 
replicated. It is not a &ldquo;me-too business&rdquo;, such as a 
consultancy or agency.</li>
                            <li>It has the potential to grow exponentially.</li>
                            <li>It has successfully raised the capital it needed to 
get to this point.</li>
                            <li>It is scaleable, in other words the business 
   has the potential to grow and become a 
   market leader.</li> 
                            <li>It has a business model that is repeatable in any 
country/region.</li>
                        </ul>
                    </div> <!-- p4 col ex left -->
                </div> <!-- p4 col ex left -->
                <div class="p4_col_ex_btn_container">
                	<img src="<?php echo $image_folder; ?>web_04_col1-btn.png" class="btn_p4_col_left FULL_WIDTH" id="btn_p4_col_left" myAccordian="p4_col_ex_left" myAltImg="web_04_col1-btn_alt.png">
                </div> <!-- p4 col ex btn container -->
            </div> <!-- p4 col left container -->
            
            <div class="HALF_WIDTH_PX p4_col_container p4_col_right_container">
            	<h2>Business owner criteria</h2>
                <div class="p4_col_ex_right" id="p4_col_ex_right" myHeight="0">
                    <div class="p4_col_content">
                        <p class="WHITE_COPY p4_list_first">He or she:</p>
                        <ul type="disc">
                            <li>Is ambitious and wants the business to become a market leader.</li>
                            <li>Embodies the qualities of a local and global role model.</li>
                            <li>Is open to feedback and mentorship.</li>
                            <li>Is humble and committed to paying it forward.</li>
                            <li>Is mature enough to appreciate the power of Endeavor’s global network.</li>
                        </ul>
                    </div> <!-- p4_col_content -->
                </div> <!-- p4 col ex right -->
                <div class="p4_col_ex_btn_container">
                	<img src="<?php echo $image_folder; ?>web_04_col2-btn.png" class="btn_p4_col_right FULL_WIDTH" id="btn_p4_col_right" myAccordian="p4_col_ex_right" myAltImg="web_04_col2-btn_alt.png">
                </div> <!-- p4 col ex btn container -->
            </div> <!-- p4 col left container -->
            
            <div class="clear form_bottom_margin"></div>
            
            <p class="BLACK_COPY">Facilitated by Endeavor South Africa, the selection criteria of the awards focus on businesses that are able to meet the following standards:</p>
            
            
            <!-- P4 3 COLUMN CONTAINERS -->
            <div class="p4_3col_container">
                <div class="p4_3col p4_3col_col1">
                    <h2 class="TURQ_COPY">Development impact:</h2>
                    <p>Exhibit high-growth potential and the capacity to add substantial economic value by creating a number of high-value jobs.</p>
                </div> <!-- p4_3col_col1 -->
                
                <div class="p4_3col_col2">
                    <h2 class="TURQ_COPY">Business innovation:</h2>
                    <p>Demonstrate real innovation that has the potential to change the way an industry operates locally and/or internationally.</p>
                </div> <!-- p4_3col_col1 -->
                
                <div class="p4_3col p4_3col_col3">
                    <h2 class="TURQ_COPY">Fit with FNB and Endeavor:</h2>
                    <p>Be interested in accepting advice and support from FNB and Endeavor, as well as contributing to the Endeavor network.</p>
                </div> <!-- p4_3col_col1 -->
                <div class="clear"></div>
            </div> <!-- p4 3col container -->
            
            <div class="clear"></div>
            
            <!-- <p class="BLACK_COPY">Factors such as brand and reputation, stakeholder relations and goodwill, environmental sustainability, social responsibility and quality of governance are all taken into account.</p> -->

        </div> <!-- PAGE 4 main container -->
    </div> <!-- PAGE 4 -->
    
    
    
    <!-- PAGE: THE PROCESS -->
    <div class="std_page page_5_container BG_GREY" id="PAGE_5"  data-6000="top:100%;" data-7000="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_5"></div>
    	<div class="main_container" id="PAGE_5_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">The Process</h1>
            <?php
			if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
			?>
            <p class="BLACK_COPY"><strong>Entries are now closed.</strong> The inaugural winner will be announced in June 2016. Below is the process that will be followed, which starts with the stringent Endeavor Entrepreneur selection process.</p>
			<?php
			}else {
			?>
            <p class="BLACK_COPY">Entries are now open and will close on <strong>31 January 2017.</strong> The winner will be announced in June 2017. Below is the process that will be followed in selecting the <strong>FNB Business Innovator of the Year 2017</strong>, who will then have an opportunity to attend the Endeavor ISP.</p>
			<?php
			}
			?>
            <div>	
                <hr class="accordian_drop_hr TURQ_COPY"/>
            </div>
            <img src="<?php echo $image_folder; ?>the_process_main_img.png" class="FULL_WIDTH">
            <h1 class="page_h1 page_bottom_h1 TURQ_COPY">FNB Business innovation awards process</h1>
            <p class="COPY_ITALIC process_disclaimer">Note that should you enter the <strong>FNB Business INNOVATION Awards</strong>, you will need to make yourself available for all the stages of the process, including the Gala Award Ceremony.</p>
        </div> <!-- PAGE 5 main container -->
    </div> <!-- PAGE 5 -->
    
    
    
    
    
    
   <!-- ABOUT ENDEAVOR -->
    <div class="std_page page_7_container BG_WHITE" id="PAGE_6" data-7500="top:100%;" data-8500="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_6"></div>
    	<div class="main_container" id="PAGE_6_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">About endeavor</h1>
            <img src="<?php echo $image_folder; ?>web_07_endeavor-logo.png" class="p7_logo" />
			<p class="BLACK_COPY">Endeavor South Africa is the leading supporter of high-impact entrepreneurs around the world. By high-impact they mean individuals with the biggest dreams, the greatest potential to create companies that matter and grow, and the highest likelihood to inspire others. They are the only organisation to focus on the scale-up, not the start-up, because that is where Endeavor believes the highest job and wealth creation happen. 
			</p>
            <div>	
                <hr class="accordian_drop_hr TURQ_COPY"/>
                <img src="<?php echo $image_folder; ?>web_accordian_btn_white.png" class="accordian_drop_btn" id="btn_acc_drop_p7" myAccordian="acc_ex_7" myAltImg="web_accordian_btn_white_alt.png">
            </div>
            <div class="accordian_ex_container" id="acc_ex_7" myHeight="0">
            	<p class="BLACK_COPY">
            FNB partnered with Endeavor as they are leading the global movement to catalyse long-term economic growth by selecting, mentoring and accelerating the best high-impact entrepreneurs around the world. Once selected, <strong>Endeavor Entrepreneurs</strong> are provided with customised support from a volunteer network of 2 700+ global and local business leaders who serve as mentors, advisors, connectors, investors and role models. </p>

				<p class="BLACK_COPY">Endeavor provides its Entrepreneurs with access to funding, markets, talent and a support system with the purpose of helping them to:</p>
				<p class="COPY_REGULAR BLACK_COPY">THINK BIGGER</p>
                
                <ul type="disc">
					<li>Through the selection process.</li>
					<li>By inspiration and example of a global network of business leaders.</li>
					<li>By peer pressure (role modelling).</li>
					<li>Custom Advisory Board.</li>
                </ul>
                <p class="BLACK_COPY">For more information on Endeavor, visit <a href="http://www.endeavor.co.za" target="_blank" class="BLACK_COPY">www.endeavor.co.za</a></p> 
            </div> <!-- accordian ex container -->
        </div> <!-- PAGE 6 main container -->
    </div> <!-- PAGE 6 -->
    
    


    
    <!-- ALUMNI -->
    <div class="std_page page_6_container BG_WHITE" id="PAGE_7" data-9000="top:100%;" data-10000="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_7"></div>
        <div class="main_container" id="PAGE_7_CONTENT" myHeight="0" myTop="0">
        	<h1 class="page_h1 TURQ_COPY">Alumni</h1>
            <p>The <span class="COPY_BOLD">FNB Business INNOVATION Awards</span> has been built on the strength of its previous judges, finalists and winners, as well as the ongoing support that is provided by both FNB Business and the Endeavor mentors. This ensures that winners and finalists remain a part of the awards, benefiting from the mentorship for years to come.</p>
            <div class="alumni_colour_cont">
                <div class="alumni_nav_cont" id="alumni_btn_2015">
                    <p>2015</p>
                </div> <!-- alumni nav cont 1 -->
                <div class="alumni_nav_cont" id="alumni_btn_2016">
                    <p>2016</p>
                </div> <!-- alumni nav cont 2 -->
            </div> <!-- alumni colour cont -->
        </div> <!-- ALUMNI MAIN CONT -->

        <!-- ALUMNI 2015 MODAL -->
        <div class="alumni_sliders_modal_cont" id="alumni_2015_modal" myHeight="0" myTop="0">
            <div class="archive_colour_cont">
                <div class="archive_nav_cont">
                    <p>2015 judges</p>
                </div> <!-- archive nav cont 1 -->
                <div class="archive_nav_cont">
                    <p>2015 finalists</p>
                </div> <!-- archive nav cont 2 -->
                <div class="archive_nav_cont">
                    <p>2015 winner</p>
                </div> <!-- archive nav cont 3 -->
            </div> <!-- archive colour cont -->
            <!-- read more buttons -->
            <div class="archive_nav_cont archive_arrow_rm rm1_open" mySlider="judges_slider_container" id="rm1_btn"></div>
            <div class="archive_nav_cont archive_arrow_rm rm_closed" mySlider="finalists_slider_container" id="rm2_btn"></div>
            <div class="archive_nav_cont archive_arrow_rm rm_closed" mySlider="winner_container" id="rm3_btn"></div>
            <div class="clear"></div>
            <!-- Judges slider -->
            <div id="judges_slider_container">
                <div class="DESKTOP judge_desktop_slider">
                    <div id="judges_slider" class="p6_slider">
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_cynthia.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">CYNTHIA MKHOMBO</h2> -->
                            <!-- <p class="BLACK_COPY">Cynthia is an <span class="COPY_BOLD">Endeavor Entrepreneur, founder and CEO of 
    Masana</span>, and "one of the most inﬂuential women in business
    and professional services" according to <span class="COPY_ITALIC">CEO Magazine</span>.</p> -->
    <p class="BLACK_COPY"><strong>CYNTHIA MKHOMBO</strong><br><br>Cynthia is the <strong>founder and current CEO of Masana Hygiene Services</strong>. In 2010, <span class="COPY_REGULAR_ITALIC">CEO Magazine</span> called her "one of the most influential women in business and professional services".</p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_hlumelo.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">HLUMELO BIKO</h2> -->
                            <p class="BLACK_COPY"<strong>HLUMELO BIKO</strong><br><br>Hlumelo, <strong>Endeavor board member, executive chairman and co-founder of Spinnaker</strong>, has been involved in venture capital and private equity for 12 years and is currently engaged with several companies focusing on introducing low-cost, high-quality offerings in market segments currently dominated by oligopolies.</p>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_michael.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Michael Vacy-Lyle</h2> -->
                            <p class="BLACK_COPY"><strong>MICHAEL VACY-LYLE</strong><br><br>Michael is <span class="COPY_BOLD">CEO of FNB Business</span>. His vision for FNB Business is to become the number one transactional bank for businesses in South Africa. He believes in an entrepreneurial culture and can-do attitude in everything the bank does.</p>
                        </div>
                        
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_catherine.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Catherine Townshend</h2> -->
                            <p class="BLACK_COPY"><strong>CATHERINE TOWNSHEND</strong><br><br><strong>Joining Endeavor SA in 2012</strong>, Catherine has assisted some of the country’s most innovative entrepreneurs realise their global growth aspirations. </p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_clair.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Claire Busetti</h2> -->
                            <p class="BLACK_COPY"><strong>CLAIRE BUSETTI</strong><br><br>Claire is a <span class="COPY_BOLD">council member of the National Advisory Council on Innovation</span>, a member of FirstRand’s Vumela Enterprise Development Fund’s investment committee and a member of the Gauteng Enterprise Development Board, among others. </p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>judge_howard.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Howard Arrand</h2> -->
                            <p class="BLACK_COPY"><strong>HOWARD ARRAND</strong><br><br>Howard is the <strong>Provincial Head for FNB Business in KwaZulu-Natal</strong>. He has previously participated as an ISP judge in the Endeavor process, and also served as a judge/convenor of judges for the EY World Entrepreneur Awards.</p>
                        </div>
                    </div> <!-- judges_slider -->
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="judge_arrow_left">
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="judge_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->
            
            <!-- Finalists slider -->
            <div id="finalists_slider_container">
                <div class="DESKTOP judge_desktop_slider">
                    <div id="finalists_slider" class="p6_slider">
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_01_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist_01.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>SAM HUTCHINSON</strong><br/><br/>Everlytic is a digital communications company that solves the problem of letting their clients send out bulk email and mobile communication to their customers.<br/><br/><strong>Website</strong>: <a href="http://www.everlytic.co.za" class="finalist-link" target="_blank">www.everlytic.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_02_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_02.png" class="finalist_slider_logo">
                            
                            <p class="BLACK_COPY"><br/><strong>TIM MATTHIS &amp; PETAR SOLDO</strong><br/><br/>Genex Insights is focused on providing research insights to its customers. The focus is on three main areas: marketing research, customer experience measurement and online media analysis.<br/><br/><strong>Website</strong>: <a href="http://www.genex.co.za" class="finalist-link" target="_blank">www.genex.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        
                        <!-- <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_03_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_03.png" class="finalist_slider_logo">
                            
                            <p class="BLACK_COPY"><br/><strong>BRUCE MORGAN</strong><br/><br/>GreatSoft CRM is a cloud-based solutions company that develops and implements technologies specifically for the accounting profession to maximise their profitability.<br/><br/><strong>Website</strong>: <a href="http://www.greatsoft.co.za" class="finalist-link">www.greatsoft.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div> -->
                        
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_04_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_04.png" class="finalist_slider_logo">
                            
                            <!-- <p class="BLACK_COPY">inQuba is a company offering technology and services aligned with creating positive customer experiences and engagement.</p> -->
                            <p class="BLACK_COPY"><br/><strong>TRENT ROSSINI</strong><br/><br/>inQuba is a company offering technology and services aligned with creating positive customer experiences and engagement.<br/><br/><strong>Website</strong>: <a href="http://www.inquba.com" class="finalist-link" target="_blank">www.inquba.com</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_05_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_05.png" class="finalist_slider_logo">
                            
                            <!-- <p class="BLACK_COPY">Merchant Capital was launched to provide working capital solutions to entrepreneurs that are different from what typical business owners get access to; there is no interest rate and there is no fixed repayment term.</p> -->
                            <p class="BLACK_COPY"><br/><strong>DOV GIRNUN</strong><br/><br/>Merchant Capital provides working capital solutions to entrepreneurs that are different from what typical business owners get access to; there is no interest rate and no fixed repayment term.<br/><br/><strong>Website</strong>: <a href="http://www.merchantcapital.co.za" class="finalist-link" target="_blank">www.merchantcapital.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_06_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_06.png" class="finalist_slider_logo">
                            
                            <!-- <p class="BLACK_COPY">Smoke CCS has developed a software solution – Eyerys – that makes it possible for companies to listen to customers at every touchpoint. This helps companies build more profitable businesses driven by the voice of the customer. </p> -->
                            <p class="BLACK_COPY"><br/><strong>ANDREW COOK &amp; ANDREW BURNS</strong><br/><br/>Smoke CCS has developed a software solution, Eyerys, that makes it possible for companies to listen to customers at every touchpoint. This helps companies build more profitable businesses driven by the voice of the customer. <br/><br/><strong>Website</strong>: <a href="http://www.smokeccs.co.za" class="finalist-link" target="_blank">www.smokeccs.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                            <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist_07_photo.jpg" class="finalist_profile_img LEFT">
                            <img src="<?php echo $image_folder; ?>finalist_07.png" class="finalist_slider_logo">
                            
                            <!-- <p class="BLACK_COPY">SPARK Schools was founded with a mission to provide access to high-quality education for all South Africans at a more affordable rate.</p> -->
                            <p class="BLACK_COPY"><br/><strong>STACEY BREWER &amp; RYAN HARRISON</strong><br/><br/>SPARK Schools was founded with a mission to provide access to high-quality education for all South Africans at a more affordable rate.<br/><br/><strong>Website</strong>: <a href="http://www.sparkschools.co.za" class="finalist-link" target="_blank">www.sparkschools.co.za</a></p>
                            <a href="https://blog.fnb.co.za/2015/06/innovative-companies-that-forge-unique-ways-of-doing-business/" target="_blank">
                                <input type="button" name="btn_submit_form" value="Watch now" id="btn_submit_form" class="square_submit_btn watch_now_btn"/>
                            </a>
                        </div>
                    </div> <!-- judges_slider -->
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="finalist_arrow_left">
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="finalist_arrow_right">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->
            
            <!-- Winner slider -->
            <div id="winner_container">
                <p class="TURQ_COPY winnercopy">Congratulations to <strong>Bruce Morgan</strong>, founder of <strong>GreatSoft CRM</strong> on being awarded the <strong>FNB Business Innovator of the Year 2015</strong>.</p>
                <img src="<?php echo $image_folder; ?>winner_photo.jpg" class="winner_photo">
                <div class="winner_content_cont">
                    
                    <img src="<?php echo $image_folder; ?>winner_page_logo.png"  width="160" height="50">
                        <div id="winnercopyslider">
                            <div class="winnecopyitem"><p>South African CRM software provider GreatSoft (Pty) Ltd impressed the 2015 BIA judges’ panel, earning them the title of winner of the inaugural <strong>FNB Business INNOVATION Awards</strong>.<br><br>
The developmental strides made by CEO Bruce Morgan toward realising the company’s ambition of global expansion are a testament to the power of the awards to propel winners towards the achievement of their business goals.</p></div>

                            <div class="winnecopyitem"><p>GreatSoft is a provider of its own internally developed customer relationship management systems for tax management, document management, billing and payroll for financial professionals. Despite the competition, they differentiated themselves by making their solution cloud-based, answering their customers’ need for accessibility. This customer-centric stance is just one of the many reasons why GreatSoft stood out as a fit for FNB and the Endeavor network.</p></div>
                            <div class="winnecopyitem"><p>Winning the BIA Awards does more than offer a business the opportunity for expansion; it affords momentum for the owners and management, and changes their perceptions of what is achievable. Morgan explains that the gruelling judging process helped them identify aspects of their business strategy that needed to be refined. Being flown to Silicon Valley, San Francisco, afforded them the opportunity to network with judges and fellow entrepreneurs from around the world, gaining insights into the global market into which they hoped to expand.</p></div>
                            <div class="winnecopyitem"><p>The results have been shifts in goals from doubling over two years to tripling over four to five years and the staff headcount has grown from 55 to 75. Increased focus on acquiring valued staff and developing talent through their internship programme has improved their capacity, which has become imperative as the group’s combined revenues are 40% higher than a year ago and they intend to double revenues into Q3 and Q4 of 2016.</p></div>
                        </div>
                </div> <!-- winner content cont -->
                <div class="clear"></div>
            </div> <!-- Winner slider container -->
            <div class="modal-btn-close"></div>
        </div> <!-- ALUMNI 2015 SLIDER MODAL -->




        <!-- ALUMNI 2016 SLIDER MODAL -->
        <div class="alumni_sliders_modal_cont" id="alumni_2016_modal" myHeight="0" myTop="0">
            <!-- <h1 class="page_h1 TURQ_COPY" id="profiles_h1">Profiles</h1> -->
            <!-- <div class="profiles_colour_cont"> -->
            <div class="archive_colour_cont">
                <div class="archive_nav_cont">
                    <p>2016 judges</p>
                </div> <!-- archive nav cont 2 -->
                <div class="archive_nav_cont">
                    <p>2016 finalists</p>
                </div> <!-- archive nav cont 1 -->
                <div class="archive_nav_cont">
                    <p>2016 winner</p>
                </div> <!-- archive nav cont 3 -->
            </div> <!-- archive colour cont -->



            <!-- read more buttons -->
            <!-- <div class="profiles_nav_cont profiles_arrow_rm rm_closed" mySlider="judges_slider_container2016" id="rm1_btn2016"></div>
            <div class="profiles_nav_cont profiles_arrow_rm rm1_open" mySlider="finalists_slider_container2016" id="rm2_btn2016"></div>
            <div class="profiles_nav_cont profiles_arrow_rm rm_closed" mySlider="winner_container2016" id="rm3_btn2016"></div> -->
            <div class="archive_nav_cont archive_arrow_rm rm1_open" mySlider="judges_slider_container2016" id="rm2_btn2016"></div>
            <div class="archive_nav_cont archive_arrow_rm rm_closed" mySlider="finalists_slider_container2016" id="rm1_btn2016"></div>
            <div class="archive_nav_cont archive_arrow_rm rm_closed" mySlider="winner_container2016" id="rm3_btn2016"></div>
            
            <div class="clear"></div>
            <!-- 2016 Judges slider -->
            <div id="judges_slider_container2016">
                <div class="DESKTOP judge_desktop_slider">
                    <div id="judges_slider2016" class="p6_slider">
                    
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Howard.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Howard Arrand</h2> -->
                            <p class="BLACK_COPY"><strong>HOWARD ARRAND</strong><br><br>Howard is the <strong>Provincial Head for FNB Business in KwaZulu-Natal</strong>. He has previously participated as an ISP judge in the Endeavor process, and also served as a judge/convenor of judges for the EY World Entrepreneur Awards.</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>MarcelClaasen.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">MARCEL KLAASSEN</h2> -->
                            <p class="BLACK_COPY"><strong>MARCEL KLAASSEN</strong><br><br>As the current <strong>Head of Sales at FNB</strong>, Marcel is responsible for new sales and client value management. </p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Cynthia.jpg" class="judge_profile_img LEFT" alt="Cynthia Mkhombo">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">CYNTHIA MKHOMBO</h2> -->
                            <p class="BLACK_COPY"><strong>CYNTHIA MKHOMBO</strong><br><br>Cynthia is the <strong>founder and current CEO of Masana Hygiene Services</strong>. In 2010, <span class="COPY_REGULAR_ITALIC">CEO Magazine</span> called her "one of the most influential women in business and professional services".</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Raymond.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Raymond Ndlovu</h2> -->
                            <p class="BLACK_COPY"><strong>RAYMOND NDLOVO</strong><br><br>Raymond currently serves as an <strong>Investment Executive at Remgro Limited</strong>, utilising his extensive local and international experience as a specialist in various fields in the financial services industry.</p>
                        </div>
                        <div class="item">
                            <img src="<?php echo $image_folder; ?>Cathrine.jpg" class="judge_profile_img LEFT">
                            <!-- <h2 class="judge_name COPY_BOLD BLACK_COPY">Catherine Townshend</h2> -->
                            <p class="BLACK_COPY"><strong>CATHERINE TOWNSHEND</strong><br><br><strong>Joining Endeavor SA in 2012</strong>, Catherine has assisted some of the country’s most innovative entrepreneurs realise their global growth aspirations.</p>
                        </div>
                        
                        
                        

                    </div> <!-- judges_slider -->
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="judge_arrow_left2016">
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="judge_arrow_right2016">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->
            
            
            
            <!-- 2016 Finalists slider -->
            <div id="finalists_slider_container2016">
                <div class="DESKTOP judge_desktop_slider">
                    <div id="finalists_slider2016" class="p6_slider">

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_01_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_01.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>CATHERINE L&Uuml;CKHOFF</strong><br/><br/>NicheStreem is a custom music streaming service that offers music-lovers access to pre-compiled playlists, in multiple streams, without the need for Internet access.<br/><br/><strong>Website</strong>: <a href="http://www.nichestreem.com" class="finalist-link" target="_blank">www.nichestreem.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_6" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_02_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_02.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>STUART PRIOR &amp; SCOTT SARGENT</strong><br/><br/>Rhino Wood turns sustainably sourced softwood into durable hardwood – without the price tag.<br/><br/><strong>Website</strong>: <a href="http://www.rhinowood.co.za" class="finalist-link" target="_blank">www.rhinowood.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_7" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_03_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_03.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>TONI GLASS</strong><br/><br/>The Toni Glass Collection is a contemporary tea brand that produces tea in the old-fashioned way. Only full-leaf teas from select tea gardens are used, insuring that the brand’s tea retains its natural integrity.<br/><br/><strong>Website</strong>: <a href="http://www.toniglasscollection.co.za" class="finalist-link" target="_blank">www.toniglasscollection.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_8" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_04_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_04.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>KIRSTY CHADWICK</strong><br/><br/>The Training Room Online is an innovative, cost-effective digital learning platform that streamlines e-learning material for any audience, in any location, from 
any device.<br/><br/><strong>Website</strong>: <a href="http://www.ttro.com" class="finalist-link" target="_blank">www.ttro.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_9" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_05_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_05.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>TREVOR WOLFE &amp; REMON GEYSER</strong><br/><br/>delvv.io offers clients feedback from a network of more than 120 000 creative professionals, faster and more cost-effectively than traditional research vendors.<br/><br/><strong>Website</strong>: <a href="http://www.delvv.io" class="finalist-link" target="_blank">www.delvv.io</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_0" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_06_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_06.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>MELVYN LUBEGA &amp; ANDREW BARNES</strong><br/><br/>GO1 is an innovative e-learning platform that offers a training and education solution that aggregates both online and offline training options.<br/><br/><strong>Website</strong>: <a href="http://www.go1.com" class="finalist-link" target="_blank">www.go1.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_2" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_07_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_07.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>DENNIS MARKETOS, PAUL MARKETOS &amp; PAUL DE KOCK</strong><br/><br/>IsoMetrix, a rapidly expanding product from Metrix Software Solutions, allows companies to track and report on governance, risk management and compliance via a user-friendly, customisable dashboard.<br/><br/><strong>Website</strong>: <a href="http://www.isometrix.com" class="finalist-link" target="_blank">www.isometrix.com</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_3" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_08_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_08.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>ERIK OOSTHUIZEN &amp; GARETH FARROW</strong><br/><br/>Kliq Holdings is a wireless Internet data provider offering listed companies, corporates and SMEs in remote locations their own affordable, high-performance and reliable 
network packages.<br/><br/><strong>Website</strong>: <a href="http://www.kliq.co.za" class="finalist-link" target="_blank">www.kliq.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_4" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_09_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_09.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>DAVID SHAPIRO &amp; JONATHAN SHAPIRO</strong><br/><br/>LESCO Manufacturing is an innovative local company, designing and manufacturing ground-breaking new electrical products for the local market.<br/><br/><strong>Website</strong>: <a href="http://www.lescosk.co.za" class="finalist-link" target="_blank">www.lescosk.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_5" class="square_submit_btn watch_now_btn2"/>
                        </div>

                        <div class="item">
                            <img src="<?php echo $image_folder; ?>finalist2016_10_photo.jpg" class="finalist_profile_img LEFT" alt="Cynthia Mkhombo">
                            <img src="<?php echo $image_folder; ?>finalist2016_10.png" class="finalist_slider_logo">
                            <p class="BLACK_COPY"><br/><strong>GRAHAM ROWE &amp; RICHARD JOHNSON</strong><br/><br/>Guidepost is a software solution that enhances healthcare delivery for chronic illness patients around
 the globe in a cost-effective and scaleable way.<br/><br/><strong>Website</strong>: <a href="http://www.guidepost.co.za" class="finalist-link" target="_blank">www.guidepost.co.za</a></p>
                            <input type="button" name="btn_submit_form" value="Watch now" id="finalistVideo_1" class="square_submit_btn watch_now_btn2"/>
                        </div>


                    </div> <!-- judges_slider -->
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_left.png" class="judge_slider_arrow slide_arrow_left" id="finalist_arrow_left2016">
                    <img src="<?php echo $image_folder; ?>finalist_slide_arrow_right.png" class="judge_slider_arrow slide_arrow_right" id="finalist_arrow_right2016">
                </div> <!-- DESKTOP - for slider -->
            </div> <!-- judges slider container -->

             <!-- Winner2016 slider -->
            <div id="winner_container2016">
                <!-- <h1>2016 WINNER</h1> -->
                <p class="TURQ_COPY winnercopy">Congratulations to <strong>Bevan Ducasse</strong>, founder of <strong>wiGroup</strong> on being awarded the <strong>FNB Business Innovator of the Year 2016</strong>.</p>
                <img src="<?php echo $image_folder; ?>winner2016_photo.png" class="winner_photo">
                <div class="winner_content_cont">
                    
                    <img src="<?php echo $image_folder; ?>winner2016.png"  >
                        <div id="winnercopyslider2016">
                            <div class="winnecopyitem"><p>The awards not only recognised Ducasse for his innovation in driving business growth, it also credited him and his company for their potential to scale through the Endeavor network. <br/><br/>
                            wiGroup has demonstrated a trajectory of exponential innovation, from its start as a wallet to its current standing as an international platform that enables point-of-sale trade. </p></div>

                            <div class="winnecopyitem"><p>Ducasse embodies entrepreneurship and is passionate about building a significant company that embraces disruptive thinking and scale. Not unlike the FNB Business culture, he rewards and empowers a team of remarkable people who bring the business to life. The decision-making process and the habits of the organisation are all supportive of testing and driving improvements and new efficiencies. FNB is proud to be associated with a business that exudes such passion, drive, courage and fun, all whilst revolutionising the customer payment experience.</p></div>
                            <div class="winnecopyitem"><p>As the <strong>FNB Business Innovator of the Year</strong>, Ducasse will attend the prestigious Endeavor ISP in Boston in September this year, courtesy of FNB Business and SWISS International Airlines. As an <strong>Endeavor Entrepreneur</strong>, he gains access to a global network of founders as well as access to a pool of mentors comprising infuential business leaders, and an advisory board to help him achieve his business goals. </p></div>
                        </div>
                </div> <!-- winner content cont -->
                <div class="clear"></div>
            </div> <!-- Winner slider container -->


            <div class="modal-btn-close"></div>
            <div id="profiles_video_container"><div id="player"></div><div class="btn-close"></div></div>
        </div> <!-- ALUMNI 2016 SLIDER MODAL -->




    </div> <!-- PAGE ALUMNI -->
    
    
    
    <!-- CONTACT US PAGE -->
    <div class="std_page page_8_container BG_GREY" id="PAGE_8" data-10000="top:100%;" data-11000="top:100%;" data-11500="top:0%;">
    	<div class="mobi_page_anchor" id="MOBI_PAGE_8"></div>
    	<div class="main_container" id="PAGE_8_CONTENT" myHeight="0" myTop="0">
    		<h1 class="page_h1 TURQ_COPY">Contact us</h1>
            <p class="contact_us_copy TURQ_COPY">For any queries, email us at <a href="mailto:innovationawards@fnb.co.za" class="TURQ_COPY COPY_BOLD" style="text-decoration:underline;">innovationawards@fnb.co.za</a></p>
            <hr class="accordian_drop_hr page_8_line TURQ_COPY"/>
        </div> <!-- PAGE 8 main container -->
        <div class="footer_container">
        	<p class="footer_copy BLACK_COPY"><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
        </div> <!-- footer container -->
    </div> <!-- PAGE 8 -->
    
<!-- PAGES / LAYERS END HERE -------------------------------------- -->
    
    <!-- TOP LEFT LOGO COPY - WHITE BACKGROUND -->
    <div class="topLeftLogo_white DESKTOP" data-0="opacity:1;" data-2450="opacity:1;" data-2500="opacity:0;" data-3950="opacity:0;" data-4000="opacity:1;">
        <img src="<?php echo $image_folder; ?>web_top-left-copy-white.png" class="FULL_WIDTH">
        <?php
        // CLOSE ENTRIES / CHANGE BUTTON
        
        if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
            ?>
        <div class="square_btn enter_now_btn finalist_btn"><p class="square_btn_p">Finalists</p></div>
            <?php
        }else {
            ?>
        <a href="entry_login.php" target="_blank"><div class="square_btn enter_now_btn"><p class="square_btn_p">Enter now</p></div></a>
            <?php
        }
        ?>
        <div class="clear"></div>
        <p class="BLACK_COPY entries_close_copy"><!-- Entries close <strong>31 January 2017. --></strong></p>
    </div> <!-- top left logo -->
    
    <!-- TOP LEFT LOGO COPY - TURQ BACKGROUND -->
    <div class="topLeftLogo_white DESKTOP" data-950="opacity:0;" data-2450="opacity:0;" data-2500="opacity:1;" data-3950="opacity:1;" data-4000="opacity:0;">
        <img src="<?php echo $image_folder; ?>web_top-left-copy-turq.png" class="topLeftCopy_img FULL_WIDTH">
        <?php
        // CLOSE ENTRIES / CHANGE BUTTON
        
        if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
            ?>
        <div class="square_btn enter_now_btn finalist_btn"><p class="square_btn_p">Finalists</p></div>
            <?php
        }else {
            ?>
        <a href="<?php echo $project_data['pages']['entry_login']; ?>" target="_blank"><div class="square_btn enter_now_btn"><p class="square_btn_p">Enter now</p></div></a>
            <?php
        }
        ?>
        <div class="clear"></div>
        <p class="WHITE_COPY entries_close_copy"><!-- Entries close <strong>31 January 2017. --></strong></p>
    </div> <!-- top left logo -->


    <!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
    <div class="topRightLogoCont DESKTOP" data-0="opacity:1;" data-2450="opacity:1;" data-2500="opacity:0;" data-3950="opacity:0;" data-4000="opacity:1;">
        <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
        <div class="clear"></div>
        <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
        <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
        <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
    </div> <!-- topRightLogo WHITE -->
    
    <!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - TURQ BACKGROUND -->
    <div class="topRightLogoCont DESKTOP" data-950="opacity:0;" data-2450="opacity:0;" data-2500="opacity:1;" data-3950="opacity:1;" data-4000="opacity:0;">
        <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_turq.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
        <img src="<?php echo $image_folder; ?>social_btn_in_turq.svg" class="in_share_btn">
        <img src="<?php echo $image_folder; ?>social_btn_twitter_turq.svg" class="twitter_share_site">
        <img src="<?php echo $image_folder; ?>social_btn_fb_turq.svg" class="fb_share_thispage">
    </div> <!-- topRightLogo TURQ -->

    


    
    <!-- BOTTOM LEFT COPY - WHITE BACKGROUND -->  
    <div class="web_left_bottom_img DESKTOP" data-0="opacity:1; bottom:20px; display: block;" data-1500="opacity:1; display: block;" data-1600="opacity:0; display: none;" data-3000="opacity:0; display: none;" data-3100="opacity:1; display: block;" data-11400="bottom:20px;" data-11500="bottom:50px;">

        <div class="countdown">
            <div class="countdown_close-text">Entries close in</div>
            <div class="countdown__unit countdown__unit--first"><span class="countdown__counter days_counter">00</span>d</div>
            <div class="countdown__unit"><span class="countdown__counter hours_counter">00</span>h</div>
            <div class="countdown__unit countdown__unit--noborder"><span class="countdown__counter minutes_counter">00</span>m</div>
        </div>

    	<!-- <img src="<?php //echo $image_folder; ?>web_left-bottom-copy-white.png" class="botLeftCopy_img" id="winner_copy_btn_white"> -->
        <p  class="winner_copy_btn_turq">Be the next <strong>FNB&nbsp;Business
        <BR/>
        Innovator of the Year</strong>.</p>
        
        <p class="footer_small"><a href="<?php echo $project_data['pages']['entry_terms']; ?>" target="_blank">Terms and conditions</a><!-- <br/><a href="privacy.html" target="_blank">Privacy policy</a> --><BR>&nbsp;</p>
    </div> <!-- top left logo -->
    
    <!-- BOTTOM LEFT COPY - TURQ BACKGROUND -->  
	<div class="web_left_bottom_img DESKTOP" data-0="opacity:0;bottom:20px; display: none;" data-1500="opacity:0; display: none;" data-1600="opacity:1; display: block;" data-3000="opacity:1; display: block;" data-3100="opacity:0; display: none;">

        <div class="countdown">
            <div class="countdown_close-text" style="color: white;">Entries close in</div>
            <div class="countdown__unit countdown__unit--first"><span class="countdown__counter days_counter">00</span>d</div>
            <div class="countdown__unit"><span class="countdown__counter hours_counter">00</span>h</div>
            <div class="countdown__unit countdown__unit--noborder"><span class="countdown__counter minutes_counter">00</span>m</div>
        </div>

    	<!-- <img src="<?php //echo $image_folder; ?>web_left-bottom-copy-turq.png" class="botLeftCopy_img" id="winner_copy_btn_turq"> -->
        <p id="winner_copy_btn_turq" class="winner_copy_btn_turq">Be the next <strong>FNB&nbsp;Business
        <BR/>
        Innovator of the Year</strong>.</p>
        
        <p class="footer_small turq_terms"><a href="<?php echo $project_data['pages']['entry_terms']; ?>" target="_blank" class="turq_terms">Terms and conditions</a><!-- <br/><a href="privacy.html" target="_blank" class="turq_terms">Privacy policy</a> --><BR>&nbsp;</p>
    </div> 

	<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 
    <a href="products.php"><img src="images/btn-products.jpg" class="web_right_bottom_img" data-0="opacity:1; bottom:20px;" data-1500="opacity:1;" data-1600="opacity:0;" data-3000="opacity:0;" data-3100="opacity:1;" data-11400="bottom:20px;" data-11500="bottom:70px;"></a>
    <!-- BOTTOM RIGHT COPY - TURQ BACKGROUND --> 
    <a href="products.php"><img src="images/btn-products.jpg" class="web_right_bottom_img"  data-0="opacity:0; bottom:20px;" data-1500="opacity:0;" data-1600="opacity:1;" data-3000="opacity:1;" data-3100="opacity:0;"/></a>


	
    
    
    
    <!-- LIGHTBOX STUFFS -->
    <div class="skrollr_lightbox-mask" id="lightbox_mask" data-0="top:0%;">
    	<img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_form_topLogo">
    </div>
    
    <img src="<?php echo $image_folder; ?>hamburgur_icon.svg" class="web_menu_btn_img DESKTOP" id="nav_menu_btn_open" data-0="top:0%">
    
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->

	<nav class="main_nav_container" id="main_nav_container" data-0="top:15px;">
		<ul class="main_nav DESKTOP">
			<li class="nav_item" myPos="2500">About the awards</li>
			<hr />
			<li class="nav_item" myPos="4000">The prize</li>
			<hr />
			<li class="nav_item" myPos="5500">Selection criteria</li>
			<hr />
			<li class="nav_item" myPos="7000">The process</li>
			<hr />
			<li class="nav_item" myPos="8500">About Endeavor</li>
			<hr />
            <li class="nav_item" myPos="10000">Alumni</li>
			<hr />
            <li class="nav_item" myPos="11500">Contact us</li>
            <hr />
		</ul>
        
        <div class="nav_social_btns">
        	<img src="<?php echo $image_folder; ?>social_btn_fb_turq.svg" class="fb_share_thispage">
            <img src="<?php echo $image_folder; ?>social_btn_twitter_turq.svg" class="twitter_share_site">
            <img src="<?php echo $image_folder; ?>social_btn_in_turq.svg" class="in_share_btn">
        </div>
        
        <img src="<?php echo $image_folder; ?>web_menu_btn_contract.png" class="FULL_WIDTH" id="nav_menu_btn_close">
	</nav> <!-- main nav container -->

	

    <div class="trigger3" data-700="" data-1300="" data-2800="" data-4300="" data-5700="" data-6500="" data-7300="" data-8400="" data-10300="" data-emit-events></div>

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
	<script type="text/javascript" src="js/skrollr.min.js"></script>
    <!--[if lt IE 9]>
	<script type="text/javascript" src="js/skrollr.ie.min.js"></script>
	<![endif]-->
    <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
    <!-- custom fnb functions -->
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>

	<script type="text/javascript">
	// ACCORDIAN ARRAYS
	var accordianIDs = new Array();
	accordianIDs[0] = 'acc_ex_2';
	accordianIDs[1] = 'acc_ex_3';
	accordianIDs[2] = 'p4_col_ex_left';
	accordianIDs[3] = 'p4_col_ex_right';
	accordianIDs[4] = 'acc_ex_7';
	var accBtnIDs = new Array();
	accBtnIDs[0] = 'btn_acc_drop_p2';
	accBtnIDs[1] = 'btn_acc_drop_p3';
	accBtnIDs[2] = 'btn_p4_col_left';
	accBtnIDs[3] = 'btn_p4_col_right';
	accBtnIDs[4] = 'btn_acc_drop_p7';
	
	var lastOpenedAcc = null;
	var curDataPos = 0;
	var scrolling = false;
	var animContainerHeight = 0;
	// On loading page
	var loadingPage = true;	
	// -----------------------------------
	// **
	// BUTTON ACTIONS
	$("#btn_video_play").click(function() {
		document.getElementById('bgvid').play();
		$(this).css('display','none');
	});
	$("#bgvid").click(function() {
		document.getElementById('bgvid').pause();
		$('#btn_video_play').css('display','block');
	});
	
	
	
	$('.accordian_drop_btn').click(function() {
		openMyAccordian(this);
	});
	$('.btn_p4_col_left').click(function() {
		openMyAccordian(this);
	});
	$('.btn_p4_col_right').click(function() {
		openMyAccordian(this);
	});
    $('#nav_menu_btn_open').click(function() {
        $('#main_nav_container').show(250);
    });
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

	// Navigation items
	$('.nav_item').click(function () {
        var myPos = $(this).attr('myPos');
        blindScroll(myPos);
    });

	$('#winner_copy_btn_white').click(function () {
		//location.href = "<?php echo $project_data['pages']['entry_login']; ?>";
		OpenInNewTab("<?php echo $project_data['pages']['entry_login']; ?>");
	});
	$('#winner_copy_btn_turq').click(function () {
		//location.href = "<?php echo $project_data['pages']['entry_login']; ?>";
		OpenInNewTab("<?php echo $project_data['pages']['entry_login']; ?>");
	});
	$('.finalist_btn').click(function () {
		blindScroll(10000); // Profiles
	});

	/* ------------ START WITH PAGE JOBS ON PAGE LOAD --------------------------- */
	var s;
	var mobi_owl;
	var judges_slider;
	var finalists_slider;
	var judges_slider2016;
	var finalists_slider2016;
	var web_owl;
	
    var preloadImages = new Array(
        "images/faceanim/face2-000.jpg",
        "images/faceanim/face2-001.jpg",
        "images/faceanim/face2-002.jpg",
        "images/faceanim/face2-003.jpg",
        "images/faceanim/face2-004.jpg",
        "images/faceanim/face2-005.jpg",
        "images/faceanim/face2-006.jpg",
        "images/faceanim/face2-007.jpg",
        "images/faceanim/face2-008.jpg",
        "images/faceanim/face2-009.jpg",
        "images/faceanim/face2-010.jpg",
        "images/faceanim/face2-011.jpg",
        "images/faceanim/face2-012.jpg",
        "images/faceanim/face2-013.jpg",
        "images/faceanim/face2-014.jpg",
        "images/faceanim/parra-layer-dots-1.png",
        "images/faceanim/parra-layer-lines-1.png",
        "images/faceanim/parra-layer-lines-2.png"
    );

	var jowl;
	$(document).ready(function (){
	//window.onload = function() {
		$('#main_nav_container').hide(200);
		


        // Run preloader
        preload("images/faceanim/face2-000.jpg",
        "images/faceanim/face2-001.jpg",
        "images/faceanim/face2-002.jpg",
        "images/faceanim/face2-003.jpg",
        "images/faceanim/face2-004.jpg",
        "images/faceanim/face2-005.jpg",
        "images/faceanim/face2-006.jpg",
        "images/faceanim/face2-007.jpg",
        "images/faceanim/face2-008.jpg",
        "images/faceanim/face2-009.jpg",
        "images/faceanim/face2-010.jpg",
        "images/faceanim/face2-011.jpg",
        "images/faceanim/face2-012.jpg",
        "images/faceanim/face2-013.jpg",
        "images/faceanim/face2-014.jpg",
        "images/faceanim/parra-layer-dots-1.png",
        "images/faceanim/parra-layer-lines-1.png",
        "images/faceanim/parra-layer-lines-2.png");


		// Activate Skrollr or not
		if (windowWidth < 800) {
			// DO MOBI STUFFS
			$("video").prop('muted', true); //mute
		}else {
			//Calling it twice doesn't hurt.
			s = skrollr.init({
				keyframe: function(element, name, direction) {
					$(element).trigger(name, [direction]);
				}
			});
		}
		web_owl = $("#owl-demo").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});
		judges_slider = $("#judges_slider").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});
		finalists_slider = $("#finalists_slider").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});
		
		judges_slider2016 = $("#judges_slider2016").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});
		finalists_slider2016 = $("#finalists_slider2016").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});
		
		mobi_owl = $("#mobi_owl-demo").owlCarousel({
			slideSpeed: 200, //Set AutoPlay to 3 seconds
			paginationSpeed: 800,
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
	  	});

        var winner_copy_slider = $('#winnercopyslider').owlCarousel({
            singleItem: true,
            items : 1,
            itemsDesktop : [1920,1],
            itemsDesktopSmall : [640,1]
        })
        var winner_copy_slider2016 = $('#winnercopyslider2016').owlCarousel({
			singleItem: true,
			items : 1,
			itemsDesktop : [1920,1],
			itemsDesktopSmall : [640,1]
        })
		jowl = $("#judges_slider").data('owlCarousel');
		fowl = $("#finalists_slider").data('owlCarousel');
		jowl2016 = $("#judges_slider2016").data('owlCarousel');
		fowl2016 = $("#finalists_slider2016").data('owlCarousel');
		setTimeout(function() {
			if (setExHeights(accordianIDs)) {
				if (windowWidth < 800) {
					// do nothing
					$('#lightbox_mask').hide();
				}else {
					
					$('#lightbox_mask').hide();
					verticalCenterAll();
                    setTimeout(function() {
                        $('#alumni_2015_modal').hide();
                        $('#alumni_2016_modal').hide();
                    }, 500);
				}
			}
            loadingPage = false;
		}, 500);
		//loadingPage = false;
		//window.addEventListener( 'resize', onWindowResize, false );


        // VIDEO PLAYER FOR PROFILES
        // $('.finalist-img-btn .watch_now_btn').on('click', function(event){
        $('.watch_now_btn2').on('click', function(event){
            event.preventDefault();
            var index = parseInt($(event.currentTarget).attr('id').substr($(event.currentTarget).attr('id').indexOf('_')+1, $(event.currentTarget).attr('id').length), 10);
            var videoIDs=[
                'tO5TWC2CytI',
                'zcpSJz2Mnf8',
                'WQJM6xU5jZc',
                '0sAU8rXZzqo',
                'YIZ884WG6AY',
                '58VadeF_Co0',
                'CPS42-xvbSA',
                'C0z1QyM22Tk',
                'u-4ISVEkThk',
                'c5l4kQbibUs',
                'umMp6mc7Y_o'
            ];
            $('#profiles_video_container').show();
            $('#profiles_h1').css({
                "opacity": 0
            });
            player.loadVideoById(videoIDs[index]);
        })  
        $('.btn-close').on('click', function(){
            $('#profiles_h1').css({
                "opacity": 1
            });
            stopVideo();
            $('#profiles_video_container').hide();
        })

        // ALUMNI YEAR MODAL HIDE AND DISPLAY
        $('#alumni_btn_2015').on('click', function(event){
            $('#alumni_2015_modal').show(200,function() {
                verticalCenterMe($('#alumni_2015_modal'));
            });
        });
        $('#alumni_btn_2016').on('click', function(event){
            // closeSliders('finalists_slider_container2016')
            $('#alumni_2016_modal').show(200,function() {
                verticalCenterMe($('#alumni_2016_modal'));
            });
            
        });
        $('.modal-btn-close').on('click', function(){
            var p = $(this).parent();
            $(p).hide();
        })

	});



	$("#judge_arrow_left").click(function() {
		jowl.prev();
	});
	$("#judge_arrow_right").click(function() {
		jowl.next();
	});
	$("#finalist_arrow_left").click(function() {
		fowl.prev();
	});
	$("#finalist_arrow_right").click(function() {
		fowl.next();
	});
	$("#judge_arrow_left2016").click(function() {
		jowl2016.prev();
	});
	$("#judge_arrow_right2016").click(function() {
		jowl2016.next();
	});
	$("#finalist_arrow_left2016").click(function() {
		fowl2016.prev();
	});
	$("#finalist_arrow_right2016").click(function() {
		fowl2016.next();
	});
	
	/* SKROLLR KEYFRAME TRIGGER ACTIONS */
	$(".trigger3").on('data700', function(e, direction) {
		curDataPos = 700;
		$('.web_scroll_image').show();
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data1300', function(e, direction) {
		curDataPos = 1300;
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data2800', function(e, direction) {
		curDataPos = 2800;
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data4300', function(e, direction) {
		curDataPos = 4300;
		// if (console.log(direction));
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data5700', function(e, direction) {
		curDataPos = 5700;
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data6500', function(e, direction) {
		curDataPos = 6500;
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data7300', function(e, direction) {
		curDataPos = 7300;
		closeAndCenterAll(accBtnIDs);
	});
	$(".trigger3").on('data8400', function(e, direction) {
		curDataPos = 8400;
		closeAndCenterAll(accBtnIDs);
	});

    <?php include('_include/landing_face_js.js'); ?>
	
	
	/* 2016 FUNCTIONS --- CLEAN AND MOVE THE ABOVE FUNCTIONS */
	function closeSliders(openSliderID) {
        // console.log('>closeSliders > open: ', openSliderID);
		var myHeight = $('#PAGE_7_CONTENT').height();
		// if (console.log("sliderID: "+openSliderID+" | pre size: myHeight: "+myHeight));
     // 2015   // 
		if (openSliderID == 'judges_slider_container') {
			$('#finalists_slider_container').hide();
            $('#winner_container').hide();
			$('#winner_container2016').hide();
			$('#rm1_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm1_open.png)');
			$('#rm2_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm3_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
		}else if (openSliderID == 'finalists_slider_container') {
			$('#judges_slider_container').hide();
			$('#winner_container').hide();
			$('#rm1_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm2_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm2_open.png)');
			$('#rm3_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			
			
		}else if (openSliderID == 'winner_container') {
			$('#finalists_slider_container').hide();
			$('#judges_slider_container').hide();
			$('#rm1_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm2_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm3_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm3_open.png)');
		
        // 2016 //
        }else if (openSliderID == 'judges_slider_container2016') {
			$('#finalists_slider_container2016').hide();
            $('#winner_container2016').hide();
			$('#rm1_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
            $('#rm2_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm1_open.png)');
			$('#rm3_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
		
        }else if (openSliderID == 'finalists_slider_container2016') {
            $('#judges_slider_container2016').hide();
            $('#winner_container2016').hide();
            $('#rm1_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm2_open.png)');
            $('#rm2_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
            $('#rm3_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');

        }else if (openSliderID == 'winner_container2016') {
			$('#judges_slider_container2016').hide();
            $('#finalists_slider_container2016').hide();
			$('#rm1_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
            $('#rm2_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm3_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm3_open.png)');
			
		}else {
			$('#finalists_slider_container').hide();
			$('#judges_slider_container').hide();
			$('#winner_container').hide();
			$('#rm1_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm2_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm3_btn').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			
			$('#finalists_slider_container2016').hide();
			$('#judges_slider_container2016').hide();
            $('#winner_container2016').hide();
			$('#rm1_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
            $('#rm2_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
			$('#rm3_btn2016').css('background-image','url(<?php echo $image_folder; ?>archive_rm_closed.png)');
		}
		return true;
	}
    // closeSliders();

	$('.archive_arrow_rm').click(function() {
		var mySliderID = $(this).attr('mySlider');
		$('#'+mySliderID).show();
		if (closeSliders(mySliderID)) {
			setTimeout(function() {
				verticalCenterMe($('#PAGE_7_CONTENT'));
                verticalCenterMe($('#alumni_2015_modal'));
			}, 500);
		}
	});
	$('.profiles_arrow_rm').click(function() {
		var mySliderID = $(this).attr('mySlider');
		$('#'+mySliderID).show();
		if (closeSliders(mySliderID)) {
			setTimeout(function() {
				verticalCenterMe($('#PAGE_9_CONTENT'));
			}, 500);
		}
	});
	
	function OpenInNewTab(url) {
	  var win = window.open(url, '_blank');
	  win.focus();
	}
	
	function onResizeDoSomething() {
		windowWidth = window.innerWidth;
		windowHeight = window.innerHeight;
		// if (console.log("windowWidth: "+windowWidth+" | windowHeight: "+windowHeight));
		if (windowWidth < 1000) {
			window.location = '<?php echo $project_data['mobi_full_address']; ?>';
		}else {
			verticalCenterAll();
		}
	};
	 
	var resizeTimer = null;
	$(window).bind('resize', function() {
		if (resizeTimer) clearTimeout(resizeTimer);
		resizeTimer = setTimeout(onResizeDoSomething, 500);
	});

    var tag = document.createElement('script');

    tag.src = "https://www.youtube.com/iframe_api";
    var firstScriptTag = document.getElementsByTagName('script')[0];
    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
      height: '410',
      width: '640',
      events: {
        'onReady': onPlayerReady,
        // 'onStateChange': onPlayerStateChange
      }
    });
    }

    // 4. The API will call this function when the video player is ready.
    function onPlayerReady(event) {
        event.target.playVideo();
    }

    // 5. The API calls this function when the player's state changes.
    //    The function indicates that when playing a video (state=1),
    //    the player should play for six seconds and then stop.
    var done = false;
    function onPlayerStateChange(event) {
        if (event.data == YT.PlayerState.PLAYING && !done) {
        }
    }
    function stopVideo() {
        player.stopVideo();
    }



    /*-------------------countdown--------------------*/

    var deadline = 'January 31 2017 23:59:59 GMT+0200';
    function getTimeRemaining(endtime){
      var t = Date.parse(endtime) - Date.parse(new Date());
      var seconds = Math.floor( (t/1000) % 60 );
      var minutes = Math.floor( (t/1000/60) % 60 );
      var hours = Math.floor( (t/(1000*60*60)) % 24 );
      var days = Math.floor( t/(1000*60*60*24) );

      function formatcounter(n){
        if( n < 0){
            n = 0;
        }
        if(n < 10 && n >= 0){
            n = "0" + n;
        }

        return n;
      }
      return {
        'total': t,
        'days': formatcounter(days),
        'hours': formatcounter(hours),
        'minutes': formatcounter(minutes),
        'seconds': formatcounter(seconds),
        'valid': (t > 0)?true : false
      };
    }

    setInterval(function(){
        var t = getTimeRemaining(deadline);

        for (var i = 0; i<document.querySelectorAll('.days_counter').length; i++ ){
            document.querySelectorAll('.days_counter')[i].innerHTML = t.days;
        }

        // document.querySelectorAll('.days_counter').forEach(function(i){
        //     i.innerHTML = t.days
        // })
        // 
        // 
         for (var i = 0; i<document.querySelectorAll('.hours_counter').length; i++ ){
            document.querySelectorAll('.hours_counter')[i].innerHTML = t.hours;
        }

        // document.querySelectorAll('.hours_counter').forEach(function(i){
        //     i.innerHTML = t.hours
        // })

        for (var i = 0; i<document.querySelectorAll('.minutes_counter').length; i++ ){
            document.querySelectorAll('.minutes_counter')[i].innerHTML = t.minutes;
        }
        // document.querySelectorAll('.minutes_counter').forEach(function(i){
        //     i.innerHTML = t.minutes
        // })

        if(!t.valid){

            for (var i = 0; i<document.querySelectorAll('.enter_now_btn').length; i++ ){
                document.querySelectorAll('.enter_now_btn')[i].style.display = 'none';
            }
            // document.querySelectorAll('.enter_now_btn').forEach(function(i){
            //     i.style.display = 'none'
            // })
        }

    }, 1000)

    /*-------------------END countdown--------------------*/
    
    
</script>
    
</body>

</html>