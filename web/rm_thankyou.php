<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');

$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FNB Business INNOVATION Awards</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>

<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>
    
    
<div class="main_container client_list_container">
	<h3 class="thankyou_h3">Thank you.</h3>
    <p class="thankyou_copy">Your nomination has been sent.</p>
    <button class="LEFT square_btn btn_orange_arrow_left" onclick="javascript:window.location='<?php echo $project_data['pages']['rm_client_invites']; ?>'">
		<p>Back</p>
	</button>
</div> <!-- form container -->
	
    
    
<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->
<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 
<img src="<?php echo $image_folder; ?>/web_right-bottom-copy-white.png" class="web_right_bottom_img"/>
<!-- DISCLAIMER FOOTER -->
<div class="rm_footer_container">
	<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><span class="COPY_REGULAR_ITALIC" style="font-size:9px;">Terms and conditions apply.</span><BR><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
</div> <!-- footer container -->


<?php
// ADD RM navigation here
include('_include/rm_navigation.php');
?>
    
    
    
    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script src='js/frame_functions.js'></script>
	<script type="text/javascript">
		//$(document).ready(function (){
		window.onload = function() {
			$('#main_nav_container').hide(200);
		};
		$('#nav_menu_btn_open').click(function() {
			$('#main_nav_container').show(250);
		});
		$('#mobi_nav_menu_btn_open').click(function() {
			$('#main_nav_container').show(250);
		});
		$('#nav_menu_btn_close').click(function() {
			$('#main_nav_container').hide(250);
		});	
	</script>
</body>

</html>