<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - All Entries";
$project_page['file_name'] = "send_entry_reminders.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);
if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
	
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['home'].'');
	exit;
}

include('emailers/client_entry_reminder2017/emailer_reminder.php');
function sendReminderEmailer($nameto,$emailto,$userID) {
	sleep(1);
	global $_EtiFrame;
	global $project_data;
	global $_ProjectF;
	
	// Send reminder
	$emailMessage = buildReminderEmail();
	// To Applicant					
	$Subject = "Remember to submit your FNB Business Innovation awards entry.";
	$userEmailM = $emailMessage;
	// $emailto = 'arno@etiket.co.za';
	//$_EtiFrame -> sendBasicEmail($userEmailM,$fromname,$fromaddress,$nameto,$emailto,$Subject);
	if (sendPHPMail($userEmailM,$Subject,$emailto,$nameto)) {
		$_ProjectF -> saveEmailtoDB('ENTRY REMINDER',$userID,$userEmailM,$nameto,$emailto,$Subject);
		return true;
	}else {
		return false;
	}
}

require '_phpMailer/PHPMailerAutoload.php';
function sendPHPMail($message,$subject,$toAddress,$toName) {
	$mail = new PHPMailer;



	// $mail->isSMTP();                                      // Set mailer to use SMTP
	// $mail->Host = 'smtp.fnbbusinessinnovationawards.co.za';  // Specify main and backup SMTP servers
	// $mail->SMTPAuth = true;                               // Enable SMTP authentication
	// $mail->Username = 'competition@fnbbusinessinnovationawards.co.za';                 // SMTP username
	// $mail->Password = 'Design4etiket';                           // SMTP password
	// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted

	// $mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('competition@fnbbusinessinnovationawards.co.za', 'Innovation Awards Competition');
	$mail->addAddress($toAddress, $toName);               // Name is optional
	$mail->addReplyTo('innovationawards@fnb.co.za', 'Innovation Awards');

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->AltBody = '';

	if(!$mail->send()) {
	    echo 'Message could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	    return false;
	} else {
	    echo 'Message has been sent';
	    return true;
	}
	return true;
}

$_tableDefaultValues = array(
	'business_name' => 'Business name*',
	'annual_turnover' => '0',
	'business_reg_number' => 'Business registration number*',
	'industry' => '0',
	'founded_year' => 'Founded (year)*',
	'founder' => 'Founder*',
	'amount_of_founders' => '0',
	'is_founder_led' => '0',
	'founder_role' => 'What is the role of the founder in the business?*',
	'email' => 'Email address*',
	'tel' => 'Telephone number*',
	'number_of_staff' => 'Number of staff*',
	'address' => 'Address*',
	'city' => 'City*',
	'province' => 'Province*',
	'operate_outside_of_sa' => '0',
	'operate_outside_elaborate' => 'If yes, please elaborate*',
	'bank_with_fnb' => '0',
	'bank_with_fnb_rm' => 'If yes, please provide your Relationship Banker’s name*',
	'previous_entry' => '0',
	'previous_entry_progress' => 'If yes, please state how far you progressed*',
	'description_products' => 'Provide a brief description of company product or service offering*',
	'business_target_market' => 'Business\' target market*',
	'revenue_last_year' => 'Revenue – last year*',
	'projected_revenue' => 'Projected revenue – current year*',
	'is_grow_exponentially' => '0',
	'description_grow_exponentially' => 'If yes, please elaborate and provide projected business growth timelines*',
	'unique_part_of_business' => 'What part of your business is unique? Please elaborate*',
	'is_business_unique' => '0',
	'have_intellectual_property' => '0',
	'description_intellectual_property' => 'If yes, please elaborate*',
	'description_awards_help' => 'How could these awards help your business grow?* (max 500 words)',
	'is_endeavor' => '0'
);


$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fixed-positioning.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    

</head>

<body id="client_invite_page">

	<img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img rm_menu_btn DESKTOP" id="nav_menu_btn_open">
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->

	<img src="images/dpi_72/web_01_rightTopLogo.png" class="web_form_topLogo">
    <div class="clear"></div>
	<div class="form_container client_list_wide_container">
    	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Entries Status</h1>
        <?php

		//$_Project_db->where('rm_id',$_SESSION['rm_info']['id']);
		$_AllEntryUsers = $_Project_db -> get($_db_['table']['entry_users'],'id');
		if (is_array($_AllEntryUsers) && count($_AllEntryUsers) >= 1) {
			// InsertData
			$insertData = array();
			?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            
            <td class="status_head head_company">Legal entity name</td>
            <td class="status_head head_name">Founder</td>
            <td class="status_head head_invited">Invited</td>
            <td class="status_head head_tel">Tel:</td>
            <td class="status_head head_email">Email</td>
            <td class="status_head head_status">Status</td>
          </tr>
		  <?php
		  $bgStyle = "grey_tr";
		  
		  $incomplete_entries = array();
		  $emailCount = 0;
			foreach($_AllEntryUsers as $entryUser) {
				$entry_data = false;

				//	HAS NO ENTRY
				if ($entryUser['entry_id'] === 0) {
					$entry_data = false;
				}else if ($entryUser['entry_id'] >= 1) {
					//	HAS ENTRY
					$entry_data = true;
					$needUpdate = false;
					$insertData['user_id'] = $entryUser['id'];
					$insertData['status'] = $entryUser['status'];

					//	FETCH ENTRY
					$_Project_db -> where('id',$entryUser['entry_id']);
					$entry = $_Project_db -> get($_db_['table']['entries'],'-id');
					if (count($entry) >= 1) {
						$uEntry = $entry[0];
						// collect insert data
						foreach($uEntry as $key => $value) {
							if ($key == 'id') { $insertData['old_id'] = $value; }else {
								$insertData[$key] = $value;
							}
						}
						$industryName = "Other";
						/*if (isset($uEntry['industry'])) {
							if ($uEntry['industry'] == "") {
								$industryName = "Other";
							}else {
								$industryName = $uEntry['industry'];
							}
						}
						if (isset($industry_array[$industryName])) {
							$industry_array[$industryName] = $industry_array[$industryName]+1;
						}else {
							$industry_array[$industryName] = 1;
						}*/
						
						
						
						// Clean default entry data
						// DEFINE KEY VALUES
						/*$UpdateArray = array();
						foreach($uEntry as $key => $entryValue) {
							if (isset($_tableDefaultValues[$key]) && $entryValue === $_tableDefaultValues[$key]) {
								if (strlen($_tableDefaultValues[$key]) >= 2) {
									$UpdateArray[$key] = "";
								}else if ($_tableDefaultValues[$key] == 0) {
									//$UpdateArray[$key] = '0';
								}
							}
						}*/
						/*if (count($UpdateArray) >= 1) {
							$needUpdate = true;
							// update current entry data with clean new data.
							//$_Project_db -> where('id',$uEntry['id']);
							//$_Project_db -> update($_db_['table']['entries'], $UpdateArray);
						}*/
						
						// Find other entries with same user ID and remove
						/*$_Project_db -> where ('user_id',$uEntry['user_id']);
						$OtherUserEntries = $_Project_db -> get($_db_['table']['entries'],'-id');
						if (count($OtherUserEntries) >= 2) {
							foreach($OtherUserEntries as $otherEntry) {
								if ($otherEntry['id'] == $uEntry['id']) {
									// do nothing
								}else {
									$uoearray = array(
										'user_id' => '0'
									);
									//$_Project_db -> where('id',$otherEntry['id']);
									//$_Project_db -> update($_db_['table']['entries'],$uoearray);
								}
							}
						}*/
						
						
					}else {
						/*
						$uEntry = array();
						$uEntry['founder'] = "NA";
						$uEntry['business_name'] = "NA";
						$uEntry['tel'] = "NA";
						*/
					}
					
					//$_Project_db -> insert($_db_['table']['entries_final'],$insertData);
				}
				
				
				if ($entry_data == true && $entryUser['status'] != "SUBMITTED") {
					
					/*
					* DO REMINDER STUFFS HERE
					*/
					$reminded = "OOO";
					$daysDif = $_DateTime -> datum_diff($entryUser['last_login_date']." ".$entryUser['last_login_time'], $_DateTime->vandag()." ".$_DateTime->tyd());
					if ($daysDif > 14) {
						$reminded = $_ProjectF -> checkReminded('ENTRY REMINDER',$entryUser['email'],14);
						//$reminded = 'SEND';
						if ($reminded == "SEND" && $emailCount < 10) {
							if (sendReminderEmailer($entryUser['email'],$entryUser['email'],$entryUser['id'])) {
								$reminded = "<strong>SENDED</strong>";
							}else {
								$reminded = "ERRORSEND";
							}
							$emailCount++;
						}else {
							// do nothing
						}
					}else {
						// do nothing
						$reminded = "NOT 14";
					}
					
					
					if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
					$entryInfo = array();
					?>
					<tr class="status_tr <?php echo $bgStyle; ?>">
						<td><?php if ($needUpdate == true) { echo "|*|".$uEntry['id']."| "; $_Project_db -> debug($UpdateArray); } ?><?php echo $uEntry['business_name']; ?></td>
						<td><?php echo $entryUser['email']; ?></td>
						<td><?php echo $reminded; ?></td>
						<td><?php echo $entryUser['register_date']; ?></td>
						<td><?php echo $entryUser['status']; ?></td>
						<td><?php
						if ($entryUser['status'] == "SUBMITTED") { ?><span class="TURQ_COPY">Entry complete</span><?php
							
						
						}else if ($entryUser['status'] == "SAVED") { ?><span class="ORANGE_COPY">Entry Saved</span><?php
							
						}else if ($entryUser['status'] == "REGISTERED") { ?><span class="DARK_GREY_COPY">Register Only</span><?php
							
						}else { echo $entryUser['status']; }
						?></td>
					</tr>
					<?php
					
				}
			}
		  ?>
          <tr class="status_footer">
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
		<?php
			// Add entry to final entry folder
			
			
		
		}else {
		?>
        	
        <?php
		}
		?>
        <div class="clear">&nbsp;</div>
        <tr class="status_footer">
          	<td></td>
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
       
        
    </div> <!-- form container -->

    <div class="clear">&nbsp;<BR><BR>&nbsp;<BR><BR>&nbsp;<BR></div>
    <div class="rm_footer_container">
		<p class="rm_footer_copy BLACK_COPY"><span class="COPY_REGULAR_ITALIC" style="font-size:9px;">Terms and conditions apply.</span><BR><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
		<img src="images/web_right-bottom-copy-white.png" class="rm_swisslogo">
	</div> <!-- footer container -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>