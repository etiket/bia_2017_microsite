<?php

/**
* Date and Time Class for all custom date and time functions
* @author Arno Cilliers <arno@etiket.co.za>
* Copyright 2014 Etiket
*/
class DateAndTime {
	
	protected $_month_name_array;
	function __construct() {
		$this->_month_name_array = array("nomonth","January","February","March","April","May","June","July","August","September","October","November","December");
		//$this->_month_name_array = array("nomonth","Januarie","Februarie","Maart","April","Mei","Junie","Julie","Augustus","September","Oktober","November","Desember");
	}
	// DATE TIME --
	public function vandag() {
		$vandag = getdate(time());
		($vandag['mon'] <= 9) ? $mon = "0".$vandag['mon'] : $mon = $vandag['mon'];
		($vandag['mday'] <= 9) ? $mday = "0".$vandag['mday'] : $mday = $vandag['mday'];
		return $vandag['year']."-".$mon."-".$mday;
	}
	// returns the time in 24-hr format "21:18"
	public function tyd() {
		return date("H:i:s", time());
	}
	/**
    *
    * @param date $old_date The first date object
    * @param date $new_date The second date object
    * @return decimal Contains the difference in days
    */
	function datum_diff($old_date, $new_date) {
		$offset = strtotime($new_date . " UTC") - strtotime($old_date . " UTC");
		return $offset/60/60/24;
	}
	/**
    *
    * @param date $old_date The first date object
    * @param date $new_date The second date object
    * @return decimal Contains the difference in minutes
    */
	function tyd_diff($old_tyd, $new_tyd) {
		$offset = strtotime($new_tyd . " UTC") - strtotime($old_tyd . " UTC");
		return $offset/60;
	}
	
	/**
	*
	* @param date $clean_date The Date string from the database
	* @return array with different string formatted dates
	*/
	function format_date($clean_date) {
		$return = array();
		$date_array = explode("-",$clean_date);
		$month_id = $date_array[1]-0;
		//$return['full_long_date'] = $date_array[2]." ".$this->_month_name_array[$month_id]." ".$date_array[0];
		$return['full_long_date'] = $this->_month_name_array[$month_id]." ".$date_array[2].", ".$date_array[0];
		$return['short_date'] = $date_array[2]." ".substr($this->_month_name_array[$month_id],0,3);
		
		return $return;
	}
}