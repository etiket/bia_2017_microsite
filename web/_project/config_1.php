<?php
include("config_0.php");

// DB Tables
$_db_['table']['subscribers'] = $db_connection['table_prefix']."subscribers";
$_db_['table']['entries'] = $db_connection['table_prefix']."entries2017";
$_db_['table']['entry_users'] = $db_connection['table_prefix']."entry_users";
$_db_['table']['entry_founders'] = $db_connection['table_prefix']."entry_founders";
$_db_['table']['rms'] = $db_connection['table_prefix']."rms";
$_db_['table']['rm_invites'] = $db_connection['table_prefix']."rm_invites";
$_db_['table']['invite_accepts'] = $db_connection['table_prefix']."invite_accepts";
$_db_['table']['fnumbers'] = $db_connection['table_prefix']."fnumbers";
$_db_['table']['emails_sent'] = $db_connection['table_prefix']."emails_sent";
$_db_['table']['errors'] = $db_connection['table_prefix']."errors";
//-- PROJECT DATA --
$project_data['project_name'] = "FNB Innovative Business Awards 2017";
$project_data['keywords'] = "FNB Innovative";
$project_data['favicon'] = $project_data['full_address']."/images/favicon.png";
$project_data['phase_folder'] = $project_data['full_address']."/";
$project_data['frame_footer'] = '_project/frame_footer.php';
$project_data['pages'] = array();
$project_data['pages']['landing'] = $project_data['full_address']."index.php";
$project_data['pages']['home'] = $project_data['full_address']."index.php";
$project_data['pages']['thankyou'] = $project_data['full_address']."thankyou.php";
// RM (RELATIONSHIP MANAGERS PAGES -----
$project_data['pages']['rm_login'] = $project_data['full_address']."rm-login-register.php";
$project_data['pages']['rm_register'] = $project_data['full_address']."rm-login-register.php";
$project_data['pages']['rm_client_invites'] = $project_data['full_address']."rm_client_invites.php";
$project_data['pages']['rm_thankyou'] = $project_data['full_address']."rm_thankyou.php";
$project_data['pages']['rm_error'] = $project_data['full_address']."error.php";
$project_data['pages']['rm_status'] = $project_data['full_address']."rm_status.php";
$project_data['pages']['rm_terms'] = $project_data['web_full_address']."rm-terms.php";
// PUBLIC PAGES
$project_data['pages']['landing'] = $project_data['full_address']."index.php";
$project_data['pages']['password_recovery'] = $project_data['full_address']."password_recovery.php";
$project_data['pages']['entry_form'] = $project_data['full_address']."entry-form.php";
$project_data['pages']['entry_login'] = $project_data['full_address']."entry_login.php";
$project_data['pages']['entry_terms'] = $project_data['web_full_address']."terms.php";
//-- ADMIN AND SUPPORT INFO -----
$project_data['admin']['default_user_id'] = 31;
$project_data['email_system']['from_name'] = "fnbbusinessinnovationawards.co.za";
$project_data['email_system']['from_address'] = "competition@fnbbusinessinnovationawards.co.za";
$project_data['support']['publisher'] = "https://plus.google.com/116427055070017697691/about";
//-- FACEBOOK APP --
$project_fb_app = array();
$project_fb_app['admin_id'] = '670010811';
$project_fb_app['app_id'] = '';
$project_fb_app['secret'] = '';
$project_fb_app['canvas_URL'] = $project_data['full_address'].'/';
$project_fb_app['app_image'] = $project_data['full_address'].'images/fb_icon250.png';
$project_fb_app['share_message'] = 'FNB';
$project_fb_app['share_caption'] = 'FNB Innovative Business Awards';
$project_fb_app['share_picture'] = $project_fb_app['app_image'];
$project_fb_app['share_link'] = $project_data['full_address'].'/';
$project_fb_app['share_description'] = "";
$project_fb_app['share_icon'] = $project_fb_app['app_image'];
//-- TWITTER ----
$project_twitter = array();
$project_twitter['title'] = 'FNB-INNOVATIVE-BUSINESS-AWARDS.';
$project_twitter['image'] = $project_fb_app['app_image'];
