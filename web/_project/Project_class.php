<?php
/**
* Project Class description goes here
* @author Arno Cilliers <arno@etiket.co.za>
* Copyright 2014 Etiket
*/
class Project {
	
	protected $_pageData;
	
	function __construct() {
		
	}



	/*
	Entry Form Field Creation Function
    $type: text/textarea
    $id: String of name and ID
    $placeholder: String of placeholder and initial value if no value supplied from db
    $floatSide: left/right
    $width: 1 -> 26 or full
    $margins: left/right/both/none
	*/
	public function createField($type,$id,$placeholder,$data,$floatSide,$width,$margins) {
		$value = '';
		$addClass = "";
		if (isset($data[$id])) {
			if ($data[$id] != NULL && $data[$id] != "") {
				$value = $data[$id];
				$addClass = "";
			}else {
				$value = '';
				$addClass = "";
			}
		}

		if ($type === 'text') {
			?>
			<div class="form_field_container form_<?php echo $floatSide; ?>_container col-<?php echo $width; ?> form_field_<?php echo $margins; ?>_margin">
    			<input type="text" name="<?php echo $id; ?>" id="<?php echo $id; ?>" class="ift<?php echo $addClass; ?>" placeholder="<?php echo $placeholder; ?>" value="<?php echo $value; ?>" validateMe="YES" />
    		</div>
    		<?php
		}else if ($type === 'textarea') {
			?>
			<div class="form_field_container form_<?php echo $floatSide; ?>_container col-<?php echo $width; ?> form_field_<?php echo $margins; ?>_margin">
    			<textarea name="<?php echo $id; ?>" id="<?php echo $id; ?>" class="ift<?php echo $addClass; ?>" placeholder="<?php echo $placeholder; ?>" validateMe="YES"><?php echo $value; ?></textarea>
    		</div>
			<?php
		}

		if ($floatSide === "right" || $width === 'full') {
			?><div class="clear"></div><?php
		}
		?>
		
        <?php
	}


	public function createStepNav($stepNo,$formID) {
		global $project_data;
		?>
				<!-- NAVIGATION CONTAINER --------------------------------------- -->
				<div class="step_navigation_container">
					<div class="navigation_controls_cont">
						<div class="nav_dots_cont">
							<p>
							<?php
								$i = 1;
								$step = 1;
								$type = "dot_grey";
								while($i < 10) {
									if ($stepNo === $step && $type != "dot_line") { $type = "dot_selected"; }
									?>
									<img src="<?php echo $project_data['full_address']; ?>images/<?php echo $type; ?>.png" <?php if ($type === "dot_grey" || $type === "dot_selected") { echo 'class="nav_dot_img"'; } ?> />
									<?php
									if ($type === "dot_grey" || $type === "dot_selected") {
										$type = "dot_line";
									}else if ($type === "dot_line") {
										$type = "dot_grey";
										$step++;
									}
									$i++;
								}

							?>
							</p>
						</div>
						<?php
							if ($stepNo <= 1) {
							}else {
								?>
								<button class="LEFT square_btn btn_orange_arrow_left" onclick="onBackClick(this); return false;">
									<p>Back</p>
								</button>
								<?php
							}

							if ($stepNo === 5) {
								if (isset($_SESSION['entry_userinfo']['status']) && $_SESSION['entry_userinfo']['status'] != "SUBMITTED") {
									?>
									<button class="RIGHT square_btn btn_orange_arrow_right BG_TURQ" name="btn_submit_form" id="btn_submit_form" value="SUBMIT" onclick="return false;">
										<p>Submit</p>
									</button>
									<?php
								}
							}else {
								if (isset($_SESSION['entry_userinfo']['status']) && $_SESSION['entry_userinfo']['status'] != "SUBMITTED") {
									?>
									<button class="RIGHT square_btn btn_orange_arrow_right" onclick="onNextClick('<?php echo $formID; ?>'); return false;">
										<p>Next</p>
									</button>
									<?php
								}
							}
						?>
					</div> <!-- navigation controlls cont -->
					<div class="clear">&nbsp;</div>
					<?php
					if (isset($_SESSION['entry_userinfo']['status']) && $_SESSION['entry_userinfo']['status'] != "SUBMITTED") {
						?>
						<button class="LEFT square_btn btn_orange" name="btn_save_form" onclick="onSaveClick('<?php echo $formID; ?>');  return false;" value="SAVE">
							<p>Save</p>
						</button>
						<?php
					}
					?>
					<div class="clear">&nbsp;<br class="MOBILE"/>&nbsp;</div>
				</div> <!-- NAVIGATION CONTAINER -->
		<?php
	}



	/*
	*
	* Check and register RM
	*/
	public function checkRm($rm_email,$rm_fnumber) {
		global $_Project_db;
		global $_db_;
		$returnArray = array();
		$rm_email = strtolower($rm_email);
		$_Project_db->where('fnumber',$rm_fnumber);
		$existing_rms = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existing_rms) && count($existing_rms) >= 1) {
			if ($existing_rms[0]['email'] == $rm_email) {
				$returnArray = $existing_rms[0];
			}else {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "*The email address does not match the F-number associated with it.";
			}
		}else {
			
			$_Project_db->where('fnumber',$rm_fnumber);
			$existingFs = $_Project_db->get($_db_['table']['fnumbers'],'fnumber');
			if (is_array($existingFs) && count($existingFs) >= 1) {
				$fnumber_id = $existingFs[0]['id'];
				$insertArray = array(
					'fnumber' => $rm_fnumber,
					'fnumber_id' => $fnumber_id,
					'email' => $rm_email
					);
				if ($_Project_db->insert($_db_['table']['rms'],$insertArray)) {
					$insert_id = $_Project_db->insert_id();
					$returnArray['fnumber'] = $rm_fnumber;
					$returnArray['fnumber_id'] = $fnumber_id;
					$returnArray['email'] = $rm_email;
					$returnArray['id'] = $insert_id;
				}else {
					$returnArray['error'] = true;
					$returnArray['errormessage'] = "*System error, please try again.";
				}
			}else {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "*To login, please remove the ".urlencode('&quot;<strong>F</strong>&quot; before your employee<BR>')." number and use your FNB email address.";
			}
		}
		
		return $returnArray;
	}
	
	/*
	*
	* RM login
	*/
	public function RM_Login($rm_email,$rm_fnumber) {
		global $_Project_db;
		global $_db_;
		
		$returnArray = array();
		// Clean Fnumber
		//$rm_fnumber = strtolower($rm_fnumber);
		//$rm_fnumber = str_replace("f","",$rm_fnumber);
		//$rm_fnumber = str_replace(" ","",$rm_fnumber);
		//$rm_fnumber = trim($rm_fnumber);
		//$rm_fnumber = intval($rm_fnumber);
		// Clean other
		$rm_email = strtolower($rm_email);
		
		$_Project_db->where('fnumber',$rm_fnumber);
		$existing_rms = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existing_rms) && count($existing_rms) >= 1) {
			if ($existing_rms[0]['email'] == $rm_email) {
				$returnArray = $existing_rms[0];
			}else {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "*The F-number does not match the email address associated with it.";
			}
		}else {
			$returnArray['error'] = true;
			$returnArray['errormessage'] = "*The F-number has not been registered yet.";
		}
		return $returnArray;
	}
	
	/*
	*
	* RM register
	*/
	public function RM_Register($rm_name,$rm_fnumber,$rm_email,$rm_region) {
		global $_Project_db;
		global $_db_;
		$returnArray = array();
		// Clean Fnumber
		//$rm_fnumber = strtolower($rm_fnumber);
		//$rm_fnumber = str_replace("f","",$rm_fnumber);
		//$rm_fnumber = str_replace(" ","",$rm_fnumber);
		//$rm_fnumber = trim($rm_fnumber);
		//$rm_fnumber = intval($rm_fnumber);
		// Clean other
		$rm_email = strtolower($rm_email);
		$rm_email = trim($rm_email);
		$rm_name = trim($rm_name);
		// Check if someone else is not already registered
		$_Project_db->where('email',$rm_email);
		$existingRMs = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existingRMs) && count($existingRMs) >= 1  && substr($rm_fnumber, 0, 1 ) != "0") {
			$returnArray['error'] = true;
			$returnArray['errormessage'] = "*Someone else is already registered with the same email address. Please check your email and F-number again or contact your manager for assistance.";
		}else {
			
			// Check if someone else is not already registered with same fnumber
			$_Project_db->where('fnumber',$rm_fnumber);
			$existingFregs = $_Project_db->get($_db_['table']['rms'],'id');
			if (is_array($existingFregs) && count($existingFregs) >= 1  && substr($rm_fnumber, 0, 1 ) != "0") {
				$returnArray['error'] = true;
				$returnArray['errormessage'] = "*Someone else is already registered with the same F-number. Please check your email and F-number again or contact your manager for assistance.";
			}else {
				// check for existing rm fnumbers
				$_Project_db->where('fnumber',$rm_fnumber);
				$existingFs = $_Project_db->get($_db_['table']['fnumbers'],'fnumber');
				if (is_array($existingFs) && count($existingFs) >= 1  && substr($rm_fnumber, 0, 1 ) != "0") {
					$fnumber_id = $existingFs[0]['id'];
					$insertArray = array(
						'name' => $rm_name,
						'fnumber' => $rm_fnumber,
						'fnumber_id' => $fnumber_id,
						'email' => $rm_email,
						'region' => $rm_region
					);
					if ($_Project_db->insert($_db_['table']['rms'],$insertArray)) {
						$insert_id = $_Project_db->insert_id();
						$returnArray['name'] = $rm_name;
						$returnArray['fnumber'] = $rm_fnumber;
						$returnArray['fnumber_id'] = $fnumber_id;
						$returnArray['email'] = $rm_email;
						$returnArray['region'] = $rm_region;
						$returnArray['id'] = $insert_id;
					}else {
						$returnArray['error'] = true;
						$returnArray['errormessage'] = "*System error, please try again.";
					}
				}else {
					$returnArray['error'] = true;
					$returnArray['errormessage'] = "*To register, please remove the ".urlencode('&quot;<strong>F</strong>&quot; before your employee ')." number and use your FNB email address.";
				}
			}
		}
		
		return $returnArray;
	}
	
	/*
	*
	* RM Security Check in
	*/
	public function rmCheckIn() {
		global $_Project_db;
		global $_db_;
		//echo "SESSION ID: ".$_SESSION['rm_info']['id'];
		$_Project_db->where('id',$_SESSION['rm_info']['id']);
		$existing_rms = $_Project_db->get($_db_['table']['rms'],'id');
		if (is_array($existing_rms) && count($existing_rms) >= 1) {
			if ($existing_rms[0]['fnumber'] == $_SESSION['rm_info']['fnumber'] && $existing_rms[0]['email'] == $_SESSION['rm_info']['email']) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public function generateInviteCode() {
		return md5($_SERVER['REMOTE_ADDR']);
	}
	
	public function checkClientInvite($email) {
		global $_Project_db;
		global $_db_;
		
		$_Project_db->where('client_email',$email);
		$existingEmail = $_Project_db->get($_db_['table']['rm_invites'],'id');
		if (is_array($existingEmail) && count($existingEmail) >= 1) {
			return false;
		}else {
			return true;
		}
	}
	
	public function checkClientEntries($email) {
		global $_Project_db;
		global $_db_;
		
		$_Project_db->where('email',$email);
		$existingEmail = $_Project_db->get($_db_['table']['entries']);
		if (is_array($existingEmail) && count($existingEmail) >= 1) {
			return $existingEmail[0];
		}else {
			return "TRUE";
		}
	}
	

	
	public function rsvpInvite($unique_code,$rsvpStatus) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('unique_code',$unique_code);
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites']);
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			$invite_id = $existingInvites[0]['id'];
			$rm_id = $existingInvites[0]['rm_id'];
			$invite_email = $existingInvites[0]['client_email'];
			$insertArray = array(
				'accept_date' => $_DateTime->vandag(),
				'accept_time' => $_DateTime->tyd(),
				'accept_code' => $unique_code,
				'status' => $rsvpStatus,
				'invite_id' => $invite_id,
				'rm_id' => $rm_id
			);
			if ($_Project_db->insert($_db_['table']['invite_accepts'],$insertArray)) {
				$insert_id = $_Project_db->insert_id();
				$returnArray['email'] = $invite_email;
				$returnArray['invite_id'] = $invite_id;
				$returnArray['rm_id'] = $rm_id;
				$returnArray['success'] = true;
				$returnArray['accept_id'] = $insert_id;
				$returnArray['id'] = $insert_id;
			}else {
				$returnArray['error'] = "DB insert Fail";
			}
		}else {
			$returnArray['error'] = "No Existing Invites Match.";
		}
		
		return $returnArray;
	}

	/*
	*
	* Update RM invite after form SUMISSION
	*/
	public function acceptRMInvite($invite_id,$status) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		// Get accept details
		$_Project_db->where('id',$invite_id);
		$acceptedInvites = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
		if (is_array($acceptedInvites) && count($acceptedInvites) >= 1) {
			$accInvite = $acceptedInvites[0];
			
				// Update Accepted invite
				$updateAccepted = array(
					'accept_date' => $_DateTime->vandag(),
					'accept_time' => $_DateTime->tyd(),
					'status' => $status
				);
				$_Project_db->where('id',$invite_id);
				$_Project_db->update($_db_['table']['invite_accepts'],$updateAccepted);
				
				// Update original RM invite
				$updatInvite = array(
					'invite_acc_id' => $invite_id,
					'status' => $status
				);
				$_Project_db->where('id',$accInvite['invite_id']);
				$_Project_db->update($_db_['table']['rm_invites'],$updatInvite);

		}else {
			$returnArray['error'] = "No Existing Invites Match.";
		}
		
		return $returnArray;
	}
	
	
	
	/*
	*
	* GET user info
	*/
	public function getEntryUser($user_id) {
		global $_Project_db;
		global $_db_;
		
		$_Project_db -> where('id',$user_id);
		$userInfo = $_Project_db -> get($_db_['table']['entry_users'],'-id');
		if (isset($userInfo['id']) && $userInfo['id'] >= 1) {
			return $userInfo;
		}else { return false; }
	}
	
	
	
	
	/*
	*
	* Link invite to new User
	*/
	public function linkInvite($user_id,$invite_id) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		
		// Check if user is not already linked to an invite
		$user = $this -> getEntryUser($user_id);
		if ($user != false) {
			if (isset($user['invite_id']) && $user['invite_id'] >= 1) {
				// do nothing
				return false;
			}else {
				$updateUser = array(
					'invite_id' => $invite_id
				);
				$_Project_db -> where('id',$user_id);
				if ($_Project_db -> update($_db_['table']['entry_users'],$updateUser)) {
					$updateData = array(
						'user_id' => $user_id
					);
					$_Project_db->where('id',$invite_id);
					if ($_Project_db->update($_db_['table']['invite_accepts'],$updateData)) {
						$_SESSION['accept_id'] == NULL;
						return true;
					}
				}else {
					return false;
				}
			}
		}
	}
	
	/*
	*
	* Match RM with Entry User
	*/
	public function matchRMandUser($user_id,$user_email,$invite_status) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		$user_email = trim(strtolower($user_email));
		// Check if user email is in the invites
		$_Project_db -> where('client_email',$user_email);
		$existingInvites = $_Project_db -> get($_db_['table']['rm_invites'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			// Check if invite has already been accepted
			$thisInvite = $existingInvites[0];
			if (isset($thisInvite['invite_acc_id']) && $thisInvite['invite_acc_id'] >= 1) {
				// Get accepted invite data
				$_Project_db -> where('id',$thisInvite['invite_acc_id']);
				$existingAccInvite = $_Project_db -> get($_db_['table']['invite_accepts'],'-id');
				if (is_array($existingAccInvite) && count($existingAccInvite) >= 1) {
					$returnArray['invite_data'] = $existingAccInvite[0];
					$this -> acceptRMInvite($existingAccInvite[0]['id'],$invite_status);
					$this -> linkInvite($user_id,$existingAccInvite[0]['id']);
				}
				$returnArray['already_accepted'] = true;
				return $returnArray;
			}else {
				// Create Accepted Invite
				$returnArray['already_accepted'] = false;
				$rsvpInvite = $this->rsvpInvite($thisInvite['unique_code'],$invite_status);
				if (isset($rsvpInvite['accept_id']) && $rsvpInvite['accept_id'] >= 1) {
					$this -> acceptRMInvite($rsvpInvite['accept_id'],$invite_status);
					$this -> linkInvite($user_id,$rsvpInvite['accept_id']);
					$returnArray['invite_data'] = $rsvpInvite;
					return $returnArray;
				}else {
					return false;
				}
			}
		}else {
			return false;
		}
	}
	
	/*
	*
	* Check if user entries are related to an invite
	*/
	public function isEntryAnInvite($user_id,$user_email,$invite_status) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		$user_email = trim(strtolower($user_email));
		// Check if user email is in the invites
		$_Project_db -> where('client_email',$user_email);
		$existingInvites = $_Project_db -> get($_db_['table']['rm_invites'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			// Check if invite has already been accepted
			$thisInvite = $existingInvites[0];
			if (isset($thisInvite['invite_acc_id']) && $thisInvite['invite_acc_id'] >= 1) {
				// Get accepted invite data
				$_Project_db -> where('id',$thisInvite['invite_acc_id']);
				$existingAccInvite = $_Project_db -> get($_db_['table']['invite_accepts'],'-id');
				if (is_array($existingAccInvite) && count($existingAccInvite) >= 1) {
					$returnArray['invite_data'] = $existingAccInvite[0];
					$this -> acceptRMInvite($existingAccInvite[0]['id'],$invite_status);
					$this -> linkInvite($user_id,$existingAccInvite[0]['id']);
				}
				$returnArray['already_accepted'] = true;
				return $returnArray;
			}else {
				// Create Accepted Invite
				$returnArray['already_accepted'] = false;
				$rsvpInvite = $this->rsvpInvite($thisInvite['unique_code'],$invite_status);
				
				if (isset($rsvpInvite['accept_id']) && $rsvpInvite['accept_id'] >= 1) {
					$this -> acceptRMInvite($rsvpInvite['accept_id'],$invite_status);
					$this -> linkInvite($user_id,$rsvpInvite['accept_id']);
					$returnArray['invite_data'] = $rsvpInvite;
					return $returnArray;
				}else {
					return false;
				}
				
				
			}
		}else {
			return false;
		}
	}
	
	/*
	*
	* Get RM from InviteID
	*/
	public function getInviteRMdetails($invite_id) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$invite_id = intval($invite_id);
		$returnArray = array();
		// Get accept details
		$_Project_db->where('id',$invite_id);
		$acceptedInvites = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
		if (is_array($acceptedInvites) && count($acceptedInvites) >= 1) {
			$rm_id = $acceptedInvites[0]['rm_id'];
			$_Project_db->where('id',$rm_id);
			$inviteRMs = $_Project_db->get($_db_['table']['rms'],'-id');
			if (is_array($inviteRMs) && count($inviteRMs) >= 1) {
				$inviteRM = $inviteRMs[0];
				return $inviteRM;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public function checkAndAcceptInvite($clientEmail) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		$_Project_db->where('client_email',$clientEmail);
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites'],'-id');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			$invite_id = $existingInvites[0]['id'];
			$unique_code = $existingInvites[0]['unique_code'];
			$_Project_db->where('invite_id',$invite_id);
			$exInviteAccepts = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
			if (is_array($exInviteAccepts) && count($exInviteAccepts) >= 1) {
				$this->acceptInvite($invite_id);
			}else {
				$rsvpResult = $this->rsvpInvite($unique_code,'ACCEPTED');
			}
			return true;
		}else {
			return false;
		}
	}

	/*
	*
	* Get the status of an RM invite
	*/
	public function getInviteStatus($inviteId,$accInvite_id = NULL) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		
		if ($accInvite_id == NULL) {
			$_Project_db->where('invite_id',$inviteId);
		}else if ($accInvite_id >= 1) {
			$_Project_db->where('id',$accInvite_id);
		}
		$accInvites = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
		if (isset($accInvites[0]) && is_array($accInvites[0])) {
			/*
			if ($invite[0]['status'] == "RSVP" || $invite[0]['status'] == "NEW ENTRY" || $invite[0]['status'] == "SAVED" ) {
				return "Entry incomplete";
			}else if ($invite[0]['status'] == "SUBMITTED") {
				return "Entry complete";
			}else {
				return "Unknown action";
			}
			*/
			return $accInvites[0]['status'];
		}else {
			return "NOT RESPONDED";
		}
	}
	
	
	public function getAcceptStatus($inviteId) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		// CHECK FOR accept
		$status = false;
		$_Project_db->where('invite_id',$inviteId);
		$accepts = $_Project_db->get($_db_['table']['invite_accepts'],'-id');
		if (count($accepts) >= 1) {
			$status = "RSVP";
			foreach($accepts as $accept) {
				if ($accept['status'] != "SUBMITTED") {
					$status = $accept['status'];
				}else if ($accept['status'] == "SUBMITTED") {
					$status = $accept['status'];
					break;
				}else {
					$status = "Unknown action";
				}
			}
			return $status;
		}else {
			return false;
		}
	}
	
	
	public function getAllStatus($inviteId,$email) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		

			// Check for entry in entries table
			$_Project_db -> where('email',$email);
			$existingEntryUser = $_Project_db -> get($_db_['table']['entry_users'],'-id');
			if (count($existingEntryUser) >= 1) {
				//$entry_id = $existingEntries[0]['id'];
				//$_Project_db -> where('entry_id',$entry_id);
				//$existingUsers = $_Project_db -> get($_db_['table']['entry_users'],'-id');
				//if (count($existingUsers) >= 1) {
					$result = $existingEntryUser[0]['status'];
					if ($result == "RSVP" || $result == "REGISTERED" || $result == "SAVED" || $result == "NEW ENTRY") {
						$returnArray['status'] = "Entry incomplete";
						$returnArray['user_id'] = $existingEntryUser[0]['id'];
						$returnArray['entry_id'] = $existingEntryUser[0]['entry_id'];
					}else if ($result == "SUBMITTED") {
						$returnArray['status'] = "Entry complete";
						$returnArray['user_id'] = $existingEntryUser[0]['id'];
						$returnArray['entry_id'] = $existingEntryUser[0]['entry_id'];
					}else {
						$returnArray['status'] = "Unknown action";
						$returnArray['user_id'] = 0;
						$returnArray['entry_id'] = 0;
					}
					return $returnArray;
			}else {
				$returnArray['status'] = 'No Action';
				$returnArray['user_id'] = 0;
				$returnArray['entry_id'] = 0;
				
				return $returnArray;
			}

	}
	
	public function getRMInfo($rm_id) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$_Project_db -> where('id',$rm_id);
		$rms = $_Project_db -> get($_db_['table']['rms'],'-id');
		if (count($rms) >= 1) {
			return $rms[0];
		}else {
			return false;
		}
	}
	
	
	/*
	*
	* Register New Business Entry USer
	*/
	public function registerFNBuser($email,$password,$confirmpass) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		// Clean email
		$email = strtolower($email);
		$email = trim($email);
		$email = str_replace(" ","",$email);
		// Clean Password
		$password = trim($password);
		$confirmpass = trim($confirmpass);
		
		$_Project_db->where('email',$email);
		$existiong_users = $_Project_db->get($_db_['table']['entry_users'],'id');
		if (is_array($existiong_users) && count($existiong_users) >= 1) {
			$returnArray['existing_email'] = true;
		}else {
			// Create new user
			/**/
			// Password retype the same
			if ($password == $confirmpass) {
				if (is_string($password) && $password != "") {
					// Create password Hash
					$passwordHash = $_Project_db->create_hash($password);
					// Insert user into DB
					$insertData = array(
						'email' => $email,
						'password' => $passwordHash,
						'register_date' => $_DateTime->vandag(),
						'register_time' => $_DateTime->tyd(),
						'last_login_date' => $_DateTime->vandag(),
						'last_login_time' => $_DateTime->tyd(),
						'status' => 'REGISTERED',
						'entry_id' => 0
					);
					if ($_Project_db -> insert($_db_['table']['entry_users'],$insertData)) {
						$returnArray['register'] = true;
						// Login user
						if ($userLogin = $this->loginFNBuser($email,$password)) {
							if (isset($userLogin['entry_userinfo'])) {
								$returnArray['entry_userinfo'] = $userLogin['entry_userinfo'];
								$returnArray['signin'] = true;
							}else if (isset($userLogin['invalid_user'])) {
								$returnArray['invalid_user'] = true;
							}
						}else {
							$returnArray['signin'] = false;
						}
					}else {
						$returnArray['insert_error'] = true;
					}
					
				}else {
					$returnArray['password_not_valid'] = true;
				}
			}else {
				$returnArray['password_no_match'] = true;
			}
		}
		
		return $returnArray;
	}
	
	
	/*
	*
	* Log in Business Entry Form User
	*/
	public function loginFNBuser($email,$login_password) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$returnArray = array();
		// Clean email
		$email = strtolower($email);
		$email = trim($email);
		$email = str_replace(" ","",$email);
		// Clean Password
		$login_password = trim($login_password);
		
		$_Project_db->where('email',$email);
		$existiong_users = $_Project_db->get($_db_['table']['entry_users'],'id');
		if (is_array($existiong_users) && count($existiong_users) >= 1) {
			foreach($existiong_users as $userDATA) {
				$userPassword = $userDATA['password'];
				$validatedPass = $_Project_db->validate_password($login_password, $userPassword);
				if ($validatedPass == 1 && $userDATA['status'] != "DISABLED") {
					
					// Update last login
					$updateData = array(
						'last_login_date' => $_DateTime->vandag(),
						'last_login_time' => $_DateTime->tyd()
					);
					$_Project_db -> where('id',$userDATA['id']);
					$_Project_db -> update($_db_['table']['entry_users'],$updateData);
					
					
					$returnArray['signin']= true;
					$returnArray['entry_userinfo'] = $userDATA;
					$_SESSION['entry_userinfo'] = $userDATA;
				}else {
					$returnArray['signin'] = false;
					$_SESSION['entry_userinfo'] = NULL;
				}
			}
		}else {
			$returnArray['signin']= false;
			$returnArray['invalid_email'] = true;
		}
		
		return $returnArray;
	}
	
	
	public function saveEmailtoDB($email_type,$user_id,$emailMessage,$name_to,$email_to,$subject) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$insertData = array(
			'email' => $email_to,
			'name_to' => $name_to,
			'email_type' => $email_type,
			'subject' => $subject,
			'email_message' => $emailMessage,
			'send_date' => $_DateTime -> vandag(),
			'send_time' => $_DateTime -> tyd(),
			'user_id' => $user_id
		);
		if ($_Project_db -> insert($_db_['table']['emails_sent'],$insertData)) {
			//$insert_id = $_Project_db -> insert_id();
			return true;
		}else {
			return false;
		}
	}
	
	public function checkReminded($email_type,$email,$days) {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		$_Project_db -> where('email',$email);
		$existingReminders = $_Project_db -> get($_db_['table']['emails_sent'],'-id');
		
		$returnValue = 'DONTSEND';
		if (is_array($existingReminders) && count($existingReminders) >= 1) {
			// Check if reminder email is older than 30 days.
			$daysDif = $_DateTime -> datum_diff($existingReminders[0]['send_date']." ".$existingReminders[0]['send_time'], $_DateTime->vandag()." ".$_DateTime->tyd());
			if ($daysDif < $days && $existingReminders[0]['email_type'] == 'ENTRY REMINDER') {
				$returnValue = 'DONTSEND';
			}else if ($daysDif >= $days) {
				$returnValue = 'SEND';
			}
		}else {
			$returnValue = 'SEND';
		}
		
		return $returnValue;
	}
	
	public function getRMNames() {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		$existingRMs = $_Project_db -> get($_db_['table']['rms'],'id');
		
		$returnList = array();
		if (count($existingRMs)>=1) {
			foreach($existingRMs as $rm) {
				array_push($returnList,strtolower($rm['name']));
			}
			return $returnList;
		}else {
			return false;
		}
	}
	public function getAllInvites() {
		global $_Project_db;
		global $_db_;
		global $_DateTime;
		
		$existingInvites = $_Project_db -> get($_db_['table']['rm_invites'],'id');
		
		$returnList = array();
		if (count($existingInvites)>=1) {
			foreach($existingInvites as $invite) {
				$cname = strtolower($invite['client_company_name']);
				$cname = str_replace('cc','',$cname);
				$cname = str_replace('c.c.','',$cname);
				$cname = str_replace('(pty)','',$cname);
				$cname = str_replace('pty','',$cname);
				$cname = str_replace('ltd','',$cname);
				$cname = str_replace('co','',$cname);
				$cname = str_replace('.','',$cname);
				$cname = str_replace('-','',$cname);
				$cname = str_replace(' ','',$cname);
				$cname = str_replace('&','',$cname);
				$inviteinfo = array(
					'client_company_name' => $cname,
					'client_name' => strtolower($invite['client_name']),
					'id' => $invite['id'],
					'rm_id' => $invite['rm_id']
				);
				array_push($returnList,$inviteinfo);
			}
			return $returnList;
		}else {
			return false;
		}
	}
}

