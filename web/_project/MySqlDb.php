<?php

class MysqlDB {

	protected $_mysql;
	protected $_where = array();
	protected $_query;
	protected $_paramTypeList;
	protected $_tableList = array();
	protected $_table_prefix;
	protected $_insert_id;

	public function __construct($connArray) {
		$this->_mysql = new mysqli($connArray['host'], $connArray['username'], $connArray['password'], $connArray['db']) or die('There was a problem connecting to the database');
		// Save tablelist
		$this->_tableList = $this->_list_tables();
		$this->_table_prefix = $connArray['table_prefix'];
		$this->_setToUTF8();
	}
	/**
    *
    * @param NONE.
    * @description sets the connection to UTF8
    * @return NONE
    */
	protected function _setToUTF8() {
		if (!$this->_mysql->set_charset("utf8")) {
			//printf("Error loading character set utf8: %s\n", $this->_mysql->error);
		} else {
			//printf("Current character set: %s\n", $this->_mysql->character_set_name());
		}
	}
	
   /**
    *
    * @param string $query Contains a user-provided select query.
    * @param int $numRows The number of rows total to return.
    * @return array Contains the returned rows from the query.
    */
   public function query($query) 
   {
      $this->_query = filter_var($query, FILTER_SANITIZE_STRING);

      $stmt = $this->_prepareQuery();
      $stmt->execute();
      $results = $this->_dynamicBindResults($stmt);
      return $results;
   }

   /**
    * A convenient SELECT * function.
    *
    * @param string $tableName The name of the database table to work with.
    * @param int $numRows The number of rows total to return.
	* @return array Contains the returned rows from the select query.
	*/
	public function get($tableName, $order_by = NULL, $numRows = NULL) 
	{

		$this->_query = "SELECT * FROM $tableName";
		$stmt = $this->_buildQuery($numRows,$order_by);
		$stmt->execute();

		$results = $this->_dynamicBindResults($stmt);
	  
		// Clear values
		$this->_clear_protected_values();
	  
		return $results;
	}

   /**
    *
    * @param <string $tableName The name of the table.
    * @param array $insertData Data containing information for inserting into the DB.
    * @return boolean Boolean indicating whether the insert query was completed succesfully.
    */
   public function insert($tableName, $insertData) 
   {
		$this->_query = "INSERT into $tableName";
		$stmt = $this->_buildQuery(NULL, NULL, $insertData);
		$stmt->execute();
		// Clear values
		$this->_clear_protected_values();
		if ($stmt->affected_rows) {
			$this->_insert_id = $stmt->insert_id;
			return true;
		}else {
			return false;
		}
	  
   }
   /**
   *
   * @ return int value of the latest insert id
   */
   public function insert_id() {
	   return $this->_insert_id;
   }

   /**
    * Update query. Be sure to first call the "where" method.
    *
    * @param string $tableName The name of the database table to work with.
    * @param array $tableData Array of data to update the desired row.
    * @return boolean
    */
	public function update($tableName, $tableData) 
	{
		$this->_query = "UPDATE $tableName SET ";

		$stmt = $this->_buildQuery(NULL, NULL,$tableData);
		$stmt->execute();
		// Clear values
		$this->_clear_protected_values();

		//if ($stmt->affected_rows)
			return true;
	}

   /**
    * Delete query. Call the "where" method first.
    *
    * @param string $tableName The name of the database table to work with.
    * @return boolean Indicates success. 0 or 1.
    */
   public function delete($tableName) {
      $this->_query = "DELETE FROM $tableName";

      $stmt = $this->_buildQuery();
      $stmt->execute();
		
		// Clear values
		$this->_clear_protected_values();
		
      if ($stmt->affected_rows)
         return true;
		 
		
   }

   /**
    * This method allows you to specify a WHERE statement for SQL queries.
    *
    * @param string $whereProp A string for the name of the database field to update
    * @param mixed $whereValue The value for the field.
    */
   public function where($whereProp, $whereValue) 
   {
      $this->_where[$whereProp] = $whereValue;
   }


	/**
	* Clear the protected vars so that the functions can be used again without
	* any issues
	* return Boolean
	*/
	protected function _clear_protected_values() {
		$this->_paramTypeList = NULL;
		$this->_where = NULL;
		return true;
	}
   /**
    * This method is needed for prepared statements. They require
    * the data type of the field to be bound with "i" s", etc.
    * This function takes the input, determines what type it is,
    * and then updates the param_type.
    *
    * @param mixed $item Input to determine the type.
    * @return string The joined parameter types.
    */
	protected function _determineType($item) 
	{
	switch (gettype($item)) {
		case 'string':
            return 's';
            break;

		case 'integer':
            return 'i';
            break;

		case 'blob':
            return 'b';
            break;

		case 'double':
			return 'd';
			break;
		/*
		default:
			return 's';
			break;
		*/
		}
   }

   /**
    * Abstraction method that will compile the WHERE statement,
    * any passed update data, and the desired rows.
    * It then builds the SQL query.
    *
    * @param int $numRows The number of rows total to return.
    * @param array $tableData Should contain an array of data for updating the database.
    * @return object Returns the $stmt object.
    */
   protected function _buildQuery($numRows = NULL,$order_by = NULL, $tableData = false) 
   {
      $hasTableData = null;
      if (gettype($tableData) === 'array') {
         $hasTableData = true;
      }

      // Did the user call the "where" method?
      if (!empty($this->_where)) {
         $keys = array_keys($this->_where);
         $where_prop = $keys[0];
         $where_value = $this->_where[$where_prop];

         // if update data was passed, filter through
         // and create the SQL query, accordingly.
         if ($hasTableData) {
            $i = 1;
				$pos = strpos($this->_query, 'UPDATE');
				if ( $pos !== false) {
					foreach ($tableData as $prop => $value) {
						// determines what data type the item is, for binding purposes.
						$this->_paramTypeList .= $this->_determineType($value);
						// prepares the reset of the SQL query.
						if ($i === count($tableData)) {
							$this->_query .= $prop . " = ? WHERE " . $where_prop . "= " . $where_value;
						} else {
							$this->_query .= $prop . ' = ?, ';
						}

						$i++;
					}
				}
         } else {
            // no table data was passed. Might be SELECT statement.
            $this->_paramTypeList = $this->_determineType($where_value);
            $this->_query .= " WHERE " . $where_prop . "= ?";
         }
      }

      // Determine if is INSERT query
      if ($hasTableData) {
         $pos = strpos($this->_query, 'INSERT');

         if ($pos !== false) {
            //is insert statement
            $keys = array_keys($tableData);
            $values = array_values($tableData);
            $num = count($keys);

            // wrap values in quotes
            foreach ($values as $key => $val) {
               $values[$key] = "'{$val}'";
               $this->_paramTypeList .= $this->_determineType($val);
            }
            $this->_query .= '(' . implode($keys, ', ') . ')';
            $this->_query .= ' VALUES(';
            while ($num !== 0) {
               ($num !== 1) ? $this->_query .= '?, ' : $this->_query .= '?)';
               $num--;
            }
         }
      }
		
		// Did the user call a ORDER BY
		if (isset($order_by) && $order_by != NULL) {
			$this->_query .= " ORDER BY " . $order_by;
		}
      // Did the user set a limit
      if (isset($numRows)) {
         $this->_query .= " LIMIT " . (int) $numRows;
      }
		
		
      // Prepare query
      $stmt = $this->_prepareQuery();

      // Bind parameters
      if ($hasTableData) {
         $args = array();
         $args[] = $this->_paramTypeList;
         foreach ($tableData as $prop => $val) {
            $args[] = &$tableData[$prop];
         }
		 //$this->debug($args);
         call_user_func_array(array($stmt, 'bind_param'), $args);
      } else {
         if ($this->_where)
            $stmt->bind_param($this->_paramTypeList, $where_value);
      }

      return $stmt;
   }

   /**
    * This helper method takes care of prepared statements' "bind_result method
    * , when the number of variables to pass is unknown.
    *
    * @param object $stmt Equal to the prepared statement object.
    * @return array The results of the SQL fetch.
    */
   protected function _dynamicBindResults($stmt) 
   {
      $parameters = array();
      $results = array();

      $meta = $stmt->result_metadata();

      while ($field = $meta->fetch_field()) {
         $parameters[] = &$row[$field->name];
      }

      call_user_func_array(array($stmt, 'bind_result'), $parameters);

      while ($stmt->fetch()) {
         $x = array();
         foreach ($row as $key => $val) {
            $x[$key] = $val;
         }
         $results[] = $x;
      }
      return $results;
   }


   /**
   * Method attempts to prepare the SQL query
   * and throws an error if there was a problem.
   */
   protected function _prepareQuery() 
   {
      if (!$stmt = $this->_mysql->prepare($this->_query)) {
         trigger_error("Problem preparing query", E_USER_ERROR);
      }
      return $stmt;
   }


   public function __destruct() 
   {
		$this->_mysql->close();
   }
   
   public function debug($var) {
	   echo '<pre>';
	   print_r($var);
	   echo '</pre>';
   }
   
   /**
   * Query the database for all the available tables
   * @return array an Array object with all the table names
   */
	protected function _list_tables()
	{
		$tableList = array();
		$res = mysqli_query($this->_mysql,"SHOW TABLES");
		while($cRow = mysqli_fetch_array($res))
		{
			$tableList[] = $cRow[0];
		}
		return $tableList;
	}
	public function getTableList() {
		return $this->_tableList;
	}
	public function getTable($name) {
		$sname = $this->_table_prefix.$name;
		$foundname = NULL;
		foreach ($this->_tableList as $tablename) {
			if ($tablename == $sname) {
				$foundname = $tablename;
				break;
			}
		}
		
		if ($foundname != NULL) {
			return $foundname;
		}else {
			return false;
		}
	}
	
	/**
	* Give valid table name to the name that the user supply
	* @param string String containing the basic name of the table without prefix
	* @return string or Boolean Returns a string with the full table name or false
	*/
	public function tableName($name) {
		$lookfor = $this->_table_prefix.$name;
		$found = false;
		foreach($this->tableList as $table) {
			if ($table == $lookfor) {
				$found = $lookfor;
			}
		}
		if ($found == $lookfor) {
			return $lookfor;
		}else {
			return false;
		}
	}
	
	
	
	/*
	 * Password hashing with PBKDF2.
	 * Author: havoc AT defuse.ca
	 * www: https://defuse.ca/php-pbkdf2.htm
	 */
	
	// These constants may be changed without breaking existing hashes.
	const PBKDF2_HASH_ALGORITHM = "sha256";
	const PBKDF2_ITERATIONS = 1000;
	const PBKDF2_SALT_BYTE_SIZE = 24;
	const PBKDF2_HASH_BYTE_SIZE = 24;
	
	const HASH_SECTIONS = 4;
	const HASH_ALGORITHM_INDEX = 0;
	const HASH_ITERATION_INDEX = 1;
	const HASH_SALT_INDEX = 2;
	const HASH_PBKDF2_INDEX = 3;
	
	public function create_hash($password)
	{
		// format: algorithm:iterations:salt:hash
		$salt = base64_encode(mcrypt_create_iv(self::PBKDF2_SALT_BYTE_SIZE, MCRYPT_DEV_URANDOM));
		return self::PBKDF2_HASH_ALGORITHM . ":" . self::PBKDF2_ITERATIONS . ":" .  $salt . ":" .
			base64_encode($this->_pbkdf2(
				self::PBKDF2_HASH_ALGORITHM,
				$password,
				$salt,
				self::PBKDF2_ITERATIONS,
				self::PBKDF2_HASH_BYTE_SIZE,
				true
			));
	}
	
	public function validate_password($password, $correct_hash)
	{
		$params = explode(":", $correct_hash);
		if(count($params) < self::HASH_SECTIONS)
		   return false;
		$pbkdf2 = base64_decode($params[self::HASH_PBKDF2_INDEX]);
		return $this->_slow_equals(
			$pbkdf2,
			$this->_pbkdf2(
				$params[self::HASH_ALGORITHM_INDEX],
				$password,
				$params[self::HASH_SALT_INDEX],
				(int)$params[self::HASH_ITERATION_INDEX],
				strlen($pbkdf2),
				true
			)
		);
	}
	
	public function randomPassword($charCount) {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array(); //remember to declare $pass as an array
		$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
		for ($i = 0; $i < $charCount; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}
		return implode($pass); //turn the array into a string
	}
	
	// Compares two strings $a and $b in length-constant time.
	protected function _slow_equals($a, $b)
	{
		$diff = strlen($a) ^ strlen($b);
		for($i = 0; $i < strlen($a) && $i < strlen($b); $i++)
		{
			$diff |= ord($a[$i]) ^ ord($b[$i]);
		}
		return $diff === 0;
	}
	
	/*
	 * PBKDF2 key derivation function as defined by RSA's PKCS #5: https://www.ietf.org/rfc/rfc2898.txt
	 * $algorithm - The hash algorithm to use. Recommended: SHA256
	 * $password - The password.
	 * $salt - A salt that is unique to the password.
	 * $count - Iteration count. Higher is better, but slower. Recommended: At least 1000.
	 * $key_length - The length of the derived key in bytes.
	 * $raw_output - If true, the key is returned in raw binary format. Hex encoded otherwise.
	 * Returns: A $key_length-byte key derived from the password and salt.
	 *
	 * Test vectors can be found here: https://www.ietf.org/rfc/rfc6070.txt
	 *
	 * This implementation of PBKDF2 was originally created by https://defuse.ca
	 * With improvements by http://www.variations-of-shadow.com
	 */
	protected function _pbkdf2($algorithm, $password, $salt, $count, $key_length, $raw_output = false)
	{
		$algorithm = strtolower($algorithm);
		if(!in_array($algorithm, hash_algos(), true))
			die('PBKDF2 ERROR: Invalid hash algorithm.');
		if($count <= 0 || $key_length <= 0)
			die('PBKDF2 ERROR: Invalid parameters.');
	
		$hash_length = strlen(hash($algorithm, "", true));
		$block_count = ceil($key_length / $hash_length);
	
		$output = "";
		for($i = 1; $i <= $block_count; $i++) {
			// $i encoded as 4 bytes, big endian.
			$last = $salt . pack("N", $i);
			// first iteration
			$last = $xorsum = hash_hmac($algorithm, $last, $password, true);
			// perform the other $count - 1 iterations
			for ($j = 1; $j < $count; $j++) {
				$xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
			}
			$output .= $xorsum;
		}
	
		if($raw_output)
			return substr($output, 0, $key_length);
		else
			return bin2hex(substr($output, 0, $key_length));
	}
}

