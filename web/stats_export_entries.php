<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - All Entries";
$project_page['file_name'] = "stats_export_entries.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

include('_include/entry-form-field-values.php');

// Create header row
$header = array();
foreach($inputLabels as $key => $value) {
	$addMe[$key] = $key;
}
//array_push($header,$addMe);
$addedHeader = array(
	'saved_step' => 'saved_step',
	'entry_date' => 'entry_date',
	'entry_time' => 'entry_time',
	'status' => 'status',
	'user_id' => 'user_id',
	'id' => 'id'
);
$header = array_merge($addMe,$addedHeader);
//array_push($header,array('saved_step','entry_date','entry_time','status','user_id','id'));
//$_Project_db -> debug($header);

$_AllEntryUsers = $_Project_db -> get($_db_['table']['entry_users'],'id');
$AllEntries = array();
array_push($AllEntries,$header);
//$_Project_db -> debug($header);
if (is_array($_AllEntryUsers) && count($_AllEntryUsers) >= 1) {
	foreach($_AllEntryUsers as $entryUser) {
		$_Project_db -> where('id',$entryUser['entry_id']);
		$entry = $_Project_db -> get($_db_['table']['entries'],'-id');
		if (count($entry) >= 1) {
			$uEntry = $entry[0];
			$pushEntry = array();
			foreach($header as $key => $value) {
				if (isset($uEntry[$key]) && $uEntry[$key] != NULL) {
					if(strlen($uEntry[$key]) >= 10) {
						$pushEntry[$key] = (string) $uEntry[$key]." ";
					}else {
						$pushEntry[$key] = $uEntry[$key];
					}
				}else {
					$pushEntry[$key] = "";
				}
			}
			array_push($AllEntries,$pushEntry);
		}
	}
}

require '_include/php-excel.class.php';
$mydata = array(1 => 'Etiket', 'Etiket Brand Design');
$xls = new Excel_XML;
$xls->addWorksheet('AllEntries', $AllEntries);
$xls->sendWorkbook('assets/AllEntries'.$vandag.'.xls');

?>
