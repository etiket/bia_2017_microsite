<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Relationship manager client invite form.";
$project_page['file_name'] = "rm_client_invites.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);
include('_include/cd.php');

// END OF CAMPAIGN
/*
session_write_close();
header('Location: '.$project_data['pages']['landing']);
exit;
*/
// END --


if (isset($_SESSION['rm_info'])) {
	//$_Project_db->debug($_SESSION['rm_info']);
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['rm_login'].'?errormessage=Please login or register before accessing the nomination form.');
	exit;
}

include('_include/rm-emailers.php');

require '_phpMailer/PHPMailerAutoload.php';
function sendPHPMail($message,$subject,$toAddress,$toName) {
	$mail = new PHPMailer;
	// $mail->isSMTP();                                      // Set mailer to use SMTP
	// $mail->Host = 'smtp.fnbbusinessinnovationawards.co.za';  // Specify main and backup SMTP servers
	// $mail->SMTPAuth = true;                               // Enable SMTP authentication
	// $mail->Username = 'competition@fnbbusinessinnovationawards.co.za';                 // SMTP username
	// $mail->Password = 'Design4etiket';                           // SMTP password
	//$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('competition@fnbbusinessinnovationawards.co.za', 'FNB Business INNOVATION Awards');
	$mail->addAddress('arno@etiket.co.za', 'Arno Cilliers');     // Add a recipient
	$mail->addAddress($toAddress, $toName);               // Name is optional
	$mail->addReplyTo('innovationawards@fnb.co.za', 'Innovation Awards');

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->AltBody = '';

	if(!$mail->send()) {
	    // echo 'Message could not be sent.';
	    // echo 'Mailer Error: ' . $mail->ErrorInfo;
	    return false;
	} else {
	    // echo 'Message has been sent';
	    return true;
	}
}



$image_folder = $project_data['full_address']."images/";
// SAVE DATA
if (isset($_POST['btn_submit_form'])) {
	$errorArray = array();
	if (isset($_POST['client_name'])) { $client_names = $_POST['client_name']; }
	if (isset($_POST['company_name'])) { $company_names = $_POST['company_name']; }
	if (isset($_POST['client_email'])) { $client_emails = $_POST['client_email']; }
	
	//if ($_ProjectF->rmCheckIn()) {
		if (is_array($client_names)) {
			$i = 0;
			foreach($client_names as $name) {
				$client_email = strtolower($client_emails[$i]);
				$client_email = trim($client_email);
				$unique_code = $_ProjectF->generateInviteCode().$i.rand(0,99999).$i.rand(0,99999);
				if ($_ProjectF->checkClientInvite($client_email)) {
					$insertArray = array(
						'rm_id' => $_SESSION['rm_info']['id'],
						'rm_fnumber' => $_SESSION['rm_info']['fnumber'],
						'rm_email' => strtolower($_SESSION['rm_info']['email']),
						'client_name' => $name,
						'client_company_name' => $company_names[$i],
						'client_email' => $client_email,
						'unique_code' => $unique_code,
						'invite_date' => $vandag,
						'invite_time' => $tyd
					);
					//$_Project_db->debug($insertArray);
					if ($_Project_db->insert($_db_['table']['rm_invites'],$insertArray)) {
						$insert_id = $_Project_db->insert_id();
						if ($_ProjectF -> checkClientEntries($client_email) == "TRUE") {
							// Email the fucker
							if (sendInvitation($name,$client_email,$company_names[$i],$unique_code,$_SESSION['rm_info']['name'],$_SESSION['rm_info']['email'])) {
								$successArray[] = "Email Sent and Invite Done for: ".$name;
								sleep(1);
								if (sendRMConfirmation($_SESSION['rm_info']['name'],$_SESSION['rm_info']['email'],$company_names[$i],$client_email)) {

								}else {
									$SaveError = array(
										'error_type' => 'sendRMConfirmation',
										'error_description' => 'client_email: '.$client_email.' , company_names[$i]: '.$company_names[$i].' , SESSION[rm_info][name]: '.$_SESSION['rm_info']['name'].' , SESSION[rm_info][email]: '.$_SESSION['rm_info']['email'],
										'error_page' => $project_page['file_name'],
										'user_id' => $_SESSION['rm_info']['id'],
										'user_type' => $_db_['table']['rms'],
										'error_datetime' => $vandag
									);
									$_Project_db->insert($_db_['table']['errors'],$SaveError);
								}
							}else {
								$errorArray[] = "sendInvitation Failed";
								$SaveError = array(
									'error_type' => 'sendInvitation',
									'error_description' => 'Name: '.$name.' , client_email: '.$client_email.' , company_names[$i]: '.$company_names[$i].' , unique_code: '.$unique_code.' , SESSION[rm_info][name]: '.$_SESSION['rm_info']['name'].' , SESSION[rm_info][email]: '.$_SESSION['rm_info']['email'],
									'error_page' => $project_page['file_name'],
									'user_id' => $_SESSION['rm_info']['id'],
									'user_type' => $_db_['table']['rms'],
									'error_datetime' => $vandag
								);
								$_Project_db->insert($_db_['table']['errors'],$SaveError);
							}
						}else {
							// Get Status from clientEntries
							$entryInfo = $_ProjectF -> checkClientEntries($client_email);
							$_Project_db -> where('id',$entryInfo['user_id']);
							$entryUser = $_Project_db -> get($_db_['table']['entry_users']);
							if (isset($entryUser) && $entryUser['id'] >= 1) {
								if ($entryUser['invite_id'] >= 1) {
									// REMOVE INVITE
									$_Project_db -> where('id',$insert_id);
									$_Project_db -> delete($_db_['table']['rm_invites']);
								}else {
									// RSVP invite
									$AcceptInvite = $_ProjectF -> rsvpInvite($unique_code,$entryInfo['status']);
									// Link User and Invite
									$_ProjectF -> linkInvite($entryInfo['user_id'],$AcceptInvite['accept_id']);
									$AcceptInvite = NULL;
								}
							}
							$entryInfo = NULL;
							
						}
					}else {
						$errorArray[] = "Insert into Invites Failed";
					}
				}else {
					//$errorArray[] = "Check for Client Invites Returned with 1 or more results so it failed.";
				}
				$i++;
			}
		}else {
			$errorArray[] = "Client names is not an Array.";
		}
		
		if (count($errorArray) >= 1) {
			
		}else {
			session_write_close();
			header('Location: '.$project_data['pages']['rm_thankyou'].'?success=Invitations sent');
			exit;
		}
	//}else {
		//$errorArray[] = "RM information Check in is not valid.";
	//}
}
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FNB Business INNOVATION Awards</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>


</head>

<body id="client_invite_page">
<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>


<div class="form_container client_list_container">
	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Nominate your clients</h1>
    <p><?php
		if (isset($_SESSION['rm_info'])) {
			//$_Project_db->debug($_SESSION['rm_info']);
		}
		if (isset($errorArray)) {
			foreach ($errorArray as $error) {
				echo $error."<BR>";
			}
		}
		?></p>
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" name="form_entryform" id="form_clients_list">
    	<!-- LAYER 1 -->
        <div class="client_container">
            <div class="client_col1">
                <input type="text" name="client_name[]" maxlength="100" class="input_text full_text_width" placeholder="Client name and surname*" value="Client name and surname*">      
            </div> <!-- client_col1 -->
            
            <div class="client_col2">
                <input type="text" name="company_name[]" maxlength="100" class="input_text full_text_width" placeholder="Client business name*" value="Client business name*">
            </div> <!-- client_col2 -->
            
            <img src="<?php echo $image_folder; ?>rm_remove_client_btn.png" class="rm_remove_client" onClick="javascript:removeClient(this);" />
            
            <div class="client_col3">
                <input type="text" name="client_email[]" maxlength="100" class="input_text full_text_width" placeholder="Client email address*" value="Client email address*">
            </div> <!-- client_col3 -->
        </div> <!-- client container -->
        <div class="clear"></div>
        <div id="more_clients">
        </div> <!-- more clients -->
        <img src="<?php echo $image_folder; ?>rm_add_client_btn.PNG" class="rm_add_client RIGHT" id="rm_add_client_btn" />
        <p class="RIGHT" id="add_client_line_p">Add line</p>
        <div class="clear"></div>
        <input type="hidden" name="btn_submit_form" value="Submit">
        <!-- <input type="button" name="btn_submit_form" value="Submit" id="btn_submit_form" class="square_submit_btn LEFT"/> -->
        <button class="LEFT square_btn btn_orange_arrow_right" id="btn_submit_form">
			<p>Submit</p>
		</button>
	</form>
    <div class="clear">&nbsp;</div>
    <p class="validation_error_message COPY_BOLD" id="form_validate_error"><?php
    	if (isset($_GET['errormessage'])) {
			echo $_GET['errormessage'];
		}
	?></p>
    <div class="clear">&nbsp;</div>
</div> <!-- form container -->
	
<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->
<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 
<img src="<?php echo $image_folder; ?>/web_right-bottom-copy-white.png" class="web_right_bottom_img"/>
<!-- DISCLAIMER FOOTER -->
<div class="rm_footer_container">
	<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><a href="<?php echo $project_data['pages']['rm_terms']; ?>" class="COPY_REGULAR_ITALIC BLACK_COPY" style="font-size:7px;text-decoration:underline;" target="_blank"><strong>Terms and conditions.</strong></a><BR><span style="font-size:5px;"><strong>First National Bank - a division of FirstRand Bank Limited.</strong> An Authorised Financial Services and Credit Provider (NCRCP20).</span></p>
</div> <!-- footer container -->

<?php
// ADD RM navigation here
include('_include/rm_navigation.php');
?>
    
    <!-- LIGHTBOX STUFFS -->
    <div class="lightbox-mask" id="lightbox_mask">
    	<img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_form_topLogo">
    </div>
    <div class="lightbox" id="user_form_container">
        <div class="lightbox_content">
            <p class="form_error_copy COPY_REGULAR">Please fill in all the required fields (*) to complete your submission.</p>
            <div class="square_btn back_btn" id="close_form_btn"><p class="square_btn_p">Back</p></div>
            <div class="clear"></div>
        </div> <!-- lightbox content -->
    </div> <!-- lightbox -->

    
	<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/frame_functions.js"></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/fnb_functions.js"></script>
    <script type="text/javascript" src="<?php echo $project_data['full_address']; ?>js/rm-functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	$('input:text').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('');
            }
        }).blur(function () {
            if ($(this).val() == "") {
                $(this).val($(this).attr('placeholder'))
            }
        }
    );
	$('textarea').focus(function () {
            if ($(this).html() === $(this).attr('placeholder')) {
                $(this).html('');
            }
        }).blur(function () {
            if ($(this).html() == "") {
                $(this).html($(this).attr('placeholder'))
            }
        }
    );
	
	
	
	// FUNCTIONS
	var clientRows;
	function addClient(amount) {
		clientRows = document.getElementById('more_clients').querySelectorAll('[class^="client_container"]');
		if (clientRows.length >= 6) { $('#rm_add_client_btn').hide(500); $('#add_client_line_p').html('Max lines reached for this submission, you will be able to nominate again after this submission.'); }
		$('#more_clients').append(clientHTML);
	}
	function removeClient(target) {
		var removeMe = $(target).parent();
		$(removeMe).remove();
		clientRows = document.getElementById('more_clients').querySelectorAll('[class^="client_container"]');
		if (clientRows.length <= 6) { $('#rm_add_client_btn').show(500); $('#add_client_line_p').html('Add line'); }
	}
	function clientHTML() {
		var clientHTML = '<div class=\"client_container\">';
		
		clientHTML += '<div class=\"client_col1\">';
        clientHTML += '<input type=\"text\" name=\"client_name[]\" maxlength=\"100\" class=\"input_text full_text_width\" placeholder=\"Client name and surname*\" value=\"Client name and surname*\" onFocus=\"javascript:frame_clearTxtInput(this);\" onBlur=\"javascript:frame_clearTxtInput(this);\">';
		clientHTML += '</div>';
		
		clientHTML += '<div class=\"client_col2\">';
		clientHTML += '<input type=\"text\" name=\"company_name[]\" maxlength=\"100\" class=\"input_text full_text_width\" placeholder=\"Client business name*\" value=\"Client business name*\" onFocus=\"javascript:frame_clearTxtInput(this);\" onBlur=\"javascript:frame_clearTxtInput(this);\">';
		clientHTML += '</div>';
		
		clientHTML += '<img src=\"<?php echo $image_folder; ?>rm_remove_client_btn.png\" class=\"rm_remove_client\" onClick=\"javascript:removeClient(this);\" />';
		
		clientHTML += '<div class=\"client_col3\">';
		clientHTML += '<input type=\"text\" name=\"client_email[]\" maxlength=\"100\" class=\"input_text full_text_width\" placeholder=\"Client email address*\" value=\"Client email address*\" onFocus=\"javascript:frame_clearTxtInput(this);\" onBlur=\"javascript:frame_clearTxtInput(this);\">';
		clientHTML += '</div>';
		
		clientHTML += '</div>';
		clientHTML += '<div class=\"clear\"></div>';
		return clientHTML;
	}
	$('#rm_add_client_btn').click(function() {
		addClient(1);
	});
	$('.rm_remove_client').click(function() {
		removeClient(this);
	});


	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
		
	};

	
	
	
	// BUTTONS AND USER ACTIONS
	$('#close_form_btn').bind('mouseup touchend MozTouchRelease',function() {
		$('#user_form_container').hide(0,function() {
			$('#lightbox_mask').hide(0);
		});	
	});

	/* ----- */
	/* SUBMIT FUNCTION */
	$('#btn_submit_form').click(function(event) {
		event.preventDefault();
		var error = new Array();
		
		/*$("#form_clients_list input[type=text]").each(function() {
			if(!isNaN(this.value) || this.value === $(this).attr('placeholder')) {
				frame_markErrorFields(this);
				if (this.value.indexOf('surname*') >= 0) {
					error.push('*Please enter your client’s name and surname.');
				}else if (this.value.indexOf('business name*') >= 0) {
					error.push('*Please enter the name of your client’s business.');
				}else if (this.value.indexOf('email') >= 0) {
					error.push('*Please enter your client’s email address. Please note that this cannot be an FNB email address.');
				}else {
					error.push('Unknown error');
				}				
			}else {
				// Check if any field has an @fnb.co.za email address
				if (this.value.indexOf("fnb.co.za") > -1) {
					frame_markErrorFields(this);
					error.push('*Please enter a valid FNB email address.');
				}else {
					frame_markCheckedFields(this);
				}
			}
		});*/
		clientRows = document.querySelectorAll('[class^="client_container"]');
		var lineNumber = 1;
		for (var i = 0; i < clientRows.length; i++) {
			var thisRow = clientRows[i];
			var inputs = thisRow.querySelectorAll('[class^="input_text"]');
			for (var t = 0; t < inputs.length; t++) {
				var thisInput = inputs[t];
				if(!isNaN(thisInput.value) || thisInput.value === $(thisInput).attr('placeholder')) {
					frame_markErrorFields(thisInput);
					if (thisInput.value.indexOf('surname*') >= 0) {
						error.push('*Please enter your client’s name and surname.');
					}else if (thisInput.value.indexOf('business name*') >= 0) {
						error.push('*Please enter the name of your client’s business.');
					}else if (thisInput.value.indexOf('email') >= 0) {
						error.push('*Please enter your client’s email address. Please note that this cannot be an FNB email address.<br/>');
					}else {
						error.push('Unknown error');
					}				
				}else {
					// Check if any field has an @fnb.co.za email address
					if (thisInput.value.indexOf("fnb.co.za") > -1) {
						frame_markErrorFields(thisInput);
						error.push('*Please enter a valid FNB email address.');
					}else if (thisInput.value.indexOf('email') >= 0 && frame_validateEmail(thisInput.value)) {
						error.push('*Please enter a valid email address. Please note that this cannot be an FNB email address.<br/>');
					}else {
						frame_markCheckedFields(thisInput);
					}
				}
			};
		}
		// console.log(clientRows.length);
		calculateErrorBeforeSubmit(error,'form_clients_list');
		/*
		// Display error or submit form
		var errorMessage = "*Please enter your client’s name and surname.<BR>*Please enter the name of your client’s business.<BR>*Please enter your client’s email address. Please note that this cannot be an FNB email address.";
		if (error.length >= 1) {
			$('#form_validate_error').html(errorMessage);
		}else {
			$("form#form_clients_list").submit();
		}
		*/
	});
	
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>