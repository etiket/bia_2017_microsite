<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Entry Form";
$project_page['file_name'] = "form.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);


$_addToURL = "";
$image_folder = $project_data['full_address']."images/";
$form_device = "DESKTOP";
$today = $vandag." ".$tyd;
$closeDate = "2017-01-31 23:59";
if ($_DateTime->datum_diff($today,$closeDate) <= 0) {
	if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
		$_SESSION['uq'] = $_GET['uq'];
		$_addToURL = "?uq=".$_GET['uq'];
	}else if (isset($_SESSION['uq']) && $_SESSION['uq'] == "c7e8c79013697044886") {
		$_addToURL = "?uq=".$_SESSION['uq'];
	}else {
		header("Location: ".$project_data['full_address']);
	}
}

// END OF CAMPAIGN
//session_write_close();
//header('Location: '.$project_data['pages']['landing']);
//exit;
// END --


// Validate the user
if (isset($_SESSION['entry_userinfo']) && is_array($_SESSION['entry_userinfo']) && $_SESSION['entry_userinfo']['id'] >= 1 ) {
	//$_Project_db->debug($_SESSION['entry_userinfo']);
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['entry_login'].'?errormessage=You have to login or register before you may enter.');
	exit;
}

include('_include/form_submit.php');
?><!DOCTYPE html>
<html>
<head id="www-fnbbusinessinnovationawards-co.za" class="no-skrollr" />
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="-1">

<title>FNB Business INNOVATION Awards 2017</title>


	<!-- Mobile Specific Metas
  ================================================== -->
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="800"/>
    

	<!-- CSS
  ================================================== -->
    

    
    <link rel="stylesheet" href="css/formstyles/normalize.css">
	<link rel="stylesheet" href="css/formstyles/form.css">
    
    
    
    <script src="js/modernizr-2.8.3.min.js"></script>
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- MAJOR META TAGS -->
    <link rel="canonical" href="http://www.fnbbusinessinnovationawards.co.za" />
    <meta name="description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised.">
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="The FNB Business Innovation Awards" />
    <meta property="og:site_name" content="www-fnbbusinessinnovationawards-co.za" />
    <meta property="og:type" content="website" />
    <meta property="fb:admins" content="670010811"/>
    
    <!-- Modify the project_data in a headscript to customise each page -->
    <meta property="og:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="keywords" content="FNB, Business, Innovation Awards, Endeavor, entrepreneurs, Paul Harris, Endeavor Board Member, Mike Vacy-Lyle, FNB Business Innovator of the Year">
    <meta property="og:url" content="<?php echo $project_data['full_address']; ?>" />
    <meta property="og:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon600.png" />
    
    
    <!-- TWITTER META TAGS -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="FNB BUSINESS INNOVATION AWARDS.">
    <meta name="twitter:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised.">
    <meta name="twitter:image" content="<?php echo $project_data['full_address'].$image_folder; ?>project_social_icons/fb_icon600.png">
    
    <!-- REL -->
    <link rel="publisher" href="https://plus.google.com/116427055070017697691/about" />

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

    <body style="margin-bottom:45px;">
    
    <?php
	// INCLUDE VerSaDUHDUH tag
	include('_include/versaduhTag.php');
	?>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_form_topLogo DESKTOP">
        <div class="clear"></div>
    
        <div class="wrapper" id="form2016">
        	<div class="content">        		
        		<div class="form-holder">
		        	<h1>Entry Form</h1>
		        	<!-- FORM -->
                    <form action="<?php echo $_SERVER['PHP_SELF'].$_addToURL; ?>" method="post" enctype="multipart/form-data" name="form_entryform" id="form_entryform">
        				<!-- ROW 1 -->
        				<div class="row">
                        	<?php
								$fieldName = $tableKeys[0];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Business name*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="business_name" id="business_name" maxlength="150" class="col-33 no-margin<?php echo $addClass; ?>" placeholder="Business name*" value="<?php echo $vAl; ?>">
                            <?php
								$fieldName = $tableKeys[1];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "0"; $addClass = " placeholder_text"; }
							?>
	        				<select id="annual_turnover" name="annual_turnover" class="col-33<?php echo $addClass; ?>" placeholder="0">
	        					<option value="0" <?php if ($vAl == NULL || $vAl == "0") { echo "selected"; } ?>>Annual turnover* </option>
	        					<option value="R0 - R10-million" <?php if ($vAl == "R0 - R10-million") { echo "selected"; } ?>>R0 - R10-million</option>
	        					<option value="R10 - R60-million" <?php if ($vAl == "R10 - R60-million") { echo "selected"; } ?>>R10 - R60-million</option>
	        					<option value="R60 - R150-million" <?php if ($vAl == "R60 - R150-million") { echo "selected"; } ?>>R60 - R150-million</option>
	        					<option value="R150-million +" <?php if ($vAl == "R150-million +") { echo "selected"; } ?>>R150-million +</option>
	        				</select>
                            <?php
								$fieldName = $tableKeys[2];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Business registration number*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="business_reg_number" id="business_reg_number" maxlength="150" class="col-33<?php echo $addClass; ?>" placeholder="Business registration number*" value="<?php echo $vAl; ?>">
        				</div>
        				
        				<!-- ROW 2 -->
        				<div class="row">
                        	<?php
								$fieldName = $tableKeys[3];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "0"; $addClass = " placeholder_text"; }
							?>
	        				<select id="industry" name="industry" id="industry" class="col-16 no-margin<?php echo $addClass; ?>" placeholder="0">
	        					<option value="0" <?php if ($vAl == "0" || $vAl == NULL) { echo "selected"; } ?>>Industry*</option>
	        					<option value="Agriculture" <?php if ($vAl == "Agriculture") { echo "selected"; } ?>>Agriculture</option>
	        					<option value="Automotive" <?php if ($vAl == "Automotive") { echo "selected"; } ?>>Automotive</option>
	        					<option value="Contractors/Trade" <?php if ($vAl == "Contractors/Trade") { echo "selected"; } ?>>Contractors/Trade</option>
	        					<option value="Entertainment" <?php if ($vAl == "Entertainment") { echo "selected"; } ?>>Entertainment</option>
	        					<option value="Finance" <?php if ($vAl == "Finance") { echo "selected"; } ?>>Finance</option>
	        					<option value="Healthcare" <?php if ($vAl == "Healthcare") { echo "selected"; } ?>>Healthcare</option>
	        					<option value="Media/Marketing" <?php if ($vAl == "Media/Marketing") { echo "selected"; } ?>>Media/Marketing</option>
	        					<option value="Education" <?php if ($vAl == "Education") { echo "selected"; } ?>>Education</option>
	        					<option value="Retail/eCommerce" <?php if ($vAl == "Retail/eCommerce") { echo "selected"; } ?>>Retail/eCommerce</option>
	        					<option value="Travel" <?php if ($vAl == "Travel") { echo "selected"; } ?>>Travel</option>
	        					<option value="Manufacturing" <?php if ($vAl == "Manufacturing") { echo "selected"; } ?>>Manufacturing</option>
	        					<option value="Food/Hospitality" <?php if ($vAl == "Food/Hospitality") { echo "selected"; } ?>>Food/Hospitality</option>
	        					<option value="Other" <?php if ($vAl == "Other") { echo "selected"; } ?>>Other</option>
	        				</select>
                            
                            <?php
								$fieldName = $tableKeys[4];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Founded (year)*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="founded_year" id="founded_year" maxlength="4" class="col-16<?php echo $addClass; ?>" placeholder="Founded (year)*" value="<?php echo $vAl; ?>">
                            
                            <?php
								$fieldName = $tableKeys[5];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Founder*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="founder" id="founder" maxlength="30" class="col-33<?php echo $addClass; ?>" placeholder="Founder*" value="<?php echo $vAl; ?>">
                            
                            <?php
								$fieldName = $tableKeys[6];
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "1"; $addClass = " placeholder_text"; }
							?>
	        				<select id="amount_of_founders" name="amount_of_founders" class="col-33" placeholder="1">
                            	<?php
								$selected = "";
								$i = 1;
								while ($i < 11) {
									if ($vAl == $i) { $selected = " selected"; }else { $selected = ""; }
									if ($i == 1) {
										?><option value="1" <?php echo $selected; ?>>More than one founder</option><?php
									}else {
										?><option value="<?php echo $i; ?>" <?php echo $selected; ?>><?php echo $i; ?></option><?php
									}
									$i++;
								}
								?>
	        				</select>
        				</div>
        				
        				<!-- ROW 3 -->
                        <?php
							$fieldName = $tableKeys[7]; // is_founder_led
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
        				<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-1">Is the business founder led?*</p>
	        					<div class="help" id="0"></div>
	        					<div class="toggle-yes-no" id="is_founder_led_yesno">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="is_founder_led" id="is_founder_led" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="NA">
                                </div>
	        				</div>
                            <?php
								$fieldName = $tableKeys[8]; // founder_role
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "What is the role of the founder in the business?*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="founder_role" id="founder_role" maxlength="150" class="yn-answer-1<?php echo $addClass; ?>" placeholder="What is the role of the founder in the business?*" value="<?php echo $vAl; ?>">
						</div>

						<!-- ROW 4  -->
        				<div class="row">
                        	<?php
								$fieldName = $tableKeys[9]; // email
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = $_SESSION['entry_userinfo']['email']; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="email" id="email" maxlength="150" class="col-33 no-margin<?php echo $addClass; ?>" placeholder="Email address*" value="<?php echo $vAl; ?>">
                            <?php
								$fieldName = $tableKeys[10]; // tel
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Telephone number*"; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="tel" id="tel" maxlength="150" class="col-33<?php echo $addClass; ?>" placeholder="Telephone number*" value="<?php echo $vAl; ?>">
                            <?php
								$fieldName = $tableKeys[11]; // number_of_staff
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Number of staff*"; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="number_of_staff" id="number_of_staff" maxlength="12" class="col-33<?php echo $addClass; ?>" placeholder="Number of staff*" value="<?php echo $vAl; ?>">
        				</div>

						<!-- ROW 5  -->
        				<div class="row">
                        	<?php
								$fieldName = $tableKeys[12]; // address
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Address*"; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="address" id="address" maxlength="250" class="col-50 no-margin<?php echo $addClass; ?>" placeholder="Address*" value="<?php echo $vAl; ?>">
                            <?php
								$fieldName = $tableKeys[13]; // city
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "City*"; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="city" id="city" maxlength="100" class="col-25<?php echo $addClass; ?>" placeholder="City*" value="<?php echo $vAl; ?>">
                            <?php
								$fieldName = $tableKeys[14]; // province
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Province*"; $addClass = " placeholder_text"; }
							?>
        					<input type="text" name="province" id="province" maxlength="100" class="col-25<?php echo $addClass; ?>" placeholder="Province*" value="<?php echo $vAl; ?>">
        				</div>
        				
                        <!-- ROW 5.B -->
                        <?php
							$fieldName = $tableKeys[15]; // operate_outside_of_sa
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
        				<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-2">Does the business operate in countries outside South Africa?*</p>
	        					<div class="toggle-yes-no" id="operate_outside">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="operate_outside_of_sa" id="operate_outside_of_sa" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="operate_outside_elaborate*">
                                </div>
	        				</div>
                            <?php
								$fieldName = $tableKeys[16]; // operate_outside_elaborate
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "If yes, please elaborate*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="operate_outside_elaborate" id="operate_outside_elaborate" maxlength="150" class="yn-answer-2 yn-answer<?php echo $addClass; ?>" yesno="operate_outside" placeholder="If yes, please elaborate*" value="<?php echo $vAl; ?>">
						</div>
                        
        				<!-- ROW 6 -->
                        <?php
							$fieldName = $tableKeys[17]; // bank_with_fnb
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
        				<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-2">Does the business bank with FNB?*</p>
	        					<div class="toggle-yes-no" id="bank_with_fnb_yesno">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes" || isset($_myRMName)) { echo " selected"; $vAl = "Yes"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="bank_with_fnb" id="bank_with_fnb" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="bank_with_fnb_rm">
                                </div>
	        				</div>
                            <?php
								$fieldName = $tableKeys[18]; // bank_with_fnb_rm
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else {
									if (isset($_myRMName)) {
										$vAl = $_myRMName;
										?><input type="hidden" name="rm_id" id="rm_id" value="<?php echo $_myRMDetails['id']; ?>"><?php
									}else { $vAl = "If yes, please provide your Relationship Banker’s name*"; $addClass = " placeholder_text"; }
								}
							?>
	        				<input type="text" name="bank_with_fnb_rm" id="bank_with_fnb_rm" maxlength="150" class="yn-answer-2 yn-answer<?php echo $addClass; ?>" yesno="bank_with_fnb_yesno" placeholder="If yes, please provide your Relationship Banker’s name*" value="<?php echo $vAl; ?>">
						</div>

        				<!-- ROW 7 -->
                        <?php
							$fieldName = $tableKeys[19]; // previous_entry
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
        				<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-2">Have you previously entered the awards?*</p>
	        					<div class="toggle-yes-no" id="previously_entered">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="previous_entry" id="previous_entry" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="previous_entry_progress">
                                </div>
	        				</div>
                            <?php
								$fieldName = $tableKeys[20]; // previous_entry_progress
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "If yes, please state how far you progressed*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="previous_entry_progress" id="previous_entry_progress" maxlength="150" class="yn-answer-2 yn-answer<?php echo $addClass; ?>" yesno="previously_entered" placeholder="If yes, please state how far you progressed*" value="<?php echo $vAl; ?>">
						</div>
						
						<!-- ROW 8 -->
						<div class="">
                        	<?php
								$fieldName = $tableKeys[21]; // previous_entry
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Provide a brief description of company product or service offering*"; $addClass = " placeholder_text"; }
							?>
							<textarea name="description_products" id="description_products" rows="3" class="<?php echo $addClass; ?>" placeholder="Provide a brief description of company product or service offering*"><?php echo $vAl; ?></textarea>
						</div>
						
						<!-- ROW 9 -->
						<div class="row">
                        	<?php
								$fieldName = $tableKeys[22]; // business_target_market
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Business' target market*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="business_target_market" id="business_target_market" maxlength="250" class="col-100<?php echo $addClass; ?>" placeholder="Business' target market*" value="<?php echo $vAl; ?>">
						</div>
						
						<!-- ROW 10 -->
						<div class="row">
                        	<?php
								$fieldName = $tableKeys[23]; // revenue_last_year
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Revenue – last year*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="revenue_last_year" id="revenue_last_year" maxlength="150" class="col-33 no-margin with-help<?php echo $addClass; ?>" placeholder="Revenue – last year*" value="<?php echo $vAl; ?>"><div class="help" id="1"></div>
                            <?php
								$fieldName = $tableKeys[24]; // projected_revenue
								if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
								}else { $vAl = "Projected revenue – current year*"; $addClass = " placeholder_text"; }
							?>
	        				<input type="text" name="projected_revenue" id="projected_revenue" maxlength="150" class="col-33  with-help<?php echo $addClass; ?>" placeholder="Projected revenue – current year*" value="<?php echo $vAl; ?>"><div class="help" id="2"></div>
	        			</div>
	        			
	        			<!-- ROW 11 -->
                        <?php
							$fieldName = $tableKeys[25]; // is_grow_exponentially
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
	        			<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-6">Does your business have the opportunity to grow exponentially?*</p>
	        					<div class="toggle-yes-no" id="growth_oportunity">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="is_grow_exponentially" id="is_grow_exponentially" value="<?php echo $vAl; ?>" class="hidden_yes_no<?php echo $addClass; ?>" myInput="description_grow_exponentially">
                                </div>
	        				</div>
						</div>
						
						<!-- ROW 12 -->
                        <?php
							$fieldName = $tableKeys[26]; // description_grow_exponentially
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "If yes, please elaborate and provide projected business growth timelines*"; $addClass = " placeholder_text"; }
						?>
						<div class="">
							<textarea name="description_grow_exponentially" id="description_grow_exponentially" rows="3" class="yn-answer<?php echo $addClass; ?>" yesno="growth_oportunity" placeholder="If yes, please elaborate and provide projected business growth timelines*"><?php echo $vAl; ?></textarea>
						</div>
						
	        			<!-- ROW 13 -->
                        <?php
							$fieldName = $tableKeys[28]; // is_business_unique
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
	        			<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-3">Is your business unique?*</p><div class="help" id="3"></div>
	        					<div class="toggle-yes-no" id="business_unique">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="is_business_unique" id="is_business_unique" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="unique_part_of_business">
                                </div>
	        				</div>
						</div>
						
						<!-- ROW 14 -->
                        <?php
							$fieldName = $tableKeys[27]; // unique_part_of_business
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "What part of your business is unique? Please elaborate*"; $addClass = " placeholder_text"; }
						?>
						<div class="yn-answer" yesno="business_unique">
                            <textarea name="unique_part_of_business" id="unique_part_of_business" rows="3" class="<?php echo $addClass; ?>" placeholder="What part of your business is unique? Please elaborate*"><?php echo $vAl; ?></textarea>
						</div>
						

						
	        			<!-- ROW 16 -->
                        <?php
							$fieldName = $tableKeys[29]; // have_intellectual_property
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
	        			<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-4">Does your business have its own Intellectual Property?*</p><div class="help" id="4"></div>
	        					<div class="toggle-yes-no" id="has_ip">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="have_intellectual_property" id="have_intellectual_property" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="description_intellectual_property">
                                </div>
	        				</div>
						</div>
						
						<!-- ROW 17 -->
                        <?php
							$fieldName = $tableKeys[30]; // description_intellectual_property
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "If yes, please elaborate*"; $addClass = " placeholder_text"; }
						?>
						<div class="yn-answer" yesno="has_ip">
							<textarea name="description_intellectual_property" id="description_intellectual_property" rows="3" class="<?php echo $addClass; ?>" placeholder="If yes, please elaborate*"><?php echo $vAl; ?></textarea>
						</div>
						
						<!-- ROW 18 -->
                        <?php
							$fieldName = $tableKeys[31]; // description_awards_help
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "How could these awards help your business grow?* (max 500 words)"; $addClass = " placeholder_text"; }
						?>
						<div class="">
							<textarea name="description_awards_help" id="description_awards_help" rows="6" class="<?php echo $addClass; ?>" placeholder="How could these awards help your business grow?* (max 500 words)"><?php echo $vAl; ?></textarea>
						</div>
						
	        			<!-- ROW 19 -->
                        <?php
							$fieldName = $tableKeys[32]; // is_endeavor
							if (isset($_ThisEntry[$fieldName]) && $_ThisEntry[$fieldName] != "") { $vAl = $_ThisEntry[$fieldName]; $addClass = "";
							}else { $vAl = "0"; $addClass = " placeholder_text"; }
						?>
	        			<div class="row">
	        				<div class="col-66 no-margin">
	        					<p class="yn-question-5">Are you already an Endeavor Entrepreneur?*</p>
	        					<div class="toggle-yes-no" id="already_endeavor_yesno">
                                	<div class="toggle-btn btn-yes<?php if ($vAl == "Yes") { echo " selected"; } ?>">Yes</div>
                                    <div class="toggle-btn btn-no<?php if ($vAl == "No") { echo " selected"; } ?>">No</div>
                                    <input type="hidden" name="is_endeavor" id="is_endeavor" value="<?php echo $vAl; ?>" class="hidden_yes_no" myInput="NA">
                                </div>
	        				</div>
						</div>
						
						<!-- <div class="captchaHolder"></div>
						
						<div class="row">
							<input type="text" name="code" id="code" maxlength="250" class="col-33 no-margin" placeholder="Enter code shown*" value="">
						</div> -->
                        <input type="hidden" name="NA" id="NA" value="NO VALUE OF IMPRTANCE" >
                        <input type="hidden" name="form_action" value="<?php
							if (isset($_ThisEntry) && $_ThisEntry['id'] >= 1) {
								echo "UPDATE";
							}else {
								echo "NEW";
							}
							?>" >
						
						
						<?php
						if ($_SESSION['entry_userinfo']['status'] == "SUBMITTED") {
							
						}else {
							?>
						<input type="hidden" name="action_type" id="action_type" value="save" />
                        
                        <input type="hidden" name="btn_pressed" id="btn_pressed" value="NA" />
                        
						<input type="button" name="btn_save_form" id="btn_save_form" class="btn-save" value="SAVE" />
						<input type="button" name="btn_submit_form" class="btn-submit" id="btn_submit_form" value="SUBMIT"/>
						<?php
						}
						?>
        			</form>
        		</div>
        		
        	</div>
        	
        </div>
        <div class="clear"></div>

	<!-- LIGHTBOX STUFFS -->
    <div class="lightbox-mask" id="lightbox_mask">
    	
    </div>
    <div class="lightbox" id="user_form_container">
    	
        <div class="lightbox_content">
            <p class="form_error_copy COPY_REGULAR">Please fill in all the required fields (*) to complete your submission.</p>
            <div class="square_btn CC_back_btn" id="close_form_btn"><p class="square_btn_p">Back</p></div>
            <div class="clear"></div>
        </div> <!-- lightbox content -->
    </div> <!-- lightbox -->
    
    
    <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img DESKTOP" id="nav_menu_btn_open">
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->
    <?php include('_include/navigation.php'); ?>
    
	<script src="js/jquery-1.11.0.min.js"></script>
	<!-- <script src="js/plugins.js"></script> -->
	<script src="js/toastr.min.js"></script>
	<script src="js/form.js"></script>
<script type="text/javascript">

	$('input:text').focus(function () {
		if ($(this).val() === $(this).attr('placeholder')) {
			$(this).val('');
		}
		validation_mark_input_pass(this);
		textStyle(this);
	}).blur(function () {
		if ($(this).val() == "") {
			$(this).val($(this).attr('placeholder'))
		}
		textStyle(this);
	}
		);
	$('textarea').focus(function () {
		if ($(this).html() === $(this).attr('placeholder')) {
			$(this).html('');
		}
		validation_mark_input_pass(this);
		textStyle(this);
	}).blur(function () {
		if ($(this).html() == "") {
			$(this).html($(this).attr('placeholder'))
		}
		textStyle(this);
	}
	);
	
	
	

	$("#description_awards_help").on('keyup', function() {
		var words = this.value.match(/\S+/g).length;
		if (words > 200) {
		  // Split the string on first 200 words and rejoin on spaces
		  var trimmed = $(this).val().split(/\s+/, 200).join(" ");
		  // Add a space at the end to make sure more typing creates new words
		  $(this).val(trimmed + " ");
		}
		else {
		  //$('#display_count').text(words);
		  //$('#word_left').text(200-words);
		}
	});


	$('select').focus(function () {
            validation_mark_input_pass(this);
        }).blur(function () {
            // do nothing
        }
    );
	function validation_mark_input_fail(element) {
		if ((typeof element) == 'string') {
			element = $('#'+element);
		}
		// add no margin
		var addNoMargin = false;
		if ($(element).hasClass('no-margin')) { addNoMargin = true; }else if ($(element).css("marginLeft") == "0" || $(element).css("marginLeft") == "0%" || $(element).css("marginLeft") == "0px"){  addNoMargin = true; }
		if ($(element).hasClass('inputPass')) { $(element).removeClass('inputPass'); }
		if ($(element).hasClass('inputFail')) {
			// do nthing
		}else {
			if (addNoMargin == true) { $(element).addClass('inputFail_noMargin'); }else { $(element).addClass('inputFail'); }
		}
	}
	
	function textStyle(element) {
		// add or remove placeholder class
		if ($(element).val() === $(element).attr('placeholder')) {
			if ($(element).hasClass('placeholder_text')) {
				// do nothing
			}else {
				$(element).addClass('placeholder_text');
			}
		}else {
			if ($(element).hasClass('placeholder_text')) {
				$(element).removeClass('placeholder_text');
			}else {
				// do nothing
			}
		}
	}
	
	
	function validation_mark_input_pass(element) {
		if ((typeof element) == 'string') {
			element = $('#'+element);
		}
		if ($(element).hasClass('inputFail')) { $(element).removeClass('inputFail'); }
		if ($(element).hasClass('inputPass')) {
			// do nothing
		}else {
			$(element).addClass('inputPass');
		}
	}
	/* BUILD ERROR */
	function frame_buildErrorModal(errorArray,errormessage) {
		if (errorArray.length >= 1) {
			errorArray = null;
			$('#user_form_container').show(0,function() {
				$('#lightbox_mask').show(0);
			});
			window.scrollTo(0, 0);
		}else {
			return true;
		}
	}
	// close error modal
	$('#close_form_btn').click(function() {
		$('#user_form_container').hide(0,function() {
			$('#lightbox_mask').hide(0);
		});	
	});

	/* ----- */
	/* SUBMIT FUNCTION */
	var checkTextValues = new Array();
	var numbersArray = new Array();
	var yesNoArray = new Array();
	$('#btn_submit_form').bind('mouseup touchend MozTouchRelease',function() {
		$('#btn_pressed').val('SUBMIT');
		//event.preventDefault(); // cancel default behavior
		var error = new Array();
		// Text values
		checkTextValues['business_name'] = $('#business_name').val();
		checkTextValues['annual_turnover'] = $('#annual_turnover').val();
		checkTextValues['business_reg_number'] = $('#business_reg_number').val();
		checkTextValues['industry'] = $('#industry').val();
		checkTextValues['founded_year'] = $('#founded_year').val();
		checkTextValues['founder'] = $('#founder').val();
		checkTextValues['founder_role'] = $('#founder_role').val();
		checkTextValues['email'] = $('#email').val();
		checkTextValues['tel'] = $('#tel').val();
		checkTextValues['number_of_staff'] = $('#number_of_staff').val();
		checkTextValues['address'] = $('#address').val();
		checkTextValues['city'] = $('#city').val();
		checkTextValues['province'] = $('#province').val();
		//checkTextValues['operate_outside_elaborate'] = $('#operate_outside_elaborate').val();
		//checkTextValues['bank_with_fnb_rm'] = $('#bank_with_fnb_rm').val();
		//checkTextValues['previous_entry_progress'] = $('#previous_entry_progress').val();
		checkTextValues['description_products'] = $('#description_products').val();
		checkTextValues['business_target_market'] = $('#business_target_market').val();
		checkTextValues['revenue_last_year'] = $('#revenue_last_year').val();
		checkTextValues['projected_revenue'] = $('#projected_revenue').val();
		//checkTextValues['description_grow_exponentially'] = $('#description_grow_exponentially').val();
		//checkTextValues['unique_part_of_business'] = $('#unique_part_of_business').val();
		//checkTextValues['description_is_business_unique'] = $('#description_is_business_unique').val();
		//checkTextValues['description_intellectual_property'] = $('#description_intellectual_property').val();
		checkTextValues['description_awards_help'] = $('#description_awards_help').val();
		// CHECK NORMAL TEXT INPUT VALUES
		for(var key in checkTextValues)
		{
			if (checkTextValues[key] != null) {
				var inputVal = $.trim(checkTextValues[key]);
				if (inputVal != $('#'+key).attr('placeholder') && inputVal != "") {
					validation_mark_input_pass(key);
					//if (console.log("PASS: "+key+" = "+checkTextValues[key]));
				}else {
					validation_mark_input_fail(key);
					//if (console.log("FAIL: "+key+" = "+checkTextValues[key]));
					error.push("FAIL: "+key+" = "+checkTextValues[key]);
				}
			}else {
				// error null value
				validation_mark_input_fail(key);
				//if (console.log("NULL: "+key+" = "+checkTextValues[key]));
				error.push("FAIL: "+key+" = "+checkTextValues[key]);
			}
			if (console.log(key+" | "+$('#'+key).attr('placeholder')));
		}
		
		yesNoArray['is_founder_led'] = $('#is_founder_led').val();
		yesNoArray['operate_outside_of_sa'] = $('#operate_outside_of_sa').val();
		yesNoArray['bank_with_fnb'] = $('#bank_with_fnb').val();
		yesNoArray['previous_entry'] = $('#previous_entry').val();
		yesNoArray['is_grow_exponentially'] = $('#is_grow_exponentially').val();
		yesNoArray['is_business_unique'] = $('#is_business_unique').val();
		yesNoArray['have_intellectual_property'] = $('#have_intellectual_property').val();
		yesNoArray['is_endeavor'] = $('#is_endeavor').val();
		
		// CHECK ALL THE YES & NO SELECTION VALUES
		for(var key in yesNoArray)
		{
			// Check yes no related input and unmark
			var myParentID = $('#'+key).parent();
				myParentID = myParentID.attr('id');
			var myInputID = $('#'+key).attr('myInput');
			var myInputVal = $('#'+myInputID).val();
				myInputVal = $.trim(myInputVal);
				
			if (yesNoArray[key] == 'Yes') {
				if (myInputVal == "" || myInputVal == null || myInputVal == $('#'+myInputID).attr('placeholder')) {
					// Mark myInputID
					validation_mark_input_fail(myInputID);
					validation_mark_input_fail(myParentID);
					//if (console.log("YESNO - FAIL: "+key+" | myInputID: "+myInputID+" | myInputVal: "+myInputVal));
					error.push("YESNO - FAIL: "+key+" | myInputID: "+myInputID+" | myInputVal: "+myInputVal);
				}else {
					// Clear mark
					validation_mark_input_pass(myInputID);
					validation_mark_input_pass(myParentID);
					//if (console.log("YESNO - PASS: "+key+" | myInputID: "+myInputID+" | myInputVal: "+myInputVal));
				}
			}else if (yesNoArray[key] == 'No') {
				// Clear related input field
				if (myInputID != "NA") { $('#'+myInputID).val(""); }
				validation_mark_input_pass(myInputID);
				validation_mark_input_pass(myParentID);
				//if (console.log("YESNO - PASS: "+key+" | myInputID: "+myInputID+" | myInputVal: "));
			}else {
				// Mark Yes and No fields with fail
				validation_mark_input_fail(myParentID);
				//if (console.log("YESNO - FAIL: "+key+" | myInputID: "+myInputID+" | myInputVal: "+myInputVal));
				error.push("YESNO - FAIL: "+key+" | myInputID: "+myInputID+" | myInputVal: "+myInputVal);
			}
			if ("yesno: "+console.log($('#'+key).val()+" | "+myInputID+" | "+$('#'+myInputID).attr('placeholder')));
		}
		
		if (frame_buildErrorModal(error)) {
			//if ($('#form_entryform').attr('onsubmit','')) {
				
				$("form#form_entryform").submit();
			//}
		}
		
		
		
	});
	
	
	$('#btn_save_form').bind('mouseup touchend MozTouchRelease',function() {
		$('#btn_pressed').val('SAVE');
		//if ($('#form_entryform').attr('onsubmit','')) {
			//alert("just before save"+$('#btn_pressed').val());
			$("form#form_entryform").submit();
		//}
	});
	
	//$(document).ready(function (){
	window.onload = function() {
		//window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide(200);
	};
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	
	
</script>
    </body>
</html>
