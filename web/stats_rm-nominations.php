<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - RM Nominations";
$project_page['file_name'] = "stats_rm-nominations.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
	
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['rm_login'].'');
	exit;
}
$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>STATS - RM Client Nomination Stats</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
	<style>
	 a, a:link, a:visited { color:#009999; }
	</style>
</head>

<body id="client_invite_page">
    
	<div class="form_container client_list_wide_container">
    	<h1 class="page_h1 rm_list_h1 TURQ_COPY">RM Nomination status</h1>
        <p>Note: The list below is a complete list of the client invites submitted by the RMs only. This is not a complete list of all entries.</p>
        <?php
		$TOTAL_INVITES = 0;
		$TOTAL_SUBMITTED = 0;
		$TOTAL_RSVP = 0;
		$TOTAL_NA = 0;
		$TOTAL_RMS = 0;
		$RMregionArray = array();
		
		$completed_entries = array();
		$existingInvites = $_Project_db->get($_db_['table']['rm_invites'],'rm_fnumber');
		if (is_array($existingInvites) && count($existingInvites) >= 1) {
			$_rmFnumber = 0;
			?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
          	<td class="status_head head_name">RM information</td>
            <td class="status_head head_client_name">Client name</td>
            <td class="status_head head_company">Legal entity name</td>
            <td class="status_head head_email">Email</td>
            <td class="status_head head_status">Status</td>
          </tr>
		  <?php
		  $bgStyle = "grey_tr";
			foreach($existingInvites as $invite) {
				if ($invite['rm_fnumber'] != $_rmFnumber) {
					$TOTAL_RMS++;
					$_rmFnumber = $invite['rm_fnumber'];
				}
				$RMInfo = $_ProjectF->getRMInfo($invite['rm_id']);
				$inviteStatus = $_ProjectF->getAllStatus($invite['id'],$invite['client_email']);
				$acceptStatus = $_ProjectF->getAcceptStatus($invite['id']);
				/*
				// CHECK FOR DOUBLE
				$_Project_db -> where('unique_code',$invite['unique_code']);
				$doubles = $_Project_db -> get($_db_['table']['rm_invites'],'id');
				if (count($doubles) >= 1) {
					$myDoubles = array();
					foreach($doubles as $double) {
						if ($double['client_email'] != $invite['client_email']) {
							array_push($myDoubles,$double['client_email']);
						}
					}
				}
				*/
				
				// RM region counter
				$regionName = "Other";
						if (isset($RMInfo['region'])) {
							if ($RMInfo['region'] == "") {
								$regionName = "Other";
							}else {
								$regionName = $RMInfo['region'];
							}
						}
						if (isset($RMregionArray[$regionName])) {
							$RMregionArray[$regionName] = $RMregionArray[$regionName]+1;
						}else {
							$RMregionArray[$regionName] = 1;
						}
				
				
				//****** */
				if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
				
				$completedInfo = array();
				?>
				<tr class="status_tr <?php echo $bgStyle; ?>">
                	<td><?php
                    if ($inviteStatus['status'] == "Entry complete" || $inviteStatus['status'] == "SUBMITTED") {
						?><span class="TURQ_COPY"><strong><?php echo $RMInfo['name']."<BR>".$RMInfo['region']."<BR></strong>F#: ".$invite['rm_fnumber']."<BR>Email: ".$invite['rm_email']; ?></span>
                        <?php
						$completedInfo['rm_name'] = $RMInfo['name'];
						$completedInfo['rm_fnumber'] = $invite['rm_fnumber'];
						$completedInfo['rm_email'] = $invite['rm_email'];
						$completedInfo['rm_region'] = $RMInfo['region'];
					}else {
						?><span class="DARK_GREY_COPY"><strong><?php echo $RMInfo['name']."<BR>".$RMInfo['region']."<BR></strong>F#: ".$invite['rm_fnumber']."<BR>Email: ".$invite['rm_email']; ?></span><?php
					}
					?></td>
					<td><?php echo $invite['client_name']; ?></td>
					<td><?php echo $invite['client_company_name']; ?></td>
					<td><?php echo $invite['client_email']."<BR>";
						if (isset($myDoubles) && count($myDoubles) >= 1) {
							foreach($myDoubles as $double) {
								echo "<span style='color:red;'>".$double."</span><BR>";
							}
						}
						?></td>
					<td><?php
                    // STATUS
					if ($inviteStatus['status'] == "Entry complete" || $inviteStatus['status'] == "SUBMITTED") { ?><a href="stats_rm-viewentry.php?uq=c7e8c79013697044886&eid=<?php echo $inviteStatus['entry_id']; ?>" target="_blank"><span class="TURQ_COPY">Entry complete</span></a><?php
					$TOTAL_SUBMITTED++;
					$completedInfo['client_name'] = $invite['client_name'];
					$completedInfo['client_company_name'] = $invite['client_company_name'];
					$completedInfo['client_email'] = $invite['client_email'];
					$completedInfo['entry_id'] = $inviteStatus['entry_id'];
					$completedInfo['user_id'] = $inviteStatus['user_id'];
					//
					array_push($completed_entries,$completedInfo);
					}else if ($inviteStatus['status'] == "Entry incomplete") { ?><span class="ORANGE_COPY">Entry incomplete</span><BR><?php echo $acceptStatus;
					}else if ($inviteStatus['status'] == "No Action") { ?><span class="ORANGE_COPY">No Action</span><BR><?php echo $acceptStatus; ?><?php $TOTAL_NA++;
					}else { echo $inviteStatus['status']; }
					
					if ($acceptStatus == "RSVP") {
						$TOTAL_RSVP++;
					}
						?></td>
				</tr>
          		<?php
				$TOTAL_INVITES++;
			}
		  ?>
          <tr class="status_footer">
          	<td></td>
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
		<?php
		}else {
		?>
        	<p>You have not yet nominated your clients. <a href="<?php echo $project_data['pages']['rm_client_invites']; ?>" class="BLACK_COPY">Click here</a> to nominate now.</p>
        <?php
		}
		?>
        <div class="clear">&nbsp;</div>
        <p><?php echo "TOTAL INVITES: <strong>".$TOTAL_INVITES."</strong>"; ?></p>
        <p><?php echo "TOTAL SUBMITTED ENTRIES: ".$TOTAL_SUBMITTED; ?></p>
        <p><?php echo "TOTAL EMAIL LINKS CLICK BUT NO ENTRY: ".$TOTAL_RSVP; ?></p>
        <p><?php echo "TOTAL NOT REGISTERED: ".$TOTAL_NA; ?></p>
        <p><?php echo "TOTAL ACTIVE RMS: ".$TOTAL_RMS; ?></p>
        
        <hr>
        <p><strong>RM region breakdown for all invites:</strong></p>
        <p><?php foreach($RMregionArray as $key => $val) {
					echo $key." = ".$val."<BR>";
		}
		?>
		</p>
        
        <h1 class="page_h1 rm_list_h1 TURQ_COPY">Completed Invites</h1>
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="status_head head_name">RM information</td>
                    <td class="status_head head_name">Client name</td>
                    <td class="status_head head_company">Legal entity name</td>
                    <td class="status_head head_email">Email</td>
                    <td class="status_head head_status">Status</td>
                  </tr>
        <?php
		//
		$completedRegionArray = array();
		foreach($completed_entries as $comp) {
			if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
			?>
            		<tr class="status_tr <?php echo $bgStyle; ?>">
                	<td><span class="TURQ_COPY"><strong><?php echo $comp['rm_name']."<BR>".$comp['rm_region']."</strong><BR>F#: ".$comp['rm_fnumber']."<BR>Email: ".$comp['rm_email']; ?></span></td>
					</td>
					<td><?php echo $comp['client_name']; ?></td>
					<td><?php echo $comp['client_company_name']; ?></td>
					<td><?php echo $comp['client_email'] ?></td>
					<td><span class="TURQ_COPY"><a href="stats_rm-viewentry.php?uq=c7e8c79013697044886&eid=<?php echo $comp['entry_id']; ?>" target="_blank">Entry complete</a></span></td>
				</tr>
            <?php
			// RM region counter
				$regionName = "Other";
				if (isset($comp['rm_region'])) {
					if ($comp['rm_region'] == "") {
						$regionName = "Other";
					}else {
						$regionName = $comp['rm_region'];
					}
				}
				if (isset($completedRegionArray[$regionName])) {
					$completedRegionArray[$regionName] = $completedRegionArray[$regionName]+1;
				}else {
					$completedRegionArray[$regionName] = 1;
				}
		}
		
		?>
        <tr class="status_footer">
          	<td></td>
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
        
        
        <hr>
        <p class="TURQ_COPY"><strong>RM region Breakdown for completed entries:</strong></p>
        <p class="TURQ_COPY"><strong><?php foreach($completedRegionArray as $key => $val) {
					echo $key." = ".$val."<BR>";
		}
		?></strong></p>
        
    </div> <!-- form container -->
	
    <div class="clear">&nbsp;<BR><BR>&nbsp;<BR><BR>&nbsp;<BR></div>

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>