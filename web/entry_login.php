<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Entry form login and register.";
$project_page['file_name'] = "entry_login.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

$_addToURL = "";
$image_folder = $project_data['full_address']."images/";
include('_include/cd.php');



/*
// END OF CAMPAIGN
session_write_close();
header('Location: '.$project_data['pages']['landing']);
exit;
// END --
*/
// rsvp the invite
include('_include/rm_rsvp_invite.php');

if (isset($_POST['action_type_register']) && $_POST['action_type_register'] == 'register') {
	if ($_POST['input_reg_email'] != "" && $_POST['input_reg_password'] != "" && $_POST['input_reg_email'] != NULL && $_POST['input_reg_password'] != NULL) {
		// DO REGISTER
		sleep(2);
		$register_user = $_ProjectF -> registerFNBuser($_POST['input_reg_email'],$_POST['input_reg_password'],$_POST['input_reg_confirmpass']);
		//if (isset($register_user['signin'])) {
			
			if (isset($register_user['signin']) && $register_user['signin'] == true && isset($register_user['register']) && $register_user['register'] == true) {
				$_SESSION['entry_userinfo'] = $register_user['entry_userinfo'];
				// Link the invite to the new Created user
				if (isset($_POST['Invite_Id']) && $_POST['Invite_Id'] >= 1) {
					$_ProjectF->linkInvite($register_user['entry_userinfo']['id'],$_POST['Invite_Id']);
					$_SESSION['entry_userinfo']['invite_id'] = $_POST['Invite_Id'];
				}
				// Direct user to entry page
				session_write_close();
				header('Location: '.$project_data['pages']['entry_form'].$_addToURL);
				exit;
			}else if (isset($register_user['signin']) && $register_user['signin'] == false || (isset($register_user['insert_error']) && $register_user['insert_error'] == true)) {
				$reg_errormessage = "Your registration was unsuccessful. Please try again.";
			}else if (isset($register_user['password_not_valid']) && $register_user['password_not_valid'] == true) {
				$reg_errormessage = "Your that you have entered is not a valid string.";
			}else if (isset($register_user['password_no_match']) && $register_user['password_no_match'] == true) {
				$reg_errormessage = "Your password and confirm password does not match";
			}else if (isset($register_user['invalid_user']) && $register_user['invalid_user'] == true) {
				$reg_errormessage = "Your registration was unsuccessful. Please try again.";
			}else if (isset($register_user['existing_email'])) {
				$reg_errormessage = "This email address is already registered. Please try again.";
			}
	}
}else if (isset($_POST['action_type_login']) && $_POST['action_type_login'] == 'login') {
	// DO LOGIN
	if ($_POST['input_login_email'] != "" && $_POST['input_login_password'] != "" && $_POST['input_login_email'] != NULL && $_POST['input_login_password'] != NULL) {
		// DO REGISTER
		
		$login_user = $_ProjectF -> loginFNBuser($_POST['input_login_email'],$_POST['input_login_password']);
		if (isset($login_user['signin'])) {
			if ($login_user['signin'] == true && isset($login_user['entry_userinfo'])) {
				$_SESSION['entry_userinfo'] = $login_user['entry_userinfo'];
				// Link the invite to the new Created user
				if (isset($_POST['Invite_Id']) && $_POST['Invite_Id'] >= 1) {
					$_ProjectF->linkInvite($login_user['entry_userinfo']['id'],$_POST['Invite_Id']);
					$_SESSION['entry_userinfo']['invite_id'] = $_POST['Invite_Id'];
				}
				sleep(1);
				// Direct user to entry page
				session_write_close();
				header('Location: '.$project_data['pages']['entry_form'].$_addToURL);
				exit;
			}else if ($login_user['signin'] == false) {
				$login_errormessage = "Your username or password is incorrect. Please enter the correct username or password.";
			}else if (isset($login_user['invalid_email'])) {
				$login_errormessage = "The email address is not registered. You have to register before you can login.";
			}
		}
	}else {
		$login_errormessage = "Your username or password is incorrect. Please enter the correct username or password.";
	}
}else {
	if (isset($_SESSION['entry_userinfo'])) {
		$_SESSION['entry_userinfo'] = NULL;
	}
	$_SESSION = NULL;
	session_unset();
	session_destroy();
	session_write_close();
}




?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style-responsive.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>
<?php
	// INCLUDE FNB floodlight tag
	include('_include/floodlight_tag-entry_login.php');
?>
<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>

    <!-- ------------------------------------------------------------------------------------------------------------------ -->
	<div class="main_container login_n_register_container">
    	<p><?php
			// if (isset($_SESSION['entry_userinfo'])) {
			// 	//$_Project_db->debug($_SESSION['entry_userinfo']);
			// }else if (isset($_GET['errormessage'])) {
			// 	echo $_GET['errormessage'];
			// }
			
			// if ($_POST) {
			// 	//$_Project_db->debug($_POST);
			// }
			
			?></p>
        <div class="form_half_container LEFT">
        	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Login</h1>
            <form action="<?php echo $_SERVER['PHP_SELF'].$_addToURL; ?>" method="post" enctype="multipart/form-data" name="form_entry_login" id="form_entry_login">
				<div class="l1_container">
					<input type="text" name="input_login_email" id="input_login_email" maxlength="150" class="input_text full_text_width" placeholder="Email" value="<?php
	                if (isset($User_Email)) { echo $User_Email;
					}else { echo "Email"; }
					?>">
                	<input type="text" name="input_login_password" id="input_login_password" maxlength="30" class="input_text full_text_width" placeholder="Password" value="Password">
                </div> <!-- l1 container -->
                <div class="clear"></div>
                <?php
					if (isset($Invite_Id)) {
						?><input type="hidden" name="Invite_Id" id="Invite_Id" value="<?php echo $Invite_Id; ?>"><?php
					}
				?>
                <input type="hidden" name="action_type_login" value="login">
                <!-- <input type="button" name="btn_login" value="Login" id="btn_login" class="square_submit_btn rm_login_btn LEFT"/> -->
                <button class="LEFT square_btn btn_orange_arrow_right form_field_right_margin" name="btn_login" id="btn_login" value="Login" onclick="return false;">
					<p>Login</p>
				</button>
                <a href="<?php echo $project_data['pages']['password_recovery']; ?>" class="DARK_GREY_COPY COPY_REGULAR_ITALIC">Forgot password?</a>
            </form>
            <div class="clear"></div>
            <p class="form_message validation_error_message validation_error_message COPY_BOLD" id="login_error_container"><?php
            if (isset($login_errormessage)) {
				echo $login_errormessage;
			}
			?></p>
        </div> <!-- form half container LEFT-->
        <div class="vertcl_turq_line LEFT"></div> <!-- vertcl turq line -->
        <div class="form_half_container RIGHT">
        	<!-- <p>No new registrations will be accepted, as entries for the FNB Business INNOVATION Awards 2015 have closed. However, if you have already registered, you can complete your entry until 7 February 2015 midday by login in to your profile.</p> -->
        	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Register</h1>
            <form action="<?php echo $_SERVER['PHP_SELF'].$_addToURL; ?>" method="post" enctype="multipart/form-data" name="form_entry_register" id="form_entry_register">
			<div class="l1_container">
				<input type="text" name="input_reg_email" id="input_reg_email" maxlength="150" class="input_text full_text_width" placeholder="Email" value="<?php
                if (isset($User_Email)) { echo $User_Email;
				}else { echo "Email"; }
				?>">
                <input type="text" name="input_reg_password" id="input_reg_password" maxlength="30" class="input_text full_text_width" placeholder="Password" value="Password">
                <input type="text" name="input_reg_confirmpass" id="input_reg_confirmpass" maxlength="30" class="input_text full_text_width" placeholder="Confirm password" value="Confirm password">
                </div> 
                <div class="clear"></div>
                <?php
					if (isset($Invite_Id)) {
						?><input type="hidden" name="Invite_Id" id="Invite_Id" value="<?php echo $Invite_Id; ?>"><?php
					}
				?>
                <input type="hidden" name="action_type_register" value="register">
                <!-- <input type="button" name="btn_register" value="Register" id="btn_register" class="square_submit_btn entry_register_btn LEFT"/> -->
                <button class="LEFT square_btn btn_orange_arrow_right form_field_right_margin" name="btn_register" value="Register" id="btn_register" onclick="return false;">
					<p>Register</p>
				</button>
            </form>
            <div class="clear"></div>
            <p class="form_message validation_error_message validation_error_message COPY_BOLD" id="reg_error_container"><?php
            if (isset($reg_errormessage)) {
				
					echo $reg_errormessage;
				
			}
			?></p>
        </div> <!-- form half container RIGHT -->
    	
        
        
        <div class="clear"></div>
        <p class="form_message COPY_REGULAR DARK_GREY_COPY">Should you experience any technical difficulties with this website, contact us on <a href="mailto:innovationawards@fnb.co.za" class="TURQ_COPY"><strong>innovationawards@fnb.co.za</strong></a></p>
    </div> <!-- form container -->

    
    <!-- LIGHTBOX STUFFS -->
    <div class="lightbox-mask" id="lightbox_mask">
    	<img src="images/dpi_72/web_01_rightTopLogo.png" class="web_form_topLogo">
    </div>
    <div class="lightbox" id="user_form_container">
        <div class="lightbox_content">
            <p class="form_error_copy COPY_REGULAR">Please fill in all the required fields (*) to complete your submission.</p>
            <div class="square_btn back_btn" id="close_form_btn"><p class="square_btn_p">Back</p></div>
            <div class="clear"></div>
        </div> <!-- lightbox content -->
    </div> <!-- lightbox -->

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn DESKTOP"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site DESKTOP">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage DESKTOP">
</div> <!-- topRightLogo WHITE -->
<!-- BOTTOM RIGHT COPY - WHITE BACKGROUND --> 

<!-- DISCLAIMER FOOTER -->
<div class="rm_footer_container">
	<p id="magic_footer_pos" class="rm_footer_copy BLACK_COPY"><span class="COPY_REGULAR_ITALIC" style="font-size:9px;">Terms and conditions apply.</span><BR><span class="COPY_BOLD BLACK_COPY">First National Bank - a division of FirstRand Bank Limited.</span> An Authorised Financial Services and Credit Provider (NCRCP20).</p>
</div> <!-- footer container -->
<img src="<?php echo $image_folder; ?>/web_right-bottom-copy-white.png" class="web_right_bottom_img"/>

<?php include('_include/navigation.php'); ?>
    
	<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">

	$('input:text').focus(function () {
            if ($(this).val() === $(this).attr('placeholder')) {
                $(this).val('');
				if ($(this).attr('id') == 'input_reg_password' || $(this).attr('id') == 'input_reg_confirmpass' || $(this).attr('id') == 'input_login_password') {
					$(this).attr('type','password');
				}
            }
        }).blur(function () {
            if ($(this).val() == "") {
                
				if ($(this).attr('id') == 'input_reg_password' || $(this).attr('id') == 'input_reg_confirmpass' || $(this).attr('id') == 'input_login_password') {
					$(this).attr('type','text');
				}
				$(this).val($(this).attr('placeholder'));
            }
        }
    );
	$('textarea').focus(function () {
            if ($(this).html() === $(this).attr('placeholder')) {
                $(this).html('');
            }
        }).blur(function () {
            if ($(this).html() == "") {
                $(this).html($(this).attr('placeholder'))
            }
        }
    );
	
	
	
	
	//$(document).ready(function (){
	window.onload = function() {
		//window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide();
	};

	// BUTTONS AND USER ACTIONS
	$('#close_form_btn').bind('mouseup touchend MozTouchRelease',function() {
		$('#user_form_container').hide(0,function() {
			$('#lightbox_mask').hide(0);
		});	
	});

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	
	var checkTextValues = new Array();
	var email = "";
	$('#btn_register').bind('mouseup touchend MozTouchRelease',function(event) {
		event.preventDefault();
	//$("#myform").submit(function(e) {
		var error = new Array();
		
		// Text values
		var input_reg_password = $('#input_reg_password').val();
		var input_reg_confirmpass = $('#input_reg_confirmpass').val();
		email = $('#input_reg_email').val();
		//email = email.replace(" ","");
		email = email.replace(/^\s+|\s+$/gm,'');
		// THE PASSWORDS
		if(input_reg_password != "" && input_reg_password != null && input_reg_confirmpass != "" && input_reg_confirmpass != null )
		{
			if (input_reg_confirmpass == input_reg_password) {
				frame_markCheckedFields('input_reg_password');
				frame_markCheckedFields('input_reg_confirmpass');
			}else {
				frame_markErrorFields('input_reg_password');
				frame_markErrorFields('input_reg_confirmpass');
				error.push('Your registration was unsuccessful. Please try again.');
			}
		}else {
			frame_markErrorFields('input_reg_password');
			frame_markErrorFields('input_reg_confirmpass');
			error.push('Your registration was unsuccessful. Please try again.');
		}
	
		if (frame_validateEmail(email)) {
			frame_markCheckedFields('input_reg_email');
		}else {
			frame_markErrorFields('input_reg_email');
			error.push('Your email address is not valid. Please try again.');
		}
		
		if (showFNBerror(error,'reg_error_container')) {
			$("form#form_entry_register").submit();
		}
	});
	
	$('#btn_login').bind('mouseup touchend MozTouchRelease',function(event) {
		event.preventDefault();
		var error = new Array();
		
		// Text values
		checkTextValues['input_login_email'] = $('#input_login_email').val().replace(/^\s+|\s+$/gm,'');
		checkTextValues['input_login_password'] = $('#input_login_password').val();
		// CHECK NORMAL TEXT INPUT VALUES
		for(var key in checkTextValues)
		{
			//alert("key " + key + " has value " + checkTextValues[key]);
			if (frame_charCount(checkTextValues[key],2,100) && checkTextValues[key] != $('#'+key).attr('placeholder')) {
				frame_markCheckedFields(key);
			}else {
				frame_markErrorFields(key);
				error.push('Your username or password is incorrect. Please enter the correct username or password.');
			}
		}
	
		
		if (showFNBerror(error,'login_error_container')) {
			$("form#form_entry_login").submit();
		}
	});
	</script>
</body>

</html>