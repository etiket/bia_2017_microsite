<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - RM View Entry";
$project_page['file_name'] = "stats_rm-viewentry.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886" && isset($_GET['eid']) && $_GET['eid'] >= 1) {
	$_Project_db -> where('id',$_GET['eid']);
	$_thisEntry = $_Project_db -> get($_db_['table']['entries'],'-id');
	if (count($_thisEntry) >= 1) {
		$_t = $_thisEntry[0];
	}else {
		session_write_close();
		header('Location: '.$project_data['pages']['rm_login'].'');
		exit;
	}
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['rm_login'].'');
	exit;
}

include('_include/entry-form-field-values.php');

$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>STATS - RM Client Nomination Stats</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

    <!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
	<style>
	 p { font-size:16px; line-height:160%; line-height:1.6;}
	</style>
</head>

<body id="client_invite_page">
    
	<div class="form_container client_list_wide_container">
        <?php
		if (isset($_t['id'])) {
			?>
            <h1 class="page_h1 rm_list_h1 TURQ_COPY"><?php echo $_t['business_name']; ?></h1>
            <p>
            <?php
                foreach($_t as $key => $value) {
                    if (isset($formValues[$key])) {
                        if ($value === "0" || $value === "YES" || $value === "NO" || $formValues[$key] === "0" || $value === "") {
                            if ($value === "0" || $value === "" || $value === NULL) { $value = "NOT SELECTED"; }
                            echo $inputLabels[$key].":<br/><strong>".$value."</strong><br/><br/>";
                        }else {
                             if ($value === "0" || $value === "" || $value === NULL) { $value = "NO DATA ENTERED"; }
                            echo $formValues[$key].":<br/><strong>".$value."</strong><br/><br/>";
                        }
                    }
                }
                ?>
            </p>
            <?php
		}
		?>
        <div class="clear"></div>
    </div> <!-- form container -->
	<div class="clear"></div>
    <div class="clear">&nbsp;<BR><BR>&nbsp;<BR><BR>&nbsp;<BR></div>


<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->


    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>