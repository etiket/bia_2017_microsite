<?php
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "FNB Business INNOVATION Awards 2017";
$project_page['file_name'] = "index.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);


$image_folder = $project_data['full_address']."images/";
$today = $vandag." ".$tyd;
$closeDate = "2017-01-31 23:59";
$finallyDate = "2017-06-9 23:00";

?>
<!DOCTYPE html>
<html>
<head id="www-fnbbusinessinnovationawards-co.za" class="no-skrollr">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html" />

<META HTTP-EQUIV="Pragma" CONTENT="no-cache" />
<META HTTP-EQUIV="Expires" CONTENT="-1" />

<title><?php echo $project_page['name']; ?></title>


  <!-- Mobile Specific Metas
  ================================================== -->
    <meta name="HandheldFriendly" content="true" />    
  <meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="800"/>
    

  <!-- CSS
    <!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="css/owl-carousel/owl.carousel.css" />
    <!-- Default Theme -->
    <link rel="stylesheet" href="css/owl-carousel/owl.theme.css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link href="css/style_responsive.css" rel="stylesheet" type="text/css" />
    <link href="css/fixed-positioning.css" rel="stylesheet" type="text/css" />
    <link href="css/products.css" rel="stylesheet" type="text/css" />
    
  
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
    
    
    
    <!-- MAJOR META TAGS -->
    <link rel="canonical" href="http://www.fnbbusinessinnovationawards.co.za" />
    <meta name="description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta property="og:locale" content="en_US"/>
    <meta property="og:title" content="The FNB Business Innovation Awards" />
    <meta property="og:site_name" content="www-fnbbusinessinnovationawards-co.za" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png" />
    <meta property="fb:admins" content="670010811"/>
    
    <!-- Modify the project_data in a headscript to customise each page -->
    <meta property="og:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="keywords" content="FNB, Business, Innovation Awards" />
    
    
    
    <!-- TWITTER META TAGS -->
    <meta name="twitter:card" content="summary"/>
    <meta name="twitter:title" content="FNB BUSINESS INNOVATION AWARDS." />
    <meta name="twitter:description" content="Innovation is a theme that remains central to FNB's business philosophy. We believe that innovation is a key driver of business growth and scalability, and that it contributes to our philosophy of creating a better world through high-quality job creation. Because of this belief, we thought it necessary to develop a platform where innovative businesses in South Africa are recognised." />
    <meta name="twitter:image" content="<?php echo $image_folder; ?>project_social_icons/fb_icon470x270.png">
    
    <!-- REL -->
    <link rel="publisher" href="https://plus.google.com/116427055070017697691/about" />

  <!-- Favicons
  ================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
  <link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <script>
    var windowWidth = window.innerWidth;
    var windowHeight = window.innerHeight;
    
    
  </script>
    
    <?php
  // Google analytics
    include('_include/google_analytics.php');
  ?>

</head>

<body>

<div class="page_1b_container">
    <div class="products">
      <div class="products__holder">
        <h1 class="page_h1 TURQ_COPY">OUR INNOVATIVE BUSINESS SOLUTIONS</h1>
        <div class="products__le-products">
          <div class="products__product" data-product="1">
            <img src="images/product1.jpg" />
            <p>24/7 Business Desk</p>
            <img src="images/line.jpg" />
          </div>
          <div class="products__product" data-product="2">
            <img src="images/product2.jpg" />
            <p>FNB App 5.0</p>
            <img src="images/line.jpg" />
          </div>
          <div class="products__product" data-product="3">
            <img src="images/product3.jpg" />
            <p>FNB SmartTILL<sup>TM</sup></p>
            <img src="images/line.jpg" />
          </div>
          <div class="products__product" data-product="4">
            <img src="images/product4.jpg" />
            <p>DocTrail<sup>TM</sup></p>
            <img src="images/line.jpg" />
          </div>
          <div class="products__product" data-product="5">
            <img src="images/product5.jpg" />
            <p>eBucks Rewards for Business</p>
            <img src="images/line.jpg" />
          </div>
        </div>
      </div>
    </div>

    <div class="popup-holder desktop">
      <div class="popup">
        <div class="modal-btn-close"></div>
        <div class="popup-content">
          <h1 class="page_h1 TURQ_COPY">[Product Name]</h1>
          <div class="clearfix holder">
            <div class="popup__image-holder"></div>
            <div class="popup__product-description clearfix">
              <p class="popup__intro">24hr Business Desk: another first from FNB. <br><b>0877 FNB 247</b></p>
              <p>Dedicated to solving your clients’ business banking admin queries quickly, easily and efficiently.</p>
              <p>FNB Business is pleased to introduce the 24/7 Business Desk, a dedicated team of bankers to help you solve any administrative banking queries, now available anytime, day or night.</p>
              <p>Our team of expert bankers are just a phone call away. We will personally handle your query quickly and efficiently, and <b>are available 24/7</b>. This means banking hours no longer apply. The 24/7 Business Desk is another <b>INNOVATIVE</b> first from FNB Business – helping you do business 24/7.</p>
              <p class="popup__terms">Terms, conditions and qualifying criteria apply.</p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="content-mobile">
      <div class="popup-content">
          <h1 class="page_h1 TURQ_COPY">[Product Name]</h1>
          <div class="clearfix holder">
            <div class="popup__image-holder"></div>
            <div class="popup__product-description clearfix">
              <p class="popup__intro">24hr Business Desk: another first from FNB. <br><b>0877 FNB 247</b></p>
              <p>Dedicated to solving your clients’ business banking admin queries quickly, easily and efficiently.</p>
              <p>FNB Business is pleased to introduce the 24/7 Business Desk, a dedicated team of bankers to help you solve any administrative banking queries, now available anytime, day or night.</p>
              <p>Our team of expert bankers are just a phone call away. We will personally handle your query quickly and efficiently, and <b>are available 24/7</b>. This means banking hours no longer apply. The 24/7 Business Desk is another <b>INNOVATIVE</b> first from FNB Business – helping you do business 24/7.</p>
              <p class="popup__terms">Terms, conditions and qualifying criteria apply.</p>
            </div>
          </div>
        </div>
        <div class="btn-holder back-button"><img src="images/btn-products.jpg" style="display:block;" alt=""></div>
    </div>
</div>

<div class="popups-content" style="display: none;">
  <!-- 1 -->
  <div class="" id="product-1">
    <div class="popup-content">
      <h1 class="page_h1 TURQ_COPY">Business Bankers 24/7</h1>
      <div class="clearfix holder">
        <div class="popup__image-holder"><img src="images/p1.jpg"></div>
        <div class="popup__product-description clearfix">
          <p class="popup__intro">24hr Business Desk: another first from FNB. <span style="display: inline-block;"><b>0877 FNB 247</b></span></p>
          <p>Dedicated to solving your clients’ business banking admin queries quickly, easily and efficiently.</p>
          <p>FNB Business is pleased to introduce the 24/7 Business Desk, a dedicated team of bankers to help you solve any administrative banking queries, now available anytime, day or night.</p>
          <p>Our team of expert bankers are just a phone call away. We will personally handle your query quickly and efficiently, and <b style="font-size: 13.5px;">are available 24/7</b>. This means banking hours no longer apply. The 24/7 Business Desk is another <b>INNOVATIVE</b> first from FNB Business – helping you do business 24/7.</p>
          <p class="popup__terms">Terms, conditions and qualifying criteria apply.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- 2 -->
  <div class="" id="product-2">
    <div class="popup-content">
      <h1 class="page_h1 TURQ_COPY">FNB App 5.0</h1>
      <div class="clearfix holder">
        <div class="popup__image-holder"><img src="images/p2.jpg"></div>
        <div class="popup__product-description clearfix">
          <p class="popup__intro"><b>Best for INNOVATIVE businesses.</b></p>
          <p>Introducing FNB App 5.0, the most secure and flexible app we’ve ever created for businesses.</p>
          <p><b style="font-size: 13.5px;">Benefits to your business</b>:</p>
          <ul class="popup__ul">
            <li><b>Dual authorisation</b> | Anywhere, anytime</li>
            <li><b>Multiple login</b> | Instantly switch banking profiles</li>
            <li><b>Report fraud</b> | 24/7 business security</li>
            <li><b>FNB Pay</b> | Make faster payments with tap and pay</li>
            <li><b>Add users</b> | Authorise new users*</li>
          </ul>
          <br/>
          <p class="popup__terms">*Available for Online Banking Enterprise<sup>TM</sup> clients only. </p>
        </div>
      </div>
    </div>
  </div>
  <!-- 3 -->
  <div class="" id="product-3">
    <div class="popup-content">
      <h1 class="page_h1 TURQ_COPY">FNB SmartTILL<sup>TM</sup></h1>
      <div class="clearfix holder">
        <div class="popup__image-holder"><img src="images/p3.jpg"></div>
        <div class="popup__product-description clearfix">
          <p class="popup__intro">The <b>INNOVATIVE</b> retail cash solution for your business.</p>
          <p>FNB SmartTILL<sup>TM</sup> offers point-of-sale cash security unlike any other product on the market.</p>
          <p><b style="font-size: 13.5px;">Benefits to your business</b>:</p>
          <ul class="popup__ul">
            <li><b>Reduce shortages</b> to as low as R0.00</li>
            <li><b>Removes the need</b> for manual cash-ups</li>
            <li><b>Automated and predefined</b> float management</li>
            <li><b>Dispense cash</b> automatically and correctly</li>
            <li><b>Real-time, web-based monitoring</b> of cash in the till</li>
            <li><b>Reduce the risk</b> of theft and robbery</li>
          </ul>
          <br/>
          <p><a href="mailto:smartcash@fnb.co.za" style="font-weight: bold; color: black; text-decoration: none;">smartcash@fnb.co.za</a>  |  <a href="http://www.fnb.co.za" target="_blank" style="font-weight: bold; color: black; text-decoration: none;">www.fnb.co.za</a></p>
          <p class="popup__terms">Terms and conditions apply. Only FNB clients can qualify for an <br>FNB SmartTILL<sup>TM</sup>. </p>
        </div>
      </div>
    </div>
  </div>
  <!-- 4 -->
  <div class="" id="product-4">
    <div class="popup-content">
      <h1 class="page_h1 TURQ_COPY">DocTrail<sup>TM</sup></h1>
      <div class="clearfix holder">
        <div class="popup__image-holder"><img src="images/p4.jpg"></div>
        <div class="popup__product-description clearfix">
          <p class="popup__intro">Delight your auditors with <b>DocTrail<sup>TM</sup></b>.</p>
          <p>DocTrail<sup>TM</sup> makes your business' requisition and audit process as simple as making a payment. It allows your business to upload and attach source documents to payments or transfers on Online Banking Enterprise<sup>TM</sup>, so that managers and auditors know exactly what they were for. What's more, this "document trail" is available online for up to five years. </p>
          <p>DocTrail<sup>TM</sup> is another <b>INNOVATIVE</b> first from FNB.</p>
          <p>For more information, visit <b><a href="http://www.fnb.co.za" style=" color: black; text-decoration: none;">www.fnb.co.za</a></b> or call <b>087 575 0000</b></p>
          <p class="popup__terms">Terms and conditions apply.</p>
        </div>
      </div>
    </div>
  </div>
  <!-- 5 -->
  <div class="" id="product-5">
    <div class="popup-content">
      <h1 class="page_h1 TURQ_COPY">eBucks Rewards for Business</h1>
      <div class="clearfix holder">
        <div class="popup__image-holder"><img src="images/p5.jpg"></div>
        <div class="popup__product-description clearfix">
          <p class="popup__intro"><b>Earn up to 100% back in eBucks</b> on your business online payment transaction fees.</p>
          <p>eBucks Rewards for Business is a free rewards programme that gives FNB Business clients the ability to earn and spend eBucks as a reward for banking with FNB. eBucks can be spent on almost anything - office equipment, stationery, staff incentives, team building or travel. </p>
          <p>eBucks Rewards for Business is another <b>INNOVATIVE</b> first from FNB - helping you grow your business.</p>
          <p>To register for eBucks Rewards for Business, visit <a href="http://www.eBucks.com" style="font-weight: bold; color: black; text-decoration: none;">eBucks.com</a></b></p>
          <p class="popup__terms">Terms, conditions and rules apply.</p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="top-holder">
 <div class="topRightLogoCont " data-0="opacity:1;" data-2450="opacity:1;" data-2500="opacity:0;" data-3950="opacity:0;" data-4000="opacity:1;">
      <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
      <div class="clear"></div>
      <span class="desktop">
        <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
        <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
        <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
      </span> 
  </div>

<?php include('_include/navigation.php'); ?> 
</div>


<script src='js/jquery-1.11.0.min.js'></script>
<script type="text/javascript">
  
  $(document).ready(function (){
  //window.onload = function() {
    $('#main_nav_container').hide(200);
    $('.popup').hide();
    // showPopup(2);

  })

  function showPopup(n){
    $('.products__holder').hide();
    var htmlstring = $('#product-'+n).find('.popup-content').html();
    $('.popup').find('.popup-content').html(htmlstring);
    $('.popup').show(250);
  }


  function showMobileContent(p){
    $('.products').hide();
    var htmlstring = $('#product-'+p).find('.popup-content').html();
    $('.content-mobile').find('.popup-content').html(htmlstring);
    $('.content-mobile').show(250);

  }
  
  $('#nav_menu_btn_open').click(function() {
    $('#main_nav_container').show(250);
  });

  $('#nav_menu_btn_close').click(function() {
    $('#main_nav_container').hide(250);
  });

  $('.modal-btn-close').click(function(){
    $('.popup').hide(150);
    $('.products__holder').show();
  })

  $('.back-button').on('click', function(){
    $('.content-mobile').hide(150);
    $('.products').show();
  })

  
  $('.products__product').on('click', function(e){
    var p = parseInt(e.target.dataset.product);
    if(window.innerWidth > 980){
      showPopup(p);
    }else{
      showMobileContent(p);
    }
  })

    

</script>

    <script type="text/javascript" src="js/frame_functions.js"></script>
    
    <script type="text/javascript" src="js/fnb_functions.js"></script>
</body>
</html>