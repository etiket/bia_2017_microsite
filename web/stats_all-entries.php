<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Stats - All Entries";
$project_page['file_name'] = "rm_client_invites.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

if (isset($_GET['uq']) && $_GET['uq'] == "c7e8c79013697044886") {
	$_rmList = $_ProjectF -> getRMNames();
	$_inviteList = $_ProjectF -> getAllInvites();
	if (count($_inviteList) >= 1) {
		$_inviteCompanyNames = array_column($_inviteList, 'client_company_name');
	}
	
	//$_Project_db -> debug($_inviteList);
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['home'].'');
	exit;
}

include('_include/entry-form-field-values.php');

$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
	<style>
	 a, a:link, a:visited { text-decoration:none; color:#009999; }
	</style>
</head>

<body id="client_invite_page">
	<div class="form_container client_list_wide_container">
    	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Entries Status</h1>
        <?php

		//$_Project_db->where('rm_id',$_SESSION['rm_info']['id']);
		$_AllEntryUsers = $_Project_db -> get($_db_['table']['entry_users'],'id');
		if (is_array($_AllEntryUsers) && count($_AllEntryUsers) >= 1) {
			// InsertData
			$insertData = array();
			?>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            
            <td class="status_head head_company">Legal entity name</td>
            <td class="status_head head_name">Founder</td>
            <td class="status_head head_invited">Invited</td>
            <td class="status_head head_tel">Tel:</td>
            <td class="status_head head_email">Email</td>
            <td class="status_head head_status">Status</td>
          </tr>
		  <?php
		  $TOTAL_SUBMITTED = 0;
		  $TOTAL_SAVED = 0;
		  $TOTAL_REGISTERED = 0;
		  $TOTAL_USERS = 0;
		  $TOTAL_ENTRIES = 0;
		  $TOTAL_INVITED = 0;
		  $TOTAL_MATCHED_WITH_RM = 0;
		  $TOTAL_BANKWITH_FNB = 0;
		  $TOTAL_NONFNB = 0;
		  $industry_array = array();
		  $bgStyle = "grey_tr";
		  
		  $incomplete_entries = array();
			foreach($_AllEntryUsers as $entryUser) {
				$TOTAL_USERS++;
				$entry_data = false;
				//
				if ($entryUser['entry_id'] === 0) {
					$entry_data = false;
					$TOTAL_REGISTERED++;
				}else if ($entryUser['entry_id'] >= 1) {
					$entry_data = true;
					$needUpdate = false;
					$insertData['user_id'] = $entryUser['id'];
					$insertData['status'] = $entryUser['status'];
					$_Project_db -> where('id',$entryUser['entry_id']);
					$entry = $_Project_db -> get($_db_['table']['entries'],'-id');
					if (count($entry) >= 1) {
						$uEntry = $entry[0];
						// collect insert data
						foreach($uEntry as $key => $value) {
							if ($key == 'id') { $insertData['old_id'] = $value; }else {
								$insertData[$key] = $value;
							}
						}
						$industryName = "Other";
						if (isset($uEntry['industry'])) {
							if ($uEntry['industry'] == "") {
								$industryName = "Other";
							}else {
								$industryName = $uEntry['industry'];
							}
						}
						// Check industry
						if (isset($industry_array[$industryName])) {
							$industry_array[$industryName] = $industry_array[$industryName]+1;
						}else {
							$industry_array[$industryName] = 1;
						}
						// check bank with FNB
						if (isset($uEntry['bank_with_fnb']) && $uEntry['bank_with_fnb'] == "YES") {
							$TOTAL_BANKWITH_FNB++;
						}else {
							$TOTAL_NONFNB++;
						}
						
						
						// Clean default entry data
						// DEFINE KEY VALUES
						$UpdateArray = array();
						foreach($uEntry as $key => $entryValue) {
							if (isset($formValues[$key]) && $entryValue === $formValues[$key]) {
								if (strlen($formValues[$key]) >= 2) {
									$UpdateArray[$key] = "";
								}else if ($formValues[$key] == 0) {
									//$UpdateArray[$key] = '0';
								}
							}
						}
						if (count($UpdateArray) >= 1) {
							$needUpdate = true;
							// update current entry data with clean new data.
							//$_Project_db -> where('id',$uEntry['id']);
							//$_Project_db -> update($_db_['table']['entries'], $UpdateArray);
						}
						
						// Find other entries with same user ID and remove
						$_Project_db -> where ('user_id',$uEntry['user_id']);
						$OtherUserEntries = $_Project_db -> get($_db_['table']['entries'],'-id');
						if (count($OtherUserEntries) >= 2) {
							foreach($OtherUserEntries as $otherEntry) {
								if ($otherEntry['id'] == $uEntry['id']) {
									// do nothing
								}else {
									$uoearray = array(
										'user_id' => '0'
									);
									//$_Project_db -> where('id',$otherEntry['id']);
									//$_Project_db -> update($_db_['table']['entries'],$uoearray);
								}
							}
						}
						
						
					}else {
						/*
						$uEntry = array();
						$uEntry['founder'] = "NA";
						$uEntry['business_name'] = "NA";
						$uEntry['tel'] = "NA";
						*/
					}

				}
				
				
				if ($entry_data == true) {
					if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
					$entryInfo = array();
					// check and link the entry with existing invite
					$hasInvite = $_ProjectF -> isEntryAnInvite($entryUser['id'],$entryUser['email'],$entryUser['status']);
					if (isset($hasInvite['already_accepted']) && $hasInvite['already_accepted'] == true) {
						$hasInvite = "<span class='TURQ_COPY'><strong>YES</strong></span>";
						$TOTAL_INVITED++;
					}else if ($hasInvite == false) {
						if (array_search(strtolower(trim($uEntry['rm_name'])),$_rmList) >= 1) {
							$hasInvite = "<span class='TURQ_COPY'><strong>MATCH</strong></span>";
							$TOTAL_MATCHED_WITH_RM++;
						}else {
							$business_name = strtolower(trim($uEntry['business_name']));
							$business_name = str_replace('cc','',$business_name);
							$business_name = str_replace('c.c.','',$business_name);
							$business_name = str_replace('(pty)','',$business_name);
							$business_name = str_replace('pty','',$business_name);
							$business_name = str_replace('ltd','',$business_name);
							$business_name = str_replace('co','',$business_name);
							$business_name = str_replace('.','',$business_name);
							$business_name = str_replace('&','',$business_name);
							$business_name = str_replace('-','',$business_name);
							$business_name = str_replace(' ','',$business_name);
							//echo $business_name;
							//invitekey = array_search($business_name, array_column($_inviteList, 'client_company_name'));
							$invitekey = array_search($business_name,$_inviteCompanyNames);
							if ($invitekey >= 1) {
								$hasInvite = "<span class='TURQ_COPY'><strong>|M|</strong></span>";
								$TOTAL_MATCHED_WITH_RM++;
							}else {
								$hasInvite = "NO";
							}
							//$_Project_db -> debug($invitekey);
						}
						
						

					}else { $hasInvite = "NO"; }
					?>
					<tr class="status_tr <?php echo $bgStyle; ?>">
						<td><?php if ($needUpdate == true) { //echo "|*|".$uEntry['id']."| "; //$_Project_db -> debug($UpdateArray);
						} ?><?php echo $uEntry['business_name']; ?></td>
						<td><?php echo $uEntry['founder_name']; ?></td>
						<td><?php echo $hasInvite; ?></td>
						<td><?php echo $uEntry['tel']; ?></td>
						<td><?php echo $entryUser['email']; ?></td>
						<td><?php
						if ($entryUser['status'] == "SUBMITTED") { ?><a href="stats_rm-viewentry.php?uq=c7e8c79013697044886&eid=<?php echo $uEntry['id']; ?>" target="_blank"><span class="TURQ_COPY">Entry complete</span></a><?php
							$TOTAL_SUBMITTED++;
						
						}else if ($entryUser['status'] == "SAVED") { ?><span class="ORANGE_COPY">Entry Saved</span><?php
							$TOTAL_SAVED++;
							$entryInfo['business_name'] = $uEntry['business_name'];
							$entryInfo['founder'] = $uEntry['founder_name'];
							$entryInfo['tel'] = $uEntry['tel'];
							$entryInfo['email'] = $entryUser['email'];
							$entryInfo['status'] = $entryUser['status'];
							array_push($incomplete_entries,$entryInfo);
						}else if ($entryUser['status'] == "REGISTERED") { ?><span class="DARK_GREY_COPY">Register Only</span><?php
							$TOTAL_REGISTERED++;
							$entryInfo['business_name'] = $uEntry['business_name'];
							$entryInfo['founder'] = $uEntry['founder_name'];
							$entryInfo['tel'] = $uEntry['tel'];
							$entryInfo['email'] = $entryUser['email'];
							$entryInfo['status'] = $entryUser['status'];
							array_push($incomplete_entries,$entryInfo);
						}else { echo $entryUser['status']; }
						//echo " | ".count($OtherUserEntries); ?></td>
					</tr>
					<?php
					$TOTAL_ENTRIES++;
				}
			}
		  ?>
          <tr class="status_footer">
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
		<?php
			// Add entry to final entry folder
			
			
		
		}else {
		?>
        	
        <?php
		}
		?>
        <div class="clear">&nbsp;</div>
        <p><?php echo "TOTAL SUBMITTED ENTRIES: <strong>".$TOTAL_SUBMITTED."</strong>"; ?></p>
        <p><?php echo "TOTAL REGISTERED ONLY: <strong>".$TOTAL_REGISTERED."</strong>"; ?></p>
        <p><?php echo "TOTAL USERS REGISTERED: <strong>".$TOTAL_USERS."</strong>"; ?></p>
        <p><?php echo "TOTAL SAVED: <strong>".$TOTAL_SAVED."</strong>"; ?></p>
        <p><?php echo "TOTAL ENTRIES FROM INVITES: <strong>".$TOTAL_INVITED."</strong>"; ?></p>
        <p><?php echo "TOTAL ENTRIES MATCHED WITH RMs: <strong>".$TOTAL_MATCHED_WITH_RM."</strong> (entries where the name in the 'FNB relationship banker name' field matches with one of the registered RMs' name.)"; ?></p>
        <p><?php echo "TOTAL BANK WITH FNB ENTRIES: <strong>".$TOTAL_BANKWITH_FNB."</strong> (saved entries and submitted entries)"; ?></p>
        <p><?php echo "TOTAL NOT WITH FNB ENTRIES: <strong>".$TOTAL_NONFNB."</strong> (saved entries and submitted entries)"; ?></p>
        
        <hr>
        <p><strong>Industry breakdown</strong></p>
        <p><?php foreach($industry_array as $key => $val) {
					echo $key." = ".$val."<BR>";
		}
		?>
		</p>
        <!--
        <h1 class="page_h1 rm_list_h1 TURQ_COPY">List of Incomplete entries</h1>
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td class="status_head head_company">Legal entity name</td>
                    <td class="status_head head_name">Founder</td>
                    <td class="status_head head_tel">Tel:</td>
                    <td class="status_head head_email">Email</td>
                    <td class="status_head head_status">Status</td>
                  </tr>
        <?php
		/*
		foreach($incomplete_entries as $comp) {
			if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
			?>
            		<tr class="status_tr <?php echo $bgStyle; ?>">
					<td><?php echo $comp['business_name']; ?></td>
					<td><?php echo $comp['founder']; ?></td>
                    <td><?php echo $comp['tel']; ?></td>
					<td><?php echo $comp['email']."<BR>"; ?></td>
					<td><?php
					if ($comp['status'] == "SUBMITTED") { ?><span class="TURQ_COPY">Entry complete</span><?php
					}else if ($comp['status'] == "SAVED") { ?><span class="ORANGE_COPY">Entry Saved</span><?php
					}else if ($comp['status'] == "REGISTERED") { ?><span class="DARK_GREY_COPY">Register Only</span><?php
					}else { echo $comp['status']; } ?></td>
				</tr>
            <?php
		}
		*/
		?>
        <tr class="status_footer">
          	<td></td>
          	<td></td>
            <td></td>
            <td></td>
            <td></td>
          </td>
        </table>
        -->
        <a href="<?php echo $project_data['web_full_address']; ?>stats_export_entries.php">
	        <button class="LEFT square_btn btn_orange" onclick="javascript:window.location='<?php echo $project_data['pages']['entry_form']; ?>?startform=1'">
				<p>Download raw spreadsheet</p>
			</button>
		</a>
    </div> <!-- form container -->
	
    
    <?php
	/*
	// GET ALL ENTRIES
	$_AllEntries = $_Project_db -> get($_db_['table']['entries'],'id');
	foreach($_AllEntries as $fentry) {
		$entryID = $fentry['id'];
		$_Project_db -> where('old_id',$entryID);
		$thisFinalEntry = $_Project_db->get($_db_['table']['entries_final'],'id');
		if (count($thisFinalEntry) >= 1) {
			
		}else {
			$insertData = NULL;
			$insertData = array();
			$insertData['status'] = "SUBMITTED";
					// collect insert data
					foreach($fentry as $key => $value) {
						if ($key == 'id') { $insertData['old_id'] = $value; }else {
							$insertData[$key] = $value;
						}
					}
					//$_Project_db -> insert($_db_['table']['entries_final'],$insertData);
		}
	}
	*/
	?>
    <div class="clear"><p>&nbsp;<br/><br/><br/><br/><br/>&nbsp;<br/></p></div>



	


<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->

    <!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
	<script type="text/javascript">
	var windowWidth = window.innerWidth;
	var windowHeight = window.innerHeight;

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};

	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	</script>
</body>

</html>