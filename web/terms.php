<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "External terms and conditions";
$project_page['file_name'] = "terms.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

$_addToURL = "";
$image_folder = $project_data['full_address']."images/";


?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Terms and conditions</title>

	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>

	<link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/terms-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <style>

    	
    </style>
</head>
<body>
<div class="main_container terms_cont">
  <h1 class="page_h1 TURQ_COPY">TERMS AND CONDITIONS</h1>
  <p class="TURQ_COPY COPY_BOLD"><span style="text-transform:uppercase;">COMPETITION RULES:</span></p>
	<p class="COPY_BOLD">
Date these rules were first published: 12 September 2016<br/>
Date these rules were last changed:  12 September 2016
	</p>
	<p>
Read these competition rules carefully. These competition rules (“rules”) explain your rights and duties in connection with this competition. If you take part in this competition and/or accept any prize, these rules will apply to you and you agree that the promoter(s) can assume that you have read and agreed to be legally bound by these competition rules.
	</p>

	<table class="terms_main_table" cellpadding="0" cellspacing="0">
		<tr>
			<td class="col1 header_row"><p>Competition name:</p></td>
			<td class="col2 header_row"><p><strong>FNB Business Innovation Awards 2017</strong></p></td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td><p>Promoter(s) name(s):</p></td>
			<td><p>This competition is run by FNB Business, a segment of First National Bank, a division of FirstRand Bank Limited, Reg. No. 1929/001225/06 (“FNB”) and Endeavor Entrepreneurship Institute NPC, Reg. No. 2004/014973/08 (“Endeavor”).
			<br/><br/>
In these rules we refer to the above promoter(s) as “the promoter(s)”, or “us” or “we”. We will refer to participants and winners as “you”. </p></td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Entries open:</p>
			</td>
			<td>
				<p>Entries open on Monday 12 September 2016 at 08h00.</p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Entries close: </p>
			</td>
			<td>
				<p>Entries close on Tuesday 31 January 2017 at 23h59. No submissions will be accepted after these deadlines.
					<br><br>
All fully completed entries must be received by the promoter(s) before the closing time. 
					<br><br>
The promoter(s) reserve the right to extend the competition. Notice of this will be posted on <a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a></p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Prize details:</p>
			</td>
			<td>
				<p>The winner of the FNB Business Innovation Awards will be given an 
opportunity to participate in the coveted 2 (two) day Endeavor* 
International Selection Panel (ISP) during 2017. The ISP brings together high-impact entrepreneurs to have their businesses evaluated by  world-class business leaders, and gives them the opportunity to become 
part of an exclusive global network.
					<br><br>
The prize includes travel and accommodation for 2 (two) representatives 
of the winning business. Any additional representatives attending the ISP 
will be responsible for their own costs associated with the ISP. Please note that spending money is not included in the prize.
					<br><br>
					<i>
*Endeavor is the leading supporter of high-impact entrepreneurs around 
the world. By high-impact we mean individuals with the biggest dreams, 
the greatest potential to create companies that matter and grow, and the highest likelihood to inspire others. Endeavor is the only organization to 
focus on the scale-up, not the start-up, because that is where Endeavor 
thinks the highest job and wealth creation happens.
					</i></p>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Winners announced in:</p>
			</td>
			<td>
				<p>June 2017 or as soon as possible thereafter.</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Eligibility: who qualiﬁes to take part? </p>
			</td>
			<td>
				<p>This competition is open to all South African businesses (companies, close corporations, trusts, sole proprietors) who meet the criteria set out below: </p>
				<p>The businesses should:</p>
				<ul>
                	<li>Have a minimum annual turnover of R10-million.</li>
                	<li>Be founder led.</li>
                	<li>Be a unique business that cannot easily be replicated. It should not be a “me too” business, such as a consultancy or agency.</li>
                	<li>Have the potential to grow exponentially.</li>
                	<li>Have successfully raised the capital it needed to get to this point.</li>
                	<li>Be scaleable; in other words, the business should have the potential to grow and become a market leader with a business model that is repeatable in any country/region.</li>
                </ul>
				<p>Furthermore, the businesses need to be able to meet the following standards:</p>
				<ul>
					<li>Development impact: exhibit high-growth potential and the capacity to add substantial economic value by creating a number of high-value jobs.</li>
					<li>Business innovation: demonstrate real innovation that has the potential to change the way an industry operates locally and/or internationally.</li>
					<li>Fit with FNB and Endeavor: be interested in accepting advice and support from FNB and Endeavor, as well as contributing to the Endeavor network.</li>
				</ul>
			</td>
		</tr>

		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Who cannot take part?</p>
			</td>
			<td>
				<p>The following persons may not take part in this competition even if they
qualify to take part. They will forfeit (give up) any prizes awarded to them: 
				</p>
				<ol type="a">
					<li>Any employee of the promoter(s)</li>
					<li>Any director, member, partner, agent of, or consultant of 
   the promoter(s).</li>
					<li>Any other person who is directly or indirectly controls the promoter(s).</li>
					<li>Any supplier of goods and services in connection with this competition. </li>
				</ol>

				<p>
The spouse, life partner, siblings, children, or parents of any of the persons
named in a, b, c, or d, above.</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Judging criteria:</p>
			</td>
			<td>
				<p>The promoter(s) are looking for the most innovative entrepreneur
business of the year.
<br><br>
All entries will go through a vetting and judging process to determine 
the winner.
<br><br>
There will be various phases of judging. Endeavor’s panel of senior
business leaders will conduct the initial phases of the judging (including
interviews and/or examining the participants’ financials) and will submit
a list of finalists (who receive unanimous votes from the panel) to FNB
for evaluation. FNB accepts no responsibility or liability for the entry
filtering process and has no influence over the entry-filtering process 
and/or the choice of finalists.
<br><br>
FNB, together with Endeavor will interview the finalists and make the final
decision as to the winner of the competition. 
<br><br>
Broadly, judging of each entry will based on the following selection criteria: 
</p>
				<ol type="1">
					<li>Turnover: In excess of R10,000,000.00 (ten million rand) per year;</li>
					<li>Leadership: Founder-led business;</li>
					<li>Development Impact: Exhibit high-growth potential and the capacity 
to add substantial economic value by creating a number of 
high-value jobs;</li>
					<li>Business Innovation: Demonstrate real innovation that has the potential to change the way an industry operates locally and/or internationally;</li>
					<li>Fit with FNB and Endeavor: Be interested in accepting advice and support from FNB and Endeavor, as well as contributing to the 
Endeavor network.</li>
				</ol>

				<p>
				More detail on the judging criteria can be found at 
<a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a>
				</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Important conditions – read carefully:</p>
			</td>
			<td>
				<ol type="1">
					<li>If the business is owned by more than one person, the promoter(s) will not be legally responsible to anyone for disputes between such persons or for errors or omissions during the submission process. The owners each agree that they will not hold the promoter(s) legally responsible for any loss or damage they suffer because their business was entered into this competition.  The person who enters on behalf of other persons warrants that they all have agreed to the competition rules. If the person entering breaches this warranty, he or she alone will be legally responsible to the other persons and will at the discretion of the promoter(s) defend the promoter(s) against any legal claim or action by that person. This means that if any of the promoters are sued, the person who entered will, at the promoter’s(s’) discretion, defend them against such claims, and pay all legal costs or expenses, or amounts they paid or must pay in damages to any person or to settle any claim, back to the promoter(s). Legal costs and expenses will be calculated on an attorney and own client scale.</li>
					<li>While the promoter(s) may provide assistance and advice to the winner, the promoter(s) cannot guarantee that the winner’s business will be commercially successful. By entering the competition and accepting the prize, the winner agrees to indemnify (hold harmless) the promoter(s) if the winner or any other person suffers any loss or damage because of their participation in this competition, regardless of how or when they suffered such loss or damage.</li>
					<li>You enter this competition at your own risk. The promoter(s) do not warrant that your business will be successful. </li>
					<li>Before the winner will be awarded a prize, they will sign a contract with Endeavor and must agree to Endeavor’s terms and conditions of service, confidentiality and so forth. Winners have the right to decline to do so. If they decline to do so, they will forfeit (give up) their participation in the ISP and any subsequent assistance from Endeavor. 
					<li>Winners must abide by the rules, terms and conditions which are determined by any third party carrier, transporter, accommodation provider, catering provider or other service provider associated with 
the prize. </li>
					<li>The promoter(s) reserve the right at their own discretion without having to give reasons for this to immediately remove any entry or disqualify a winner if his/her business or any aspect thereof is alleged to be an infringement of any 3rd party’s rights, including intellectual property rights such as copyright, patents, designs, trademarks, confidential information, etc. If this happens,  the promoter(s) will not be legally responsible to any person and the winner will forfeit the prize or the remaining value thereof.  The promoter(s) do not have to investigate the validity of such 3rd parties’ complaints before exercising these rights.</li>
					<li>You also agree to hold the promoter(s) harmless against any claims by parties that your business or any aspect thereof infringes their rights in any way. This means that if any of the promoters are sued, you will at their discretion defend them against such claims and/or pay back any legal costs or expenses, or amounts they paid or must pay in damages to any person or to settle any claim, back to the promoters. Legal costs and expenses will be calculated on an attorney and own client scale. It will be your sole responsibility to conduct, at your cost and prior to submitting your entry for the competition, all the necessary checks to determine whether your business or any aspect thereof infringes any third party’s rights. You should contact a reputable intellectual property lawyer to assist you. </li>
					<li>By entering this competition, you agree to release the promoters from any legal responsibility to you or any other person, because the promoters are developing, launching or considering identical or similar businesses. You need to be aware that the promoters receive, assist with or develop new ideas all the time and that it is possible that an identical or similar business may be in any stage of launch or development.  You agree that you will not take legal action against the promoters because they or any person associated with them are developing, launching, assisting or considering a business that is similar to yours, provided the promoters can prove that they considered such a business prior to you submitting your entry for this competition or without reference to your entry for this competition.</li>
					<li>You also understand that your business innovations may or will be published on a public website. This means that other persons can copy your innovations.  It also could mean that your innovation may no longer qualify for patent or eventually design protection. You carry this risk if you enter the competition. We recommend that, before submitting your entry to the promoters, you obtain legal advice from a reputable intellectual property attorney.</li>
					<li>The promoter(s) do however agree that your business’ financial details will be kept strictly confidential.</li>
				</ol>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How to enter? </p>
			</td>
			<td>
				<ol type="1">
					<li>You have to submit your entry online via <a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a>, using the online entry forms and online templates we make available. You must submit all the required information on time.</li>
					<li>All entries will be reviewed by the Endeavor and FNB judging panels.</li>
				</ol>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Is there a limit on the number of times you 
can enter? </p>
			</td>
			<td>
				<p>Yes, only 1 (one) entry per participant per year.</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How will winner(s) 
be chosen?</p>
			</td>
			<td>
				<p>Please refer to the judging criteria set out above.
<br><br>
<strong>Note: The judges’ decision is final and no correspondence will be entered into.  This means you cannot appeal any decision by the judges.</strong>
</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>How will winner(s) names be announced?</p>
			</td>
			<td>
				<p>Winners will be contacted by phone or email, and will be announced on 
<a href="http://www.fnbbusinessinnovationawards.co.za" target="_blank" style="color:#000000; text-decoration:underline;"><strong>www.fnbbusinessinnovationawards.co.za</strong></a> and at the 2017 FNB Business Innovation Awards event.
<br><br>
<strong>
Note: Whilst finalists and/or prize winners may be asked to take part in publicity for the competition, they have the right to refuse to do so.</strong></p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Deadline for 
claiming prize(s):</p>
			</td>
			<td>
				<p>Within 6 (six) months of announcement of the winner.</p>
			</td>
		</tr>
		
		<!-- ROW ------------------- -->
		<tr>
			<td>
				<p>Questions about 
these rules:</p>
			</td>
			<td>
				<p>You can forward your questions/queries to the following email address: <a href="mailto:innovationawards@fnb.co.za" style="color:#000000; text-decoration:underline;"><strong>innovationawards@fnb.co.za</strong></a></p>
			</td>
		</tr>
		
		<!-- ROW -------------------
		<tr>
			<td>
				<p></p>
			</td>
			<td>
				<p></p>
			</td>
		</tr> -->		
		<!-- tr>td*2>p -->
	</table>

	<p class="TURQ_COPY"><span style="text-transform:uppercase;"><strong>General rules that apply:</strong></span></p>

	<p class="TURQ_COPY">The following rules apply to competitions run on social network sites like Facebook, Twitter, Foursquare 
or Pinterest:</p>

	<ul>
		<li>Site refers to any social network site on which this competition will be run.
		<li>If the competition requires you to post anything, your post must be original and written by you. For purposes 
      of these rules “post” includes, but isn’t limited to any written material or visual or audio material such as an 
      image file, a photo, a sound or video clip. You may not use somebody else’s post or work as your post, or as 
      part of your post. If you are allowed to quote content from another source, you must state clearly who the 
      author is. Any post that we believe to constitute plagiarism will be disqualified.</li> 
		<li>You may not post updates that are derogatory or harmful to the promoter(s), the site, or any other person.   
      By posting any content you warrant to the promoter(s) and the sites that you are the sole creator, designer, 
      author or owner of the work and that you have the right to post the work.</li> 
		<li>If the competition involves voting you may not vote for yourself or create fake profiles to obtain votes or use 
      any other dishonest means to obtain votes. Any automated or bulk votes will automatically be disqualified.</li> 
		<li>You may not misuse the site or competition in any way. This includes posting content for commercial 
      purposes or distributing spam or malicious code or using the site to collect the personal data or content of 
      other users or direct visitors to other sites or pages.</li>
		<li> Posts must not contain any content that is normally prohibited on the site such as explicit or offensive content.</li> 
		<li>You agree that the promoter(s) have the absolute discretion to decide if your actions constitute any of the 
      actions prohibited above and to end your participation in the competition immediately and take appropriate 
      legal action against you. The promoter(s) may also immediately remove any posts that they reasonably 
      believe constitute any prohibited content without notice to you.  </li> 
		<li>You must comply with the terms &amp; conditions and privacy policies of the site.</li>
		<li>By posting the submission you give the promoter(s) a world-wide, royalty free and non-exclusive license to 
      reproduce, modify, adapt and publish any content you have posted on the site for purposes of the 
      competition for the purpose of promoting the products and services of the promoter(s). Note: Refer to the 
      specific competition rules above. The promoter(s) will not publish your name or photograph unless we have 
      your consent to do so.</li> 
		<li> The promoter(s) are not responsible for the sites. The promoter(s) cannot control how they operate, when 
     or if they are available, or what content they carry. Even though the promoter(s) run the competition on the 
     site the promoter(s) do not endorse the third party, its site, its products, or services or any content on the 
     site. You use the site at your own risk. You must take all necessary measures to protect yourself from risks, 
     such as viruses and other destructive code. </li> 
		<li>You must obtain at your own expense all equipment and services that are necessary to gain access to the 
      site to take part in this competition.</li> 
		<li> The promoter(s) can put in place such technical or other remedies it considers appropriate to prevent 
     abuse, or to protect any sites or our systems or other users.</li>
		<li>You understand that this competition is not in any way being sponsored, endorsed or administered by, or 
     associated with the site. You fully release the site from any legal responsibility to you or any other person.  
      By participating in this competition, you understand that you are providing information to the promoter(s)   
     and not to the site.</li> 
		<li>If you don’t comply with this clause your posts can be deleted and you will not be allowed to access the site 
      or take part in the competition.</li>
	</ul>

	<p class="TURQ_COPY">The following rules apply to competitions generally:</p>

	<div class="border_cont">
		<p><strong>Important:</strong></p>
		<ul>
			<li>You agree to indemnify the promoter(s) fully for any loss or damage the promoter(s) may suffer 
     because you breached the competition rules. This means you agree to reimburse the promoter(s) for 
     the following: any loss or damage they suffer, any expenses and costs they paid or are responsible for. 
     Legal costs means costs on an attorney and own client scale.</li>
			<li>You also agree to indemnify the promoter(s) for any loss or damage you suffered because you took 
     part in this competition or used the prize. If you enter yourself, or use or accept the prize, you 
     understand that you do so of your own free will. This means that you cannot hold the promoter(s) 
     legally responsible for any loss or damage or legal expenses you suffered because you took part in this 
     competition or used the prize.</li>
			<li>You will protect the promoter(s) from being held legally responsible for the loss or damage or legal 
     expenses of another person (legal or natural) if such loss or damage or expense was incurred because you: a) breached the competition rules b) took part in this competition or c) and such person 
     used a prize.</li>
		</ul>
	</div>

	<ul>
		<li>If the promoter(s) are not able to get hold of you after making reasonable efforts to do so, or you do not 
     claim your prize on time, you will lose your prize and the judges may award it to someone else.</li>  
		<li>If you fail to comply with any part of these rules you will be disqualified and you will forfeit any prize(s). </li>
		<li>Unless we say otherwise you must be at least 18 (eighteen) to enter.</li>
		<li>Unless we say otherwise entry is restricted to 1 (one) entry per person and multiple entries will be disqualified.</li>
		<li>Automated or bulk entries or votes will be disqualified.</li>
		<li>The prizes may not be sold or given to someone else.</li>
		<li>The prizes cannot be swapped for cash or a different prize. </li>
		<li>You are responsible for the tax associated with using or accepting any prize. </li>
		<li>You may not attempt to do anything to change the outcome of the competition in any way.</li>
		<li>The promoter(s) have the right to end this competition at any time. If this happens you agree to waive (give up) any rights that you may have about this competition and agree that you will have no rights against 
     the promoter(s).</li>
		<li>The promoter(s) reserve the right to change the rules of the competition. The promoter(s) can change the 
     rules of the competition throughout the duration of the competition.  For convenience only, the date on 
     which these rules were last amended will be shown below the heading. It is your responsibility to check the 
     rules for amendments. </li>
		<li>The clauses in these rules are severable. This means that if any clause in these rules is found to be unlawful, 
     it will be removed and the remaining clauses will still apply.</li>
		<li>Where any dates or times need to be calculated in terms of this agreement, the international standard time: 
     GMT plus two hours will be used.</li>
		<li>While the promoter(s) may allow you extra time to comply with your obligations or decide not to exercise 
     some or all of our rights, or waive certain requirements, the promoter(s) can still insist on the strict 
     application of any or all of its rights at a later stage. You must not assume that this means that the rules have been changed or that it no longer applies to you. </li>
		<li>You must send all legal notices to FNB Legal, 3rd Floor, No 1 First Place, Bank City, Johannesburg, 2001. </li>
		<li>This competition and its rules will be governed by the law of the Republic of South Africa regardless of 
     where you live or work, or where or how you enter. </li>
	</ul>

	<ul style="color:#ffffff">
		<li>&nbsp;</li>
		<li>&nbsp;</li>
	</ul>
</div>

<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont">
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_in_white.svg" class="in_share_btn DESKTOP"/>
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_twitter_white.svg" class="twitter_share_site DESKTOP">
    <img src="http://www.fnbbusinessinnovationawards.co.za/images/social_btn_fb_white.svg" class="fb_share_thispage DESKTOP">
</div> <!-- topRightLogo WHITE -->
<?php include('_include/navigation.php'); ?>


<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
    <script src='js/jquery-1.11.0.min.js'></script>
    <script type="text/javascript" src="js/frame_functions.js"></script>
    <script type="text/javascript" src="js/fnb_functions.js"></script>
<script>
	//$(document).ready(function (){
	window.onload = function() {
		//window.addEventListener( 'resize', onWindowResize, false );
		$('#main_nav_container').hide();
	};
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

</script>
</body>
</html>