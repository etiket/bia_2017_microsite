$(document).ready(function(){
	updateVisible();
	$('.toggle-btn').on('click', function(event){
		// console.log($(this).parent().attr('id'));
		toggleYesNo($(this), $(this).parent());
	});
	
	$('.help').on('click', popToast);
	
	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-bottom-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "20000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	};
	
});

toastArr=[
	"Endeavor supports entrepreneurs and we therefore require the founders of the business to be actively involved with the business and have the authority to make key decisions. This could either mean that the founding team continues to hold a majority equity stake in the business, or in the case of a minority stake, be in an active leadership position.",
	"Endeavor’s network and services are focused on assisting entrepreneurs to scale their businesses. Endeavor therefore typically only enters businesses into the selection process that have passed the R10-million revenue mark or that realistically will pass this mark during the selection process. For the purposes of the awards, the minimum turnover is R10-million in the last financial year.",
	"Endeavor selects entrepreneurs who run high-growth businesses. To be selected as an Endeavor entrepreneur, the panel will need to see revenue projections significantly beyond the country and industry average.",
	"Endeavor looks for businesses that are unique in the South African context and use an innovative model or approach as their competitive advantage. This could be in the form of a unique brand, innovative business model or through Intellectual Property. The business should preferably have the potential to be scaled or replicated internationally.",
	"Intellectual Property refers to any proprietary property such as a registered trademark, patent or otherwise legally protected material. This excludes anything not enforceable by law such as ideas or concepts, and any third-party Intellectual Property that is being licensed."
];

function popToast(){
	var id = $(this).attr('id');
	toastr["success"](toastArr[id]);
}

function toggleYesNo(btn, holder){
	var id = holder.attr('id');
	$(holder.find('.btn-yes')).removeClass('selected');
	$(holder.find('.btn-no')).removeClass('selected');
	$(holder.find('.hidden_yes_no')).val(btn.html());
	validation_mark_input_pass($(holder).attr('id'));
	btn.addClass('selected');
	
	updateVisible();
}

function updateVisible(){
	$('.yn-answer').each(function(){
		var id = $(this).attr('yesno');
		
		if($('#'+id).find('.btn-yes').hasClass('selected')){
			$(this).removeClass('hidden');
		}else{
			$(this).addClass('hidden');
		}
	});
}
