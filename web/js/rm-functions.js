// JavaScript Document
// FUNCTIONS ----

function calculateErrorBeforeSubmit(errors,formID) {
	var frmID = formID || "";
	//	Calculate and build error message
	var errorMessage = "";
	if (errors.length >= 1) {
		for (var i = 0; i < errors.length; i++) {
			errorMessage += errors[i]+"<BR>";
		}
		$('#form_validate_error').html(errorMessage);
		$('#form_validate_error').show();
	}else {
		if (frmID === "") {
			return true;
		}else {
			$("form#"+frmID).submit();	
		}
	}
	//	Submit given Form
}

/* SOCIAL SHARE BUTTON FUNCTIONS */
$('.fb_share_thispage').click(function() {
	open_fb_share("http://www.fnbbusinessinnovationawards.co.za");
});
$('.in_share_btn').click(function() {
	open_linkedin_share("http://www.fnbbusinessinnovationawards.co.za");
});
$('.twitter_share').click(function() {
	open_twitter_share("FNB Business Innovation Awards http://www.fnbbusinessinnovationawards.co.za","http://www.fnbbusinessinnovationawards.co.za");
});
// share domain
$('.fb_share_site').click(function() {
	open_fb_share("http://www.fnbbusinessinnovationawards.co.za");
});
$('.twitter_share_site').click(function() {
	open_twitter_share("FNB Business Innovation Awards http://www.fnbbusinessinnovationawards.co.za","www.fnbbusinessinnovationawards.co.za");
});


function clearFakePlaceholder(target,action) {
	if (action == 'focus') {
		if ($(target).val() === $(target).attr('placeholder')) {
			if (target.nodeName.toLowerCase() === "textarea") {
				$(target).html('');
			}else {
				$(target).val('');
			}
		}
		$(target).removeClass('placeholder_input');
	}else if (action == 'blur') {
		if ($(target).val() === "") {
			if (target.nodeName.toLowerCase() === "textarea") {
				$(target).html($(this).attr('placeholder'));
			}else {
				$(target).val($(this).attr('placeholder'));	
			}
			
			if ($(target).hasClass('placeholder_input')) {
			}else {
				$(target).addClass('placeholder_input');
			}
		}else {
			$(target).removeClass('placeholder_input');	
		}
	}
}
function onSelectListChange(targetList) {
	// var selectedValue = $(targetList).val();
	var selectedValue = targetList.options[targetList.selectedIndex].value;
	if (selectedValue === "0" || selectedValue === "1") {
		if ($(targetList).hasClass('placeholder_input')) {
		}else {
			$(targetList).addClass('placeholder_input');
		}		
	}else {
		$(targetList).removeClass('placeholder_input');	
	}
}




function onIndustrySelectList(targetList) {
	var selectedValue = targetList.options[targetList.selectedIndex].value;
	var attr = $(targetList).attr('myDesc');
	if (selectedValue === "Other") {
		if (typeof attr !== typeof undefined && attr !== false) {
		    toggleFieldDisplay($('#'+attr),"YES");
		}
	}else {
		if (typeof attr !== typeof undefined && attr !== false) {
		    toggleFieldDisplay($('#'+attr),"NO");
		    $('#'+attr).val('');
		}
	}
}