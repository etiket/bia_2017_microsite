/*
* frame_functions.js
* Developed by: Arno Cilliers
* My first attempt to create a standardised system frame
* javascript file that will hold all the basic re
* re-usable js functions
*/


/*
CLEAN OPEN A PAGE
Display a div by first running through an array of pages to hide then display
the one that was spesified
*/
function cleanOpenPage(pageOpen,closeArray) {
	for(var i = 0; i < closeArray.length; i++) {
		var pagename = closeArray[i];
		if (pageOpen == pagename) {
			//openPage(pagename);
		}else {
			noDisplay(pagename);
		}
	}
	openPage(pageOpen);
}

function frame_sanitizeString(str) {
	// return str.replace(/<\/?[^>]+(>|$)/g, "");
	return str.replace(/<(?:.|\n)*?>/gm, '');
}
// VALIDATION FUNCTIONS

/* VALIDATE: EMAIL */
function frame_validateEmail(email) {
	//	http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
	//	/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
	//console.log(str.match(/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/));
  	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  	return regex.test(email);
}
/* VALIDATE: NUMBER */
function frame_validateNumber(str) {
	if (str === null || str === undefined || str === '') {
		return false;
	}else if(isNaN(str)){
	  	return false;
	}else {
		return true;
	}
}
/* VALIDATE: PASSWORD */
function frame_validatePassword(password,repassword) {
	$.trim(password);
	$.trim(repassword);
	if (password.length >= 2) {
		if (password == repassword) {
			return true;
		}else {
			return false;
		}
	}else {
		return false;
	}
}
/* CHARACTER COUNT */
function frame_charCount(string,minChars,maxChars) {
	if (typeof string != "string") {
		string = $(string).val();
	}
	$.trim(string);
	if (string.length < minChars) {
		return false;
	}else {
		if (string.length > maxChars) {
			return false;
			//return errMessage;
		}else {
			return true;
		}
	}
}

/* WORD COUNT */
function frame_wordCount(string,minWords,maxWords) {
	$.trim(string);
	var wordsArray = string.split(" ");
	if (wordsArray.length < minWords) {
		return false;
	}else {
		if (wordsArray.length > maxWords) {
			return false;
		}else {
			return true;
		}
	}
}
/* MARK ERROR FIELDS */
function frame_markErrorFields(inputID) {
	if (typeof inputID == 'string') {
		//console.log('InputID Marked: '+inputID);
		$('#'+inputID).addClass('frame_input_marked');
	}else {
		$(inputID).addClass('frame_input_marked');
	}
}
/* UNMARK ERROR FIELDS */
function frame_unmarkErrorFields(inputID) {
	if (typeof inputID == 'string') {
		$('#'+inputID).removeClass('frame_input_marked');
	}else {
		$(inputID).removeClass('frame_input_marked');
	}
}
/* MARK CHECKED FIELDS */
function frame_markCheckedFields(inputID) {
	if (typeof inputID == 'string') {
		$('#'+inputID).addClass('frame_input_checked');
	}else {
		$(inputID).addClass('frame_input_checked');
	}
}
/* UNMARK CHECKED FIELDS */
function frame_unmarkCheckedFields(inputID) {
	if (typeof inputID == 'string') {
		$('#'+inputID).removeClass('frame_input_checked');
	}else {
		$(inputID).removeClass('frame_input_checked');
	}
}

/* BUILD ERROR */
function frame_buildErrorModal(errorArray) {
	if (errorArray.length >= 1) {
		var errormessage = "";
		for(var i = 0; i < errorArray.length; i++) {
			errormessage+=errorArray[i]+'\n';
		}
		errorArray = null;
		$('#user_form_container').show(0,function() {
			$('#lightbox_mask').show(0);
		});
		window.scrollTo(0, 0);
	}else {
		return true;
	}
}

function displayDelayRemove(target,delaytime) {
	setTimeout(function (){
		$(target).remove();
	}, delaytime);
}

function animatePosition(targetID,distance,miliseconds) {
	$( "#"+targetID ).animate({
		right: distance
	}, miliseconds, function() {
		
	});
}
function gotoURL(linkurl) {
	window.location.href = linkurl;
}
// FACEBOOK AND TWITTER SHARE WINDOWS
/*  ----------- */
/*  BASIC JS    */	
function open_fb_share(address) {
	if (address == null) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		address = "http://www.facebook.com/sharer/sharer.php?u="+domainname+pathname;
	}else {
		address = "http://www.facebook.com/sharer/sharer.php?u="+address;
	}
	//var URI = encodeURIComponent(address)
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function open_twitter_share(text,address) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		text = encodeURIComponent(text);
		pathname = encodeURIComponent(pathname);
	if (address == null) {
		address = "https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F"+domainname+pathname+"&via=FNBSA&related=FNBSA&text="+text;
	}else {
		address = encodeURIComponent(address);
		address = "https://twitter.com/intent/tweet?original_referer=http%3A%2F%2F"+address+"&via=FNBSA&related=FNBSA&text="+text;
	}
	//var URI = encodeURIComponent(address)
	//alert("Twitter address: "+address);
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function open_linkedin_share(address) {
	// https://www.linkedin.com/cws/share?url=http%3A%2F%2Fgoogle.com
	if (address == null) {
		var pathname = window.location.pathname;
		var domainname = document.domain;
		address = "https://www.linkedin.com/cws/share?url=http%3A%2F%2F"+domainname+pathname;
	}else {
		address = "https://www.linkedin.com/cws/share?url=http%3A%2F%2F"+address;
	}
	//var URI = encodeURIComponent(address)
	msg=window.open(address,"msg","height=350,width=640,left=100,top=100, scrollbars=no", "scrolling=auto");
}
function addslashes( str ) {
    return (str + '').replace(/[\\"']/g, '\\$&').replace(/\u0000/g, '\\0');
}


function frame_clearTxtInput(target) {
	if ($(target).val() === $(target).attr('placeholder')) {
		$(target).val('');
	}else if ($(target).val() == "") {
		$(target).val($(target).attr('placeholder'))
	}
}
function frame_clearTxtArea(target) {
	if ($(target).html() === $(target).attr('placeholder')) {
		$(target).html('');
	}else if ($(target).html() == "") {
		$(target).html($(target).attr('placeholder'))
	}
}

function onSelectWhereCompSource(targetList) {
	var selectedValue = targetList.options[targetList.selectedIndex].value;
	var attr = $(targetList).attr('myDesc');
	// if (selectedValue === "Other") {
	// 	if (typeof attr !== typeof undefined && attr !== false) {
	// 	    toggleFieldDisplay($('#'+attr),"YES");
	// 	}
	// }else {
	// 	if (typeof attr !== typeof undefined && attr !== false) {
	// 	    toggleFieldDisplay($('#'+attr),"NO");
	// 	    $('#'+attr).val('');
	// 	}
	// }
		if (typeof attr !== typeof undefined && attr !== false && selectedValue != '0' && selectedValue != 'Digital' && selectedValue != 'Billboard') {
		    toggleFieldDisplay($('#'+attr),"YES");
		}else {
		    toggleFieldDisplay($('#'+attr),"NO");
		    $('#'+attr).val('');
		}
		console.log(attr,selectedValue);
}


