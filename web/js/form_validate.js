/**
 * only allow numbers in a HTML input field with class name of "numbersOnly"
 */
var numbersOnly = document.getElementsByClassName('.numbersOnly');
for (var n=0; n<numbersOnly.length; n++){
  numbersOnly[n].addEventListener('keypress', function (e){
        if(e.charCode >= 48 && e.charCode <=57){
            return;
        }else{
            e.preventDefault();
        }
    });
}

/**
 * validates a string against options
 * options example{
 *   min: 0,
 *   max: 100,
 *   numbersOnly: true,
 *   lettersOnly: false,
 *   error: "This is error message."
 * }
 *
 */
function validateString(fieldObject, options){
  var validateMe = fieldObject.attr('validateMe');
  if ((typeof validateMe !== typeof undefined && validateMe !== false)) {
    if (validateMe === "YES") {
      var str = fieldObject.val();
      var o = {
        min: options.min || 0,
        max: options.max || null,
        numbersOnly: options.numbersOnly || false,
        lettersOnly: options.lettersOnly || false,
        error: options.error || "ERROR not specified.",
      }
      if(!((str.length < o.min || (o.max !=null && str.length > o.max)) || (containsLetters(str) && o.numbersOnly) || (containsNumbers(str) && o.lettersOnly) )){
        return true;
      }else{
        return o.error;
      }
    }else {
      return true;
    }
  }else {
    return true;
  }
}

/**
 * validate email address
 * options = {
 *   error: "this is error."
 * }
 */
function validateEmail(fieldObject, options){
  var o = {
    error: options.error || "Not a valid email address."
  }
  var email = /^[a-z0-9](\.?[a-z0-9_-]){0,}@[a-z0-9-]+\.([a-z]{1,6}\.)?[a-z]{2,6}$/
  return email.test(myTrim(fieldObject.val()))?true:o.error
}
function myTrim(x) {
    return x.replace(/^\s+|\s+$/gm,'');
}


/**
 * contact numbers
 * accepts cellphone numbers, telkom numbers starting with 0 or +27
 * options = {
 *   error: "this is error"
 * } 
 */
function validateContact(fieldObject, options){
  var o = {
    error: options.error || "error validating contact number"
  }
  var SAphoneNumber = /^(\+27|0)[0-9]{9}$/
  return SAphoneNumber.test(fieldObject.val())?true:o.error
}

/**
 * validates business registration number
 * format must be: 0000/000000/00
 * options = {
 *   error: "this is error"
 * }
 */
function validateRegistrationNumber(fieldObject, options){
  var o = {
    error: options.error || "business registration number invalid"
  }
  var regNo = /^[0-9]{4,4}\/[0-9]{6,6}\/[0-9]{2,2}$/
  return regNo.test(fieldObject.val())?true:o.error
}

/**
 * check value of number field
 * options = {
 *   error: "this is error",
 *   min: 0,
 *   max: 2016,
 * }
 */
function checkNumberValue(fieldObject, options){
  var o = {
    error: options.error || "ERROR validating number value",
    min: options.min || 0,
    max: options.max || 1,
  }
  var n = fieldObject.val();
  if((n < o.min || n > o.max) || containsLetters(n)){
    return o.error;
  }else{
    return true;
  }
}

/**
 * validate dropdowns
 * value = 0 // false
 * options={
 *   error: "this is be error",
 * }
 */
function validateDropdown(fieldObject, options){
  var o = {
    error: options.error || "error validating dropdown"
  }

  // var value = fieldObject.find(":selected").val();
  var value = fieldObject.prop('selectedIndex');
  //console.log("fieldObject: "+$(fieldObject).attr('id'));
  if(value === 0 || value === "0" || value === null || value === undefined){
    return o.error;
  }else{
    return true;
  }
}

/**
 * validate radio buttons
 * just returns the value of the radio group - should only be a "yes" or "no"
 */
function validateRadio(obj){
  return obj.val();
}

// ---------------- helper functions ------------------------------------------------
function containsNumbers(str){
  return (str.match(/[0-9]/i) != null);
}

function containsLetters(str){
  s = str.toLowerCase();
  return (str.match(/[a-z]/i) != null);
}

