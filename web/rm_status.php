<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Relationship manager status.";
$project_page['file_name'] = "rm_status.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);
include('_include/cd.php');


if (isset($_SESSION['rm_info'])) {
	
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['rm_login'].'');
	exit;
}
$image_folder = $project_data['full_address']."images/";
?><!DOCTYPE html>
<html>
<head class="no-skrollr">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>FNB BIA - RM Status</title>


	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" /> -->
    
	<meta name="apple-mobile-web-app-capable" content="yes" />
    
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
    

	<!-- CSS
  ================================================== -->
    
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/rm-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/lightbox-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
	
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>

</head>

<body>

<?php
// INCLUDE VerSaDUHDUH tag
include('_include/versaduhTag.php');
?>

<div class="form_container client_list_container">
	<h1 class="page_h1 rm_list_h1 TURQ_COPY">Nomination status</h1>
    <?php
	$_Project_db->where('rm_id',$_SESSION['rm_info']['id']);
	$existingInvites = $_Project_db->get($_db_['table']['rm_invites'],'-id');
	if (is_array($existingInvites) && count($existingInvites) >= 1) {
		?>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td class="status_head head_name">Client name</td>
        <td class="status_head head_company">Client business name</td>
        <td class="status_head head_email">Client email address</td>
        <td class="status_head head_status">Status</td>
      </tr>
	  <?php
	  $bgStyle = "grey_tr";
		foreach($existingInvites as $invite) {
			if ($bgStyle == "grey_tr") { $bgStyle = "white_tr"; }else { $bgStyle = "grey_tr"; }
			?>
			<tr class="status_tr <?php echo $bgStyle; ?>">
				<td><?php echo $invite['client_name']; ?></td>
				<td><?php echo $invite['client_company_name']; ?></td>
				<td><?php echo $invite['client_email']; ?></td>
				<td><?php
				if (isset($invite['invite_acc_id']) && $invite['invite_acc_id'] >= 1) {
                	$inviteStatus = $_ProjectF->getInviteStatus($invite['id'],$invite['invite_acc_id']);
				}else {
					$inviteStatus = $_ProjectF->getInviteStatus($invite['id']);
				}
				if ($inviteStatus == "SUBMITTED") { ?><span class="TURQ_COPY">Entry complete</span><?php
				}else if ($inviteStatus == "RSVP") { ?><span class="ORANGE_COPY">Entry incomplete</span><?php
				}else if ($inviteStatus == "SAVED") { ?><span class="ORANGE_COPY">Entry incomplete</span><?php
				}else if ($inviteStatus == "NEW ENTRY") { ?><span class="ORANGE_COPY">Entry incomplete</span><?php
				}else if ($inviteStatus == "NOT RESPONDED") { ?><span class="ORANGE_COPY">Entry incomplete</span><?php
				}else { echo $inviteStatus; } ?></td>
			</tr>
      		<?php
		}
	  ?>
      <tr class="status_footer">
      	<td></td>
        <td></td>
        <td></td>
        <td></td>
      </td>
    </table>
	<?php
	}else {
	?>
    	<p>You have not yet nominated your clients. <a href="<?php echo $project_data['pages']['rm_client_invites']; ?>" class="BLACK_COPY">Click here</a> to nominate now.</p>
    <?php
	}
	?>
    <div class="clear">&nbsp;</div>
</div> <!-- form container -->
	
<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont DESKTOP">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage">
</div> <!-- topRightLogo WHITE -->

<?php
// ADD RM navigation here
include('_include/rm_navigation.php');
?>
    
    
   
    
<!-- LIGHTBOX STUFFS -->
<div class="lightbox-mask" id="lightbox_mask">
	<img src="images/dpi_72/web_01_rightTopLogo.png" class="web_form_topLogo">
</div>
<div class="lightbox" id="user_form_container">
    <div class="lightbox_content">
        <p class="form_error_copy COPY_REGULAR">Please fill in all the required fields (*) to complete your submission.</p>
        <div class="square_btn back_btn" id="close_form_btn"><p class="square_btn_p">Back</p></div>
        <div class="clear"></div>
    </div> <!-- lightbox content -->
</div> <!-- lightbox -->

<!-- Grab Google CDN's jQuery. fall back to local if necessary -->
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script> -->
<script src='js/jquery-1.11.0.min.js'></script>
<script type="text/javascript" src="js/frame_functions.js"></script>
<script type="text/javascript" src="js/fnb_functions.js"></script>
<script type="text/javascript">
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});

	//$(document).ready(function (){
	window.onload = function() {
		$('#main_nav_container').hide();
	};
</script>
</body>

</html>