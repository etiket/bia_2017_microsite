	<img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img rm_menu_btn" id="nav_menu_btn_open">
	<nav class="main_nav_container" id="main_nav_container">
		<ul class="main_nav">
        	<a href="<?php echo $project_data['full_address']; ?>"><li class="nav_item">Home</li></a>
			<hr />
			<a href="<?php echo $project_data['pages']['entry_login']; ?>"><li class="nav_item">Login/register</li></a>
			<hr />
            <a href="<?php echo $project_data['pages']['password_recovery']; ?>"><li class="nav_item">Reset password</li></a>
			<hr />
            <?php
			if (isset($_SESSION['entry_userinfo'])) {
				?>
                <a href="<?php echo $project_data['pages']['entry_form']; ?>"><li class="nav_item">My entry</li></a>
				<hr />
                <?php
			}
			?>
		</ul>

        <div class="nav_social_btns">
        	<img src="<?php echo $image_folder; ?>social_btn_fb_turq.svg" class="fb_share_thispage" />
            <img src="<?php echo $image_folder; ?>social_btn_twitter_turq.svg" class="twitter_share_site" />
            <img src="<?php echo $image_folder; ?>social_btn_in_turq.svg" class="in_share_btn" />
            <div class="clear"></div>
        </div>
        
        <img src="<?php echo $image_folder; ?>web_menu_btn_contract.png" class="DISPLAY_BLOCK FULL_WIDTH" id="nav_menu_btn_close" />
	</nav> <!-- main nav container -->