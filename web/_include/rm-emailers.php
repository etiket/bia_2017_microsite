<?php
// Send CLIENT invitation emailer
function sendInvitation($name,$emailto,$company_name,$unique_code,$from_name,$rm_email) {
	global $_EtiFrame;
	global $project_data;
	
	$company_name = addslashes($company_name);
	$image_folder = $image_folder = $project_data['full_address'];
	$image_folder = str_replace("web/","",$image_folder);
	$image_folder = $image_folder."emailers/rm_client-nomination2017/images/";
	
	$emailMessage = '<table width="600" border="1" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; border-color:#000000;" align="center">';
	$emailMessage .= '<tr>';
    $emailMessage .= '<td>';


	$emailMessage .= '<table width="600" border="0" cellspacing="0" cellpadding="0">';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td>';
    
	$emailMessage .= '<table width="327" border="0" cellspacing="0" cellpadding="0">';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td valign="top"><img src="'.$image_folder.'img_01.png" width="327" height="406" alt="" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></td>';
	$emailMessage .= '<td bgcolor="#FFFFFF" valign="top">';
        
        
	$emailMessage .= '<table width="266" border="0" cellspacing="0" cellpadding="0">';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td valign="top" bgcolor="#FFFFFF">';
	$emailMessage .= '<img src="'.$image_folder.'img_02.png" width="273" height="223" alt="Remember to submit your FNB Business INNOVATION Awards entry." style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />';
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:left; letter-spacing:normal;">';
	$emailMessage .= '<p style="margin:0 0 0 0;">';
	$emailMessage .= $company_name.' has been nominated<br/>to enter the <strong>FNB Business INNOVATION<br/>Awards 2017</strong>.';
	$emailMessage .= '</p>';
            	
            
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td align="left" valign="top">';
	$emailMessage .= '<img src="'.$image_folder.'img_04.png" width="273" height="38" alt="Enter now" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />';
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:left; letter-spacing:normal;">';
	$emailMessage .= '<p style="margin:0 0 0 0;">';
	$emailMessage .= '<span style="color:#009999; font-size:11px; font-weight:normal;">';
	$emailMessage .= 'Complete your entry at<br/>';
	$emailMessage .= '<span style="text-decoration:none; color:#009999; font-weight:bold;"><font style="display:none;">@</font>www.fnbbusinessinnovationawards.co.za</span>';
	$emailMessage .= '</span>';
	$emailMessage .= '</p>';
            	
            
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';

        
        
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';
   
    
	$emailMessage .= '</td>';
  	$emailMessage .= '</tr>';
  	$emailMessage .= '<tr>';
	$emailMessage .= '<td align="center" valign="middle" height="50" bgcolor="#FFFFFF">';
    
	$emailMessage .= '<table width="570" border="0" cellspacing="0" cellpadding="0" align="center">';
    $emailMessage .= '<tr>';
	$emailMessage .= '<td align="left" height="50" bgcolor="#FFFFFF" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:8px; color:#000000; text-align:left; letter-spacing:normal; height:50px;">';
	$emailMessage .= '<p><span style="font-size:9px;"><i>Terms and conditions apply.</i></span><br/><br/>';
	$emailMessage .= '<strong>First National Bank - a division of FirstRand Bank Limited</strong>. An Authorised Financial Services and Credit Provider (NCRCP20). </p>';
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';

    
    
	$emailMessage .= '</td>';
  	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';


	$emailMessage .= '</td>';
 	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';

	
	$fromname = $from_name;
	$fromaddress = 'competition@fnbbusinessinnovationawards.co.za';
	$nameto = $name;
	$Subject = "You have been nominated to enter the FNB Business INNOVATION Awards 2017.";
	if (sendPHPMail($emailMessage,$Subject,$emailto,$nameto)) {
		return true;
	}else {
		return false;
	}
	/*if ($_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject)) {
		
		$Subject = "BACKUP - You have been nominated to enter the FNB Business INNOVATION Awards 2017.";
		$emailto = "arno@etiket.co.za";
		$_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject);
		return true;
	}else {
		return false;
	}*/
	
}


// Send RM Confirmation emailer

function sendRMConfirmation($name,$emailto,$company_name,$client_email) {
	global $_EtiFrame;
	global $project_data;
	
	$company_name = addslashes($company_name);
	$image_folder = $project_data['full_address'];
	$image_folder = str_replace("web/","",$image_folder);
	$image_folder = $image_folder."emailers/rm_nomination-confirm2017/images/";
	
	$emailMessage = '<table width="600" border="1" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; border-color:#000000;" align="center">';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td>';


	$emailMessage .= '<table width="600" border="0" cellspacing="0" cellpadding="0">';
	$emailMessage .= '<tr>';
	$emailMessage .= '<td>';
    
	$emailMessage .= '<table width="334" border="0" cellspacing="0" cellpadding="0">';
      	$emailMessage .= '<tr>';
        	$emailMessage .= '<td><img src="'.$image_folder.'img_01.png" width="334" height="453" alt="" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></td>';
        	$emailMessage .= '<td bgcolor="#FFFFFF" valign="top">';
        
        
        	$emailMessage .= '<table width="266" border="0" cellspacing="0" cellpadding="0">';
          	$emailMessage .= '<tr>';
            	$emailMessage .= '<td valign="top" bgcolor="#FFFFFF">';
            	$emailMessage .= '<img src="'.$image_folder.'img_02.png" width="266" height="278" alt="Thank you for nominating your client.." style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />';
            	$emailMessage .= '</td>';
          	$emailMessage .= '</tr>';
          	$emailMessage .= '<tr>';
            	$emailMessage .= '<td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:left; letter-spacing:normal;">';
            		$emailMessage .= '<p style="margin:0 0 0 0;">';
                    	$emailMessage .= 'A confirmation emailer with details on how to enter<br/>has been sent to <strong>'.$company_name.'</strong> at<br/><strong>'.$client_email.'</strong>';
					$emailMessage .= '</p>';
            	
            
            	$emailMessage .= '</td>';
          	$emailMessage .= '</tr>';
          	$emailMessage .= '<tr>';
            	$emailMessage .= '<td align="left" valign="top">';
            		$emailMessage .= '<img src="'.$image_folder.'img_04.png" width="266" height="38" alt="Enter now" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />';
            	$emailMessage .= '</td>';
          	$emailMessage .= '</tr>';
          	$emailMessage .= '<tr>';
            	$emailMessage .= '<td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:left; letter-spacing:normal;">';
            		$emailMessage .= '<p style="margin:0 0 0 0;">';
                    	$emailMessage .= '<span style="color:#009999; font-size:11px; font-weight:bold;">';
                    		$emailMessage .= 'To check the entry status of your clients<br/>or to nominate more clients.<BR />';
                    	$emailMessage .= '</span>';
					$emailMessage .= '</p>';
            	
            
            	$emailMessage .= '</td>';
          	$emailMessage .= '</tr>';
          	$emailMessage .= '<tr>';
            	$emailMessage .= '<td align="left" valign="top"><a href="'.$project_data['pages']['rm_login'].'"><img src="'.$image_folder.'img_loginbtn.png" width="54" height="29" alt="Login" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></a>';
            	$emailMessage .= '</td>';
          	$emailMessage .= '</tr>';
        	$emailMessage .= '</table>';

        
        
        	$emailMessage .= '</td>';
      	$emailMessage .= '</tr>';
    	$emailMessage .= '</table>';
   
    
    	$emailMessage .= '</td>';
  	$emailMessage .= '</tr>';
  	$emailMessage .= '<tr>';
  		$emailMessage .= '<td align="center" valign="middle" height="50" bgcolor="#FFFFFF">';
    
    	$emailMessage .= '<table width="570" border="0" cellspacing="0" cellpadding="0" align="center">';
      	$emailMessage .= '<tr>';
        	$emailMessage .= '<td align="left" height="50" bgcolor="#FFFFFF" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:8px; color:#000000; text-align:left; letter-spacing:normal; height:50px;">';
    		$emailMessage .= '<p><span style="font-size:9px;"><i>Terms and conditions apply.</i></span><br/><br/>';
    	  	$emailMessage .= '<strong>First National Bank - a division of FirstRand Bank Limited</strong>. An Authorised Financial Services and Credit Provider (NCRCP20). </p>';
    		$emailMessage .= '</td>';
      	$emailMessage .= '</tr>';
    	$emailMessage .= '</table>';

    
    
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';
	$emailMessage .= '</td>';
	$emailMessage .= '</tr>';
	$emailMessage .= '</table>';
	
	$fromname = "RM Nominations";
	$fromaddress = 'competition@fnbbusinessinnovationawards.co.za';
	$nameto = $name;
	$Subject = "Thank you for nominating your client.";

	if (sendPHPMail($emailMessage,$Subject,$emailto,$nameto)) {
		return true;
	}else {
		return false;
	}
	/*
	if ($_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject)) {
		
		// BACKUP test EMAIL
		$Subject = "BACKUP - Thank you for nominating your client.";
		$emailto = "arno@etiket.co.za";
		$_EtiFrame->sendBasicEmail($emailMessage,$fromname,$fromaddress,$nameto,$emailto,$Subject);
		return true;
	}else {
		return false;
	}*/
	
}