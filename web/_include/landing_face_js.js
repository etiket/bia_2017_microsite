var facePage = document.getElementById('PAGE_1');
var faceImage = document.getElementById('faceImg');
var winW = window.innerWidth;
var parraLayerLines1 = document.getElementById('parraLayerLines1');
var parraLayerLines2 = document.getElementById('parraLayerLines2');
var parraLayerDots1 = document.getElementById('parraLayerDots1');


//(function() {
function launchFace() {
    facePage.onmousemove = function (event) {
        // http://stackoverflow.com/questions/7790725/javascript-track-mouse-position
        var dot, eventDoc, doc, body, pageX, pageY;

        event = event || window.event; // IE-ism
        // console.log(event.pageX,event.pageY);
        // If pageX/Y aren't available and clientX/Y are,
        // calculate pageX/Y - logic taken from jQuery.
        // (This is to support old IE)
        
        if (event.pageX == null && event.clientX != null) {
            eventDoc = (event.target && event.target.ownerDocument) || document;
            doc = eventDoc.documentElement;
            body = eventDoc.body;

            event.pageX = event.clientX +
              (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
              (doc && doc.clientLeft || body && body.clientLeft || 0);
            event.pageY = event.clientY +
              (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
              (doc && doc.clientTop  || body && body.clientTop  || 0 );
        }
        
        // Use event.pageX / event.pageY here
        var frame = Math.round(((event.pageX/windowWidth)*100)/7);
        // console.log(frame);
        document.getElementById('pAGEx').innerHTML = frame;
        if (frame < 10) {
            frame = '00'+frame;
        }else if (frame < 100 && frame >= 10) {
            frame = '0'+frame;
        }
        // facePage.style.backgroundImage = 'url(images/faceanim/face-'+frame+'.jpg)';
        faceImage.src = 'images/faceanim/face2-'+frame+'.jpg';

        var xpos1 = (event.pageX * 0.01)+(winW/2);
        var xpos2 = (event.pageX * 0.02)+(winW/2);
        var xpos3 = (event.pageX * 0.03)+(winW/2);
        parraLayerLines1.style.left = xpos2+"px";
        parraLayerLines2.style.left = xpos3+"px";
        parraLayerDots1.style.left = xpos1+"px";
    }
}
// })();


// PRELOAD IMAGES
var images = {};
function preload() {
    for (i = 0; i < preload.arguments.length; i++) {
        var Im = new Image();
        //images[i] = new Image()
        Im.src = preload.arguments[i];
        images[i] = Im;
        
        Im.onload = function() {
            loadedImages.push(this.src);
            checkLoadedImages();
             //if (console.log("ImageLoaded: "+this.src));
        };
    }
}
var loadedImages = new Array();
function checkLoadedImages() {
    if (loadedImages.length == preloadImages.length && loadedImages.length > 4) {
        setTimeout(function() {
            launchFace()
        }, 1000);
        started = true;
    }
}

