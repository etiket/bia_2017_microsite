	<img src="<?php echo $image_folder; ?>web_menu_icon.png" class="web_menu_btn_img rm_menu_btn DESKTOP" id="nav_menu_btn_open">
    <!-- RM NAVIGATION -->
    <div class="mobi_float_container MOBILE">
    	<div class="main_mobi_container">
            <img src="<?php echo $image_folder; ?>web_menu_icon.png" class="mobi_menu_btn" id="mobi_nav_menu_btn_open">
            <img src="<?php echo $image_folder; ?>mobi_float_logo.png" class="mobi_float_logo">
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
    </div> <!-- mobi flot container -->
    <nav class="main_nav_container" id="main_nav_container">
		<ul class="main_nav DESKTOP">
        	<?php
			if (isset($_SESSION['rm_info'])) {
				?>
			<a href="<?php echo $project_data['pages']['rm_client_invites']; ?>"><li class="nav_item" id="nav1">Nominate your clients</li></a>
			<hr />
			<a href="<?php echo $project_data['pages']['rm_status']; ?>"><li class="nav_item" id="nav2">Nomination status</li></a>
            <hr />
            	<?php
			}else {
                if (isset($project_page['file_name']) && $project_page['file_name'] != "rm-login-register.php") {
                    ?>
                        <a href="<?php echo $project_data['pages']['rm_login']; ?>"><li class="nav_item" id="nav2">Login/register</li></a>
                    <hr /><?php
                }
            }
			?>
		</ul>

        <div class="nav_social_btns">
        	<img src="<?php echo $image_folder; ?>social_btn_fb_turq.svg" class="fb_share_thispage" />
            <img src="<?php echo $image_folder; ?>social_btn_twitter_turq.svg" class="twitter_share_site" />
            <img src="<?php echo $image_folder; ?>social_btn_in_turq.svg" class="in_share_btn" />
            <div class="clear"></div>
        </div>
        
        <img src="<?php echo $image_folder; ?>web_menu_btn_contract.png" class="FULL_WIDTH" id="nav_menu_btn_close">
	</nav> <!-- main nav container -->
    