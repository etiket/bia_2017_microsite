<?php

include('entry-form-field-values.php');
include('entry-form-emailers.php');

// SAVE DATA
if (isset($_POST['action_type'])) {
	$insertArray = NULL;
	$emailMessage = "";
	$_saved_step = '1';
	if (isset($_POST['this_step'])) {
		$_saved_step = $_POST['this_step'];
	}
	// DEFINE KEY VALUES 
	foreach($formValues as $key => $defaultValue) {
		if (isset($_POST[$key]) && $_POST[$key] != NULL) { $_POST[$key] = trim($_POST[$key]); }
		if (isset($_POST[$key]) && $_POST[$key] === $defaultValue) {
			if (isset($_POST['action_type']) && ($_POST['action_type'] === "SAVE" || $_POST['action_type'] === "SAVENEXT")) {
				
			}else {
				$frame_error['validation_error'] = "Input ERROR: ".$key." = ".$_POST[$key];
				break;
			}
		}else if (isset($_POST[$key])) {
			$insertArray[$key] = $_POST[$key];
		}else {
			// do nothing
		}
	}
	if ($_POST['action_type'] === "SAVENEXT" || (isset($_POST['action_submit_form']) && $_POST['action_submit_form'] === "YES")) {
		$_saved_step++;
	}
	
	if (count($frame_error) >= 1) {
		//echo "FRAME ERROR: ".$frame_error;
	}else {
		//sleep(3);
		//$_Project_db->debug($insertArray);
		$insertArray['entry_date'] = $vandag;
		$insertArray['entry_time'] = $tyd;
		$insertArray['user_id'] = $_SESSION['entry_userinfo']['id'];
		$insertArray['saved_step'] = $_saved_step;
		if (!(isset($insertArray['email'])) || $insertArray['email'] == NULL || $insertArray['email'] == "") { $insertArray['email'] = $_SESSION['entry_userinfo']['email']; }
		// -- IF NEW ENTRY .... SAVE OR SUBMIT
		if ($_POST['form_action'] == "NEW") {
			/*
			*
			* NEW
			*
			*/
			$insertArray['status'] = "NEW ENTRY";
			if ($_Project_db->insert($_db_['table']['entries'],$insertArray)) {
				$insert_id = $_Project_db->insert_id();
				$_entry_id = $insert_id;
				// Update the entry user entry_id
				$_Project_db -> where('id',$_SESSION['entry_userinfo']['id']);
				$updateUserData = array(
					'entry_id' => $_entry_id,
					'status' => 'NEW ENTRY'
				);
				if ($_Project_db -> update($_db_['table']['entry_users'],$updateUserData)) {
					$_SESSION['entry_userinfo']['entry_id'] = $_entry_id;
				}
				// Accept the invite
				if (isset($_SESSION['entry_userinfo']['invite_id']) && $_SESSION['entry_userinfo']['invite_id'] >= 1) {
					$_ProjectF->acceptRMInvite($_SESSION['entry_userinfo']['invite_id'],'NEW ENTRY');
				}else {
					$inviteData = $_ProjectF -> matchRMandUser($_SESSION['entry_userinfo']['id'],$_SESSION['entry_userinfo']['email'],"NEW ENTRY");
					if ($inviteData != false) {
						$_SESSION['entry_userinfo']['invite_id'] = $inviteData['invite_data']['id'];
					}
				}
				
				// Add Founders
				if (isset($_POST['founders']) && isset($_POST['founder_shareholdings'])) {
					if (count($_POST['founders']) === count($_POST['founder_shareholdings'])) {
						$foundersArray = $_POST['founders'];
						$founder_shareholdings = $_POST['founder_shareholdings'];
						if (is_array($foundersArray) && count($foundersArray) >= 1) {
							$i = 0;
							//$emailMessage .= "<BR><strong>Other Founders:</strong><BR>";
							foreach($foundersArray as $founder) {
								$insertData = array(
									'name' => $founder,
									'role' => 'na',
									'shareholding' => $founder_shareholdings[$i],
									'entry_id' => $insert_id
								);
								$_Project_db->insert($_db_['table']['entry_founders'],$insertData);
								//$emailMessage .= "<strong>".$founder."</strong> | ".$founder_roles[$i]."<BR>";
								$i++;
							}
						}
					}
				}
				
			}else {
				$frame_error['db_insert'] = 'Could not insert the Entry into the database.';
			}
		}else if ($_POST['form_action'] == "UPDATE") {
			/*
			*
			* UPDATE
			*
			*/
			$insertArray['status'] = "UPDATED";
			$update_id = $_SESSION['entry_userinfo']['entry_id'];
			$_Project_db -> where('id',$update_id);
			if ($updated = $_Project_db -> update($_db_['table']['entries'],$insertArray)) {
				$_entry_id = $update_id;
				
				// Add Founders
				if (isset($_POST['amount_of_founders'])) {
					// Remove all extra founders
					$_Project_db -> where('entry_id',$_entry_id);
					$existing = $_Project_db -> get($_db_['table']['entry_founders']);
					foreach($existing as $founder) {
						$_Project_db -> where('id',$founder['id']);
						$_Project_db -> delete($_db_['table']['entry_founders']);
					}
					// Check for new founders
					if (isset($_POST['founders']) && isset($_POST['founder_shareholdings'])) {
						if (count($_POST['founders']) === count($_POST['founder_shareholdings'])) {
							$foundersArray = $_POST['founders'];
							$founder_shareholdings = $_POST['founder_shareholdings'];
							if (is_array($foundersArray) && count($foundersArray) >= 1) {
								$i = 0;
								//$emailMessage .= "<BR><strong>Other Founders:</strong><BR>";
								foreach($foundersArray as $founder) {
									$insertData = array(
										'name' => $founder,
										'role' => 'na',
										'shareholding' => $founder_shareholdings[$i],
										'entry_id' => $_entry_id
									);
									$_Project_db->insert($_db_['table']['entry_founders'],$insertData);
									//$emailMessage .= "<strong>".$founder."</strong> | ".$founder_roles[$i]."<BR>";
									$i++;
								}
							}
						}
					}
				}
				// Accept the invite
				if (isset($_SESSION['entry_userinfo']['invite_id']) && $_SESSION['entry_userinfo']['invite_id'] >= 1) {
					$_ProjectF->acceptRMInvite($_SESSION['entry_userinfo']['invite_id'],'SAVED');
				}else {
					$inviteData = $_ProjectF -> matchRMandUser($_SESSION['entry_userinfo']['id'],$_SESSION['entry_userinfo']['email'],"SAVED");
					if ($inviteData != false) {
						$_SESSION['entry_userinfo']['invite_id'] = $inviteData['invite_data']['id'];
					}

				}
			}
		}
		
		// CHECK IF SAVE OR SUBMIT WAS PRESSED
		//if (!(isset($_POST['btn_save_form']))) {

		if (isset($_POST['action_submit_form']) && $_POST['action_submit_form'] === "YES") {
			// Accept the invite
			
			if (isset($_SESSION['entry_userinfo']['invite_id']) && $_SESSION['entry_userinfo']['invite_id'] >= 1) {
				$_ProjectF->acceptRMInvite($_SESSION['entry_userinfo']['invite_id'],'SUBMITTED');
			}else {
				$inviteData = $_ProjectF -> matchRMandUser($_SESSION['entry_userinfo']['id'],$_SESSION['entry_userinfo']['email'],"SUBMITTED");
				if ($inviteData != false) {
					$_SESSION['entry_userinfo']['invite_id'] = $inviteData['invite_data']['id'];
				}
			}
			

			// Update the status of the USER
				$_Project_db -> where('id',$_SESSION['entry_userinfo']['id']);
				$updateUserData = array(
					'status' => 'SUBMITTED'
				);
				if ($_Project_db -> update($_db_['table']['entry_users'],$updateUserData)) {
					$_SESSION['entry_userinfo']['status'] = 'SUBMITTED';
				}
			
			// Check if the save to DB was successfull
			if (isset($_entry_id) && $_entry_id >= 1) {
				// Update the status of the ENTRY
				$_Project_db -> where('id',$_entry_id);
				$updateEntryData = array(
					'status' => 'SUBMITTED'
				);
				if ($_Project_db -> update($_db_['table']['entries'],$updateEntryData)) {
					// DO NOTHING
				}
				

				// Email the submitted data to FNB and me
				$fromname = "fnbbusinessinnovationawards.co.za";
				$fromaddress = 'competition@fnbbusinessinnovationawards.co.za';
				// START and FINISH off Email Message before sending
				$_Project_db -> where('id',$_SESSION['entry_userinfo']['entry_id']);
				$entryFormData = $_Project_db -> get($_db_['table']['entries']);
				$emailMessage = buildEntryEmail($entryFormData[0]);
				$fnbEmail = $emailMessage;
				// To ETIKET as BACKUP
				$nameto = "Arno";
				$emailto = "arno@etiket.co.za";
				$Subject = "BACKUP - Thank you for entering the FNB Business Innovation Awards 2017.";
				if (sendPHPMail($emailMessage,$Subject,$emailto,$nameto)) {

					// To Applicant					
					$Subject = "Thank you for entering the FNB Business Innovation Awards 2017.";
					$nameto = $entryFormData['business_name'];
					$emailto = $_SESSION['entry_userinfo']['email'];
					$userEmailM = $emailMessage;
					sendPHPMail($userEmailM,$Subject,$emailto,$nameto);
					sleep(1);
					
					// // To FNB Team
					$Subject = "NEW ENTRY - Thank you for entering the FNB Business Innovation Awards 2017.";
					$nameto = "FNB Team";
					$emailto = "innovationawards@fnb.co.za";
					sendPHPMail($userEmailM,$Subject,$emailto,$nameto);

					session_write_close();
					header('Location: '.$project_data['pages']['entry_form']."?action=SUBMITTED AND EMAILED");
					exit;
				}else {
					session_write_close();
					header('Location: '.$project_data['pages']['entry_form']."?action=FAILED TO EMAIL");
					exit;
				}
			}
		//}else if (isset($_POST['btn_save_form'])){
		// }else if (isset($_POST['btn_save_form']) && $_POST['btn_save_form'] === "SAVE") {
		}else if ($_POST['action_type'] === "SAVE" || $_POST['action_type'] === "SAVENEXT") {
			// Check if the save to DB was successfull
			if (isset($_entry_id) && $_entry_id >= 1) {
				// Update the status of the user
				$_Project_db -> where('id',$_SESSION['entry_userinfo']['id']);
				$updateUserData = array(
					'status' => 'SAVED'
				);
				if ($_Project_db -> update($_db_['table']['entry_users'],$updateUserData)) {
					$_SESSION['entry_userinfo']['status'] = 'SAVED';
				}
				
				session_write_close();
				header('Location: '.$project_data['pages']['entry_form']."?save_action=".$_POST['action_type']);
				exit;
			}
		}
	}
}




// Check if form already exists
if (isset($_SESSION['entry_userinfo']['entry_id']) && $_SESSION['entry_userinfo']['entry_id'] >= 1) {
	$_Project_db -> where('id',$_SESSION['entry_userinfo']['entry_id']);
	$existing = $_Project_db -> get($_db_['table']['entries']);
	if (count($existing >= 1)) { $_thisFormData = $existing[0]; }
}else {
	
}

// Check for RM details
/*
if (isset($_SESSION['entry_userinfo']['invite_id'])) {
	if ($_ProjectF -> getInviteRMdetails($_SESSION['entry_userinfo']['invite_id']) != false) {
		$_myRMDetails = $_ProjectF -> getInviteRMdetails($_SESSION['entry_userinfo']['invite_id']);
		$_myRMName = $_myRMDetails['name'];
	}
}
*/

//	gts = Go To Step set
if (!(isset($_thisFormData['saved_step'])) && !(isset($_GET['startform']))) {
	$_thisFormData['saved_step'] = '0';
}else if (isset($_GET['startform'])) {
	$_thisFormData['saved_step'] = '1';
}else if (isset($_GET['gts']) && $_GET['gts'] >= 0 && $_thisFormData['saved_step'] >= $_GET['gts'] && $_thisFormData['saved_step'] <= 6) {
	$_thisFormData['saved_step'] = $_GET['gts'];
}else if ($_thisFormData['saved_step'] >= 6) {
	$_thisFormData['saved_step'] = 6;
}
