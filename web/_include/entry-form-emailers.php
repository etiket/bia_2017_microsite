<?php
	
	
// - BUILD ENTRY EMAIL
function buildEntryEmail($entryData) {
	global $_EtiFrame;
	global $project_data;
	global $formValues;
	global $inputLabels;
	
	$image_folder = $project_data['full_address'];
	$image_folder = str_replace("web/","",$image_folder);
	$image_folder = $image_folder."emailers/client_entry_submit2017/images/";
	
	$emailMessage = "";
	$emailMessage .= "<table width='640' border='0' cellspacing='0' cellpadding='0' align='center'>";
	$emailMessage .= "<tr>";
	$emailMessage .= "<td bgcolor='#FFFFFF'><img src='".$image_folder."submit_email_head2017.png' width='640' height='467' alt='Thank you for entering the FNB business innovation awards' style='display:block; float:left; margin:0px 0px 0px 0px;' align='absbottom' border='0' /></td>";
	$emailMessage .= "</tr>";
	$emailMessage .= "<tr>";
	$emailMessage .= "<td align='left' valign='top'>";
	$emailMessage .= "<p style='font-family:Calibri,Arial,Arial,Helvetica, sans-serif; font-size:14px; color:#000000; text-align:left;'>";
	foreach($entryData as $key => $value) {
		if (isset($formValues[$key])) {
			if ($value === "0" || $value === "YES" || $value === "NO" || $formValues[$key] === "0" || $value === "") {
				if ($value === "0" || $value === "") { $value = "NOT SELECTED"; }
				$emailMessage .= "<strong>".$inputLabels[$key].":</strong> ".htmlentities($value)."<br/><br/>";
			}else {
				$emailMessage .= "<strong>".$inputLabels[$key].":</strong> ".htmlentities($value)."<br/><br/>";		
			}
		}
	}
	// $emailMessage .= "<strong>Business name:</strong> ".$entryData['business_name']."<BR>";
	// $emailMessage .= "<strong>Endeavor member:</strong> ".$entryData['is_endeavor']."<BR>";
	$emailMessage .= "</p>";
    
	$emailMessage .= "</td>";
	$emailMessage .= "</tr>";
	$emailMessage .= "</table>";

	return $emailMessage;
}

require '_phpMailer/PHPMailerAutoload.php';
function sendPHPMail($message,$subject,$toAddress,$toName) {
	$mail = new PHPMailer;
	// $mail->isSMTP();                                      // Set mailer to use SMTP
	// $mail->Host = 'smtp.fnbbusinessinnovationawards.co.za';  // Specify main and backup SMTP servers
	// $mail->SMTPAuth = true;                               // Enable SMTP authentication
	// $mail->Username = 'competition@fnbbusinessinnovationawards.co.za';                 // SMTP username
	// $mail->Password = 'Design4etiket';                           // SMTP password
	//	$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('competition@fnbbusinessinnovationawards.co.za', 'FNB Business INNOVATION Awards');
	$mail->addAddress($toAddress, $toName);               // Name is optional
	$mail->addReplyTo('innovationawards@fnb.co.za', 'Innovation Awards');

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = $subject;
	$mail->Body    = $message;
	$mail->AltBody = '';

	if(!$mail->send()) {
	    // echo 'Message could not be sent.';
	    // echo 'Mailer Error: ' . $mail->ErrorInfo;
	    return false;
	} else {
	    // echo 'Message has been sent';
	    return true;
	}

	return true;
}
