<?php
// 001 Start Session
session_start();
include('_project/EtiFrame.php');
// 005 Page info
$project_page = array();
$project_page['name'] = "Entry form | FNB Business Innovation Awards";
$project_page['file_name'] = "entry-form.php";
$project_page['file_folder'] = "";
$project_fb_app = NULL;
$_EtiFrame = new EtiFrame($project_data,$project_page,$project_fb_app);

$_thisFormData = array();
$_addToURL = "";
$image_folder = $project_data['full_address']."images/";
$form_device = "DESKTOP";

include('_include/cd.php');

// Validate the user
if (isset($_SESSION['entry_userinfo']) && is_array($_SESSION['entry_userinfo']) && $_SESSION['entry_userinfo']['id'] >= 1 ) {
	//$_Project_db->debug($_SESSION['entry_userinfo']);
}else {
	session_write_close();
	header('Location: '.$project_data['pages']['entry_login'].'?errormessage=You have to login or register before you may enter.');
	exit;
}

//	Include the form submit PHP script page with all the backend stuffs
include('_include/form_submit.php');
?><!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $project_page['name']; ?></title>
	<!-- Mobile Specific Metas
  ================================================== -->	
    <meta name="HandheldFriendly" content="true" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale = 1" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
    <!-- Mobile Friendly -->
    <meta name="viewport" content="width=device-width" />
    <meta name="HandheldFriendly" content="yes" />
    <meta name="MobileOptimized" content="380px"/>
	<!-- CSS
  ================================================== -->
    <link href="css/fnb-standard-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style.css" rel="stylesheet" type="text/css" />
    <link href="css/template-layer-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/helper-styles.css" rel="stylesheet" type="text/css" />
    <link href="css/entry-form-style-responsive.css" rel="stylesheet" type="text/css" />
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<!-- Favicons
	================================================== -->
    <link rel="shortcut icon" href="<?php echo $image_folder; ?>project_social_icons/favicon.png">
    <link rel="apple-touch-icon" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg"/>
	<link rel="apple-touch-startup-image" href="https://www.fnb.co.za/03images/chameleon/iosHomeScreen/icon.jpg">
    
    <?php
	// Google analytics
	include('_include/google_analytics.php');
	?>
</head>

<body>
<?php
// INCLUDE VerSaDUHDUH tag
	include('_include/versaduhTag.php');
?>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!-- FORM STEPS CONTAINER --------------------------------------- -->
	<!-- <div class="component_cont" id="formStepsCont"> -->
		<?php
		if (isset($_thisFormData['saved_step'])) {
			switch ($_thisFormData['saved_step']) {
				case '1':
			        // ABOUT YOU
					include('form_steps/step_about_you.php');
			        break;
			    case '2':
			    	// ABOUT THE BUSINESS
					include('form_steps/step_about_the_business.php');
					break;
				case '3':
			    	// BUSINESS FINANCIALS
					include('form_steps/step_business_financials.php');
					break;
				case '4':
			    	// THE AWARDS
					include('form_steps/step_the_awards.php');
					break;
				case '5':
			    	// GENERAL
					include('form_steps/step_general.php');
					break;
				case '6':
					// INCLUDE FNB floodlight tag
					include('_include/floodlight_tag-thankyou.php');
					// THANK YOU
					include('form_steps/step_thank_you.php');
					break;
				default:
			    	// INTRO
					include('form_steps/step_intro.php');
			}
		}
		?>
	<!-- </div> -->


<?php include('_include/navigation.php'); ?>
<!-- TOP RIGHT LOGO AND SOCIAL BUTTONS - WHITE BACKGROUND -->
<div class="topRightLogoCont">
    <img src="<?php echo $image_folder; ?>web_01_rightTopLogo_white.png" class="web_01_toplogo" alt="FNB Business Innovation Awards">
    <div class="clear"></div>
    <img src="<?php echo $image_folder; ?>social_btn_in_white.svg" class="in_share_btn DESKTOP"/>
    <img src="<?php echo $image_folder; ?>social_btn_twitter_white.svg" class="twitter_share_site DESKTOP">
    <img src="<?php echo $image_folder; ?>social_btn_fb_white.svg" class="fb_share_thispage DESKTOP">
</div> <!-- topRightLogo WHITE -->


<script src='js/jquery-1.11.0.min.js'></script>
<script src="js/form_validate.js"></script>
<script src="js/toastr.min.js"></script>
<script src="js/entry-form-toastr.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js"></script> -->
<script src="<?php echo $project_data['full_address']; ?>js/_plugins/greensock/TweenMax.min.js"></script>
<script src="<?php echo $project_data['full_address']; ?>js/_plugins/greensock/plugins/MorphSVGPlugin.min.js"></script>
<script src="<?php echo $project_data['full_address']; ?>js/rm-functions.js"></script>
<script src="<?php echo $project_data['full_address']; ?>js/frame_functions.js"></script>
<script>




var curStepIndex = <?php if (isset($_thisFormData['saved_step'])) { echo $_thisFormData['saved_step']; }else { echo '1'; } ?>;
//	Form validation function
function validateForm() {
	var targetString = '';
	var targetField;
	var errorArray = new Array();
	<?php
	if (isset($_thisFormFields)) {
		foreach($_thisFormFields as $fieldID => $functionstring) {
			?>
			targetField = $( "[name='<?php echo $fieldID; ?>']" );
			//console.log(targetField);
			<?php
			echo 'if ('.$functionstring.' === true || '.$functionstring.' === "YES" || '.$functionstring.' === "NO") { frame_unmarkErrorFields(\''.$fieldID.'\'); }else { frame_markErrorFields(\''.$fieldID.'\'); errorArray.push(\'*\'+'.$functionstring.'); }';
			?>
			
			<?php
			if ($fieldID === "founder_led") {
				?>
				var fValidation = validateFounders();
				if (fValidation !== true) {
					errorArray = errorArray.concat(fValidation);
				}
				<?php
			}
		}
		?>
		
		if (checkCode($('#code_input')) != true) {
			frame_markErrorFields($('#code_input'));
			errorArray.push('*Please enter the code shown.');
		}

		if (errorArray.length >= 1) { calculateErrorBeforeSubmit(errorArray); }else { return true; }
		<?php
	}else {
		echo "return false;";
	}
	
	?>
}

//	Form Button Functions
function onNextClick(formID) {
	//	validate
	if (validateForm() === true) {
		//	change action_type hidden value to SAVENEXT
		document.getElementById('action_type').value = "SAVENEXT";
		//	submit form
		$("form#"+formID).submit();
	}else {

	}
	
}
function onBackClick(formID) {
	var prevStep;
	if ((curStepIndex-1) <= 0) { prevStep = 0; }else { prevStep = curStepIndex-1; }
	window.location="<?php echo $project_data['pages']['entry_form']; ?>?gts="+prevStep;
}
function onSaveClick(formID) {
	$("form#"+formID).submit();
}


//	Console log all the current input fields under the forms with form id starting with form_step_
function logInputFields() {
	var forms = document.querySelectorAll('[id^="form_step_"]');
	var totalInputs = 0;
	for (var i = 0; i < forms.length; i++) {
		var thisForm = forms[i];
		//if (console.log('FORM '+i+': '+$(thisForm).attr('id')));
		// var inputs = thisForm.querySelectorAll('[class^="ift"]');
		var inputs = thisForm.querySelectorAll('[type^="hidden"]');
		for (var ift = 0; ift < inputs.length; ift++) {
			var log = "";
			log += $(inputs[ift]).attr('name')+" | ";
			if (typeof $(inputs[ift]).attr('type') !== typeof undefined && $(inputs[ift]).attr('type') !== false) {
			    log += $(inputs[ift]).attr('type')+" | ";
			}
			log += inputs[ift].nodeName.toLowerCase()+" | ";
			log += inputs[ift].value+" | ";
			log += $(inputs[ift]).attr('placeholder');
			// if (console.log(log));
			totalInputs++;
		}
	}
	//if (console.log("TOTAL INPUTS: "+totalInputs));
}


/* HIDE OR SHOW ALL OPTIONAL FIELDS */
function hideShowOptional() {
	var forms = document.querySelectorAll('[id^="form_step_"]');
	for (var i = 0; i < forms.length; i++) {
		var thisForm = forms[i];
		var inputs = thisForm.querySelectorAll('[type^="hidden"]');
		for (var ift = 0; ift < inputs.length; ift++) {
			if (inputs[ift].value === "YES" || inputs[ift].value === "NO") {
				var attr = $(inputs[ift]).attr('myDesc');
				if (typeof attr !== typeof undefined && attr !== false) {
				    toggleFieldDisplay($('#'+attr),inputs[ift].value);
				}
			}
		}
	}
}

/*	On document ready
	Hide hamburger menu
	set JQuery Button Actions
*/
$(document).ready(function (){
	// hide hamburgur menu
	$('#main_nav_container').hide();
	//logInputFields();
	hideShowOptional();
	//	Template top left hamburger menu button functions
	$('#nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#mobi_nav_menu_btn_open').click(function() {
		$('#main_nav_container').show(250);
	});
	$('#nav_menu_btn_close').click(function() {
		$('#main_nav_container').hide(250);
	});
	$('#btn_submit_form').click(function(event) {
		event.preventDefault();
		//	validate
		if (validateForm() === true) {
			//	change action_type hidden value to SAVENEXT
			document.getElementById('action_submit_form').value = "YES";
			//	submit form
			$("form#form_step_general").submit();
		}
		/*else {
			if (checkCode($('#code_input')) != true) {
				var errorArray = new Array();
				frame_markErrorFields($('#code_input'));
				errorArray.push('Please enter the code shown.');
				calculateErrorBeforeSubmit(errorArray);
			}
		}*/
	});

	$('.css-checkbox').change(function(event) {
		var id = $(this).attr('name');
		id = id.replace('_radio','');
		$('#'+id).val($(this).val());
		var attr = $('#'+id).attr('myDesc');
		if (typeof attr !== typeof undefined && attr !== false) {
		    //console.log(document.getElementById(id).getAttribute('myDesc'));
		    toggleFieldDisplay($('#'+attr),$(this).val());
		}
	});

	$('.help').on('click', popToast);

	toastr.options = {
	  "closeButton": true,
	  "debug": false,
	  "newestOnTop": false,
	  "progressBar": false,
	  "positionClass": "toast-bottom-right",
	  "preventDuplicates": false,
	  "onclick": null,
	  "showDuration": "300",
	  "hideDuration": "1000",
	  "timeOut": "20000",
	  "extendedTimeOut": "1000",
	  "showEasing": "swing",
	  "hideEasing": "linear",
	  "showMethod": "fadeIn",
	  "hideMethod": "fadeOut"
	};
	

	<?php
		if (isset($_GET['save_action']) && $_GET['save_action'] == "SAVE") {
			?>popOnloadToast(<?php echo $_thisFormData['saved_step']; ?>);<?php
		}
	?>

});


function toggleFieldDisplay(targetField,value) {
	var p = $(targetField).parent();
	var attr = targetField.attr('validateMe');
	if (typeof attr !== typeof undefined && attr !== false) {
		targetField.attr('validateMe',value);
	}
	if (value === "YES") {
		p.show(500);
	}else if (value === "NO") {
		p.hide(500);
	}
}

/* CHECK SELECT LIST VALUE TO BE SELECTED AND NOT 0 */
function frame_selectListNotzero(field,errorstring) {
	var errMessage = errorstring || "";
	var selectedValue = field.options[field.selectedIndex].value;
	if (selectedValue === "0" || selectedValue === "1") {
		//return false;
		return errMessage;
	}else {
		return true;
	}
}



/* ADD MORE FOUNDERS */
function onFoundersSelect(targetList) {
	var selectedValue = targetList.options[targetList.selectedIndex].value;
	if (selectedValue === "0" || selectedValue === "1") {
		$('#extra_founders').html('');
		if ($(targetList).hasClass('placeholder_input')) {
		}else {
			$(targetList).addClass('placeholder_input');
		}
		$('#extrafounders_p_label_for_QC').hide();
	}else {
		addFounders(selectedValue);
		$(targetList).removeClass('placeholder_input');	
		$('#extrafounders_p_label_for_QC').show();
	}
}

function addFounders(amount) {
	$('#extra_founders').html('');
	for(var i = 0; i < amount; i++) {
		$('#extra_founders').append(founderInputRowDOM(i));
	}
}
function founderInputRowDOM(founderNo) {
	return '<div class="form_field_container form_left_container col-17 form_field_none_margin"><input type="text" name="founders[]" id="founder_'+founderNo+'" class="ift" placeholder="Founder name" value="" /></div><div class="form_field_container form_right_container col-9 form_field_none_margin"><input type="text" name="founder_shareholdings[]" id="founder_shareholding_'+founderNo+'" class="ift" placeholder="% shareholding" value="" /></div>';
}

function checkCode(targetField) {
	if ( $(targetField).length ) {
		if (targetField.val() === "1644398") {
			return true;
		}else {
			return false;
		}
	}else {
		return true;
	}
}


function validateFounders() {
	var extrFoundersCont = $('#extra_founders');
	var foundersFields = document.getElementsByName('founders[]');
	var shareHoldingFields = document.getElementsByName('founder_shareholdings[]');
	if (foundersFields.length >= 1) {
		var totalShares = 0;
		var errorArray = new Array();
		for(var i = 0; i < foundersFields.length; i++) {
			var thisNameField = foundersFields[i];
			var thisShareField = shareHoldingFields[i];
			//	Check founder name
			if (frame_charCount($(thisNameField).val(),1,60) != true) {
				frame_markErrorFields($(thisNameField));
				if (errorArray.length >= 1) { }else {
					// Add copy for founder name error message
					errorArray.push('*Please provide the selected number of founders’ full names.')
				}
			}else {
				frame_unmarkErrorFields($(thisNameField));
			}

			// Check founder shareholding
			if (frame_validateNumber($(thisShareField).val()) != true) {
				frame_markErrorFields($(thisShareField));
				if (errorArray.length >= 1) { }else {
					errorArray.push('*One or more of the percentages entered are not valid. The percentage should range from 0 to 100 and not exceed 100 in total.');
				}
			}else {
				frame_unmarkErrorFields($(thisShareField));
			}

			totalShares += parseFloat($(thisShareField).val());			
		}
		//	Check the total value of all the shareholding fields to make sure it does not go over 100
		
		if (totalShares > 100) {
			errorArray.push('*One or more of the percentages entered are not valid. The percentage should range from 0 to 100 and not exceed 100 in total.');	
		}

		//	Return the error array or a true value
		if (errorArray.length >= 1) {
			return errorArray;
		}else {
			return true;
		}
	}else {
		return true;
	}
}

$('input').on('paste', function () {
  var element = this;
  setTimeout(function () {
    var text = $(element).val();
    $(element).val(text);
  }, 100);
});



</script>
</body>
</html>
