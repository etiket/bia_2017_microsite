<?php

// Reminder Emailer
// - BUILD REMINDER EMAIL
function buildReminderEmail() {
	global $_EtiFrame;
	global $project_data;
	
	$image_folder = $project_data['full_address'];
	$image_folder = str_replace("web/","",$image_folder);
	$image_folder = $image_folder."emailers/client_entry_reminder/images/";
	
	$emailMessage = "";
	$emailMessage .= '<table width="600" border="1" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; border-color:#000000;" align="center">'.
	'					<tr>'.
	'    					<td>'.
	'						<table width="600" border="0" cellspacing="0" cellpadding="0">'.
	'						  <tr>'.
	'							<td>'.				
	'							 <table width="600" border="0" cellspacing="0" cellpadding="0">'.
	'							  <tr>'.
	'								<td><img src="'.$image_folder.'img_01.png" width="271" height="509" alt="" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></td>'.
	'								<td bgcolor="#FFFFFF" valign="top">'.
	'								<table width="329" border="0" cellspacing="0" cellpadding="0">'.
	'								  <tr>'.
	'									<td valign="top" bgcolor="#FFFFFF">'.
	'									<img src="'.$image_folder.'img_02.png" width="329" height="284" alt="Remember to submit your FNB Business INNOVATION Awards entry." style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />'.
	'									</td>'.
	'								  </tr>'.
	'								  <tr>'.
	'									<td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#000000; text-align:left; letter-spacing:normal;">'.
	'										<p style="margin:0 0 10px 0;">'.
	'											We have noticed that you have not yet<BR />'.
	'											completed and submitted your entry<BR />'.
	'											form for the FNB Business Innovation<BR />'.
	'											Awards 2016. This serves as a friendly<BR />'.
	'											reminder to do so before entries close<BR />'.
	'											on <strong>31 January 2016</strong>.<BR />'.
	'											<BR />'.
	'											Gain recognition for your <strong>innovative</strong>,<BR />'.
	'											scaleable business.<BR />'.
	'											<BR />'.
	'											<span style="color:#009999; font-size:15px; font-weight:bold;">'.
	'												Complete your entry now.<BR />'.
	'											</span>'.
	'										</p>'.
	'									</td>'.
	'								  </tr>'.
	'								  <tr>'.
	'									<td align="left" valign="top"><a href="'.$project_data['pages']['entry_login'].'"><img src="'.$image_folder.'img_04.png" width="67" height="20" alt="Enter now" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></a>'.
	'									</td>'.
	'								  </tr>'.
	'								</table>'.
	'								</td>'.
	'							  </tr>'.
	'							</table>'.
	'							</td>'.
	'						  </tr>'.
	'						  <tr>'.
	'							<td align="center" valign="middle" height="50" bgcolor="#FFFFFF">'.
	'							<table width="570" border="0" cellspacing="0" cellpadding="0" align="center">'.
	'							  <tr>'.
	'								<td align="left" height="50" bgcolor="#FFFFFF" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:9px; color:#000000; text-align:left; letter-spacing:normal; height:50px;">'.
	'								<p><i>Terms and conditions apply.</i><BR />'.
	'								  <strong>First National Bank - a division of FirstRand Bank Limited</strong>. An Authorised Financial Services and Credit Provider (NCRCP20). </p>'.
	'								</td>'.
	'							  </tr>'.
	'							</table>'.
	'							</td>'.
	'						  </tr>'.
	'						</table>'.
	'							</td>'.
	'						 </tr>'.
	'						</table>';
	return $emailMessage;
}