<?php

// Reminder Emailer
// - BUILD REMINDER EMAIL
function buildReminderEmail() {
	global $_EtiFrame;
	global $project_data;
	
	$image_folder = $project_data['full_address'];
	$image_folder = str_replace("web/","",$image_folder);
	$image_folder = $image_folder."emailers/client_entry_reminder2017/images/";
	
	$emailMessage = "";
	$emailMessage .= '<table width="600" border="1" cellspacing="0" cellpadding="0" style="border-style:solid; border-width:1px; border-color:#000000;" align="center">'.
	'  <tr>'.
	'    <td>'.
	'<table width="600" border="0" cellspacing="0" cellpadding="0">'.
	'  <tr>'.
	'    <td>'.
	'    '.
	'     <table width="327" border="0" cellspacing="0" cellpadding="0">'.
	'      <tr>'.
	'        <td valign="top"><img src="'.$image_folder.'img_01.png" width="343" height="395" alt="" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" /></td>'.
	'        <td bgcolor="#FFFFFF" valign="top">'.
	'        '.
	'        '.
	'        <table width="266" border="0" cellspacing="0" cellpadding="0">'.
	'          <tr>'.
	'            <td valign="top" bgcolor="#FFFFFF">'.
	'            <img src="'.$image_folder.'img_02.png" width="257" height="240" alt="Remember to submit your FNB Business INNOVATION Awards entry." style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />'.
	'            </td>'.
	'          </tr>'.
	'          <tr>'.
	'            <td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; color:#000000; text-align:left; letter-spacing:normal;">'.
	'            	<p style="margin:0 0 0 0;">'.
	'                    We noted that you have not yet completed<br/>and submitted your entry form for the <strong>FNB<br/> Business INNOVATION Awards 2017</strong>.<br/>This serves as a friendly reminder to do so<br/>before entries close on <strong>31 January 2017</strong>.'.
	'              </p>'.
	'            	'.
	'            '.
	'            </td>'.
	'          </tr>'.
	'          <tr>'.
	'            <td align="left" valign="top">'.
	'            	<img src="'.$image_folder.'img_04.png" width="257" height="29" alt="Enter now" style="display:block; float:left; margin:0px 0px 0px 0px;" align="absbottom" border="0" />'.
	'            </td>'.
	'          </tr>'.
	'          <tr>'.
	'            <td align="left" bgcolor="#FFFFFF" valign="top" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#000000; text-align:left; letter-spacing:normal;">'.
	'            	<p style="margin:0 0 0 0;">'.
	'                    <span style="color:#009999; font-size:11px; font-weight:normal;">'.
	'                    	Complete your entry at<br/>'.
	'                        <span style="text-decoration:none; color:#009999; font-weight:bold;"><font style="display:none;">@</font>www.fnbbusinessinnovationawards.co.za</span>'.
	'                    </span>'.
	'				</p>'.
	'            	'.
	'            '.
	'            </td>'.
	'          </tr>'.
	'        </table>'.
	'        '.
	'        '.
	'        </td>'.
	'      </tr>'.
	'    </table>'.
	'   '.
	'    '.
	'    </td>'.
	'  </tr>'.
	'  <tr>'.
	'  	<td align="center" valign="middle" height="50" bgcolor="#FFFFFF">'.
	'    '.
	'    <table width="570" border="0" cellspacing="0" cellpadding="0" align="center">'.
	'      <tr>'.
	'        <td align="left" height="50" bgcolor="#FFFFFF" valign="middle" style="font-family:Arial, Helvetica, sans-serif; font-size:8px; color:#000000; text-align:left; letter-spacing:normal; height:50px;">'.
	'    	<p><span style="font-size:9px;"><i>Terms and conditions apply.</i></span><br/><br/>'.
	'    	  <strong>First National Bank - a division of FirstRand Bank Limited</strong>. An Authorised Financial Services and Credit Provider (NCRCP20). </p>'.
	'    	</td>'.
	'      </tr>'.
	'    </table>'.
	'    '.
	'    '.
	'    </td>'.
	'  </tr>'.
	'</table>'.
	'	</td>'.
	' </tr>'.
	'</table>';
	return $emailMessage;
}